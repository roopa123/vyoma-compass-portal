<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Code', function (Blueprint $table) {
            $table->increments('CodeId')->unsigned();
            $table->string('CodeType', 10);
            $table->string('Code', 50);
            $table->string('Desc', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Code');
    }
}
