<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Roles', function (Blueprint $table) {
            $table->string('RoleName')->unique()->change();
        });

        Schema::table('Permissions', function (Blueprint $table) {
            $table->string('AccessLevel')->unique()->change();
        });

        Schema::table('Industry', function (Blueprint $table) {
            $table->string('IndustryName')->unique()->change();
        });

        Schema::table('Creative', function (Blueprint $table) {
            $table->string('CreativeName')->unique()->change();
        });

        Schema::table('Advertiser', function (Blueprint $table) {
            $table->string('AdvertiserName')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
