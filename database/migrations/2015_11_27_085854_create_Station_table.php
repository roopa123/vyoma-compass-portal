<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Station', function (Blueprint $table) {
        $table->increments('StationId');
            $table->string('StationName')->unique();
            $table->integer('RegionId')->unsigned();
            $table->integer('BranchId')->unsigned();
            $table->integer('StateId')->unsigned();
            $table->timestamps();
            $table->boolean('isDeleted','0');
            $table->char('status', 1, 'A');
            $table->foreign('RegionId')->references('RegionId')->on('Region');
            $table->foreign('BranchId')->references('BranchId')->on('Branch');
            $table->foreign('StateId')->references('StateId')->on('State');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Station');
    }
}
