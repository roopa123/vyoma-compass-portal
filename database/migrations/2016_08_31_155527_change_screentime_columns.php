<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeScreentimeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Host', function (Blueprint $table) {
            $table->time('ScreenStartTime')->nullable()->change();
            $table->time('ScreenEndTIme')->nullable()->change();
            $table->time('SunScreenEndTIme')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Host', function (Blueprint $table) {
            //
        });
    }
}
