<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Host', function (Blueprint $table) {
            $table->increments('HostId');
            $table->string('HostName')->unique();
            $table->integer('BayId')->unsigned();
            $table->char('MasterOrPlayer', 1);
            $table->string('HostCounterType');
            $table->integer('HostMasterId')->unsigned();
            $table->integer('HostMachineId')->unsigned();
            $table->string('CounterNumber')->nullable();
            $table->string('HostMasterName');
            $table->time('StartTime')->nullable();
            $table->time('EndTIme')->nullable();
            $table->timestamps();
            $table->boolean('isDeleted','0');
            $table->char('status', 1, 'A');
            $table->foreign('BayId')->references('BayId')->on('Bay')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Host');
    }
}
