<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppReleaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AppRelease', function (Blueprint $table) {
            $table->increments('ReleaseId')->unsigned();
            $table->string('AppCode', 50)->comment('Will come from the Code Table with CodeType = “App”');
            $table->string('VersionNo', 50)->comment('Needs to be unique for a AppCode');
            $table->string('Desc', 50);
            $table->date('ReleaseDate');
            $table->string('TarballURL', 255);
            $table->string('Checksum', 255);
            $table->char('Status', 1)->default('A')->nullable()->comment('A - Active (default) and I - InActive');
            $table->string('ReleasedFor', 50)->comment('Will come from the Code Table with CodeType = “App”');
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->timestamp('CreatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unique('ReleaseId', 'ReleaseId_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('AppRelease');
    }
}
