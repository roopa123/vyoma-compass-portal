<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Campaign', function (Blueprint $table) {
            $table->increments('CampaignId')->unsigned();
            $table->string('CampaignName');
            $table->date('CampaignStartDate');
            $table->date('CampaignEndDate');
            $table->integer('SpotsPerDay');
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('Status');
            $table->integer('UserId')->unsigned();
            $table->foreign('UserId')->references('id')->on('Users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Campaign');
    }
}
