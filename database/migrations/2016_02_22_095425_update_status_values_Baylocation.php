<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusValuesBaylocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('BayLocation', function (Blueprint $table) {
            DB::statement("UPDATE BayLocation SET status='I' WHERE status='D'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('BayLocation', function (Blueprint $table) {
            //
        });
    }
}
