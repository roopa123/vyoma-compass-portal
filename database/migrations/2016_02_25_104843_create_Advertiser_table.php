<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertiserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Advertiser', function (Blueprint $table) {
            $table->increments('AdvertiserId')->unsigned();
            $table->string('AdvertiserName');
            $table->string('Sector');
            $table->integer('IndustryId')->unsigned();
            $table->char('IsPaying');
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('Status')->default('A');
            $table->foreign('IndustryId')->references('IndustryId')->on('Industry')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Advertiser');
    }
}
