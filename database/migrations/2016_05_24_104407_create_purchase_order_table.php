<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PO', function (Blueprint $table) {
            $table->increments('POId')->unsigned();
            $table->string('ROReferenceNo');
            $table->integer('SalesRepId')->unsigned();
            $table->date('PODate');
            $table->date('StartDate');
            $table->date('EndDate');
            $table->integer('AdvertiserId')->unsigned();
            $table->integer('SalesBranchId')->unsigned();
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('Status');
            $table->integer('UserId')->unsigned();
            $table->foreign('SalesRepId')->references('id')->on('Users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('AdvertiserId')->references('AdvertiserId')->on('Advertiser')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('SalesBranchId')->references('BranchId')->on('Branch')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('UserId')->references('id')->on('Users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PO');
    }
}
