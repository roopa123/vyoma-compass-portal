<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedAtColumnNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('State', function (Blueprint $table) {
            $table->char('Status', 1)->default('A');
        });
        Schema::table('Branch', function (Blueprint $table) {
            $table->timestamp('UpdatedAt')->after('BranchName')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        Schema::table('Region', function (Blueprint $table) {
            $table->timestamp('UpdatedAt')->after('RegionName')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        Schema::table('State', function (Blueprint $table) {
            $table->timestamp('UpdatedAt')->after('StateName')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        Schema::table('Station', function (Blueprint $table) {
            $table->timestamp('UpdatedAt')->after('StateId')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        Schema::table('Bay', function (Blueprint $table) {
            $table->timestamp('UpdatedAt')->after('App')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        Schema::table('BayLocation', function (Blueprint $table) {
            $table->timestamp('UpdatedAt')->after('BayLocationDesc')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        Schema::table('Host', function (Blueprint $table) {
            $table->timestamp('UpdatedAt')->after('EndTIme')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
