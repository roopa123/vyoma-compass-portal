<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CampaignLocation', function (Blueprint $table) {
            $table->increments('CampaignLocationId')->unsigned();
            $table->integer('CampaignId')->unsigned();
            $table->string('LocationType');
            $table->integer('LocationId');
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('Status');
            $table->integer('UserId')->unsigned();
            $table->foreign('UserId')->references('id')->on('Users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CampaignId')->references('CampaignId')->on('Campaign')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CampaignLocation');
    }
}
