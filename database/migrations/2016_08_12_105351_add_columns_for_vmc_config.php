<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForVmcConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Host', function (Blueprint $table) {
            $table->timestamp('CreatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('LocalIPAddress', 50)->nullable();
            $table->string('PublicIPAddress', 50)->nullable();
            $table->char('ShowTicketData', 1)->default('Y')->nullable();
            $table->char('IsUTSDumb', 1)->default('N')->nullable();
            $table->char('PlayerWinSize', 2)->nullable();
            $table->timestamp('ScreenStartTime')->nullable();
            $table->timestamp('ScreenEndTIme')->nullable();
            $table->timestamp('SunScreenEndTIme')->nullable();
            $table->char('HdmiMode', 2)->nullable();
            $table->char('ScreenRatio', 2)->nullable();
            $table->char('IsRouterRebootEnabled', 1)->default(0)->nullable();
            $table->string('RouterUserName', 50)->nullable();
            $table->string('RouterPassword', 50)->nullable();
            $table->string('RouterRebootSchedule', 100)->nullable();
            $table->char('IsMonitEventEnabled', 1)->nullable();
            $table->char('IsMonitQueueEnabled', 1)->nullable();
            $table->char('IsCronEnabled', 1)->nullable();
            $table->char('CableType', 2)->nullable();
            $table->char('IsPlayLogUpdateEnabled', 1)->nullable();
            $table->string('PlayLogUpdateSchedule', 100)->nullable();
            $table->char('IsTicketLogUpdateEnabled', 1)->nullable();
            $table->string('TicketLogUpdateSchedule', 100)->nullable();
            $table->char('IsContentUpdateEnabled', 1)->nullable();
            $table->string('ContentUpdateSchedule', 100)->nullable();
            $table->char('IsAppUpdateEnabled', 1)->nullable();
            $table->string('AppUpdateSchedule', 100)->nullable();
            $table->char('IsConfigUpdateEnabled', 1)->nullable();
            $table->string('ConfigUpdateSchedule', 100)->nullable();
            $table->char('IsDebugModeEnabled', 1)->nullable();
            $table->char('IsSysStatEnabled', 1)->nullable();
            $table->char('IsScreenShotEnabled', 1)->nullable();
            $table->string('ScreenShotSchedule', 100)->nullable();
            $table->char('IsThxMessageEnabled', 1)->nullable();
            $table->string('MacAddress', 50)->nullable();
            $table->char('DisplaySize', 2)->nullable();
            $table->string('CurrentAppVersion', 50)->nullable();
            $table->string('NewAppVersion', 50)->nullable();
            $table->string('CurrentConfigVersion', 50)->nullable();
            $table->string('NewConfigVersion', 50)->nullable();
            $table->string('CurrentContentVersion', 50)->nullable();
            $table->string('NewContentVersion', 50)->nullable();
            $table->char('IsSecondDisplay', 1)->default('N')->nullable();
            $table->integer('PrimaryDisplayHostId')->nullable();
            $table->char('PowerAdapterModel', 2)->nullable();
            $table->char('ReinitHost', 1)->default('N')->nullable();
            $table->char('PiModel', 2)->nullable();
            $table->char('IsRegistered', 1)->nullable();
            $table->char('IsOnline', 1)->nullable();
            $table->char('DisplayType', 2)->nullable();
            $table->char('IsDAVPCertified', 1)->default('N')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
