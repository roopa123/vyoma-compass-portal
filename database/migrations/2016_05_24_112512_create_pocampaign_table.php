<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePocampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('POCampaign', function (Blueprint $table) {
            $table->increments('POCampaignId')->unsigned();
            $table->integer('OriginalPOId')->unsigned();
            $table->integer('OriginalCampaignId')->unsigned();
            $table->integer('POId')->unsigned();
            $table->integer('CampaignId')->unsigned();
            $table->integer('CreativeCampaignId')->unsigned();
            $table->integer('LocCampaignId')->unsigned();
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('Status');
            $table->integer('UserId')->unsigned();
            $table->string('HasPlayed');
            $table->foreign('UserId')->references('id')->on('Users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CampaignId')->references('CampaignId')->on('Campaign')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CreativeCampaignId')->references('CampaignId')->on('Campaign')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('LocCampaignId')->references('CampaignId')->on('Campaign')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('POId')->references('POId')->on('PO')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('POCampaign');
    }
}
