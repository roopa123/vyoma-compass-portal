<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('Bay', function (Blueprint $table) {
            $table->increments('BayId');
            $table->integer('StationId')->unsigned();
            $table->char('BayLocationCode');
            $table->char('app');
            $table->timestamps();
            $table->boolean('isDeleted','0');
            $table->char('status', 1, 'A');
            $table->foreign('StationId')->references('StationId')->on('Station');
            $table->foreign('BayLocationCode')->references('BayLocationCode')->on('BayLocation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Bay');
    }
}
