<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedAtColumnForPoFlow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Campaign', function (Blueprint $table) {
            $table->timestamp('CreatedAt')->after('SpotsPerDay')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        Schema::table('CampaignCreative', function (Blueprint $table) {
            $table->timestamp('CreatedAt')->after('CreativeId')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        Schema::table('CampaignLocation', function (Blueprint $table) {
            $table->timestamp('CreatedAt')->after('LocationId')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        Schema::table('PO', function (Blueprint $table) {
            $table->timestamp('CreatedAt')->after('SalesBranchId')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        Schema::table('POCampaign', function (Blueprint $table) {
            $table->timestamp('CreatedAt')->after('LocCampaignId')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->string('HasPlayed')->default('N')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
