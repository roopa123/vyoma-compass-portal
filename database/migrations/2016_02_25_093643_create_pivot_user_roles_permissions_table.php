<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotUserRolesPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserRole', function(Blueprint $table) {
            $table->increments('UserRoleId');
            $table->integer('UserId')->unsigned();
            $table->integer('RoleId')->unsigned();

            $table->foreign('UserId')->references('id')->on('users');
            $table->foreign('RoleId')->references('RoleId')->on('Roles');

        });

        Schema::create('RolePermission', function(Blueprint $table) {
            $table->increments('RolePermissionId');
            $table->integer('RoleId')->unsigned();
            $table->integer('PermissionId')->unsigned();

            $table->foreign('PermissionId')->references('PermissionId')->on('Permissions');
            $table->foreign('RoleId')->references('RoleId')->on('Roles');

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('UserRole');
        Schema::drop('RolePermission');
    }
}
