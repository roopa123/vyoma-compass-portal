<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyIndustryCoilInAdvertiser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Advertiser', function ($table) {
            $table->dropForeign('advertiser_industryid_foreign');            
        }); 

        Schema::table('Advertiser', function ($table) {
            $table->integer('IndustryId')->unsigned()->nullable()->change();

            $table->foreign('IndustryId')->references('IndustryId')->on('Industry');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
