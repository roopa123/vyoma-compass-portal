<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropVMCDDIScolumnsInCreative extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Creative', function ($table) {
            $table->dropColumn('VMCFileName');
            $table->dropColumn('DDiSFileName');
            $table->dropColumn('VMCFileMetaData');
            $table->dropColumn('DDiSFileMetaData');
            $table->string('VideoFileMetaData', 500)->after('InputFileName'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
