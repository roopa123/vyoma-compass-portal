<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignCreative extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CampaignCreative', function (Blueprint $table) {
            $table->increments('CampaignCreativeId')->unsigned();
            $table->integer('CampaignId')->unsigned();
            $table->integer('CreativeId')->unsigned();
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('Status');
            $table->integer('UserId')->unsigned();
            $table->foreign('UserId')->references('id')->on('Users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CampaignId')->references('CampaignId')->on('Campaign')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('CreativeId')->references('CreativeId')->on('Creative')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CampaignCreative');
    }
}
