<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeployStateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('State', function (Blueprint $table) {
            
            $table->integer('DeployStateId')->unsigned()->nullable();
            
            $table->index('DeployStateId', 'fk_State_State1_idx');
            $table->foreign('DeployStateId')->references('StateId')->on('State')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('Host', function (Blueprint $table) {
            
            $table->integer('DeployStateId')->unsigned()->nullable();
            $table->string('PlayerWinSize', 50)->nullable()->change();

            $table->index('DeployStateId', 'fk_Host_State1_idx');
            $table->foreign('DeployStateId')->references('StateId')->on('State')->onDelete('cascade')->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
