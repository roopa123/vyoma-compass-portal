<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnBayLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('BayLocation', function (Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at', 'isDeleted']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('BayLocation', function (Blueprint $table) {
            //
        });
    }
}
