<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusValuesBay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Bay', function (Blueprint $table) {
            DB::statement("UPDATE Bay SET status='I' WHERE status='D'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Bay', function (Blueprint $table) {
            //
        });
    }
}
