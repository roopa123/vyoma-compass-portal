<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusValuesStation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Station', function (Blueprint $table) {
            DB::statement("UPDATE Station SET status='I' WHERE status='D'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Station', function (Blueprint $table) {
            //
        });
    }
}
