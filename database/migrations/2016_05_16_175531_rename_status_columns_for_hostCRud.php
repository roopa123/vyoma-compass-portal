<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameStatusColumnsForHostCRud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Branch', function (Blueprint $table) {
            $table->renameColumn('status', 'Status');
        });
        Schema::table('Region', function (Blueprint $table) {
            $table->renameColumn('status', 'Status');
        });
        Schema::table('Station', function (Blueprint $table) {
            $table->renameColumn('status', 'Status');
        });
        Schema::table('Bay', function (Blueprint $table) {
            $table->renameColumn('status', 'Status');
        });
        Schema::table('BayLocation', function (Blueprint $table) {
            $table->renameColumn('status', 'Status');
        });
        Schema::table('Host', function (Blueprint $table) {
            $table->renameColumn('status', 'Status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
