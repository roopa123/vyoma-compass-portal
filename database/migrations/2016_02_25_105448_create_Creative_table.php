<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Creative', function (Blueprint $table) {
            $table->increments('CreativeId')->unsigned();
            $table->string('CreativeName');
            $table->integer('AdvertiserId')->unsigned();
            $table->string('VMCFileName');
            $table->string('DDiSFileName');
            $table->string('VMCThumbNail');
            $table->string('DDiSThumbNail');
            $table->string('VMCFileMetaData', 500);
            $table->string('DDiSFileMetaData', 500);
            $table->string('Category1')->nullable();
            $table->string('Category2')->nullable();
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->string('Status')->default('A');
            $table->foreign('AdvertiserId')->references('AdvertiserId')->on('Advertiser')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Creative');
    }
}
