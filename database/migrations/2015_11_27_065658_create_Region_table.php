<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Region', function (Blueprint $table) {
            $table->increments('RegionId');
            $table->string('RegionName')->unique();
            $table->timestamps();
            $table->boolean('isDeleted', '0');
            $table->char('status', 1, 'A');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Region');
    }
}
