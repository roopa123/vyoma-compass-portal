<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoRolePermissionForPo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = array(
              // Advertiser
              array(
                  'ParentPermission' => 'PurchaseOrder',
                  'AccessLevel'      => 'Create PO'
              ),
              array(
                  'ParentPermission' => 'PurchaseOrder',
                  'AccessLevel'      => 'Update PO'
              ),
              array(
                  'ParentPermission' => 'PurchaseOrder',
                  'AccessLevel'      => 'Show PO'
              ),
              array(
                  'ParentPermission' => 'PurchaseOrder',
                  'AccessLevel'      => 'Delete PO'
              ));
        DB::table('Permissions')->insert($permissions);

        $premissionId = App\Models\Permission::where('ParentPermission', '=' , 'PurchaseOrder')->lists('PermissionId')->toArray();
        
        foreach ($premissionId as $premissionIdkey => $premissionIdvalue) {
            $rolePermission[] = ['RoleId' => \Config::get('vyoma-constants.super_admin_role_id'), 'PermissionId' => $premissionIdvalue];
        }
        DB::table('RolePermission')->insert($rolePermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
