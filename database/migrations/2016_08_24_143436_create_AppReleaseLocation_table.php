<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppReleaseLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AppReleaseLocation', function (Blueprint $table) {
            $table->increments('AppRelLocId')->unsigned();
            $table->integer('ReleaseId')->unsigned();
            $table->char('LocationType', 1)->comment('B - Bay\nH - Host');
            $table->integer('LocationId')->unsigned()->comment('BayId if it is a Bay or HostId if it is an indiv Host');
            $table->date('PlannedReleaseDate');
            $table->date('ReleaseUpdatedDate')->nullable();            
            $table->timestamp('UpdatedAt')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->timestamp('CreatedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unique('AppRelLocId', 'AppRelLocId_UNIQUE');
            $table->index('ReleaseId', 'fk_AppReleaseLocation_AppRelease1_idx');
            $table->foreign('ReleaseId')->references('ReleaseId')->on('AppRelease')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('AppReleaseLocation');
    }
}
