<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddUpdatedByIndustryAdvertiser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Industry', function ($table) {
            $table->integer('UpdatedBy')->unsigned();
            $table->foreign('UpdatedBy')->references('id')->on('users');
        });
        Schema::table('Advertiser', function ($table) {
            $table->integer('UpdatedBy')->unsigned();
            $table->foreign('UpdatedBy')->references('id')->on('users');
        });
        Schema::table('Creative', function ($table) {
            $table->integer('UpdatedBy')->unsigned();
            $table->foreign('UpdatedBy')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
