<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNullablePocampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    Schema::table('POCampaign', function ($table) 
    {
    $table->integer('CreativeCampaignId')->unsigned()->nullable()->change()->default(NULL);
    $table->integer('LocCampaignId')->unsigned()->nullable()->change()->default(NULL);
    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
