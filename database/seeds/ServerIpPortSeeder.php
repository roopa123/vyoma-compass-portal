<?php

namespace Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class ServerIpPortSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $codeValues = array(
              array(
                  'CodeType' => 'SERVER_IP',
                  'Code' => env('APP_DOMAIN'),
                  'Desc' => ''
              ),
              array(
                  'CodeType' => 'SERVER_PORT',
                  'Code' => 80,
                  'Desc' => ''
              ),
          );
        DB::table('Code')->insert($codeValues);
    }
}
