<?php
namespace Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class CrudPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         $permissions = array(
              // Branch
              array(
                  'ParentPermission' => 'Branch',
                  'AccessLevel'      => 'Create Branch'
              ),
              array(
                  'ParentPermission' => 'Branch',
                  'AccessLevel'      => 'Update Branch'
              ),
              array(
                  'ParentPermission' => 'Branch',
                  'AccessLevel'      => 'Show Branch'
              ),
              array(
                  'ParentPermission' => 'Branch',
                  'AccessLevel'      => 'Delete Branch'
              ),
              // Region
              array(
                  'ParentPermission' => 'Region',
                  'AccessLevel'      => 'Create Region'
              ),
              array(
                  'ParentPermission' => 'Region',
                  'AccessLevel'      => 'Update Region'
              ),
              array(
                  'ParentPermission' => 'Region',
                  'AccessLevel'      => 'Show Region'
              ),
              array(
                  'ParentPermission' => 'Region',
                  'AccessLevel'      => 'Delete Region'
              ),
              // Station
              array(
                  'ParentPermission' => 'Station',
                  'AccessLevel'      => 'Create Station'
              ),
              array(
                  'ParentPermission' => 'Station',
                  'AccessLevel'      => 'Update Station'
              ),
              array(
                  'ParentPermission' => 'Station',
                  'AccessLevel'      => 'Show Station'
              ),
              array(
                  'ParentPermission' => 'Station',
                  'AccessLevel'      => 'Delete Station'
              ),
              // Bay
              array(
                  'ParentPermission' => 'Bay',
                  'AccessLevel'      => 'Create Bay'
              ),
              array(
                  'ParentPermission' => 'Bay',
                  'AccessLevel'      => 'Update Bay'
              ),
              array(
                  'ParentPermission' => 'Bay',
                  'AccessLevel'      => 'Show Bay'
              ),
              array(
                  'ParentPermission' => 'Bay',
                  'AccessLevel'      => 'Delete Bay'
              ),
              //BayLocation
              array(
                  'ParentPermission' => 'BayLocation',
                  'AccessLevel'      => 'Create BayLocation'
              ),
              array(
                  'ParentPermission' => 'BayLocation',
                  'AccessLevel'      => 'Update BayLocation'
              ),
              array(
                  'ParentPermission' => 'BayLocation',
                  'AccessLevel'      => 'Show BayLocation'
              ),
              array(
                  'ParentPermission' => 'BayLocation',
                  'AccessLevel'      => 'Delete BayLocation'
              ),
              array(
                  'ParentPermission' => 'Host',
                  'AccessLevel'      => 'Create Host'
              ),
              array(
                  'ParentPermission' => 'Host',
                  'AccessLevel'      => 'Update Host'
              ),
              array(
                  'ParentPermission' => 'Host',
                  'AccessLevel'      => 'Show Host'
              ),
              array(
                  'ParentPermission' => 'Host',
                  'AccessLevel'      => 'Delete Host'
              ),
            );

        DB::table('Permissions')->insert( $permissions );
    }
}
