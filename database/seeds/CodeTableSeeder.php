<?php
namespace Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class CodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $codeValues = array(
              // Advertiser
              array(
                  'CodeType' => 'UptimeRepo',
                  'Code'      => 'PingReceived',
                  'Desc'  => 'Host pinged Monit'
              ),
              array(
                  'CodeType' => 'UptimeRepo',
                  'Code'      => 'PingNotReceived',
                  'Desc'  => 'Host did not ping Monit'
              ),
              array(
                  'CodeType' => 'Category',
                  'Code'      => 'one',
                  'Desc'  => 'CategoryOneA'
              ),
              array(
                  'CodeType' => 'Category',
                  'Code'      => 'one',
                  'Desc'  => 'CategoryOneB'
              ),
              array(
                  'CodeType' => 'Category',
                  'Code'      => 'two',
                  'Desc'  => 'CategoryTwoA'
              ),
              array(
                  'CodeType' => 'Category',
                  'Code'      => 'two',
                  'Desc'  => 'CategoryTwoB'
              ),
              );
        DB::table('Code')->insert( $codeValues );
    }
}
