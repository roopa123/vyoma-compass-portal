<?php

namespace Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class VmcConfigDefaultValues extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $codeValues = array(
              array(
                  'CodeType' => 'PlayerWinSize',
                  'Code' => '20 15 1260 695',
                  'Desc' => "PLAYER_WIN='20 15 1260 695'  (Plasma)",
              ),
              array(
                  'CodeType' => 'PlayerWinSize',
                  'Code' => "'20 35 1055 1440'",
                  'Desc' => "PLAYER_WIN='20 35 1055 1440' (Crown)",
              ),
              array(
                  'CodeType' => 'PlayerWinSize',
                  'Code' => "'15 30 1065 1440'",
                  'Desc' => "PLAYER_WIN='15 30 1065 1440' (Micromax)",
              ),
              array(
                  'CodeType' => 'PlayerWinSize',
                  'Code' => "'25 45 1055 1440'",
                  'Desc' => "PLAYER_WIN='25 45 1055 1440'  (Panasonic)",
              ),
              array(
                  'CodeType' => 'PlayerWinSize',
                  'Code' => "'23 38 1055 1440'",
                  'Desc' => "PLAYER_WIN='23 38 1055 1440'  (Samsung)",
              ),
              array(
                  'CodeType' => 'PlayerWinSize',
                  'Code' => "'0 0 1080 1440'",
                  'Desc' => "PLAYER_WIN='0 0 1080 1440'  (1080p)",
              ),
              array(
                  'CodeType' => 'PlayerWinSize',
                  'Code' => "'0 0 1080 1344'",
                  'Desc' => "PLAYER_WIN='0 0 1080 1344' (1080p)",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => "'0 0 1080 1440'",
                  'Desc' => "PlayerWinSize",
              ),
              array(
                  'CodeType' => 'HdmiMode',
                  'Code' => 5,
                  'Desc' => '# HDMI_MODE=5'
              ),
              array(
                  'CodeType' => 'HdmiMode',
                  'Code' => 16,
                  'Desc' => '# HDMI_MODE=16 # Low-res HDMI (Samsung, Crown, Mitashi, etc)'
              ),
              array(
                  'CodeType' => 'HdmiMode',
                  'Code' => 82,
                  'Desc' => '# HDMI_MODE=82 # DVI (ViewSonic)'
              ),
              array(
                  'CodeType' => 'HdmiMode',
                  'Code' => 0,
                  'Desc' => 'NA'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 0,
                  'Desc' => 'HdmiMode'
              ),
              array(
                  'CodeType' => 'ScreenRatio',
                  'Code' => 0,
                  'Desc' => 'Default'
              ),
              array(
                  'CodeType' => 'ScreenRatio',
                  'Code' => 1,
                  'Desc' => '70-30'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 0,
                  'Desc' => 'ScreenRatio'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 'admin',
                  'Desc' => 'RouterUserName'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 'monit2013',
                  'Desc' => 'RouterPassword'
              ),
              array(
                  'CodeType' => 'RouterRebootSchedule',
                  'Code' => '0 * * * *',
                  'Desc' => 'Every Hour'
              ),
              array(
                  'CodeType' => 'RouterRebootSchedule',
                  'Code' => '0 */2 * * *',
                  'Desc' => 'Every 2 Hours'
              ),
              array(
                  'CodeType' => 'RouterRebootSchedule',
                  'Code' => '*/30 * * * *',
                  'Desc' => 'Every 30 Minutes'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '*/30 * * * *',
                  'Desc' => 'RouterRebootSchedule'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '1',
                  'Desc' => 'IsMonitEventEnabled'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '1',
                  'Desc' => 'IsMonitQueueEnabled'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '1',
                  'Desc' => 'IsCronEnabled'
              ),
              array(
                  'CodeType' => 'CableType',
                  'Code' => 1,
                  'Desc' => 'HDMI'
              ),
              array(
                  'CodeType' => 'CableType',
                  'Code' => 2,
                  'Desc' => 'DVI'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'CableType'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'IsPlayLogUpdateEnabled'
              ),
              array(
                  'CodeType' => 'PlayLogUpdateSchedule',
                  'Code' => "*/30 * * * *",
                  'Desc' => "Every 30 minutes",
              ),
              array(
                  'CodeType' => 'PlayLogUpdateSchedule',
                  'Code' => "0 * * * *",
                  'Desc' => "Every 1 Hour",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => "*/30 * * * *",
                  'Desc' => "PlayLogUpdateSchedule",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'IsContentUpdateEnabled'
              ),
              array(
                  'CodeType' => 'ContentUpdateSchedule',
                  'Code' => "*/30 * * * *",
                  'Desc' => "Every 30 Minutes",
              ),
              array(
                  'CodeType' => 'ContentUpdateSchedule',
                  'Code' => "*/10 * * * *",
                  'Desc' => "Every 10 Minutes",
              ),
              array(
                  'CodeType' => 'ContentUpdateSchedule',
                  'Code' => "0 * * * *",
                  'Desc' => "Every Hour",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => "*/30 * * * *",
                  'Desc' => "ContentUpdateSchedule",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'IsTicketLogUpdateEnabled'
              ),
              array(
                  'CodeType' => 'TicketLogUpdateSchedule',
                  'Code' => "*/30 * * * *",
                  'Desc' => "Every 30 Minutes",
              ),
              array(
                  'CodeType' => 'TicketLogUpdateSchedule',
                  'Code' => "0 * * * *",
                  'Desc' => "Every Hour",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => "0 * * * *",
                  'Desc' => "TicketLogUpdateSchedule",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'IsAppUpdateEnabled'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'IsConfigUpdateEnabled'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '0',
                  'Desc' => 'IsDebugModeEnabled'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '0',
                  'Desc' => 'IsSysStatEnabled'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '0',
                  'Desc' => 'IsScreenShotEnabled'
              ),
              array(
                  'CodeType' => 'ScreenShotSchedule',
                  'Code' => '00 12 * * *',
                  'Desc' => "At 1200",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '00 12 * * *',
                  'Desc' => "ScreenShotSchedule",
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => '0',
                  'Desc' => 'IsThxMessageEnabled'
              ),
              array(
                  'CodeType' => 'DisplayType',
                  'Code' => 1,
                  'Desc' => 'Plasma'
              ),
              array(
                  'CodeType' => 'DisplayType',
                  'Code' => 2,
                  'Desc' => 'LED'
              ),
              array(
                  'CodeType' => 'DisplayType',
                  'Code' => 3,
                  'Desc' => 'LCD'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 2,
                  'Desc' => 'DisplayType'
              ),
              array(
                  'CodeType' => 'DisplaySize',
                  'Code' => 1,
                  'Desc' => '32"'
              ),
              array(
                  'CodeType' => 'DisplaySize',
                  'Code' => 2,
                  'Desc' => '22"'
              ),
              array(
                  'CodeType' => 'DisplaySize',
                  'Code' => 3,
                  'Desc' => '19"'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'DisplaySize'
              ),
              array(
                  'CodeType' => 'PiModel',
                  'Code' => 1,
                  'Desc' => 'Pi B'
              ),
              array(
                  'CodeType' => 'PiModel',
                  'Code' => 2,
                  'Desc' => 'Pi B+'
              ),
              array(
                  'CodeType' => 'PiModel',
                  'Code' => 3,
                  'Desc' => 'Pi 2'
              ),
              array(
                  'CodeType' => 'PiModel',
                  'Code' => 4,
                  'Desc' => 'Pi 3'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 4,
                  'Desc' => 'PiModel'
              ),
              array(
                  'CodeType' => 'PowerAdapterModel',
                  'Code' => 1,
                  'Desc' => 'Samsung'
              ),
              array(
                  'CodeType' => 'PowerAdapterModel',
                  'Code' => 2,
                  'Desc' => 'Pi'
              ),
              array(
                  'CodeType' => 'Default',
                  'Code' => 2,
                  'Desc' => 'PowerAdapterModel'
              ),
              array(
                  'CodeType' => 'SERVER_IP',
                  'Code' => 'testcrud.vyoma-media.com',
                  'Desc' => ''
              ),
              array(
                  'CodeType' => 'SERVER_PORT',
                  'Code' => 80,
                  'Desc' => ''
              ),
          );
        DB::table('Code')->insert($codeValues);
    }
}
