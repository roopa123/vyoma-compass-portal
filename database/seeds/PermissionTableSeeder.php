<?php
namespace Database\Seeds;
use DB;

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Permissions')->delete();

         $permissions = array(
              // Advertiser
              array(
                  'ParentPermission' => 'Advertiser',
                  'AccessLevel'      => 'Create Advertiser'
              ),
              array(
                  'ParentPermission' => 'Advertiser',
                  'AccessLevel'      => 'Update Advertiser'
              ),
              array(
                  'ParentPermission' => 'Advertiser',
                  'AccessLevel'      => 'Show Advertiser'
              ),
              array(
                  'ParentPermission' => 'Advertiser',
                  'AccessLevel'      => 'Delete Advertiser'
              ),
              // Industry
              array(
                  'ParentPermission' => 'Industry',
                  'AccessLevel'      => 'Create Industry'
              ),
              array(
                  'ParentPermission' => 'Industry',
                  'AccessLevel'      => 'Update Industry'
              ),
              array(
                  'ParentPermission' => 'Industry',
                  'AccessLevel'      => 'Show Industry'
              ),
              array(
                  'ParentPermission' => 'Industry',
                  'AccessLevel'      => 'Delete Industry'
              ),
              // Creative
              array(
                  'ParentPermission' => 'Creative',
                  'AccessLevel'      => 'Create Creative'
              ),
              array(
                  'ParentPermission' => 'Creative',
                  'AccessLevel'      => 'Update Creative'
              ),
              array(
                  'ParentPermission' => 'Creative',
                  'AccessLevel'      => 'Show Creative'
              ),
              array(
                  'ParentPermission' => 'Creative',
                  'AccessLevel'      => 'Delete Creative'
              ),
              // User
              array(
                  'ParentPermission' => 'User',
                  'AccessLevel'      => 'Create User'
              ),
              array(
                  'ParentPermission' => 'User',
                  'AccessLevel'      => 'Update User'
              ),
              array(
                  'ParentPermission' => 'User',
                  'AccessLevel'      => 'Show User'
              ),
              array(
                  'ParentPermission' => 'User',
                  'AccessLevel'      => 'Delete User'
              ),
              //Roles
              array(
                  'ParentPermission' => 'Role',
                  'AccessLevel'      => 'Create Role'
              ),
              array(
                  'ParentPermission' => 'Role',
                  'AccessLevel'      => 'Update Role'
              ),
              array(
                  'ParentPermission' => 'Role',
                  'AccessLevel'      => 'Show Role'
              ),
              array(
                  'ParentPermission' => 'Role',
                  'AccessLevel'      => 'Delete Role'
              ),              
            );

        DB::table('Permissions')->insert( $permissions );
    }
}
