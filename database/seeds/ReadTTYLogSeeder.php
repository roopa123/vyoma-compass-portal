<?php

namespace Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class ReadTTYLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $codeValues = array(
              array(
                  'CodeType' => 'Default',
                  'Code' => 1,
                  'Desc' => 'IsReadTTYLogEnabled'
              )              
          );
        DB::table('Code')->insert($codeValues);
    }
}
