<?php
namespace Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class OpCcPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         $permissions = array(
              // Operations
              array(
                  'ParentPermission' => 'Operations',
                  'AccessLevel'      => 'Show Op'
              ),
              //CCC
              array(
                  'ParentPermission' => 'Customers',
                  'AccessLevel'      => 'Show CC'
              ),              
            );

        DB::table('Permissions')->insert( $permissions );
    }
}
