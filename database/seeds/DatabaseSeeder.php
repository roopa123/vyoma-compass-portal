<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Database\Seeds\PermissionTableSeeder;
use Database\Seeds\CodeTableSeeder;
use Database\Seeds\CrudPermissionSeeder;
use Database\Seeds\OpCcPermissionSeeder;
use Database\Seeds\VmcConfigDefaultValues;
use Database\Seeds\ServerIpPortSeeder;
use Database\Seeds\ReadTTYLogSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         //$this->call(PermissionTableSeeder::class);
         //$this->call(CodeTableSeeder::class);
         //$this->call(CrudPermissionSeeder::class);
         //$this->call(OpCcPermissionSeeder::class);
         //$this->call(VmcConfigDefaultValues::class);
         //$this->call(ServerIpPortSeeder::class);
         $this->call(ReadTTYLogSeeder::class);

        Model::reguard();
    }
}
