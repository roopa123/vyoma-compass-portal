<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Permission::class, function (Faker\Generator $faker) {
    return [
        'AccessLevel' => $faker->AccessLevel,
    ];
});

$factory->define(App\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'RoleName' => $faker->RoleName,
    ];
});

$factory->define(App\Models\Advertiser::class, function (Faker\Generator $faker) {
});

$factory->define(App\Models\Branch::class, function (Faker\Generator $faker) {
    return [

    ];
});


$factory->define(App\Models\PO::class, function (Faker\Generator $faker) {
    $advertiser = App\Models\Advertiser::all()->lists('AdvertiserId');
    $advertiser = $advertiser->toArray();
    $advertiser = $faker->randomElements($advertiser);
    $branch = App\Models\Branch::all()->lists('BranchId');
    $branch = $branch->toArray();
    $branch = $faker->randomElements($branch);
    $status = ['A', 'H', 'D'];
    $status = $faker->randomElements($status);
    return [
        'ROReferenceNo' => $faker->randomDigit,
        'SalesRepId' => factory(App\Models\User::class)->create()->id,
        'PODate' => $faker->date,
        'StartDate' => $faker->date,
        'EndDate' => $faker->date,
        'AdvertiserId' => $advertiser[0],
        'SalesBranchId' => $branch[0],
        'UpdatedAt' => $faker->time,
        'Status' => $status[0],
        'UserId' => factory(App\Models\User::class)->create()->id,
    ];
});

$factory->define(App\Models\Compaign::class, function (Faker\Generator $faker) {
    $status = ['A', 'H', 'D'];
    $status = $faker->randomElements($status);
    return [
        'CompaignName' => $faker->name,
        'CompaignStartDate' => $faker->date,
        'CompaignEndDate' => $faker->date,
        'SpotsPerDay' => $faker->randomNumber,
        'UpdatedAt' => $faker->time,
        'Status' => $status[0],
        'UserId' =>  factory(App\Models\User::class)->create()->id,
    ];
});

$factory->define(App\Models\CompaignCreative::class, function (Faker\Generator $faker) {
    $creative = App\Models\Creative::all()->lists('CreativeId');
    $creative = $creative->toArray();
    $creative = $faker->randomElements($creative);
    $status = ['A', 'D'];
    $status = $faker->randomElements($status);
    return [
        'CompaignId' => factory(App\Models\Compaign::class)->create()->id,
        'CreativeId' => $creative[0],
        'UpdatedAt' => $faker->time,
        'Status' => $status[0],
        'UserId' =>  factory(App\Models\User::class)->create()->id,
    ];
});


$factory->define(App\Models\POCompaign::class, function (Faker\Generator $faker) {
    $status = ['A', 'H'];
    $status = $faker->randomElements($status);
    $iHistory = ['Y', 'N'];
    $iHistory = $faker->randomElements($iHistory);
    $hasPlayed = ['Y', 'N'];
    $hasPlayed = $faker->randomElements($hasPlayed);
    return [
        'OriginalPOId' => factory(App\Models\PO::class)->create()->id,
        'OriginalCompaignId' => factory(App\Models\Compaign::class)->create()->id,
        'POId' => factory(App\Models\PO::class)->create()->id,
        'CompaignId' => factory(App\Models\Compaign::class)->create()->id,
        'CreativeCompaignId' => factory(App\Models\Compaign::class)->create()->id,
        'LocCompaignId' => factory(App\Models\Compaign::class)->create()->id,
        'UpdatedAt' => $faker->time,
        'Status' => $status[0],
        'IsHistory' => $iHistory[0],
        'HasPlayed' => $hasPlayed[0],
        'UserId' =>  factory(App\Models\User::class)->create()->id,
    ];
});

$factory->define(App\Models\CompaignLocation::class, function (Faker\Generator $faker) {
    $branch = App\Models\Branch::all()->lists('BranchId');
    $branch = $branch->toArray();
    $branch = $faker->randomElements($branch);
    $status = ['A', 'D'];
    $status = $faker->randomElements($status);
    return [
        'CompaignId' => factory(App\Models\Compaign::class)->create()->id,
        'LocationType' => 'B',
        'LocationId' => $branch[0],
        'UpdatedAt' => $faker->time,
        'Status' => $status[0],
        'UserId' =>  factory(App\Models\User::class)->create()->id,
    ];
});
