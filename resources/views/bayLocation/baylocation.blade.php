@extends('layouts.apptemplate')

@section('title')
    BAYLOCATION
@endsection

@section('menu')
@include('layouts.menu')
@endsection

@section('content')
@include('layouts.validationAlert')
	<div class="">
<script type="text/javascript">

function editBefore(formId){
   $('#BayLocationCode').attr("readonly", "readonly");
}
function addBefore(formId){
   $('#BayLocationCode').removeAttr("readonly");
}

</script>

    <div class="">
        <table id="jqGridBayLocation" class="table "></table>
    </div>
    @can('Show BayLocation')
        {!!
        GridRender::setGridId("jqGridBayLocation")
          ->enablefilterToolbar()
          ->setGridOption('url',URL::to('/baylocation-grid'))
          ->setGridOption('editurl',URL::to('/baylocation/crud'))
          ->setGridOption('rowNum', 20)
          ->setGridOption('rownumbers', true)
          ->setGridOption('autowidth', true)
          ->setGridOption('height', 500)
          ->setGridOption('caption','List of Bay Locations')
          ->setGridOption('viewrecords',true)
           ->setGridOption('altRows',true)
          ->setGridOption('altclass','myAltRowClass')
          ->setGridOption('cmTemplate', array('resizable' => false))
          ->setGridEvent('gridComplete', 'readOnlyColumn')
          ->setGridEvent('loadComplete', 'dynamicHeight')

          ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => false, 'view' => true, 'refresh' => false))
          ->setNavigatorOptions('add', array('closeAfterAdd' => true, 'addCaption' => 'Add Bay Location', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '193'))
          ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('edit', array('closeAfterEdit' => true, 'editCaption' => 'Edit Bay Location Details', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '193'))
          ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('view', array('closeAfterEdit' => true, 'caption' => 'View Bay Location Info', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '193'))
          ->setNavigatorOptions('del', array('closeAfterEdit' => true, 'caption' => 'Delete Bay Location Info', 'resize' => false, 'drag' => false, 'left' => '494', 'top' => '193'))
          ->setNavigatorEvent('del', 'afterSubmit', 'afterSubmitEvent')
          ->setFileProperty('FileName', 'Bay_locations')
          ->addColumn(array('label' => 'Bay Location Code','index' => 'BayLocationCode', 'hidden' => false, 'editable' => true, 'key' => true, 'align' => 'left', 'editrules' => array('required' => true), 'editoptions' => array('readonly' => 'readonly')))
          ->addColumn(array('label' => 'Bay Location Name', 'index'=>'BayLocationDesc', 'align' => 'left', 'editable' => true, 'editrules' => array('required' => true)))
          ->addColumn(array('label' => 'Status','index' => 'Status', 'hidden' => false, 'edittype'=>'select', 'align' => 'left','editoptions' => array( 'value' => 'A:Active;I:Inactive'),'editable' => true, 'editrules' => array('required' => true, 'edithidden' => false), 'width' => '90px', 'formatter' => 'statusDisplay', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => ":All;A:Active;I:Inactive")))
          ->setNavigatorEvent('edit','beforeShowForm', 'editBefore')
          ->setNavigatorEvent('add','beforeShowForm', 'addBefore')
          ->renderGrid();
        !!}
      @endcan
  </div>

  <!-- Role Based Permissions -->
  <script type="text/javascript">
    $('#add_jqGridBayLocation').hide();
    $('#edit_jqGridBayLocation').hide();
    $('#view_jqGridBayLocation').hide();
      @can('Create BayLocation')
        $('#add_jqGridBayLocation').show();
      @endcan
      @can('Update BayLocation')
        $('#edit_jqGridBayLocation').show();
      @endcan
      @can('Show BayLocation')
        $('#view_jqGridBayLocation').show();
      @endcan 
  </script>
@stop
