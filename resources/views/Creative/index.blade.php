@extends('layouts.ccctemplate')

@section('title')
    Creative
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
    @include('layouts.validationAlert')
    @can('Show Creative')
    <div class="adminPanel form-group NameRole rolesTable creativeGrid">
        <div class="baseDiv">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            <span class="text-center color-gray gridCaption customerGridCaption">{{"List of Creatives "}} </span>
            <!-- Check permission for create creatives -->
            @can('Create Creative')
            <a href="{{url('creative/create')}}" class="btn btn-success customerGridAdd"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;&nbsp;&nbsp;Add Creative</a>
            @endcan
            <div class="col-md-12 col-xs-12 padding0">
                <table id="CreativeGrid" class="col-md-12 col-xs-12 table-bordered table-hover table-striped table-condensed table-responsive table" width="1245px"><tr></tr></table>
                <div id="CreativeGridPager"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function updateUndowithReview(id) {
            message = "Are you sure, you want to revert the Creative Status?";
            bootbox.confirm(message, function(result) {
                if(result == true) {
                    $.ajax({type: "GET", url: "updateUndoStatus/"+id,
                        success: function(results){
                            if(results == 'success'){
                                displayMessage("Creative Updated successfully", true);
                                location.reload();
                            }
                        }});
                }
            });
        }

        $.ajax({
            type: "POST",
            url: "/creative-grid",
            dataType:"json",
            success: function (dataJson) {
                var oldFrom = $.jgrid.from,
                        lastSelected;

                $.jgrid.from = function (source, initalQuery) {
                    var result = oldFrom.call(this, source, initalQuery),
                            old_select = result.select;
                    result.select = function (f) {
                        lastSelected = old_select.call(this, f);
                        return lastSelected;
                    };
                    return result;
                };
                jQuery("#CreativeGrid").jqGrid(
                        {
                            "datatype": "local",
                            "mtype": "POST",
                            "treeGridModel": "adjacency",
                            "data": dataJson.rows,
                            "rowNum": 10,
                            "rownumbers": true,
                            "autowidth": true,
                            "height": 'auto',
                            "viewrecords": true,
                            "pager": '#CreativeGridPager',
                            "recreateFilter": true,
                            "altRows":true,
                            "altclass":'myAltRowClass',
                            "colModel": [
                                {"label": "Creative Id","index": "CreativeId","name": "CreativeId","search": false,"key": true,'hidden': true },
                                {"label":'Creative Name', name: 'CreativeName', search: true, resizable:false, align:'left'},
                                {"label":'Advertiser',name: 'AdvertiserName', search: true, resizable:false},
                                {"label":'File Name',name: 'FileName', search: false, 'hidden':true},
                                {name: 'FileMetaData', search: false, 'hidden':true},
                                {"label":'File',name: 'File', search: false, resizable:false},

                                {"label":'Duration',name: 'FileDuration', search: true,
                                    sortable: true,
                                    sorttype: function (cellValue,rowObject)
                                    {
                                        var jsonResul = JSON.parse(rowObject.FileDuration);
                                        return parseInt(jsonResul.duration);
                                    },
                                    resizable:false, index:'FileDuration',  align:'left',
                                    formatter: function(cellvalue, options, rowobject){
                                        var jsonval = JSON.parse(cellvalue);
                                        var resultmin = Math.floor(jsonval.duration / 60);
                                        var resultsec = jsonval.duration - resultmin * 60;
                                        if(resultmin.toString().length == 1)
                                            var resultmin = "0"+resultmin;
                                        if(resultsec.toString().length == 1)
                                            var resultsec = "0"+resultsec;
                                        var timeVal = resultmin+":"+resultsec;
                                        return timeVal;
                                    }},
                                {"label":'Sector',name: 'Sector', search: true,resizable:false},

                                {"label":'Industry',name: 'IndustryName', search: true,  resizable:false, formatter:nullFormatter},
                                {"label":'Updated By', name: 'UpdatedBy', search: true, resizable:false},

                                {"label":'Updated At',name: 'UpdatedAt', sorttype: 'date', datefmt: 'd-m-Y h.i A', search: true, resizable:false},

                                {"label":'Status', name: 'Status', search: true, resizable:false, edittype: 'textarea', formatter: function(cellvalue, options, rowobject){
                                    if(cellvalue=='A'){
                                        cellvalue='Active';
                                    }
                                    if(cellvalue=='D'){
                                        cellvalue='Deleted';
                                    }
                                    if(cellvalue=='R'){
                                        cellvalue='Review';
                                    }
                                    if(cellvalue=='U'){
                                        cellvalue='Uploaded';
                                    }
                                    return '<div class="cellValue" style="visibility:hidden;position: absolute">' +cellvalue+'</div>';
                                } },
                                {"label":'Actions',name: 'Actions', sortable: false, search: false, resizable:false, formatter: 'actions'}
                            ],

                            beforeRequest: function () {
                                modifySearchingFilter.call(this, ',');
                            },
                            loadComplete: function () {

                                /* replacing industry column null values with 'Blank' */
                                $.each($("#CreativeGrid").jqGrid('getGridParam','data'),function(index,value){
                                    if(value.IndustryName==null){
                                        value.IndustryName='(Blank)';
                                    }
                                    if(value.Status=='A'){
                                        value.Status='Active';
                                    }
                                    if(value.Status=='D'){
                                        value.Status='Deleted';
                                    }
                                    if(value.Status=='R'){
                                        value.Status='Review';
                                    }
                                    if(value.Status=='U'){
                                        value.Status='Uploaded';
                                    }
                                });

                                this.p.lastSelected = lastSelected; // set this.p.lastSelected
                                highlightFilteredColumns.call(this); //highlightFilteredColumns



                                $('.toggle-blob').click(function(){return false;});
                                var iColStatus = columnIndexForGridBasedOnTableIdAndColumnName('#CreativeGrid', 'Status');
                                var iColFile = columnIndexForGridBasedOnTableIdAndColumnName('#CreativeGrid', 'File');
                                var iColAction = columnIndexForGridBasedOnTableIdAndColumnName('#CreativeGrid', 'Actions');
                                var statusValue=[];
                                /*** status col***/
                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColStatus + 1) + ")")
                                        .each(function(value,key) {
                                            statusValue.push($(this).children('.cellValue').text());
                                            var className = "toggle"+$(this).children('.cellValue').text();
                                            $(this).append('<div class="toggle-light"><div class="'+className+'"></div></div>');

                                            if($(this).children('.cellValue').text()=='Active'){
                                                @can('Update Creative')
                                                $('.'+className).toggles({drag:false,text:{on:'Active',off:'Review'},on:true});
                                                @endcan
                                                @cannot('Update Creative')
                                                $('.'+className).toggles({drag:false,text:{on:'Active',off:'Review'},on:true}).addClass('disabled');
                                                @endcan
                                            }else if($(this).children('.cellValue').text()=='Review'){
                                                @can('Update Creative')
                                                $('.'+className).toggles({drag:false,text:{on:'Active',off:'Review'},off:true});
                                                @endcan
                                                @cannot('Update Creative')
                                                $('.'+className).toggles({drag:false,text:{on:'Active',off:'Review'},off:true}).addClass('disabled');
                                                @endcan

                                            }else if($(this).children('.cellValue').text()=='Deleted'){
                                                $('.'+className).html('<a class="statusDeleted" disabled="disabled">Deleted</a>');

                                            }else if($(this).children('.cellValue').text()=='Uploaded'){
                                                $('.'+className).html('<a class="statusUpload" disabled="disabled">Uploaded</a>');
                                            }
                                        });

                                /*** file col***/
                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColFile + 1) + ")")
                                        .each(function(value,key) {
                                            if(statusValue[value] == 'Active'){
                                                $(this).append('<img class="fileIconStatus" data-toggle="modal" data-target="#myModal" src="/assets/ccc-app/img/activeIcon.png"/>');
                                            }else if(statusValue[value] == 'Review'){
                                                $(this).append('<img class="fileIconStatus" data-toggle="modal" data-target="#myModal" src="/assets/ccc-app/img/ReviewVidIcon.png"/>');
                                            }else if(statusValue[value] == 'Deleted'){
                                                $(this).append('<img class="fileIconStatus" disabled="disabled" onclick="return event.stopPropagation();" src="/assets/ccc-app/img/deltedIcon.png"/>');
                                            }
                                            else if(statusValue[value] == 'Uploaded'){
                                                $(this).append('<img class="fileIconStatus" disabled="disabled" onclick="return event.stopPropagation();" src="/assets/ccc-app/img/deltedIcon.png"/>');
                                            }
                                        });

                                /*** Action col***/
                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColAction + 1) + ")")
                                        .each(function(value,key) {
                                            if(statusValue[value] == 'Deleted'){
                                                $("<div>", {
                                                    mouseover: function(e) {
                                                        id = $(e.target).closest("tr.jqgrow").attr("id");
                                                        $(this).children('.undo-link').attr("onclick", "updateUndowithReview('" + id + "')");
                                                    }}).css({ cursor: "pointer"})
                                                        .addClass("col-md-12 col-xs-12 wrapperForButtons")
                                                        @can('Update Creative')
                                                        .append('<a href="#" class="col-md-5 col-xs-12 undo-link">Undo</a>')
                                                @endcan
                                               .prependTo($(this).children("div"));
                                            }else
                                            {
                                                $("<div>", {
                                                    mouseover: function (e) {
                                                        id = $(e.target).closest("tr.jqgrow").attr("id");
                                                        $(this).children('.update-link').attr("href", "/creative/" + id + "/edit");
                                                        $(this).children('.delete-link').attr("onclick", "deleteRow('" + id + "', '#CreativeGrid')");
                                                    }}).css({ cursor: "pointer"})
                                                        .addClass("col-md-12 col-xs-12 wrapperForButtons")
                                                        @can('Update Creative')
                                                        .append('<a href="#" class="col-md-5 col-xs-12 update-link">Edit</a>')
                                                @endcan
                                                @can('Delete Creative')
                                                .append('<a href="#" class="col-md-5 col-xs-12 delete-link">Delete</a>')
                                                @endcan
                                                .prependTo($(this).children("div"));
                                            }
                                        });

                                /* not showing blank values in the grid */
                                $("#CreativeGrid td[title='(Blank)']").css("color","transparent");

                                setMultiSearchSelect('CreativeName', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
                                setMultiSearchSelect('AdvertiserName', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
//                                setMultiSearchSelect('FileDuration', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
                                setMultiSearchSelect('Category1', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
                                setMultiSearchSelect('Category2', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
                                setMultiSearchSelect('UpdatedBy', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
                                setMultiSearchSelect('UpdatedAt', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
                                setMultiSearchSelect('Sector', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
                                setMultiSearchSelect('IndustryName', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));
//                                setMultiSearchSelectForStatus('Status', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'), ":All;A:Active;R:Review;D:Deleted;U:Uploaded");
                                setMultiSearchSelect('Status', jQuery("#CreativeGrid"), jQuery("#CreativeGrid").jqGrid('getGridParam','data'));



                                $("td[aria-describedby='CreativeGrid_UpdatedBy']").css("padding-left","10px !important;");

                                /* hiding actions column based on the roles and permissions of users*/
                                if(statusValue.length > 0){
                                    if ( $('#CreativeGrid .wrapperForButtons').children().length <= 0 ) {
                                        $(this).jqGrid('hideCol',["Actions"]);
                                    }
                                }

                                /* set max height for all grids based on the window height */
                                var height = $(window).height();
                                $('.ui-jqgrid .ui-jqgrid-bdiv').css("max-height",height/2+"px");

                            }
                        });
                jQuery("#CreativeGrid").jqGrid('navGrid', '#pager', {edit: false, add: false, del: false, search: false, refresh: true}, {}, {}, {}, {
                    multipleSearch: true,
                    multipleGroup: true,
                    recreateFilter: true,
                    overlay: 0
                });

                jQuery("#CreativeGrid").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch: myDefaultSearch});
                $('#jqgh_CreativeGrid_CreativeName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_AdvertiserName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
//                $('#jqgh_CreativeGrid_FileDuration').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_Category1').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_Category2').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_UpdatedBy').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_UpdatedAt').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_Sector').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_IndustryName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                $('#jqgh_CreativeGrid_Status').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');

                $('#CreativeGrid_CreativeName .vSymbol').on('click',function(){
                    $('.ui-search-toolbar #gsh_CreativeGrid_CreativeName button.ui-multiselect.ui-widget.ui-state-default').click();
                    removeUnWantedSearchOptions(0, 'CreativeName',$('#CreativeGrid').jqGrid('getGridParam', 'lastSelected'),'CreativeGrid');
                    return false;
                });
                $('#CreativeGrid_AdvertiserName .vSymbol').on('click',function(){
                    $('.ui-search-toolbar #gsh_CreativeGrid_AdvertiserName button.ui-multiselect.ui-widget.ui-state-default').click();
                    removeUnWantedSearchOptions(1, 'AdvertiserName',$('#CreativeGrid').jqGrid('getGridParam', 'lastSelected'),'CreativeGrid');
                    return false;
                });
                /* $('#CreativeGrid_FileDuration .vSymbol').on('click',function(){
                 removeUnWantedSearchOptions(6,7,'CreativeGrid_FileDuration');
                 $('.ui-search-toolbar #gsh_CreativeGrid_FileDuration button.ui-multiselect.ui-widget.ui-state-default').click();
                 return false;
                 });*/
                $('#CreativeGrid_Sector .vSymbol').on('click',function(){
                    $('.ui-search-toolbar #gsh_CreativeGrid_Sector button.ui-multiselect.ui-widget.ui-state-default').click();
                    removeUnWantedSearchOptions(2, 'Sector',$('#CreativeGrid').jqGrid('getGridParam', 'lastSelected'),'CreativeGrid');
                    return false;
                });
                $('#CreativeGrid_IndustryName .vSymbol').on('click',function(){
                    $('.ui-search-toolbar #gsh_CreativeGrid_IndustryName button.ui-multiselect.ui-widget.ui-state-default').click();
                    removeUnWantedSearchOptions(3, 'IndustryName',$('#CreativeGrid').jqGrid('getGridParam', 'lastSelected'),'CreativeGrid');
                    return false;
                });
                $('#CreativeGrid_UpdatedBy .vSymbol').on('click',function(){
                    $('.ui-search-toolbar #gsh_CreativeGrid_UpdatedBy button.ui-multiselect.ui-widget.ui-state-default').click();
                    removeUnWantedSearchOptions(4, 'UpdatedBy',$('#CreativeGrid').jqGrid('getGridParam', 'lastSelected'),'CreativeGrid');
                    return false;
                });
                $('#CreativeGrid_UpdatedAt .vSymbol').on('click',function(){
                    $('.ui-search-toolbar #gsh_CreativeGrid_UpdatedAt button.ui-multiselect.ui-widget.ui-state-default').click();
                    removeUnWantedSearchOptions(5, 'UpdatedAt',$('#CreativeGrid').jqGrid('getGridParam', 'lastSelected'),'CreativeGrid');
                    return false;
                });
                $('#CreativeGrid_Status .vSymbol').on('click',function(){
                    $('.ui-search-toolbar #gsh_CreativeGrid_Status button.ui-multiselect.ui-widget.ui-state-default').click();
                    removeUnWantedSearchOptions(6, 'Status',$('#CreativeGrid').jqGrid('getGridParam', 'lastSelected'),'CreativeGrid');
                    return false;
                });
                $('.ui-search-toolbar th').css("visibility","hidden");

                var cm = jQuery("#CreativeGrid").jqGrid('getGridParam', 'colModel');

                jQuery("#CreativeGrid").click(function(e) {
                    var $td = $(e.target).closest('td'), $tr = $td.closest('tr.jqgrow'), rowId = $tr.attr('id'), ci;
                    if (rowId) {
                        ci = $.jgrid.getCellIndex($td[0]); // works mostly as $td[0].cellIndex
                        var targeTHtml = $(e.target).html();

                        /*** status col click ***/
                        if(cm[ci].name == 'Status') {
                            if(targeTHtml=='Active'){
                                targeTHtml = 'R';
                                $(e.target).parent().parent().parent().removeClass('toggleActive').addClass('toggleReview');
                                updateStatuses(rowId, targeTHtml, '#CreativeGrid');
                            }else if(targeTHtml=='Review'){
                                targeTHtml = 'A';
                                $(e.target).parent().parent().parent().removeClass('toggleReview').addClass('toggleActive');
                                updateStatuses(rowId, targeTHtml, '#CreativeGrid');
                            }
                        }

                        /*** file col click ***/
                        if(cm[ci].name == 'File') {
                            var videoData = $(e.target).parent().parent().find('td').filter("[aria-describedby='CreativeGrid_FileMetaData']")[0].innerText;
                            var getVideoMetaValue = JSON.parse($(e.target).parent().parent()
                                    .find('td').filter("[aria-describedby='CreativeGrid_FileMetaData']")[0].innerText);

                            var statusCrt = $(e.target).parent().parent().find('td').filter("[aria-describedby='CreativeGrid_Status']")[0].innerText;
                            var redirectGoogleDriveUrl="{{ config('gdrive.google_drive_webView_Path') }}"+getVideoMetaValue.url;

                            var w = 830, h = 640; // default sizes
                            var t = h/2;var l = w/2;
                            w = window.innerWidth - 200;
                            h = window.innerHeight - 200;
                            t =   h/2;l=w/2;

                            /* window to be opened in the center of the screen */
                            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                            var top = ((height / 2) - (h / 2)) + dualScreenTop;
                            var newWindow = window.open(redirectGoogleDriveUrl,'Google Drive Videos','width='+w+',height='+h+',top='+top+',' +
                                    'left='+left+'');

                            // Puts focus on the newWindow
                            if (window.focus) {
                                newWindow.focus();
                            }
                        }
                    }
                });
            }
        });
    </script>
    @endcan
@stop
