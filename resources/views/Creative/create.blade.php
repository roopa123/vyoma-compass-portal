@extends('layouts.ccctemplate')

@section('title')
    Creative
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
    @can('Create Creative')
    {{--<div class="adminPanel customerGrid">--}}
    <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
    <div class="adminPanel customerGrid">
      <form method="POST" action="/creative" name="creativeForm" id="creativeForm"
      enctype ='multipart/form-data', files = true>
      {!! Form::hidden('formRequestType', 'Submit', ['id'=>'formRequestType', 'class'=>'form-control']) !!}
          {!! csrf_field() !!}
        <h4 class="color-gray">{{"Add Creative"}}&nbsp;:</h4>
        <div class="clearfix fieldsWrapper col-md-8 col-xs-12">
          <div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('creativeName','Creative Name',array('class' => 'color-blue')) !!}
            {!! Form::text('creativeName', old('creativeName'), ['class'=>'form-control']) !!}
          </div>
          <div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('advertiser','Advertiser Name',array('class' => 'color-blue col-md-6')) !!}
            @can('Create Advertiser')
            <a href="/advertiser/create/redirect/creative" class="btn addAdvertiserBtn col-md-6 pull-right" onclick="javascript:return redirectAdvertiser('/advertiser/create/redirect/creative')"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;Add Advertiser</a>
            @endcan
            <?php echo Form::select('advertiser', $advertiserArray, \Input::old('advertiser'),['class'=>'form-control']); ?>
          </div>

          {{--<div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('file','Upload',array('class' => 'color-blue')) !!}
            {!! Form::file('file',null) !!}
          </div>--}}
           <div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('Category1','Category 1',array('class' => 'color-blue')) !!}

            <?php echo Form::select('Category1', $categoryOneArray, \Input::old('Category1'),['class'=>'form-control']); ?>

         </div>
          <div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('Category2','Category 2',array('class' => 'color-blue')) !!}

          <?php echo Form::select('Category2', $categoryTwoArray, \Input::old('Category2'),['class'=>'form-control']); ?>           
         </div>
        
            <div class="min-height-100 col-md-6 col-xs-12">
                <label class="color-blue">File</label>
                <a class="uploadAnchor form-control">
                    <span class="fileUploadIcon"></span>&nbsp;&nbsp;<span class="uploadFileLabel">Choose File</span>
                    <input type="file" name="file" onchange="getFileData(this);"/>
                </a>
                <div class="fileInfoBlock">
              <span>
               <span class="videoName col-md-12 col-xs-12 padding0"></span>
                {{--<span class="filePreviewIcon col-md-1 padding0" onclick="openVideoPreviewModal()"></span>--}}
               <!--  <span class="fileCloseIcon col-md-1 padding0" onclick="closeVideoPreviewModal()"></span> -->
              </span>
                </div>
            </div>
          <div class="min-height-100 col-md-12 col-xs-12">
            <div class="col-md-4 col-xs-12 text-center">
              <a href="{{{ URL::route('creative.index') }}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
            </div>
            <div class="col-md-4 col-xs-12 text-center">
              <button id="submitButton" class="btn btn-default IndustrySubmitBtn col-md-12 col-xs-12 formSubmitUpdatePreloader" style="width: 100%;" type="submit">Submit</button>
            </div>
            <div class="col-md-4 col-xs-12 text-center">
              <button type="button" name="addAnother" id="addAnother" class="btn btn-default IndustrySubmitBtn col-md-12 col-xs-12 formSubmitUpdatePreloader" style="width: 100%;" >Add Another</button>
            </div>
          </div>
      </div>
      </form>
    </div>
    @endcan
    <script type="text/javascript">

      /**
       Add Another toggle
       */
      $('#addAnother').on('click', function() {        
          $('#formRequestType').val('addAnother');
          document.creativeForm.submit();          
      });

      $('#submitButton').on('click', function() {
          $('#formRequestType').val('Submit');
          document.creativeForm.submit();
      });


      /*to show uploaded file name*/
      function getFileData(myFile,creativeId){
          var file = myFile.files[0];
          var filename = file.name;
          $('.filePreviewIcon').show();
          $('.fileCloseIcon').show();
          $('.videoName').text(filename);
          console.log(file);
          console.log(creativeId);
      }

      //dropDownSorting();

      function clearfileName() {
        $('.fileInfoBlock .videoName').empty();
      }

    </script>
@stop
