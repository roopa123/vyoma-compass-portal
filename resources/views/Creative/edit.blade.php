@extends('layouts.ccctemplate')

@section('title')
  Advertiser
@endsection

@section('menu')
  @include('layouts.menu')
@endsection

@section('content')
  @include('layouts.validationAlert')
  <div class="logo"></div>
  @can('Update Creative')
  <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
  <div class="industryContainer col-md-10 col-xs-12">
    {!! Form::open(['method' => 'PATCH','files' => true, 'route'=>['creative.update',$creative->CreativeId]]) !!}
    {!! csrf_field() !!}
    <h4 class="col-md-12 col-xs-12 color-gray">{{"Edit Creative "}}&nbsp;:</h4>
    <div class="clearfix fieldsWrapper col-md-8 col-xs-12">
      <div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('creativeName','Creative Name',array('class' => 'color-blue')) !!}
        {!! Form::hidden('creativeId', Input::old('creativeId', $creative->CreativeId),['class'=>'form-control', 'id' => 'creativeId']) !!}
        {!! Form::hidden('GStatus', Input::old('GStatus', $creative->GStatus),['class'=>'form-control']) !!}
        {!! Form::text('creativeName', Input::old('creativeName', $creative->CreativeName),['class'=>'form-control']) !!}
      </div>
      <div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('advertiser','Advertiser',array('class' => 'color-blue')) !!}
        @can('Create Advertiser')
        <a href="/advertiser/create/redirect/creative" class="btn addAdvertiserBtn col-md-6 pull-right" onclick="javascript:return redirectAdvertiser('/advertiser/create/redirect/creativeEdit/{{$creative->CreativeId}}')"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;Add Advertiser</a>
        @endcan
        {!! Form::select('advertiser', $advertiserArray, $creative->AdvertiserId, ['class'=>'form-control']) !!}
      </div>
      <div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('Category1','Category 1',array('class' => 'color-blue')) !!}
        {!! Form::select('Category1', $categoryOneArray, $creative->Category1, ['class'=>'form-control']) !!}
      </div>
      <div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('Category2','Category 2',array('class' => 'color-blue')) !!}
        {!! Form::select('Category2', $categoryTwoArray, $creative->Category2, ['class'=>'form-control']) !!}
      </div>
      <div class="min-height-100 col-md-6 col-xs-12">
        <label class="color-blue">File</label>
        <a class="uploadAnchor form-control">
          <span class="fileUploadIcon"></span>&nbsp;&nbsp;<span class="uploadFileLabel">Choose File</span>
          <input id="editFileUpload" type="file" name="file" onchange="getFileData(this,'{{$creative->CreativeId}}');"/>
        </a>
        <div class="fileInfoBlock">
          <span class="editBlock" >
            
            @if($fileMetadata != null)
              <span class="videoName col-md-11 col-xs-10 padding0" id='uploadedFileinput' data-creativeid={{$creative->CreativeId}}>{!! $inputFileName !!}</span>
              @if($creative->Status == 'A' || $creative->Status == 'R')
                <span class="filePreviewIcon edit col-md-1 padding0" data-toggle="modal" data-target="#myModal"
                      onclick="openVideoPreviewModal('{{$creative->CreativeId}}','{!! $inputFileName !!}',
                              '{!! $fileMetadata->url !!}','{!! $fileMetadata->size !!}','{!! $fileMetadata->duration !!}','{!! $fileMetadata->extension !!}')"
                      style="display: block"></span>
              @else
                <span class="filePreviewIconDisabled col-md-1 col-xs-2 padding0" style="display: block"> </span>
              @endif
            @else
              <span class="videoName col-md-8 padding0"></span>
              <!-- <span class="fileCloseIcon col-md-1 padding0"  onclick="deleteVideo('{{$creative->CreativeId}}','{!! $inputFileName !!}')"></span>-->
              </span>
          @endif
        </div>
      </div>
      <div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('Status','Status',array('class' => 'color-blue')) !!}
        {!! Form::select('Status', $status, $creative->Status , ['class'=>'form-control']) !!}
      </div>

      <div class="col-md-12 col-xs-12">
        <div class="col-md-6 col-xs-12 text-center">
          @if($inputFileName == null)
            <a href="{{{ URL::route('creative.index') }}}" id="cnl" disabled="disabled" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
          @else
            <a href="{{{ URL::route('creative.index') }}}" id="cnl" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
          @endif
        </div>
        <div class="col-md-6 col-xs-12 text-center">
          <button class="btn btn-default IndustrySubmitBtn col-md-12 col-xs-12 formSubmitUpdatePreloader">Update</button>
        </div>
      </div>


      {{--Video template--}}
              <!-- <div class="col-md-12 col-xs-12 modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
        <div class="videoPreviewModal editPopUp">
          <a class="text-right col-md-push-10" data-dismiss="modal" onclick="closeVideoPreviewModal()"><img class="popUpClose" src="/assets/ccc-app/img/modalClose.png"/></a>
          <div class="videoPreviewModalPopUp videoEditPage">
            <div class="videoPreviewModal-content"></div>
            <div class="videoPreviewModal-footer">
              <div class="col-md-12">File Name:&nbsp;<span class="fileName"></span></div>
              <div class="col-md-12">File Size:&nbsp;<span class="fileSize"></span></div>
              <div class="col-md-12">Run Time:&nbsp;<span class="runTime"></span></div>
              <div class="col-md-12">Download:&nbsp;
              <span class="downloadIcon">
               <a class="videoDownload" href="">
                 <img class="downloadIcon" src="/assets/ccc-app/img/downloadVideoIcon.png">
               </a>
              </span>
              </div>
            </div>
          </div>
        </div>
      </div> -->

      </form>
    </div>

    <script type="text/javascript">
      function deleteVideo(creativeId,fileName){
        var ddis, vmc;
        ddis= $('#uploadedFileddis').text().length;
        vmc= $('#uploadedFilevmc').text().length;
        if(ddis > 0 && vmc > 0){
          message = "Are you sure, If u delete user InputFile, mp4 and mpg files will also be deleted?";
        }else {
          message = "Are you sure, you want to delete user InputFile?"
        }

        bootbox.confirm(message, function(result) {
          if(result == true) {
            $.ajax({type: "GET", url: "/deleteVideo/"+fileName+"/"+creativeId,
              success: function(results){

                if(results == "sucess"){
                  displayMessage("Successfully deleted file "+fileName, true);
                }else{
                  displayMessage("error failed to delete file "+fileName, false);
                }
                location.reload();
              }});
          }
        });
      }


      /*status toggle*/
      $('.payingYesBtn').on('click', function(){
        $('.payingNoBtn').removeClass('payingActive').addClass('color-gray');
        $('.payingYesBtn').addClass('payingActive');
        $('.payingNoBtn .IsPaying_no').removeAttr("name");
        $('.payingYesBtn .IsPaying_yes').attr("name","status");
      });
      $('.payingNoBtn').on('click', function(){
        $('.payingYesBtn').removeClass('payingActive').addClass('color-gray');
        $('.payingNoBtn').addClass('payingActive');
        $('.payingYesBtn .IsPaying_yes').removeAttr("name");
        $('.payingNoBtn .IsPaying_no').attr("name","status");
      });

      /*to show uploaded file name*/
      function getFileData(myFile){
        var file = myFile.files[0];
        var filename = file.name;

        $('.filePreviewIcon').show();
        $('.fileCloseIcon').show();
        $('.videoName').text(filename);

        $('.filePreviewIcon.edit').hide();
      }

      /*close video preview popup*/
      function closeVideoPreviewModal() {
        videojs(document.getElementById('my-video'), {}, function(){
        });
        var video=videojs("my-video");
        video.pause();
        $('.videoPreviewModal').hide();
      }

      /* open video preview popup
       creativeId, fileName, url, size, duration, extension */
      function openVideoPreviewModal(id,name,url,size,duration,extention){
        var w = 830, h = 640; // default sizes
        var t = h/2;var l = w/2;
        w = window.innerWidth - 200;
        h = window.innerHeight - 200;
        t =   h/2;l=w/2;
        var redirectGoogleDriveUrl="{{ config('gdrive.google_drive_webView_Path') }}"+url;

        window.open(redirectGoogleDriveUrl,'Google Drive Videos','width='+w+',height='+h+',top='+100+',' +
                'left='+100+',right='+100,'titlebar=yes');

        /* $('.videoEditPage .videoPreviewModal-content').html('<video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" ' +
         'controls preload="auto" width="430" height="300" poster="MY_VIDEO_POSTER.jpg" data-setup="{}">' +
         //             '<source src="'+videoUrl+'" type="'+type+'"></video>');
         '<source src="'+videoUrl1+'" type="'+type+'"></video>');

         $('.videoEditPage .fileName').text(name);
         $('.videoEditPage .fileSize').text(size);
         $('.videoEditPage .runTime').text(duration);
         $('.videoEditPage .videoDownload').attr('href',videoUrl);
         videojs(document.getElementById('my-video'), {}, function(){
         });
         $('.videoPreviewModal').show();*/
      }

       $("#advertiser").change(function (e) {
      
        var creativeId = $('#creativeId').val();
        checkPoAdvertiser(creativeId);

     });

    function checkPoAdvertiser(creativeId) {
      console.log('update master name');      
            $.ajax({
              url: '/checkPoAdv/'+creativeId,
              type: "GET",
              success: function (data) {                
                if(data != "")  {
                  var message = "Creative is associated with PO, You can't change Advertiser";
                    $('#form-success').addClass('alert alert-danger').html(message).show();

                    $("#form-success.alert.alert-danger").fadeTo(2000, 1000).slideUp(1000, function(){                        
                        $(this).hide();                        
                    });
                }

              }
            });
          }



    </script>
  @endcan
@stop
