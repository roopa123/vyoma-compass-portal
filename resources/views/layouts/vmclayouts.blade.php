<!DOCTYPE html>
<html lang="en" ng-app="vmcConfigApp">
<head>
    <title>Vyoma-CRUD ::@yield('title')</title>
    <link rel="icon" href="/assets/crud-app/css/uibootstrap/images/favicon.ico" />
    @yield('css')
    {{--<link rel="stylesheet" type="text/css"  href="/assets/bootstrap-v3.2.0/css/bootstrap-theme.css"/>--}}
    <link rel="stylesheet" type="text/css"  href="/assets/bootstrap-v3.2.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/app.css" />
    <link rel="stylesheet" href="/assets/ccc-app/css/jQgrid_css.css"/>
    <link rel="stylesheet" href="/assets/crud-app/css/CCGrid.css"/>
    <link rel="stylesheet" href="/assets/vmc-config-app/lib/angular/angular-ui-switch.min.css">
    <link rel="stylesheet" href="/assets/poassets/css/po-flow-css.css" />
    <link rel="stylesheet" href="/assets/vmc-config-app/css/vmc.css" />


    <script type="text/javascript" src="/assets/crud-app/js/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/ccc-app/lib/jquery-ui.min.js"></script>
    {{--<script  src="https://code.angularjs.org/1.4.12/angular.js"></script>--}}
    <script  src="/assets/vmc-config-app/lib/angular/angular.js"></script>
    <script src="/assets/vmc-config-app/lib/angular/angular-animate.js"></script>
    <script src="/assets/vmc-config-app/lib/angular/angular-sanitize.js"></script>
    <script  src="/assets/vmc-config-app/lib/angular/ui-bootstrap-tpls-2.1.2.min.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-v3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/vmc-config-app/lib/angular/angular-ui-switch.min.js"></script>
    {{--custom js--}}
    <script defer src="/assets/vmc-config-app/js/vmc-custom.js"></script>
    <script defer src="/assets/vmc-config-app/js/app.js"></script>
    <script defer src="/assets/vmc-config-app/js/vmcConfigNewController.js"></script>
    <script defer src="/assets/vmc-config-app/js/vmcConfigEditController.js"></script>
    <script defer src="/assets/vmc-config-app/js/vmcVerticalTabsService.js"></script>
    <script defer src="/assets/vmc-config-app/js/vmcConfigService.js"></script>
</head>
<body>
<div class='grid_holder'>
    @include('layouts.notifications')
    @include('layouts.validationAlert')
</div>
@yield('menu')
@yield('content')
</body>
</html>
