
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Vyoma-CRUD ::@yield('title')</title>

        <link rel="icon" href="/assets/crud-app/css/uibootstrap/images/favicon.ico" />

    @yield('css')
        <link rel="stylesheet" type="text/css"  href="/assets/bootstrap-v3.2.0/css/bootstrap-theme.css"/>
        <link rel="stylesheet" type="text/css"  href="/assets/bootstrap-v3.2.0/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/uibootstrap/jquery-ui-1.10.0.bootstrap.css" />
        <link rel="stylesheet" href="/assets/ccc-app/lib/css/jquery-ui.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/font-awesome.css" />
        <link rel="stylesheet" type="text/css"  href="/assets/crud-app/css/app.css" />
        <link rel="stylesheet" href="/assets/ccc-app/lib/css/jquery.multiselect.css" />
        <link rel="stylesheet" href="/assets/ccc-app/lib/css/jquery.multiselect.filter.css"/>
        <link rel="stylesheet" href="/assets/ccc-app/lib/css/ui.jqgrid.css"/>
        <link rel="stylesheet" href="/assets/ccc-app/css/jQgrid_css.css"/>
        <link rel="stylesheet" href="/assets/crud-app/css/CCGrid.css"/>
        <link rel="stylesheet" href="/assets/ccc-app/css/toggles-light.css">
        <link rel="stylesheet" href="/assets/ccc-app/css/toggles-full.css">
        <link  rel="stylesheet" href="/assets/ccc-app/lib/css/video-js.css">

        <script type="text/javascript" src="/assets/ccc-app/lib/jquery-1.7.min.js"></script>
        <script type="text/javascript" src="/assets/ccc-app/lib/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/assets/ccc-app/lib/jquery.multiselect.js"></script>
        <script type="text/javascript" src="/assets/ccc-app/lib/jquery.multiselect.filter.js"></script>
        <script type="text/javascript" src="/assets/crud-app/js/trirand/jquery.jqGrid.js"></script>
        <script type="text/javascript" src="/assets/crud-app/js/trirand/i18n/grid.locale-en.js"></script>
        <script type="text/javascript" src="/assets/bootstrap-v3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/crud-app/js/bootbox.min.js"></script>
        <script type="text/javascript" src="/assets/ccc-app/lib/toggles.min.js"></script>
        <script type="text/javascript" src="/assets/ccc-app/lib/video.js"></script>

        {{-- main js custom js functions --}}
        <script type="text/javascript" src="/assets/ccc-app/js/ccc-main.js"></script>
        <script defer src="/assets/poassets/js/po-common-functions.js"></script>

        {{-- multiselect checkbox search js functions --}}
        <script type="text/javascript" src="/assets/ccc-app/js/multiSelectCheckBoxSearch.js"></script>
         <script type="text/javascript">
            window.baseUrl = window.location.hostname;
        </script>
    </head>
    <body>
        @yield('menu')

        <div class='grid_holder'>
            @include('layouts.notifications')
            @yield('content')
      </div>
    </body>
</html>
