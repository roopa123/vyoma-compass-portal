    @if (session('success'))
    	<div class="alert alert-success" id="success-alert">
        	{{ session('success') }}
    	</div>
	@endif
  @if (session('message'))
    <div class="alert alert-info">{{ session('message') }}</div>
  @endif

  @if (session('myerrors'))
    <div class="alert alert-danger" id="danger-alert">
          {{ session('myerrors') }}
      </div>
  @endif

  @if (count($errors) > 0)
      <div class="alert alert-danger" id="danger-alert">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
<script type="text/javascript">
  $("#success-alert").fadeTo(2000, 1000).slideUp(1000, function(){
    $("#success-alert").alert('close');
  });

  $("#danger-alert").fadeTo(2000, 1000).slideUp(1000, function(){
    $("#danger-alert").alert('close');
  });
</script>
