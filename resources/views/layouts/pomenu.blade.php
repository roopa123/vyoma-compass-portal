<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <button id="po-step" type="button" class="step-1 btn btn-primary btn-circle"></button>
            <p>P.O</p>
        </div>
        <div class="stepwizard-step">
            <button id="campaign-step" type="button" class="step-2 btn btn-default btn-circle" disabled="disabled"></button>
            <p>Campaign</p>
        </div>
        <div class="stepwizard-step">
            <button id="creative-step" type="button" class="step-3 btn btn-default btn-circle" disabled="disabled"></button>
            <p>Creative</p>
        </div>
        <div class="stepwizard-step">
            <button id="location-step" type="button" class="step-4 btn btn-default btn-circle" disabled="disabled"></button>
            <p>Location</p>
        </div>
        <div class="stepwizard-step">
            <button id="preview-step"  onclick="bindingpreview()" type="button" class="step-5 btn btn-default btn-circle" disabled="disabled"></button>
            <p>Preview</p>
        </div>
    </div>
</div>