
<div class="container">
    <input type="hidden" class="hiddenValueToStoreFilterdResu">
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><img class="homeLogo" src="{{URL::asset('assets/crud-app/images/logo.png')}}" width='112px'
                                                                   height="35px"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                @can('Show Op')
                <ul class="nav navbar-nav dropdownForMenu">
                    <a class="dropdown-toggle fake-link" data-toggle="dropdown">Operations
                        <span class="caret"></span>
                       {{-- <span class="pull-right caretIconWrapper">
                            <span class="caret"></span>
                        </span>--}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/') }}"><i class="fa fa-tachometer"></i>  Dashbaord</a></li>
                        @can('Show Host')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/host') }}"><i class="fa fa-user"></i>  Host</a></li>
                        @endcan
                        @can('Show Bay')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/bay') }}"><i class="fa fa-users"></i>  Bay</a></li>
                        @endcan
                        @can('Show Station')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/station') }}"><i class="fa fa-subway"></i>  Station</a></li>
                        @endcan
                        @can('Show BayLocation')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/baylocation') }}"><i class="fa fa-map-marker"></i>  Bay Location</a></li>
                        @endcan
                        @can('Show Region')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/region') }}"><i class="fa fa-map"></i>  Region</a></li>
                        @endcan
                        @can('Show Branch')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/branch') }}"><i class="fa fa-sitemap"></i>  Branch</a></li>
                        @endcan
                        {{--<li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/vmc-config') }}"><i class="fa fa-cogs"></i>  Vmc Config</a></li>--}}
                    </ul>
                    </li>
                    <li><a id="top-navbar-menu" href="#body" style="display: none;" class="sr-only">Scroll to navbar</a></li>
                </ul>
                @endcan
                @can('Show CC')
                <ul class="nav navbar-nav dropdownForMenu">
                    <a class="dropdown-toggle fake-link" data-toggle="dropdown">Customers
                        <span class="caret"></span>
                         {{--<span class="pull-right caretIconWrapper">
                            <span class="caret"></span>
                        </span>--}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        @can('Show Industry')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/industry') }}"><i class="fa fa-industry"></i> Industry </a></li>
                        @endcan
                        @can('Show Advertiser')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/advertiser') }}"><i class="fa fa-bullhorn"></i> Advertiser </a></li>
                        @endcan
                        @can('Show Creative')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/creative') }}"><i class="fa fa-lightbulb-o"></i> Creative </a></li>
                        @endcan
                        @can('Show PO')
                        <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/purchaseorder') }}"><i class="fa fa-inr"></i> Purchase Order </a></li>
                        @endcan
                    </ul>
                    <li><a id="top-navbar-menu" href="#body" style="display: none;" class="sr-only">Scroll to navbar</a></li>
                </ul>
                @endcan
                @can('Show User')
                <ul class="nav navbar-nav ">
                    <li id="oss-dev-top-bar-lqp" class="adminUserLink"><a href="{{ URL::to('/admin/users') }}"><img src="/assets/crud-app/images/usersIcon.png"/> Users </a></li>
                </ul>
                @endcan
                @can('Show Role')
                <ul class="nav navbar-nav ">
                    <li id="oss-dev-top-bar-lqp"><a href="{{ URL::to('/admin/roles') }}"><img src="/assets/crud-app/images/roleIcon.png"/> Roles </a></li>
                </ul>
                @endcan
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::check())
                        <li class="loginUserInfo">
                            <a href="#"><span class="userWelcome"></span> &nbsp <span class="userIcon">{{ Auth::user()->name }}</span></a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ URL::to('/auth/logout') }}" class="logoutAnchorBtn"> Logout &nbsp;&nbsp;&nbsp;<img src="/assets/crud-app/images/logoutIcon.png"/></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

