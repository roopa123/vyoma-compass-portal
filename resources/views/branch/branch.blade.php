<style>
.ui-jqgrid .ui-search-table{
  width: 300px !important;
  float: right;
}
</style>
@extends('layouts.apptemplate')

@section('title')
    BRANCH
@endsection

@section('menu')
@include('layouts.menu')
@endsection

@section('content')
@include('layouts.validationAlert')

<div class="">

    <div class="table-responsive">
        <table id="jqGridBranches" class="table "></table>
    </div>    
@can('Show Branch')
      {!!
    GridRender::setGridId("jqGridBranches")
      ->enablefilterToolbar()
      ->setGridOption('url',URL::to('/branch-grid'))
      ->setGridOption('editurl',URL::to('/branch/crud'))
      ->setGridOption('rowNum', 20)
      ->setGridOption('rownumbers', true)
      ->setGridOption('autowidth', true)
      ->setGridOption('height', 500)
      ->setGridOption('caption','List of Branches')
      ->setGridOption('viewrecords',true)
       ->setGridOption('altRows',true)
          ->setGridOption('altclass','myAltRowClass')
      ->setGridOption('cmTemplate', array('resizable' => false))
      ->setGridEvent('gridComplete', 'refreshPagewithcount')
      ->setGridEvent('loadComplete', 'dynamicHeightBR')

      ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => false, 'view' => true, 'refresh' => false))
      ->setNavigatorOptions('add', array('closeAfterAdd' => true, 'addCaption' => 'Add Branch', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '218'))
      ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')
      ->setNavigatorOptions('edit', array('closeAfterEdit' => true, 'editCaption' => 'Edit Branch Details', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))
      ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
      ->setNavigatorOptions('view', array('closeAfterEdit' => true, 'caption' => 'View Branch Info', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '201'))
      ->setNavigatorOptions('del', array('closeAfterEdit' => true, 'caption' => 'Delete Branch Info', 'resize' => false, 'drag' => false, 'left' => '491', 'top' => '212'))
      ->setNavigatorEvent('del', 'afterSubmit', 'afterSubmitEvent')
      ->setNavigatorEvent('refresh', 'afterSubmit', 'afterSubmitEvent')
      ->setFileProperty('FileName', 'Branches')
      ->addColumn(array('index' => 'BranchId', 'hidden' => true, 'editable' => true,
         'key' => true))
      ->addColumn(array('label' => 'Branch Name', 'index'=>'BranchName', 'edittype' => 'text',
              'editable' => true, 'align' => 'left', 'editrules' => array('required' => true),
             'formatter' => 'showlink',
             'formatoptions' => array('baseLinkUrl'=>'/station', 'idName' => 'branch')))
      ->addColumn(array('label' => 'Status','index' => 'Status', 'hidden' => false, 'edittype'=>'select', 'align' => 'left','editoptions' => array( 'value' => 'A:Active;I:Inactive'),'editable' => true, 'editrules' => array('required' => true, 'edithidden' => false), 'width' => '90px', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => ":All;A:Active;I:Inactive")))
      ->renderGrid();
    !!}
    @endcan

    <!-- Role based permissions -->
    <script type="text/javascript">
    $('#add_jqGridBranches').hide();
    $('#edit_jqGridBranches').hide();
    $('#view_jqGridBranches').hide();
      @can('Create Branch')
        $('#add_jqGridBranches').show();
      @endcan
      @can('Update Branch')
        $('#edit_jqGridBranches').show();
      @endcan
      @can('Show Branch')
        $('#view_jqGridBranches').show();
      @endcan 

    </script>

</div>
@stop
