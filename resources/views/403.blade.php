@extends('layouts.ccctemplate')

@section('title')
    Permission Denied
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="logo"></div>
<h3 class="text-center"> {{$message}}</h3>
<a href="{{$route}}" class = "btn btn-primary goBackHomeBtn">Go Back to Home</a>
@endsection
