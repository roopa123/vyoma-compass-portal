@extends('layouts.ccctemplate')

@section('title')
    Industry
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
    @include('layouts.validationAlert')
    @can('Show Industry')
    <div class="adminPanel form-group NameRole rolesTable">
    {{--<div class="col-md-12 col-xs-12 padding-0 margin-top-45">--}}
        <div class="baseDiv">
            <span class="color-gray text-center gridCaption customerGridCaption">{{"List of Industries"}}</span>
            @can('Create Industry')
            <a href="{{url('industry/create')}}" class="btn btn-success customerGridAdd"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;&nbsp;&nbsp;Add Industry</a>
            @endcan
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
                @endif
                {{--<h4 class="col-md-12 col-xs-12 color-gray">{{"List of Industry"}}&nbsp;:</h4>--}}

                        <!-- Check permission for create industry -->


                <div class="col-md-12 col-xs-12 padding0">
                    <table id="IndustryGrid" class="col-md-12 col-xs-12 table-bordered table-hover table-striped table-condensed table-responsive table"><tr></tr></table>
                    <div id="IndustryGridPager"></div>
                    <script type="text/javascript">

                        /*Delete industry throght ajax call*/
                        function deleteIndustry(id){
                            message = "Are you sure, you want to delete Industry ?";
                            bootbox.confirm(message, function(result) {
                                if(result == true) {
                                    $.ajax({type: "DELETE", url: "industry/"+id,
                                        success: function(results){
                                            if(results == 'success'){
                                                displayMessage("Industry deleted successfully", true, 'IndustryGrid');
                                            }else if(results == 'error'){
                                                displayMessage("Cannot delete Industry, its associated with Advertisers", false, 'IndustryGrid');
                                            }
                                            //location.reload();
                                        }
                                    });
                                    //$('#IndustryGrid').trigger('reloadGrid');
                                }
                            });
                        }

                        $.ajax({
                            type: "POST",
                            url: "/industry-grid",
                            dataType:"json",
                            success: function (dataJson) {
                                var oldFrom = $.jgrid.from,
                                        lastSelected;

                                $.jgrid.from = function (source, initalQuery) {
                                    var result = oldFrom.call(this, source, initalQuery),
                                            old_select = result.select;
                                    result.select = function (f) {
                                        lastSelected = old_select.call(this, f);
                                        return lastSelected;
                                    };
                                    return result;
                                };
                                jQuery("#IndustryGrid").jqGrid(
                                        {
                                            "datatype": "local",
                                            "mtype": "POST",
                                            "treeGridModel": "adjacency",
                                            "data": dataJson.rows, "rowNum": 10,
                                            "rownumbers": true, "autowidth": true,
                                            "viewrecords": true,
                                            "height": 'auto',
                                            "pager": '#IndustryGridPager',
                                            "altRows":true,
                                            "altclass":'myAltRowClass',
                                            "recreateFilter": true,
                                            beforeRequest: function () {
                                                modifySearchingFilter.call(this, ',');
                                            },
                                            loadComplete: function () {

                                                /* replacing industry column null values with 'Blank' */
                                                $.each($("#IndustryGrid").jqGrid('getGridParam','data'),function(index,value){
                                                    if(value.Status=='A'){
                                                        value.Status='Active';
                                                    }
                                                    if(value.Status=='D'){
                                                        value.Status='Delete';
                                                    }
                                                });

                                                this.p.lastSelected = lastSelected; // set this.p.lastSelected
                                                highlightFilteredColumns.call(this); //highlightFilteredColumns

//                                                changeStatusValue('IndustryGrid', 'IndustryGrid_Status');
                                                var iCol = columnIndexForGridBasedOnTableIdAndColumnName('#IndustryGrid', 'Actions');
                                                var iColStatus = columnIndexForGridBasedOnTableIdAndColumnName('#IndustryGrid', 'Status');
                                                var id, message;
                                                var statusValue=[];

                                                /*** status col***/
                                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColStatus + 1) + ")")
                                                        .each(function(value,key) {
                                                            statusValue.push($(this).text());
                                                        });

                                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iCol + 1) + ")")
                                                        .each(function (value,key) {
                                                            if(statusValue[value] == 'Delete') {
                                                                $("<div>", {
                                                                    mouseover: function (e) {
                                                                        id = $(e.target).closest("tr.jqgrow").attr("id");
                                                                        $(this).children('.update-link').attr("href", "/industry/" + id + "/edit");
                                                                    }}).css({cursor: "pointer"})
                                                                        .addClass("col-md-12 col-xs-12 wrapperForButtons")
                                                                        /*Check Update permission for industry*/
                                                                        @can('Update Industry')
                                                                          .append('<a href="" class="col-md-5 col-xs-12 update-link">Edit</a>')
                                                                @endcan
                                                                .prependTo($(this).children("div"));
                                                            }
                                                            else {
                                                                $("<div>", {
                                                                    mouseover: function (e) {
                                                                        id = $(e.target).closest("tr.jqgrow").attr("id");
                                                                        $(this).children('.update-link').attr("href", "/industry/" + id + "/edit");
                                                                        $(this).children('.delete-link').attr("onclick", "deleteIndustry('" + id + "')");
                                                                    }}).css({cursor: "pointer"})
                                                                        .addClass("col-md-12 col-xs-12 wrapperForButtons")
                                                                        /*Check Update permission for industry*/
                                                                        @can('Update Industry')
                                                                          .append('<a href="" class="col-md-5 col-xs-12 update-link">Edit</a>')
                                                                @endcan
                                                                /*Check Delete permission for industry*/
                                                                @can('Delete Industry')
                                                                  .append('<span class="col-md-5 col-xs-12 delete-link">Delete</span>')
                                                                @endcan
                                                                .prependTo($(this).children("div"));
                                                            }
                                                        });
                                                setMultiSearchSelect('IndustryName', jQuery("#IndustryGrid"), jQuery("#IndustryGrid").jqGrid('getGridParam','data'));
//                                                setMultiSearchSelectForStatus('Status', jQuery("#IndustryGrid"), jQuery("#IndustryGrid").jqGrid('getGridParam','data'), ":All;A:Active;D:Delete");
                                                setMultiSearchSelect('Status', jQuery("#IndustryGrid"), jQuery("#IndustryGrid").jqGrid('getGridParam','data'));

                                                /* hiding actions column based on the roles and permissions of users*/
                                                if(statusValue.length > 0){
                                                    if ( $('#IndustryGrid .wrapperForButtons').children().length <= 0 ) {
                                                        $(this).jqGrid('hideCol',["Actions"]);
                                                    }
                                                }
                                                /* set max height for all grids based on the window height */
                                                var height = $(window).height();
                                                $('.ui-jqgrid .ui-jqgrid-bdiv').css("max-height",height/2+"px");
                                            },
                                            "colModel": [
                                                {
                                                    "label": "Industry Id",
                                                    "index": "IndustryId",
                                                    "name": "IndustryId",
                                                    "search": false,
                                                    "key": true,
                                                    'hidden': true
                                                },
                                                {
                                                    "label": "Industry Name",
                                                    "index": "IndustryName",
                                                    "name": "IndustryName",
                                                    "search": true,
                                                    "resizable":false
                                                },
                                                {"label": "Status", "index": "Status", "name": "Status", "search": true,"resizable":false, formatter: function(cellvalue, options, rowobject){
                                                    if(cellvalue=='A'){
                                                        cellvalue='Active';
                                                    }
                                                    if(cellvalue=='D'){
                                                        cellvalue='Delete';
                                                    }
                                                    return cellvalue;
                                                }},
                                                {"label": "Actions", "name": "Actions", "formatter": "actions", "resizable":false}]
                                        });
                                jQuery("#IndustryGrid").jqGrid('navGrid', '#pager', {edit: false, add: false, del: false, search: false}, {}, {}, {}, {
                                    multipleSearch: true,
                                    multipleGroup: true,
                                    recreateFilter: true,
                                    overlay: 0
                                });
                                $('#jqgh_IndustryGrid_IndustryName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                                $('#jqgh_IndustryGrid_Status').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');

                                jQuery("#IndustryGrid").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch: myDefaultSearch});

                                $('#IndustryGrid_IndustryName .vSymbol').on('click',function(){
                                    $('.ui-search-toolbar #gsh_IndustryGrid_IndustryName button.ui-multiselect.ui-widget.ui-state-default').click();
                                    removeUnWantedSearchOptions(0, 'IndustryName',$('#IndustryGrid').jqGrid('getGridParam', 'lastSelected'),'IndustryGrid');
                                    return false;
                                });
                                $('#IndustryGrid_Status .vSymbol').on('click',function(){
                                    $('.ui-search-toolbar #gsh_IndustryGrid_Status button.ui-multiselect.ui-widget.ui-state-default').click();
                                    removeUnWantedSearchOptions(1, 'Status',$('#IndustryGrid').jqGrid('getGridParam', 'lastSelected'),'IndustryGrid');
                                    return false;
                                });
                                $('.ui-search-toolbar th').css("visibility","hidden");

                            }
                        });
                    </script>
                </div>
    @endcan
@stop
