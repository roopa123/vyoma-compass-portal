@extends('layouts.apptemplate')

@section('title')
    Industry
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
  <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>

  <div class="logo"></div>

@can('Update Industry')
  <div class="industryContainer col-md-8 col-xs-12">
  {!! Form::open(['method' => 'PATCH','route'=>['industry.update',$industry->IndustryId]]) !!}
  {!! csrf_field() !!}
  <h4 class="color-gray">{{"Edit Industry "}}&nbsp;:</h4>
  <div class="clearfix fieldsWrapper col-md-8 col-xs-12">
    <div class="min-height-100 col-md-12 col-xs-12">
      {!! Form::label('industryName','Industry Name',array('class' => 'color-blue')) !!}
      {!! Form::hidden('industryId', Input::old('industryId', $industry->IndustryId), ['class'=>'form-control']) !!}
      
      {!! Form::text('industryName', Input::old('industryName',$industry->IndustryName), ['class'=>'form-control']) !!}
    </div>
    <div class="min-height-100 col-md-12 col-xs-12">
      {!! Form::label('status','Status',array('class' => 'color-blue')) !!}
      {!! Form::select('status', $statuss, $industry->Status , ['class'=>'form-control']) !!}
    </div>
    <div class="col-md-12 col-xs-12">
      <div class="col-md-6 col-xs-12 text-center">
        <a href="{{{ URL::route('industry.index') }}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
      </div>
      <div class="col-md-6 col-xs-12 text-center">
          <button class="btn btn-default IndustrySubmitBtn col-md-12 col-xs-12 formSubmitUpdatePreloader">Update</button>
      </div>
    </div>
    </div>
  {!! Form::close() !!}
  </div>
  @endcan
@stop
