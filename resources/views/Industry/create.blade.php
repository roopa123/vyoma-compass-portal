@extends('layouts.apptemplate')

@section('title')
    Industry
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
  <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
  <div class="logo"></div>
  @can('Create Industry')
  <div class="industryContainer col-md-8 col-xs-12">
    <form method="POST" action="/industry">
      {!! csrf_field() !!}
      <fieldset>
        <h4 class="color-gray">{{"Add Industry "}}&nbsp;:</h4>
        <div class="clearfix fieldsWrapper col-md-8 col-xs-12">
          <div class="min-height-100 col-md-12 col-xs-12">
            {!! Form::label('industryName','Industry Name',array('class' => 'color-blue')) !!}
            {!! Form::text('industryName', Input::old('industryName'), ['class'=>'form-control']) !!}
          </div>                 
          <div class="col-md-12 col-xs-12">
            <div class="col-md-6 col-xs-12 text-center">
              <a href="{{{ URL::route('industry.index') }}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
            </div>
            <div class="col-md-6 col-xs-12 text-center">
                <button class="btn btn-default IndustrySubmitBtn col-md-12 col-xs-12 formSubmitUpdatePreloader">Submit</button>
            </div>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
    @endcan
@stop
