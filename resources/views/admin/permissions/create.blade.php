<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layouts.apptemplate')

@section('title')
    Permissions Management
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
@include('layouts.validationAlert')
<div class="adminPanel">
@section('content')
    <h1>Create Permissions</h1>
    {!! Form::open(['url'=>'/admin/permissions']) !!}
    <div class="form-group">
        {!! Form::label('ParentPermission','Parent Gruop:') !!}
        {!! Form::text('ParentPermission',null,['class'=>'form-control']) !!}
    </div>
     <div class="form-group">
        {!! Form::label('AccessLevel','Permission:') !!}
        {!! Form::text('AccessLevel',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Create',['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop