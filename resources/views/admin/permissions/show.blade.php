<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layout/template')
@section('content')
    <h1>Role</h1>
    <form class="form-horizontal">
        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                    <input type="text" class="form-control" id="id" placeholder="{{$roles->id}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title" placeholder="{{$roles->name}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="author" placeholder="{{$roles->description}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <h4>Selected permission</h4>
                @foreach($mypermissions as $p)
                    <input type="checkbox" name="permission[]" value="{{$p->id}}"> {{$p->name}}<br>
                @endforeach
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <a href="{{ url('roles')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
    </form>
@stop
