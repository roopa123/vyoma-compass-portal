<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layouts.apptemplate')

@section('title')
    Role Management - Update
@endsection

@section('menu')
@include('layouts.menu')
@endsection

@section('content')
<div class="adminPanel">
@include('layouts.validationAlert')
    {!! Form::model($permission,['method' => 'PATCH','route'=>['admin.permissions.update',$permission->PermissionId]]) !!}
    <div class="form-group">
        {!! Form::label('AccessLevel','Permission:') !!}
        {!! Form::text('AccessLevel',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
</div>
@stop