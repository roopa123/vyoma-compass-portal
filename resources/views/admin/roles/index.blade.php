<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layouts.apptemplate')

@section('title')
    Role Management
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
@include('layouts.validationAlert')
<div class="adminPanel form-group NameRole rolesTable">
    <span class="text-center color-gray gridCaption customerGridCaption">{{"List of Roles "}} </span>
    @can('Create Role')
        <a href="{{url('admin/roles/create')}}" class="btn btn-success customerGridAdd"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;&nbsp;&nbsp;Create Role</a>
    @endcan

    <table class="table-hover table-striped table-condensed table-responsive table rolesTable">
        <thead>
        <tr class="bg-info">
            <th>Role Name</th>
            <th colspan="2">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($roles as $role)
            <tr>
                <td>{{$role -> RoleName}}</td>
                <td>
                    @if($role -> RoleName != \Config::get('vyoma-constants.super_admin_role_name'))
                        @can('Update Role')
                            <a href="{{route('admin.roles.edit',$role->RoleId)}}" class="btn btn-default update-link">Edit</a>
                        @endcan
                        @can('Delete Role')
                            <input class="btn btn-default delete-link" type="button" style="margin-left: 7px" value ="Delete" onclick='deleteRoles({!! $role->RoleId !!})' />                        

                        @endcan
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


<script type="text/javascript">
    function deleteRoles(id){
        message = "Are you sure want to Delete Role ?";
        bootbox.confirm(message, function(result) {
            if(result == true) {
                $.ajax({type: "DELETE", url: "roles/"+id,
                    success: function(results){
                        if(results == 'success'){
                            displayMessage("Role deleted successfully", true, '');
                        }else if(results == 'error'){
                            displayMessage("Cannot delete Role, its associated with Users", false, '');
                        }
                    }

                  });                               
            }
        });
    }
</script>
@stop
