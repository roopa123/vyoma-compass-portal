<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layouts.apptemplate')

@section('title')
    Role Management - Update
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')

    <div class="adminPanel createRolesBlock">
        @include('layouts.validationAlert')
        {!! Form::model($role,['method' => 'PATCH', 'route'=>['admin.roles.update',$role->RoleId]]) !!}
        <h4 class="color-gray text-center">{{"Update Roles"}}</h4>
        <div class="form-group NameRole edit">
            {!! Form::label('Name','Name:') !!}
            {!! Form::text('RoleName', Input::old('RoleName'),['class'=>'form-control']) !!}
        </div>
        <div class="rolesSelectBlock col-md-10 col-xs-12">
            <h4>Select applicable permissions</h4>
            @include('admin.roles.permissions', ['permissions' => $mypermissions,
                'permissionGroup' => $permissionGroup,
                'selectedpermission'=>$selectedpermission]
                )
        </div>

        <div class="form-group btn-create-role text-center col-md-8 col-xs-12 min-height-100">
            <a href="{{{ URL::route('admin.roles.index') }}}" class="btn btn-primary cancel-btn col-md-5 col-xs-12">Cancel</a>
            {!! Form::submit('Save',['class' => 'btn btn-primary col-md-5 col-xs-12']) !!}
        </div>
      {{--  <div class="form-group btn-create-role text-center">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        </div>--}}
        {!! Form::close() !!}
    </div>
    </div>
@stop
