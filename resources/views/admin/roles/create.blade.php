<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layouts.apptemplate')

@section('title')
    Role Management
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')

    @include('layouts.validationAlert')
    <div class="col-md-12 col-xs-12">
        <div class="adminPanel createRolesBlock">
            {!! Form::open(['url'=>'admin/roles']) !!}
            <h4 class="color-gray text-center">{{"Create Roles"}}</h4>
            <div class="form-group NameRole">
                {!! Form::label('RoleName','Name:') !!}
                {!! Form::text('RoleName', Input::old('RoleName'), ['class'=>'form-control']) !!}
            </div>
            <div class="rolesSelectBlock col-md-10 col-xs-12">
                <h4> Select applicable permissions</h4>
                @include('admin.roles.permissions', ['permissions' => $permissions,
                    'permissionGroup' => $permissionGroup,
                    'selectedpermission'=> $selectedpermission])
            </div>
            <div class="form-group btn-create-role text-center col-md-8 col-xs-12 min-height-100">
                   <!--  <input class="btn btn-primary cancel-btn col-md-5 col-xs-6" value="Cancel" type="button"> -->

                     <a href="{{{ URL::route('admin.roles.index') }}}" class="btn btn-primary cancel-btn col-md-5 col-xs-12">Cancel</a>
                {!! Form::submit('Create',['class' => 'btn btn-primary col-md-5 col-xs-12']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @stop

    </div>
