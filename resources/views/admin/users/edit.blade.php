<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layouts.apptemplate')

@section('title')
	Permissions Management
@endsection

@section('menu')
	@include('layouts.menu')
@endsection

@section('content')
	@include('layouts.validationAlert')
	<div class="adminPanel form-group NameRole usersTable">


		{!! Form::model($user,['method' => 'PATCH', 'route'=>['admin.users.update',$user->id]]) !!}
				<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		<h4 class="color-gray">{{"Update User"}}&nbsp;:</h4>
		<div class="clearfix fieldsWrapper col-md-8 col-xs-12">
			<!-- username -->
			<div class="min-height-100 col-md-6 col-xs-12">
				{!! Form::label('Name','Name:',['class'=>'color-blue']) !!}
				{!! Form::text('name', null,['class'=>'form-control']) !!}
			</div>
			<!-- ./ username -->
			<div class="min-height-100 col-md-6 col-xs-12">
				{!! Form::label('email','Email:',['class'=>'color-blue']) !!}
				{!! Form::text('email', null,['class'=>'form-control']) !!}
			</div>
			<!-- ./ email -->

			<!-- Password -->
			<div class="min-height-100 col-md-6 col-xs-12">
				{!! Form::label('password','Password:',['class'=>'color-blue']) !!}
				{!! Form::password('password', null,['class'=>'form-control']) !!}
			</div>
			<!-- ./ password -->
			<!-- Password Confirm -->
			<div class="min-height-100 col-md-6 col-xs-12">
				{!! Form::label('password_confirmation','Confirm Password:',['class'=>'color-blue']) !!}
				{!! Form::password('password_confirmation', null,['class'=>'form-control']) !!}
			</div>
			<!-- ./ password confirm -->
			<!-- Role -->
			<div class="min-height-200 col-md-12 col-xs-12">
				{!! Form::label('role','Role:',['class'=>'color-blue']) !!}
				{!! Form::select('role[]', $roles, $userRoles, ['class'=>'form-control roleMultiSelect',
				'multiple' => 'multiple']) !!}
			</div>
			<!-- ./ Role-->
			{{--<div class="min-height-100 col-md-12 col-xs-12 text-center">
				{!! Form::submit('Save', ['class' => 'btn btn-default IndustrySubmitBtn col-md-12 col-xs-12']) !!}
			</div>--}}
			<div class="form-group btn-create-role text-center col-md-8 col-xs-12 btn-create-user">
				<a href="{{{ URL::route('admin.users.index') }}}" class="btn btn-primary cancel-btn col-md-5 col-xs-12">Cancel</a>
				{!! Form::submit('Save',['class' => 'btn btn-primary col-md-5 col-xs-12']) !!}
			</div>
			<!-- ./ form actions -->
@stop
