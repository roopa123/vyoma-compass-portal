<style>
    body{
        overflow-y: auto !important;
    }
</style>
@extends('layouts.apptemplate')

@section('title')
    Permissions Management
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
    @include('layouts.validationAlert')
    <script type="text/javascript">
        function deleteUsers(id){
            message = "Are you sure want to Delete User ?";
            bootbox.confirm(message, function(result) {
                if(result == true) {
                    $.ajax({type: "DELETE", url: "users/"+id,
                        success: function(results){
                            if(results == 'success'){
                                displayMessage("User deleted successfully", true, '');
                            }else if(results == 'error'){
                                displayMessage("Cannot Delete the User", false, '');
                            }
                        }

                    });
                }
            });
        }
    </script>
    <div class="adminPanel form-group NameRole rolesTable">
        <span class="text-center color-gray gridCaption customerGridCaption">{{"List of Users "}} </span>
        @can('Create User')
        <a href="{{url('admin/users/create')}}" class="btn btn-success customerGridAdd"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;&nbsp;&nbsp;Create User</a>
        @endcan
        <table class="table-hover table-striped table-condensed table-responsive table usersTable">
            <thead>
            <tr class="bg-info">
                <th class="col-md-2">Name</th>
                <th class="col-md-2">Email</th>
                <th class="col-md-2">Role</th>
                <th class="col-md-2">Created Date</th>
                <th class="col-md-2">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $rol = array(); ?>
            @foreach ($users as $user)
                <tr>
                    <td>{{$user['name']}}</td>
                    <td>{{$user['email']}}</td>
                    <?php $rols = ""; ?>
                    <?php $rol = array(); ?>
                    @foreach ($user['roles'] as $roles)
                        <?php $rol[] = $roles['RoleName']; ?>
                    @endforeach
                    <td>{{$rols = implode(",", $rol)}}</td>
                    <td>{{$user['created_at']}}</td>
                    <td>
                        @if($user['id'] != \Config::get('vyoma-constants.super_admin_id'))
                            @can('Update User')
                            <a href="{{route('admin.users.edit',$user['id'])}}" class="btn btn-default update-link">Edit</a>
                            @endcan
                            @can('Delete User')
                            <input class="btn btn-default delete-link" type="button" style="margin-left: 7px" value ="Delete" onclick="deleteUsers({!! $user['id'] !!})" />
                            @endcan
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @stop

        {{-- Scripts --}}
        @section('scripts')
            <script type="text/javascript" src="assets/crud-app/js/serviceClient.js" />
            <script type="text/javascript" src="assets/crud-app/js/util.js" />
            <script type="text/javascript">
                var oTable;
                $(document).ready(function() {
                    oTable = $('#users').dataTable( {
                        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                        "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sLengthMenu": "_MENU_ records per page"
                        },
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": "{{ URL::to('admin/users/data') }}",
                        "fnDrawCallback": function ( oSettings ) {
                            $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
                        }

                    });
                    /* Remove cancel arrow in create, edit, delete user  */
                    $('#cboxClose').remove();
                });
            </script>

@stop
