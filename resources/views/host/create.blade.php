@extends('layouts.apptemplate')

@section('content')
<div class="clearfix fieldsWrapper col-md-8 col-xs-12">
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('PlayerWinSize','PlayerWinSize',array('class' => 'color-blue')) !!}
        {!! Form::select('PlayerWinSize', $codeDesc['PlayerWinSize'], $codeDefaults['PlayerWinSize'] , ['class'=>'form-control', 'id' => 'PlayerWinSize']) !!}
	</div>	
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('HdmiMode','HdmiMode',array('class' => 'color-blue')) !!}
        {!! Form::select('HdmiMode', $codeDesc['HdmiMode'], $codeDefaults['HdmiMode'] , ['class'=>'form-control', 'id' => 'HdmiMode']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('ScreenRatio','ScreenRatio',array('class' => 'color-blue')) !!}
        {!! Form::select('ScreenRatio', $codeDesc['ScreenRatio'], $codeDefaults['ScreenRatio'] , ['class'=>'form-control', 'id' => 'ScreenRatio']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('RouterRebootSchedule','RouterRebootSchedule',array('class' => 'color-blue')) !!}
        {!! Form::select('RouterRebootSchedule', $codeDesc['RouterRebootSchedule'], $codeDefaults['RouterRebootSchedule'] , ['class'=>'form-control', 'id' => 'RouterRebootSchedule']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('CableType','CableType',array('class' => 'color-blue')) !!}
        {!! Form::select('CableType', $codeDesc['CableType'], $codeDefaults['CableType'] , ['class'=>'form-control', 'id' => 'CableType']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('PlayLogUpdateSchedule','PlayLogUpdateSchedule',array('class' => 'color-blue')) !!}
        {!! Form::select('PlayLogUpdateSchedule', $codeDesc['PlayLogUpdateSchedule'], $codeDefaults['PlayLogUpdateSchedule'] , ['class'=>'form-control', 'id' => 'PlayLogUpdateSchedule']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('ContentUpdateSchedule','ContentUpdateSchedule',array('class' => 'color-blue')) !!}
        {!! Form::select('ContentUpdateSchedule', $codeDesc['ContentUpdateSchedule'], $codeDefaults['ContentUpdateSchedule'] , ['class'=>'form-control', 'id' => 'ContentUpdateSchedule']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('TicketLogUpdateSchedule','TicketLogUpdateSchedule',array('class' => 'color-blue')) !!}
        {!! Form::select('TicketLogUpdateSchedule', $codeDesc['TicketLogUpdateSchedule'], $codeDefaults['TicketLogUpdateSchedule'] , ['class'=>'form-control', 'id' => 'TicketLogUpdateSchedule']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('DisplayType','DisplayType',array('class' => 'color-blue')) !!}
        {!! Form::select('DisplayType', $codeDesc['DisplayType'], $codeDefaults['DisplayType'] , ['class'=>'form-control', 'id' => 'DisplayType']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('DisplaySize','DisplaySize',array('class' => 'color-blue')) !!}
        {!! Form::select('DisplaySize', $codeDesc['DisplaySize'], $codeDefaults['DisplaySize'] , ['class'=>'form-control', 'id' => 'DisplaySize']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('PowerAdapterModel','PowerAdapterModel',array('class' => 'color-blue')) !!}
        {!! Form::select('PowerAdapterModel', $codeDesc['PowerAdapterModel'], $codeDefaults['PowerAdapterModel'] , ['class'=>'form-control', 'id' => 'PowerAdapterModel']) !!}
	</div>
	<div class="min-height-100 col-md-6 col-xs-12">
        {!! Form::label('PiModel','PiModel',array('class' => 'color-blue')) !!}
        {!! Form::select('PiModel', $codeDesc['PiModel'], $codeDefaults['PiModel'] , ['class'=>'form-control', 'id' => 'PiModel']) !!}
	</div>
</div>
@stop
