@extends('layouts.apptemplate')

@section('title')
    Hosts
@endsection

@section('menu')
@include('layouts.menu')
@endsection


@section('content')
@include('layouts.validationAlert')
<div class="">
    <div class="">
    <style type="text/css" media="screen">
    th.ui-th-column div{
        white-space:normal !important;
        height:auto !important;
        padding:2px;
    }
    .ui-jqgrid .ui-jqgrid-resize {height:100% !important;}
    </style>

 <script type="text/javascript">

  $(document).ready(function(){
        // Prevent Actions Column Export
        $('.ui-icon-refresh').click(function() { location.reload();}) ;
      });
  
  function viewBaylocationInfo(formId){
    getHostInfo($("#v_HostName").text().replace(/\s+/g, ''));
    var start = $('#v_Host.StartTime').text();
    var end =$('#v_Host.EndTIme').text();
    $('#v_Host.StartTime').text(start);
    $('#v_Host.EndTIme').text(end);

    $('.ui-icon.ui-icon-triangle-1-e').click(function(){
      getHostInfo($("#v_HostName").text().replace(/\s+/g, ''));
      getHostTime($("#v_HostName").text().replace(/\s+/g, ''));
    });

    $('.ui-icon.ui-icon-triangle-1-w').click(function(){
      getHostInfo($("#v_HostName").text().replace(/\s+/g, ''));
      getHostTime($("#v_HostName").text().replace(/\s+/g, ''));
    });
      

    function getBayLocation(bayId, hostId) {
      $.ajax({
              url: '/getbayLocation/'+bayId+'/'+hostId,
              type: "GET",
              success: function (data) {
                  $("#v_BayId").text(data.BayLocationCode);
              }
            });
    }

  }

 function intializeBefore(formId) {
  console.log('m in init');
  console.log($("#HostName").val());
  getHostBayId($("#HostName").val());

  function getHostBayId(hostname) {
    $.ajax({
              url: '/getbayId/'+hostname,
              type: "GET",
              success: function (data) {
                console.log(data.BayId)
                  updateBayLocationinit($("#StationName").val(), data.BayId);
              }
            });
  }

    function updateBayLocationinit(stationId, setselected) {
            $.ajax({
              url: '/bayInfo/'+stationId,
              type: "GET",
              success: function (data) {
                var sn = data;
                console.log(setselected)

              $('#BayId').empty();
              console.log(setselected);

              $.each(data, function(i, obj){
                console.log("bay location code "+obj.BayId);
                  if(setselected === obj.BayId) {
                    $('#BayId').append($('<option>').text(obj.BayLocationCode).attr('value', setselected).attr("selected",true));
                  }
                  else {
                    $('#BayId').append($('<option>').text(obj.BayLocationCode).attr('value', obj.BayId));
                  }
            });
          }
        });
    }
 }


  function editBefore(formId) {
   var starttime = $('#Host\\.StartTime').val();
   var endtime = $('#Host\\.EndTIme').val();
   $('#tr_HostMasterName').hide();
   if($('#MasterOrPlayer').val() == '{{ \Config::get('vyoma-constants.playerType') }}') {
      $('#tr_HostMasterName').show();
   }
    $(document).on("change","#MasterOrPlayer", function (e) {
          console.log($('#MasterOrPlayer').val());
          if($('#MasterOrPlayer').val() == '{{ \Config::get('vyoma-constants.masterType') }}') {
              $('#tr_HostMasterName').hide();
          }
          else if($('#MasterOrPlayer').val() == '{{ \Config::get('vyoma-constants.bothType') }}') {
              $('#tr_HostMasterName').hide();
          }
          else {
            $('#tr_HostMasterName').show();
          }
      });

   console.log(starttime);
   $('#Host\\.StartTime').val(starttime);
   $('#Host\\.EndTIme').val(endtime);
  $('#Host\\.StartTime').datetimepicker({
                       format: 'HH:mm',
                              pickDate: false,
                              pickSeconds: false,
                              pick12HourFormat: false
                    });
                    $('#Host\\.EndTIme').datetimepicker({
                        format: 'HH:mm',
                               pickDate: false,
                               pickSeconds: false,
                               pick12HourFormat: false

                    });
   $('#tr_BayLocationBayId').hide();
   $('#tr_BayLocation').hide();
   var hostType = $('#MasterOrPlayer').val();
   var hostMasterName = $("#HostMasterName").val();
   editDropdownFOrVMC($('#MasterOrPlayer').val());
   console.log($('#MasterOrPlayer').val());
    $('#HostMasterName').prop('readonly', true);
    if(hostType == 'M') {
            $('#MasterOrPlayer').val(hostType);            
         }
     
    $(document).on("change","#MasterOrPlayer", function (e) {
        if($("#MasterOrPlayer").val() == '{{ \Config::get('vyoma-constants.playerType') }}')
        {
          updateMasterName($('#BayId').val(), false);
        }
        else {
          $('#HostMasterName').prop('readonly', false);
          $("#HostMasterName").val(hostMasterName);
        }

     });

    function updateApp(bayId) {
      $.ajax({
              url: '/appinfo/'+bayId,
              type: "GET",
              success: function (data) {
                var sn = data;
                console.log(data)
                  $("#App").val(sn);
                  $('#App').prop('readonly', true);
              }
            });
    }

     function updateMasterName(bayId, setselected) {
      console.log('update master name');
      //console.log(bayId);
            $.ajax({
              url: '/playerinfo/'+bayId,
              type: "GET",
              success: function (data) {
                var sn = data;
                console.log(data)
                  $("#HostMasterName").val(sn);
                  $('#HostMasterName').prop('readonly', true);
              }
            });
          }
    $('.ui-icon.ui-icon-triangle-1-e').click(function(){        
          updateBayLocation($("#StationName").val(), $("#StationName").val());      
    });
    $('.ui-icon.ui-icon-triangle-1-w').click(function(){        
          updateBayLocation($("#StationName").val(), $("#StationName").val());      
    });


     $(document).on("change","#StationName", function (e) {
          updateBayLocation($("#StationName").val(), $("#StationName").val());
      });

     $(document).on("change","#BayId", function (e) {
          updateMasterName($("#BayId").val(), false);
          updateApp($("#BayId").val());
      });       


    function updateBayLocation(stationId, setselected) {
            $.ajax({
              url: '/bayInfo/'+stationId,
              type: "GET",
              success: function (data) {
                var sn = data;
              if(!data)
                $('#App').val("");
              $('#BayId').empty();
              //$('#BayId').append($('<option>').text("select"));
              $.each(data, function(i, obj){
                console.log(setselected);
                console.log(obj.BayId);
                  if(setselected == obj.BayId) {
                    $('#BayId').append($('<option>').text(obj.BayLocationCode).attr('value', $("#StationName").val()));
                  }
                  else {
                    $('#BayId').append($('<option>').text(obj.BayLocationCode).attr('value', obj.BayId));
                  }
                  $('#App').val(obj.App);
                  updateMasterName(obj.BayId, false);
            });
          }
        });
    }
      // Get the deafult selected values
      stationval = $("#StationName").val();

      // Sort options in alphabetical order
      $('#StationName').sortOptions(stationval); 
}

  function addBefore(formId){
    $('#tr_HostMasterName').hide();
    $(document).on("change","#MasterOrPlayer", function (e) {
          console.log($('#MasterOrPlayer').val());
          if($('#MasterOrPlayer').val() == '{{ \Config::get('vyoma-constants.masterType') }}') {
              $('#tr_HostMasterName').hide();
          }
          else if($('#MasterOrPlayer').val() == '{{ \Config::get('vyoma-constants.bothType') }}') {
              $('#tr_HostMasterName').hide();
          }
          else {
            $('#tr_HostMasterName').show();
          }
      });

    
    console.log('m in addBefore');   
     starttime = $('#Host\\.StartTime');
      console.log(starttime);
      starttimeval = starttime.attr('value');
      if (starttimeval) {
        starttimeval = starttimeval.slice(0,-3);
      } else {
        starttimeval = "00:00";
      }
      starttime.val(starttimeval);

      endtime = $('#Host\\.EndTIme');
      endtimeval = endtime.attr('value');
      if (endtimeval) {
        endtimeval = endtimeval.slice(0, -3);
      } else {
        endtimeval = "00:00";
      }
      endtime.val(endtimeval); 

   $('#Host\\.StartTime').datetimepicker({
                        format: 'HH:mm',
                               pickDate: false,
                               pickSeconds: false,
                               pick12HourFormat: false

                     });
                     $('#Host\\.EndTIme').datetimepicker({
                         format: 'HH:mm',
                                pickDate: false,
                                pickSeconds: false,
                                pick12HourFormat: false
                     });
   $('#tr_BayLocationBayId').hide();
   $('#MasterOrPlayer').prepend("<option value='' selected>Select</option>");
   $('#HostCounterType').prepend("<option value='' selected>Select</option>");
   $('#Status').prepend("<option value='' selected>Select</option>");

      /*Adding add another button to host add form*/
      $('#editcntjqGridHost #Act_Buttons .EditButton').prepend('<a class="addAnotherHost fm-button ui-state-default ui-corner-all fm-button-icon-left" ' +
              '><span class="ui-icon ui-icon-plus"></span>&nbsp;Add Another</a>');

    $('.addAnotherHost').on('click', function() {
      $.ajax({
          url: '/host/crud',
          type: "POST",
          dataType: "JSON",
          data : {
                  HostId: $('#HostId').val(),
                  MasterOrPlayer : $('#MasterOrPlayer').val(),
                  BayLocation : $('#BayLocation').val(),
                  BayId : $('#BayId').val(),
                  BayLocationBayId : $('#BayLocationBayId').val(),
                  HostName : $('#HostName').val(),
                  HostMasterName : $('#HostMasterName').val(),
                  HostCounterType : $('#HostCounterType').val(),
                  CounterNumber : $('#CounterNumber').val(),
                  Host_StartTime : $('#Host\\.StartTime').val(),
                  Host_EndTIme : $('#Host\\.EndTIme').val(),
                  Host_Status : $('select[id="Host.Status"]').val(),
                  oper : 'add',
                },
          success: function (data) {            
                  var sucess = data.message;
                   $('.tinfo').text(sucess);
                   $('.tinfo').css({'display':'block'});
                   $('.tinfo').fadeTo(2000, 1000).slideUp(500, function(){      
                    });
          },
          error: function(data){
            var errors = data.responseJSON;
            console.log(errors.errors);
                  $.each(errors.errors, function( key, value ) {
                       $('.ui-state-error').text(value);

                        $('.ui-state-error').css({'display':'block'});
                        $(".FormError.ui-state-error").fadeTo(2000, 1000).slideUp(1000, function(){      
                        });
                  });     

            // Render the errors with js ...
          }
      });
    });
  }

  function addAfter(formId) {
    console.log($('#StationName[name="StationName"]').val());
  }

 </script>
    <table id="jqGridHost" class="table "></table>
    </div>
    @can('Show Host')
    {!!
        GridRender::setGridId("jqGridHost")
          ->enablefilterToolbar()
          ->setGridOption('url',$buildUrl)
          ->setGridOption('editurl',URL::to('/host/crud'.$redirect))
          ->setGridOption('rowNum', 200)
          ->setGridOption('autowidth', true)
          ->setGridOption('height', 500)
          ->setGridOption('caption','List of Hosts')
          ->setGridOption('viewrecords',true)
          ->setGridOption('altRows',true)
          ->setGridOption('altclass','myAltRowClass')
          ->setGridOption('postData',array('filters'=>"{'groupOp':'AND','rules':[{'field':'Host.Status','op':'eq','data': $status }]}"))
          ->setGridOption('cmTemplate', array('resizable' => false))
          ->setGridEvent('gridComplete', 'hostFunctionAfterGridComplete')
          ->setGridEvent('loadComplete', 'dynamicHeight')

          ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => false, 'view' => true, 'refresh' => false))

          ->setNavigatorOptions('add', array('closeAfterAdd' => true, 'addCaption' => 'Add Host '.$stationName, 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))

          ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')

          ->setNavigatorOptions('edit', array('closeAfterEdit' => true, 'editCaption' => 'Edit Host Details', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))
          ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('view', array('closeAfterEdit' => true, 'caption' => 'View Host Info', 'resize' => false, 'drag' => false, 'left' => '400', 'top' => '80'))
          ->setFileProperty('FileName', 'Hosts')

          ->addColumn(array('index' => 'HostId', 'hidden' => true, 'editable' => true, 'key' => true))

          ->addColumn(array('label' => 'Host Type','index' => 'MasterOrPlayer', 'editable' => true, 'editrules' => array('required' => true), 'edittype'=>'select','editoptions' => array( 'value' => 'M:Master;P:Player;B:Both'), 'align' => 'left', 'width' => '50px', 'search' => true, 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => ":All;M:M;P:P;B:B")))

          ->addColumn(array('label' => 'Station', 'index'=>'StationName', 'width' => '80px', 'editable' => true, 'edittype' => $editTypeStation,'editoptions' => array( 'value' => $DetailsOfStation, 'readonly' => 'readonly'), 'align' => 'left',))          


          ->addColumn(array('label' => 'Bay Code', 'index' => 'BayLocationCode', 'hidden' => false,'editable' => false, 'viewable' => true, 'align' => 'left', 'width' => '60px','editrules' => array('edithidden' => true)))


          ->addColumn(array('label' => 'Bay Code', 'index' => 'BayId', 'hidden' => true,'editable' => true, 'viewable' => false, 'editrules' => array('edithidden' => true), 'edittype' => $editTypeBayLocation,'editoptions' => array( 'value' => $listOfBays, 'readonly' => 'readonly'), 'align' => 'left', 'width' => '80px'))


          ->addColumn(array('label' => 'BayLocationBayId', 'index' => 'BayLocationBayId', 'hidden' => true,'editable' => true, 'viewable' => false, 'editrules' => array('edithidden' => true), 'edittype' => $editTypeBayLocation,'editoptions' => array( 'value' => $bayID, 'readonly' => 'readonly'), 'align' => 'left', 'width' => '80px',))

          ->addColumn(array('label' => 'Bay Code', 'index' => 'BayLocation', 'hidden' => true,'editable' => $editable, 'viewable' => false, 'editrules' => array('edithidden' => true), 'edittype' => $editTypeBayLocation,'editoptions' => array( 'value' => $listOfBays, 'readonly' => 'readonly'), 'align' => 'left', 'width' => '80px'))

          ->addColumn(array('label' => 'App', 'index'=>'App', 'width' => '80px', 'editable' => true, 'edittype' => 'text', 'editoptions' => array( 'value' => $apps, 'readonly' => 'readonly'), 'align' => 'left', 'width' => '60px'))

          ->addColumn(array('label' => 'Host Name', 'index'=>'HostName', 'editable' => true, 'editrules' => array('required' => true), 'width' => '120px', 'align' => 'left'))
          
          ->addColumn(array('label' => 'Master Name','index' => 'HostMasterName', 'editable' => true, 'editrules' => array('edithidden' => true), 'width' => '120px', 'align' => 'left'))

          ->addColumn(array('label' => 'Counter Type','index' => 'HostCounterType', 'hidden' => true, 'editable' => true, 'editrules' => array('required' => true, 'edithidden' => true), 'edittype'=>'select','editoptions' => array( 'value' => 'PRS:PRS;UTS:UTS'), 'align' => 'left','width' => '70px', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => ":All;PRS:PRS;UTS:UTS")))
         
          ->addColumn(array('label' => 'Counter Number','index' => 'CounterNumber', 'editable' => true, 'editrules' => array('required' => true),'align' => 'center', 'width' => '60px'))

          ->addColumn(array('label' => 'Branch', 'index'=>'BranchName', 'align' => 'left', 'width' => '70px'))

          ->addColumn(array('label' => 'Region', 'index'=>'RegionName', 'align' => 'left','width' => '70px'))

          ->addColumn(array('label' => 'Start Time','index' => 'Host.StartTime', 'editable' => true, 'editrules' => array('required' => true, 'align' => 'left', 'edithidden' => false), 'width' => '60px', 'editoptions' => array( 'value' => $startTime)))

          ->addColumn(array('label' => 'End Time','index' => 'Host.EndTIme',  'editable' => true, 'align' => 'center', 'width' => '60px','editrules' => array('required' => true, 'edithidden' => false), 'editoptions' => array( 'value' => $endTime)))

          ->addColumn(array('label' => 'Status','index' => 'Host.Status', 'hidden' => false, 'align' => 'left','edittype'=>'select','editoptions' => array( 'value' => 'A:Active;I:Inactive'),'editable' => true, 'editrules' => array('required' => true, 'edithidden' => false), 'width' => '70px', 'formatter' => 'statusDisplay', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => $searchOption, 'defaultValue' => $search)))

          ->setNavigatorEvent('edit','beforeShowForm', 'editBefore')

          ->setNavigatorEvent('add','beforeShowForm', 'addBefore')
          ->setNavigatorEvent('add','afterShowForm', 'addAfter')
          ->setNavigatorEvent('edit','onInitializeForm', 'intializeBefore')
          ->setNavigatorEvent('view','beforeShowForm', 'viewBaylocationInfo')
          ->renderGrid();
        !!}
        @endcan
 </div>

 <!-- Role based Permissions -->
  <script type="text/javascript">
    $('#add_jqGridHost').hide();
    $('#edit_jqGridHost').hide();
    $('#view_jqGridHost').hide();
      @can('Update Host')
        $('#edit_jqGridHost').show();
      @endcan
      @can('Show Host')
        $('#view_jqGridHost').show();
      @endcan 
    </script>
@stop
