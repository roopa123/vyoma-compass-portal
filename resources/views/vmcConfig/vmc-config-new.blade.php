@extends('layouts.vmclayouts')

@section('title')
    VMC Config
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
    @include('layouts.validationAlert')
    <div>
        {{--@include('vmcConfig.vmc-config-new')--}}
        <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
        <div class="adminPanel form-group NameRole rolesTable vmcConfigFormBlock" ng-controller="vmcConfigNew">
            <form name="vmcConfigForm" class="gray-bgr-vmcConfig" novalidate="true">
                <div class="col-md-3 col-xs-12 padding0"> <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-left vmcConfig">
                        <li class="poClass" ng-class="vmcTabService.getTabClass(1)" ng-click="vmcTabService.setActiveTab(1)"><a>Basic</a></li>
                        <li class="poClass" ng-class="vmcTabService.getTabClass(2)" ng-click="vmcTabService.setActiveTab(2)"><a>Network</a></li>
                        <li class="poClass" ng-class="vmcTabService.getTabClass(3)" ng-click="vmcTabService.setActiveTab(3)"><a>Display</a></li>
                        <li class="poClass" ng-class="vmcTabService.getTabClass(4)" ng-click="vmcTabService.setActiveTab(4)"><a>Schedules</a></li>
                        <li class="poClass" ng-class="vmcTabService.getTabClass(5)" ng-click="vmcTabService.setActiveTab(5)"><a>Versions and Status</a></li>
                        <li class="poClass" ng-class="vmcTabService.getTabClass(6)" ng-click="vmcTabService.setActiveTab(6)"><a>Others</a></li>
                    </ul>
                </div>
                <div class="col-md-9 col-xs-12 padding0 vmcTabData">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div ng-class="vmcTabService.getTabPaneClass(1)" id="basic">
                            @include('vmcConfig.partials.vmc-basic-config')
                        </div>
                        <div ng-class="vmcTabService.getTabPaneClass(2)" id="network">
                            @include('vmcConfig.partials.vmc-network-config')
                        </div>
                        <div ng-class="vmcTabService.getTabPaneClass(3)" id="display">
                            @include('vmcConfig.partials.vmc-display-config')
                        </div>
                        <div ng-class="vmcTabService.getTabPaneClass(4)" id="schedule">
                            @include('vmcConfig.partials.vmc-schedule-config')
                        </div>
                        <div ng-class="vmcTabService.getTabPaneClass(5)" id="version">
                            @include('vmcConfig.partials.vmc-version-and-status-config')
                        </div>
                        <div ng-class="vmcTabService.getTabPaneClass(6)" id="others">
                            @include('vmcConfig.partials.vmc-others-config')
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection



