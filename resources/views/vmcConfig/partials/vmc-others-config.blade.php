<div ng-form="vmcOthersConfig" novalidate>
    <div class="col-md-12 col-xs-12 min-height-70">
        <div id="monitEvent" class="col-md-6 col-xs-12 min-height-70">
            <span class="color-blue">ENABLE MONIT</span>
            <div class="pull-right" data-ng-init="vmcConfig.monitEvent=true">
                <switch id="enabled" name="monitEvent" data-ng-model="vmcConfig.monitEvent" on="Enable" off="Disable" class="green"></switch>
            </div>
        </div>
        <div id="monitQueue" class="col-md-6 col-xs-12 min-height-70">
            <span class="color-blue">MONIT QUEUE</span>
            <div class="pull-right" data-ng-init="vmcConfig.monitQueue=true">
                <switch id="enabled" name="monitQueue" data-ng-model="vmcConfig.monitQueue" on="Active" off="Inactive" class="green"></switch>
            </div>
        </div>
        <div id="debugMode" class="col-md-6 col-xs-12 min-height-70">
            <span class="color-blue">DEBUG MODE</span>
            <div class="pull-right" data-ng-init="vmcConfig.debugMode=true">
                <switch id="enabled" name="debugMode" data-ng-model="vmcConfig.debugMode" on="Active" off="Inactive" class="green"></switch>
            </div>
        </div>
        <div id="sysstat" class="col-md-6 col-xs-12 min-height-70">
            <span class="color-blue">SYSSTAT</span>
            <div class="pull-right" data-ng-init="vmcConfig.sysstat=true">
                <switch id="enabled" name="systat" data-ng-model="vmcConfig.systat" on="Active" off="Inactive" class="green"></switch>
            </div>
        </div>
        <div id="thankMessage" class="col-md-6 col-xs-12 min-height-70" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'">
            <span class="color-blue">THANK MESSAGE</span>
            <div class="pull-right" data-ng-init="vmcConfig.thankMessage=true">
                <switch id="enabled" name="thankMessage" data-ng-model="vmcConfig.thankMessage" on="Active" off="Inactive" class="green"></switch>
            </div>
        </div>
        <div id="certificationStatus" class="col-md-6 col-xs-12 min-height-70"
             ng-class="{ 'has-error' : submitted && vmcOthersConfig.certificationStatus.$error.required || vmcOthersConfig.certificationStatus.$invalid && !vmcOthersConfig.certificationStatus.$pristine }">
            <label class="color-blue smallFont12 col-md-6 col-xs-12 padding-0">CERTIFICATION STATUS</label>
            <select class="form-control col-md-6 col-xs-12 pull-right width-50" name="certificationStatus" data-ng-model="vmcConfig.certificationStatus" ng-options="certificationStatus.id as certificationStatus.type for certificationStatus in certificationStatusArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcOthersConfig.certificationStatus.$error.required || vmcOthersConfig.certificationStatus.$invalid && !vmcOthersConfig.certificationStatus.$pristine" class="help-block">Host Type is Required.</span>
        </div>

        <div id="ticketData" class="col-md-6 col-xs-12 min-height-70" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'">
            <span class="color-blue">SHOW TICKET DATA</span>
            <div class="pull-right btn-group" data-ng-init="vmcConfig.ticketData='Left'">
                <span class="btn btn-default yesToggle" data-ng-model="vmcConfig.ticketData" uib-btn-radio="'Left'">YES</span>
                <span class="btn btn-default noToggle" data-ng-model="vmcConfig.ticketData" uib-btn-radio="'Right'">NO</span>
            </div>
        </div>
        <div id="davpCertified" class="col-md-6 col-xs-12 min-height-70">
            <span class="color-blue">DAVP CERTIFIED</span>
            <div class="pull-right btn-group" data-ng-init="vmcConfig.davpCertified='Left'">
                <span class="btn btn-default yesToggle" data-ng-model="vmcConfig.davpCertified" uib-btn-radio="'Left'">YES</span>
                <span class="btn btn-default noToggle" data-ng-model="vmcConfig.davpCertified" uib-btn-radio="'Right'">NO</span>
            </div>
        </div>

        <div id="reInitializeHost" class="col-md-6 col-xs-12 min-height-80">
            <span class="color-blue">REINITIALIZE HOST</span>
            <div class="pull-right btn-group" data-ng-init="vmcConfig.reInitialize='Left'">
                <span class="btn btn-default" ng-disabled="true" data-ng-model="vmcConfig.reInitialize" uib-btn-radio="'Left'">YES</span>
                <span class="btn btn-default" ng-disabled="true" data-ng-model="vmcConfig.reInitialize" uib-btn-radio="'Right'">NO</span>
            </div>
        </div>
        <div id="placeHolder" class="col-md-6 col-xs-12 min-height-80"></div>


        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0 bottomBtns">
            <div id="backBtn" class="col-md-6 col-xs-12 pull-left">
                <button id="submitButton" class="btn btn-default submitButton pull-left col-md-6 col-xs-12"
                        type="button" ng-click="vmcTabService.goToNextAndPrevTab(5)">Back</button>
            </div>
            <div id="nextBtn" class="col-md-6 col-xs-12 pull-right">
                <button id="submitButton" class="btn btn-default submitButton pull-right col-md-6 col-xs-12"
                        type="submit" ng-click="submitted=true">Submit</button>
            </div>
        </div>
    </div>
</div>