<div ng-form="vmcScheduleConfig" novalidate>
    <div class="col-md-12 col-xs-12 min-height-80">

        <div id="enablePlayLogUpdate" class="col-md-12 col-xs-12 min-height-50" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'">
            <span class="color-blue col-md-6 col-xs-12">ENABLE PLAY LOG</span>
            <div class="col-md-6 col-xs-12" data-ng-init="vmcConfig.enablePlayLogUpdate=true">
                <switch id="enabled"  name="enablePlayLogUpdate" data-ng-model="vmcConfig.enablePlayLogUpdate" on="Enable" off="Disable" class="green"></switch>
            </div>
        </div>
        <div id="playLogUpdateSchedule" class="col-md-12 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'"
             ng-class="{ 'has-error' : submitted && vmcScheduleConfig.playLogUpdateSchedule.$error.required ||
              vmcScheduleConfig.playLogUpdateSchedule.$invalid && !vmcScheduleConfig.playLogUpdateSchedule.$pristine }">
            <label class="color-blue smallFont12 col-md-6 col-xs-12" for="playLogUpdateSchedule">Play Log Update Schedule</label>
            <div class="col-md-6 col-xs-12">
                <select class="form-control" name="playLogUpdateSchedule" data-ng-model="vmcConfig.playLogUpdateSchedule"
                        ng-disabled="vmcConfig.enablePlayLogUpdate==false"
                        ng-options="playLogUpdateSchedule for playLogUpdateSchedule in playLogUpdateScheduleArr" required>
                    <option value="">select</option>
                </select>
                <span ng-show="submitted && vmcScheduleConfig.playLogUpdateSchedule.$error.required ||
                vmcScheduleConfig.playLogUpdateSchedule.$invalid && !vmcScheduleConfig.playLogUpdateSchedule.$pristine" class="help-block">Play Log Update Schedule is Required.</span>
            </div>
        </div>

        <div id="enablePlayLogUpdate" class="col-md-12 col-xs-12 min-height-50" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'">
            <span class="color-blue col-md-6 col-xs-12">ENABLE TICKET LOG</span>
            <div class="col-md-6 col-xs-12" data-ng-init="vmcConfig.enableTicketLogUpdate=true">
                <switch id="enabled"  name="enableTicketLogUpdate" data-ng-model="vmcConfig.enableTicketLogUpdate" on="Enable" off="Disable" class="green"></switch>
            </div>
        </div>
        <div id="ticketLogUpdateSchedule" class="col-md-12 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'"
             ng-class="{ 'has-error' : submitted && vmcScheduleConfig.ticketLogUpdateSchedule.$error.required || vmcScheduleConfig.ticketLogUpdateSchedule.$invalid && !vmcScheduleConfig.ticketLogUpdateSchedule.$pristine }">
            <label class="color-blue smallFont12 col-md-6 col-xs-12" for="ticketLogUpdateSchedule">Ticket Log Update Schedule</label>
            <div class="col-md-6 col-xs-12">
                <select class="form-control col-md-6 col-xs-12" name="ticketLogUpdateSchedule" data-ng-model="vmcConfig.ticketLogUpdateSchedule"
                        ng-disabled="vmcConfig.enableTicketLogUpdate==false" ng-options="ticketLogUpdateSchedule for ticketLogUpdateSchedule in ticketLogUpdateScheduleArr" required>
                    <option value="">select</option>
                </select>
                <span ng-show="submitted && vmcScheduleConfig.ticketLogUpdateSchedule.$error.required  || vmcScheduleConfig.ticketLogUpdateSchedule.$invalid && !vmcScheduleConfig.ticketLogUpdateSchedule.$pristine" class="help-block">Ticket Log Update Schedule is Required.</span>
            </div>
        </div>

        <div id="enableContentUpdate" class="col-md-12 col-xs-12 min-height-50" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'">
            <span class="color-blue col-md-6 col-xs-12">ENABLE CONTENT</span>
            <div class="col-md-6 col-xs-12" data-ng-init="vmcConfig.enableContentUpdate=true">
                <switch id="enabled"  name="enableContentUpdate" data-ng-model="vmcConfig.enableContentUpdate" on="Enable" off="Disable" class="green"></switch>
            </div>
        </div>
        <div id="contentUpdateSchedule" class="col-md-12 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'"
             ng-class="{ 'has-error' : submitted && vmcScheduleConfig.contentUpdateSchedule.$error.required ||
              vmcScheduleConfig.contentUpdateSchedule.$invalid && !vmcScheduleConfig.contentUpdateSchedule.$pristine }">
            <label class="color-blue smallFont12 col-md-6 col-xs-12" for="contentUpdateSchedule">Content Update Schedule</label>
            <div class="col-md-6 col-xs-12">
                <select class="form-control col-md-6 col-xs-12" name="contentUpdateSchedule" data-ng-model="vmcConfig.contentUpdateSchedule"
                        ng-disabled="vmcConfig.enableContentUpdate==false" ng-options="contentUpdateSchedule for contentUpdateSchedule in contentUpdateScheduleArr" required>
                    <option value="">select</option>
                </select>
                <span ng-show="submitted && vmcScheduleConfig.contentUpdateSchedule.$error.required ||
                 vmcScheduleConfig.contentUpdateSchedule.$invalid && !vmcScheduleConfig.contentUpdateSchedule.$pristine" class="help-block">Content Update Schedule is Required.</span>
            </div>
        </div>

        <div id="enableScreenShotUpdate" class="col-md-12 col-xs-12 min-height-50">
            <span class="color-blue col-md-6 col-xs-12">ENABLE SCREENSHOT</span>
            <div class="col-md-6 col-xs-12" data-ng-init="vmcConfig.enableScreenShot=true">
                <switch id="enabled" name="enableScreenShot" data-ng-model="vmcConfig.enableScreenShot" on="Enable" off="Disable" class="green"></switch>
            </div>
        </div>
        <div id="screenShotSchedule" class="col-md-12 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcScheduleConfig.screenShotSchedule.$error.required ||
              vmcScheduleConfig.screenShotSchedule.$invalid && !vmcScheduleConfig.screenShotSchedule.$pristine }">
            <label class="color-blue smallFont12 col-md-6 col-xs-12" for="screenShotSchedule">Screen Shot Schedule</label>
            <div class="col-md-6 col-xs-12">
                <select class="form-control col-md-6 col-xs-12" name="screenShotSchedule" data-ng-model="vmcConfig.screenShotSchedule"
                        ng-disabled="vmcConfig.enableScreenShot==false" ng-options="screenShotSchedule for screenShotSchedule in screenShotScheduleArr" required>
                    <option value="">select</option>
                </select>
                <span ng-show="submitted && vmcScheduleConfig.screenShotSchedule.$error.required ||
                 vmcScheduleConfig.screenShotSchedule.$invalid && !vmcScheduleConfig.screenShotSchedule.$pristine" class="help-block">Screen Shot Schedule is Required.</span>
            </div>
        </div>

        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0 bottomBtns">
            <div id="backBtn" class="col-md-6 col-xs-12 pull-left">
                <button id="submitButton" class="btn btn-default submitButton pull-left col-md-6 col-xs-12"
                        type="button" ng-click="vmcTabService.goToNextAndPrevTab(3)">Back</button>
            </div>
            <div id="nextBtn" class="col-md-6 col-xs-12 pull-right">
                <button id="submitButton" class="btn btn-default submitButton pull-right col-md-6 col-xs-12"
                        type="button" ng-click="submitted=true;vmcTabService.goToNextAndPrevTab(5)" ng-disabled="vmcScheduleConfig.$invalid">Next</button>
            </div>
        </div>
    </div>
</div>