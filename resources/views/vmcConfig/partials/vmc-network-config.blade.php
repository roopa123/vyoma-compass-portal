<div ng-form="vmcNetworkConfig" novalidate>
    <div class="col-md-12 col-xs-12 min-height-80">

        <div id="enableRouterReboot" class="col-md-12 col-xs-12 min-height-50" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'">
            <span class="color-blue col-md-6 col-xs-12 padding0">ENABLE ROUTER REBOOT</span>
            <div class="col-md-6 col-xs-12" data-ng-init="vmcConfig.enableRouterReboot=true">
                <switch id="enabled" name="enableRouterReboot" data-ng-model="vmcConfig.enableRouterReboot" on="Enable" off="Disable" class="green"></switch>
            </div>
        </div>

        <div id="routerRebootSchedule" class="col-md-12 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'">
            <span class="color-blue">Router Reboot Schedule</span>
            <div class="col-md-6 col-xs-12 pull-right padding-right-0"
                 ng-class="{ 'has-error' : submitted && vmcNetworkConfig.routerRebootSchedule.$error.required ||
                 vmcNetworkConfig.routerRebootSchedule.$invalid && !vmcNetworkConfig.routerRebootSchedule.$pristine }">
                <select class="form-control" name="routerRebootSchedule" data-ng-model="vmcConfig.routerRebootSchedule"
                        ng-options="routerRebootSchedule.id as routerRebootSchedule.name for routerRebootSchedule in routerRebootScheduleArr"
                        ng-disabled="vmcConfig.enableRouterReboot==false" required>
                    <option value="">select</option>
                </select>
                <span ng-show="submitted && vmcNetworkConfig.routerRebootSchedule.$error.required  ||
                 vmcNetworkConfig.routerRebootSchedule.$invalid && !vmcNetworkConfig.routerRebootSchedule.$pristine" class="help-block">Router Reboot Schedule is Required.</span>
            </div>
        </div>


        <div id="localIPAddress" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcNetworkConfig.localIPAddress.$error.required ||
             vmcNetworkConfig.localIPAddress.$invalid && !vmcNetworkConfig.localIPAddress.$pristine }">
            <label class="color-blue smallFont12" for="localIPAddress">LOCAL IP ADDRESS</label>
            <input type="text" class="form-control" name="localIPAddress" data-ng-model="vmcConfig.localIPAddress" required/>
            <span ng-show="submitted && vmcNetworkConfig.localIPAddress.$error.required
             || vmcNetworkConfig.localIPAddress.$invalid && !vmcNetworkConfig.localIPAddress.$pristine" class="help-block">Local IP Address is Required.</span>
        </div>

        <div id="masterLocalIPAddress" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'" class="col-md-6 col-xs-12 min-height-80"
             data-ng-init="vmcConfig.masterLocalIPAddress='192.168.1.250'"
             ng-class="{ 'has-error' : submitted && vmcNetworkConfig.masterLocalIPAddress.$error.required ||
              vmcNetworkConfig.masterLocalIPAddress.$invalid && !vmcNetworkConfig.masterLocalIPAddress.$pristine }">
            <label class="color-blue smallFont12" for="masterLocalIPAddress">MASTER LOCAL IP ADDRESS</label>
            <input type="text" name="masterLocalIPAddress" ng-disabled="vmcConfig.hostType!='2'" class="form-control" data-ng-model="vmcConfig.masterLocalIPAddress" required/>
            <span ng-show="submitted && vmcNetworkConfig.masterLocalIPAddress.$error.required ||
             vmcNetworkConfig.masterLocalIPAddress.$invalid && !vmcNetworkConfig.masterLocalIPAddress.$pristine" class="help-block">Master Local IP Address is Required.</span>
        </div>

        <div id="providerIPAddress" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='M'"
             ng-class="{ 'has-error' : submitted && vmcNetworkConfig.providerIPAddress.$error.required ||
              vmcNetworkConfig.providerIPAddress.$invalid && !vmcNetworkConfig.providerIPAddress.$pristine}">
            <label class="color-blue smallFont12" for="providerIPAddress">PROVIDER IP ADDRESS</label>
            <input type="text" name="providerIPAddress" class="form-control" data-ng-model="vmcConfig.providerIPAddress" ng-disabled="true" required/>
            <span ng-show="submitted && vmcNetworkConfig.providerIPAddress.$error.required ||
             vmcNetworkConfig.providerIPAddress.$invalid && !vmcNetworkConfig.providerIPAddress.$pristine" class="help-block">Master Public IP Address is Required.</span>
        </div>

        <div id="macAddress" class="col-md-6 col-xs-12 min-height-80">
            <label class="color-blue smallFont12" for="macAddress">MAC ADDRESS</label>
            <input type="text" name="macAddress" class="form-control" data-ng-model="vmcConfig.macAddress" ng-disabled="true"/>
        </div>

        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0" ng-if="vmcConfig.enableRouterReboot">
            <div id="routerUserName" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'"
                 ng-class="{ 'has-error' : submitted && vmcNetworkConfig.routerUserName.$error.required ||
                 vmcNetworkConfig.routerUserName.$invalid && !vmcNetworkConfig.routerUserName.$pristine }">
                <label class="color-blue smallFont12" for="routerUserName">ROUTER USER NAME</label>
                <input type="text" name="routerUserName" class="form-control" data-ng-model="vmcConfig.routerUserName" required/>
                <span ng-show="submitted && vmcNetworkConfig.routerUserName.$error.required ||
                vmcNetworkConfig.routerUserName.$invalid && !vmcNetworkConfig.routerUserName.$pristine" class="help-block">Router User Name is Required.</span>
            </div>
            <div id="routerPassword" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='M' || vmcConfig.hostType=='B'"
                 ng-class="{ 'has-error' : submitted && vmcNetworkConfig.routerPassword.$error.required ||
                 vmcNetworkConfig.routerPassword.$invalid && !vmcNetworkConfig.routerPassword.$pristine }">
                <label class="color-blue smallFont12" for="routerPassword">ROUTER PASSWORD</label>
                <input type="password" name="routerPassword" class="form-control" data-ng-model="vmcConfig.routerPassword" required/>
                <span ng-show="submitted && vmcNetworkConfig.routerPassword.$error.required ||
                 vmcNetworkConfig.routerPassword.$invalid && !vmcNetworkConfig.routerPassword.$pristine" class="help-block">Router Password is Required.</span>
            </div>
        </div>

        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0 bottomBtns">
            <div id="backBtn" class="col-md-6 col-xs-12 pull-left">
                <button id="submitButton" class="btn btn-default submitButton pull-left col-md-6 col-xs-12"
                        type="button" ng-click="vmcTabService.goToNextAndPrevTab(1)">Back</button>
            </div>
            <div id="nextBtn" class="col-md-6 col-xs-12 pull-right">
                <button id="submitButton" class="btn btn-default submitButton pull-right col-md-6 col-xs-12"
                        type="button" ng-click="submitted=true;vmcTabService.goToNextAndPrevTab(3)"
                        ng-disabled="vmcNetworkConfig.$invalid">Next</button>
            </div>
        </div>
    </div>
</div>