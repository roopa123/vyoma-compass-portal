<div ng-form="vmcDisplayConfig" novalidate>
    <div class="col-md-12 col-xs-12 min-height-80">

        <div id="dualDisplay" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'">
            <span class="color-blue">DUAL DISPLAY</span>
            <div class="pull-right btn-group" data-ng-init="vmcConfig.dualDisplay='Left'">
                <span class="btn btn-default yesToggle" data-ng-model="vmcConfig.dualDisplay" uib-btn-radio="'Left'">YES</span>
                <span class="btn btn-default noToggle" data-ng-model="vmcConfig.dualDisplay" uib-btn-radio="'Right'">NO</span>
            </div>
        </div>

        <div id="screenRatio" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'">
            <span class="color-blue col-md-6 col-xs-12 padding-0">SCREEN RATIO</span>
            <select class="form-control  col-md-6 col-xs-12 pull-right width-50" name="screenRatio" data-ng-model="vmcConfig.screenRatio" required>
                <option value="">select</option>
            </select>
        </div>

        <div id="primaryDisplayHostName" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'"
             ng-class="{ 'has-error' : submitted && vmcDisplayConfig.primaryDisplayHostName.$error.required ||
              vmcDisplayConfig.primaryDisplayHostName.$invalid && !vmcDisplayConfig.primaryDisplayHostName.$pristine}">
            <label class="color-blue smallFont12" for="host">PRIMARY DISPLAY HOST NAME</label>
            <input type="text" name="primaryDisplayHostName" class="form-control" data-ng-model="vmcConfig.primaryDisplayHostName" required/>
            <span ng-show="submitted && vmcDisplayConfig.primaryDisplayHostName.$error.required ||
             vmcDisplayConfig.primaryDisplayHostName.$invalid && !vmcDisplayConfig.primaryDisplayHostName.$pristine"
                  class="help-block">Primary Display Host is Required.</span>
        </div>

        <div id="playerWindowSize" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcDisplayConfig.playerWindowSize.$error.required ||
              vmcDisplayConfig.playerWindowSize.$invalid && !vmcDisplayConfig.playerWindowSize.$pristine }">
            <label class="color-blue smallFont12" for="playerWindowSize">PLAYER WINDOW SIZE</label>
            <select class="form-control" name="playerWindowSize" data-ng-model="vmcConfig.playerWindowSize"
                    ng-options="playerWindowSize.name for playerWindowSize in playerWindowSizeArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcDisplayConfig.playerWindowSize.$error.required ||
             vmcDisplayConfig.playerWindowSize.$invalid && !vmcDisplayConfig.playerWindowSize.$pristine" class="help-block">Player Window Size is Required.</span>
        </div>

        <div id="hdmiMode" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcDisplayConfig.hdmiMode.$error.required ||
              vmcDisplayConfig.hdmiMode.$invalid && !vmcDisplayConfig.hdmiMode.$pristine }">
            <label class="color-blue smallFont12" for="hdmiMode">HDMI MODE</label>
            <select class="form-control" name="hdmiMode" data-ng-model="vmcConfig.hdmiMode"
                    ng-options="hdmiMode for hdmiMode in hdmiModeArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcDisplayConfig.hdmiMode.$error.required ||
             vmcDisplayConfig.hdmiMode.$invalid && !vmcDisplayConfig.hdmiMode.$pristine" class="help-block">HDMI Mode is Required.</span>
        </div>

        <div id="displayType" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'"
             ng-class="{ 'has-error' : submitted && vmcDisplayConfig.displayType.$error.required ||
              vmcDisplayConfig.displayType.$invalid && !vmcDisplayConfig.displayType.$pristine }">
            <label class="color-blue smallFont12" for="displayType">DISPLAY TYPE</label>
            <select class="form-control" name="displayType" data-ng-model="vmcConfig.displayType"
                    ng-options="displayType for displayType in displayTypeArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcDisplayConfig.displayType.$error.required ||
             vmcDisplayConfig.displayType.$invalid && !vmcDisplayConfig.displayType.$pristine" class="help-block">Display Type is Required.</span>
        </div>

        <div id="displaySize" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType=='P' || vmcConfig.hostType=='B'"
             ng-class="{ 'has-error' : submitted && vmcDisplayConfig.displaySize.$error.required ||
              vmcDisplayConfig.displaySize.$invalid && !vmcDisplayConfig.displaySize.$pristine }">
            <label class="color-blue smallFont12" for="displaySize">DISPLAY SIZE</label>
            <select class="form-control" name="displaySize" data-ng-model="vmcConfig.displaySize"
                    ng-options="displaySize for displaySize in displaySizeArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcDisplayConfig.displaySize.$error.required ||
             vmcDisplayConfig.displaySize.$invalid && !vmcDisplayConfig.displaySize.$pristine" class="help-block">Display Size is Required.</span>
        </div>

        <div id="cableType" class="col-md-6 col-xs-12 min-height-100"
             ng-class="{ 'has-error' : submitted && vmcDisplayConfig.cableType.$error.required ||
              vmcDisplayConfig.cableType.$invalid && !vmcDisplayConfig.cableType.$pristine }">
            <label class="color-blue smallFont12" for="cableType">CABLE TYPE</label>
            <select class="form-control" name="cableType" data-ng-model="vmcConfig.cableType"
                    ng-options="cableType for cableType in cableTypeArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcDisplayConfig.cableType.$error.required ||
             vmcDisplayConfig.cableType.$invalid && !vmcDisplayConfig.cableType.$pristine" class="help-block">Cable Type is Required.</span>
        </div>

        <div id="screenStartTimeHeading" class="col-md-12 col-xs-12 min-height-50">
            <div class="color-blue">SCREEN START TIME :</div>
        </div>

        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0">
            <div id="vmcStartTime" class="col-md-4 col-xs-12 min-height-80">
                <label class="color-blue" for="start">START</label>
                <span class="form-control" ng-click="startTimeActive=!startTimeActive">@{{vmcConfig.startTime | date:'HH:mm a'}}</span>
                <div ng-show="startTimeActive"
                     uib-timepicker
                     ng-model="vmcConfig.startTime"
                     show-meridian="ismeridian"
                     ng-change="changed()">
                </div>
            </div>
            <div id="vmcOverTime" class="col-md-4 col-xs-12 min-height-80">
                <label class="color-blue" for="over">END</label>
                <span class="form-control" ng-click="endTimeActive=!endTimeActive">@{{vmcConfig.endTime | date:'HH:mm a'}}</span>
                <div ng-show="endTimeActive"
                     uib-timepicker
                     ng-model="vmcConfig.endTime"
                     show-meridian="ismeridian"
                     ng-change="changed()">
                </div>
            </div>
            <div id="vmcEndTime" class="col-md-4 col-xs-12 min-height-80" ng-show="vmcConfig.counterType=='PRS'">
                <label class="color-blue" for="end">END(SUNDAY)</label>
                <span class="form-control" ng-click="endTime_sun_Active=!endTime_sun_Active">@{{vmcConfig.endTime_sun | date:'HH:mm a'}}</span>
                <div ng-show="endTime_sun_Active"
                     uib-timepicker
                     ng-model="vmcConfig.endTime_sun"
                     show-meridian="ismeridian"
                     ng-change="changed()">
                </div>
            </div>
        </div>

        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0 bottomBtns">
            <div id="backBtn" class="col-md-6 col-xs-12 pull-left">
                <button id="submitButton" class="btn btn-default submitButton pull-left col-md-6 col-xs-12"
                        type="button" ng-click="vmcTabService.goToNextAndPrevTab(2)">Back</button>
            </div>
            <div id="nextBtn" class="col-md-6 col-xs-12 pull-right">
                <button id="submitButton" class="btn btn-default submitButton pull-right col-md-6 col-xs-12"
                        type="button" ng-click="submitted=true;vmcTabService.goToNextAndPrevTab(4)" ng-disabled="vmcDisplayConfig.$invalid">Next</button>
            </div>
        </div>
    </div>
</div>