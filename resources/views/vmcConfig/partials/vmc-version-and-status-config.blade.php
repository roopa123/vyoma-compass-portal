<div ng-form="vmcVersionAndStatusConfig" novalidate>
    <div class="col-md-12 col-xs-12 min-height-80">
        <div id="status" class="col-md-6 col-xs-12 min-height-80">
            <span class="color-blue">STATUS</span>
            <div class="pull-right" data-ng-init="vmcConfig.status=true">
                <switch id="enabled" name="status"  data-ng-model="vmcConfig.status" on="Active" off="Inactive" class="green"></switch>
            </div>
        </div>
        <div id="placeHolder" class="col-md-6 col-xs-12 min-height-80"></div>

        <div id="online" class="col-md-6 col-xs-12 min-height-80">
            <span class="color-blue">ONLINE</span>
            <div class="pull-right btn-group" data-ng-init="vmcConfig.online='Left'">
                <span class="btn btn-default yesToggle" ng-disabled="true" data-ng-model="vmcConfig.online" uib-btn-radio="'Left'">YES</span>
                <span class="btn btn-default noToggle" ng-disabled="true" data-ng-model="vmcConfig.online" uib-btn-radio="'Right'">NO</span>
            </div>
        </div>
        <div id="registered" class="col-md-6 col-xs-12 min-height-80">
            <span class="color-blue">REGISTERED</span>
            <div class="pull-right btn-group" data-ng-init="vmcConfig.registered='Left'">
                <span class="btn btn-default yesToggle" ng-disabled="true" data-ng-model="vmcConfig.registered" uib-btn-radio="'Left'">YES</span>
                <span class="btn btn-default noToggle" ng-disabled="true" data-ng-model="vmcConfig.registered" uib-btn-radio="'Right'">NO</span>
            </div>
        </div>

        <div id="currentAppVersion" class="col-md-6 col-xs-12 min-height-80">
            <label class="color-blue smallFont12" for="currentAppVersion">CURRENT APP VERSION</label>
            <input type="text" class="form-control" name="currentAppVersion" data-ng-model="vmcConfig.currentAppVersion" ng-disabled="true"/>
        </div>
        <div id="newAppVersion" class="col-md-6 col-xs-12 min-height-80">
            <label class="color-blue smallFont12" for="newAppVersion">NEW APP VERSION</label>
            <input class="form-control" name="newAppVersion" data-ng-model="vmcConfig.newAppVersion" ng-disabled="true"/>
        </div>


        <div id="currentConfigVersion" class="col-md-6 col-xs-12 min-height-80">
            <label class="color-blue smallFont12" for="currentConfigVersion">CURRENT CONFIG VERSION</label>
            <input type="text" name="currentConfigVersion" class="form-control" data-ng-model="vmcConfig.currentConfigVersion" ng-disabled="true"/>
        </div>
        <div id="newConfigVersion" class="col-md-6 col-xs-12 min-height-80">
            <label class="color-blue smallFont12" for="newConfigVersion">NEW CONFIG VERSION</label>
            <input type="text" name="newConfigVersion" class="form-control" data-ng-model="vmcConfig.newConfigVersion" ng-disabled="true"/>
        </div>

        <div id="currentContentVersion" class="col-md-6 col-xs-12 min-height-80">
            <label class="color-blue smallFont12" for="currentContentVersion">CURRENT CONTENT VERSION</label>
            <input type="text" name="currentContentVersion" class="form-control" data-ng-model="vmcConfig.currentContentVersion" ng-disabled="true"/>
        </div>
        <div id="newContentVersion" class="col-md-6 col-xs-12 min-height-80">
            <label class="color-blue smallFont12" for="newContentVersion">NEW CONTENT VERSION</label>
            <input type="text" name="newContentVersion" class="form-control" data-ng-model="vmcConfig.newContentVersion" ng-disabled="true"/>
        </div>


        <div id="piModel" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcVersionAndStatusConfig.piModel.$error.required || vmcVersionAndStatusConfig.piModel.$invalid && !vmcVersionAndStatusConfig.piModel.$pristine }">
            <label class="color-blue smallFont12" for="start">PI MODEL</label>
            <select class="form-control" name="piModel" data-ng-model="vmcConfig.piModel" ng-options="piModel for piModel in piModelArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcVersionAndStatusConfig.piModel.$error.required || vmcVersionAndStatusConfig.piModel.$invalid && !vmcVersionAndStatusConfig.piModel.$pristine" class="help-block">PI Version is Required.</span>
        </div>
        <div id="placeHolder" class="col-md-6 col-xs-12 min-height-80"></div>

        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0 bottomBtns">
            <div id="backBtn" class="col-md-6 col-xs-12 pull-left">
                <button id="submitButton" class="btn btn-default submitButton pull-left col-md-6 col-xs-12"
                        type="button" ng-click="vmcTabService.goToNextAndPrevTab(3)">Back</button>
            </div>
            <div id="nextBtn" class="col-md-6 col-xs-12 pull-right">
                <button id="submitButton" class="btn btn-default submitButton pull-right col-md-6 col-xs-12"
                        type="button" ng-click="submitted=true;vmcTabService.goToNextAndPrevTab(6)" ng-disabled="vmcVersionAndStatusConfig.$invalid">Next</button>
            </div>
        </div>
    </div>
</div>
