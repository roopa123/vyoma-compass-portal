{{-- new chaabges based on the latest wireframes changes --}}
<div ng-form="vmcBasicConfig">
    <div class="col-md-12 col-xs-12 min-height-80">

        <div id="hostType" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.hostType.$error.required ||
             vmcBasicConfig.hostType.$invalid && !vmcBasicConfig.hostType.$pristine }">
            <label class="color-blue smallFont12">HOST TYPE</label>
            <select class="form-control" name="hostType" data-ng-model="vmcConfig.hostType" ng-change="getMasterName('{{$arrToBind->BayId}}')"
                    ng-options="hostType.id as hostType.type for hostType in hostTypeArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcBasicConfig.hostType.$error.required ||
            vmcBasicConfig.hostType.$invalid && !vmcBasicConfig.hostType.$pristine" class="help-block">Host Type is Required.</span>
        </div>

        <div id="vmcStation" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.station.$error.required ||
              vmcBasicConfig.station.$invalid && !vmcBasicConfig.station.$pristine }">
            <label class="color-blue smallFont12" for="station">STATION</label>
            <input type="hidden" name="stationId" class="form-control" value="{{$arrToBind->StationId}}" />
            <input type="text" name="station" class="form-control" value="{{$arrToBind->StationName}}" disabled="disabled" />
            <span ng-show="submitted && vmcBasicConfig.station.$error.required ||
             vmcBasicConfig.station.$invalid && !vmcBasicConfig.station.$pristine" class="help-block">Station is Required.</span>
        </div>

        <div id="bayCode" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.bayCode.$error.required ||
              vmcBasicConfig.bayCode.$invalid && !vmcBasicConfig.bayCode.$pristine }">
            <label class="color-blue smallFont12" for="bayCode">BAY CODE</label>
            <input type="hidden" name="bayId" class="form-control" value="{{$arrToBind->BayId}}"/>
            <input type="text" name="bayLocation" class="form-control" value="{{$arrToBind->BayLocationCode}}" disabled="disabled" />
            <span ng-show="submitted && vmcBasicConfig.bayCode.$error.required ||
             vmcBasicConfig.bayCode.$invalid && !vmcBasicConfig.bayCode.$pristine" class="help-block">Bay Code is Required.</span>
        </div>

        <div id="app" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.app.$error.required ||
              vmcBasicConfig.app.$invalid && !vmcBasicConfig.app.$pristine }">
            <label class="color-blue smallFont12">APP</label>
            <input type="text" name="app" class="form-control" value="{{$arrToBind->App}}" disabled="disabled" />
            <span ng-show="submitted && vmcBasicConfig.app.$error.required ||
             vmcBasicConfig.app.$invalid && !vmcBasicConfig.app.$pristine" class="help-block">App is Required.</span>
        </div>

        <div id="host" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.host.$error.required ||
             vmcBasicConfig.host.$invalid && !vmcBasicConfig.host.$pristine }">
            <label class="color-blue smallFont12" for="host">HOST</label>
            <input type="text" name="host" class="form-control" data-ng-model="vmcConfig.host" required/>
            <span ng-show="submitted && vmcBasicConfig.host.$error.required ||
             vmcBasicConfig.host.$invalid && !vmcBasicConfig.host.$pristine" class="help-block">Host is Required.</span>
        </div>

        <div id="master" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.hostType == 'P'"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.master.$error.required ||
              vmcBasicConfig.master.$invalid && !vmcBasicConfig.master.$pristine }">
            <label class="color-blue smallFont12" for="master">MASTER</label>
            <input type="text" name="master" class="form-control" data-ng-model="vmcConfig.master" required/>
            <span ng-show="submitted && vmcBasicConfig.master.$error.required ||
             vmcBasicConfig.master.$invalid && !vmcBasicConfig.master.$pristine" class="help-block">Master is Required.</span>
        </div>

        <div id="counterType" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.counterType.$error.required ||
              vmcBasicConfig.counterType.$invalid && !vmcBasicConfig.counterType.$pristine }">
            <label class="color-blue smallFont12" for="counterType">COUNTER TYPE</label>
            <select class="form-control" name="counterType" data-ng-model="vmcConfig.counterType"
                    ng-options="counterType.id as counterType.name for counterType in counterTypeArr" required>
                <option value="">select</option>
            </select>
            <span ng-show="submitted && vmcBasicConfig.counterType.$error.required ||
             vmcBasicConfig.counterType.$invalid && !vmcBasicConfig.counterType.$pristine" class="help-block">Counter Type is Required.</span>
        </div>

        <div id="counterNumber" class="col-md-6 col-xs-12 min-height-80"
             ng-class="{ 'has-error' : submitted && vmcBasicConfig.counterNumber.$error.required ||
              vmcBasicConfig.counterNumber.$invalid && !vmcBasicConfig.counterNumber.$pristine }">
            <label class="color-blue smallFont12 col-md-6 col-xs-12 padding-0" for="counterNumber">COUNTER NUMBER</label>
            <input type="number" ng-maxlength="3" name="counterNumber"
                   class="form-control col-md-6 col-xs-12 pull-right width-50" ng-model="vmcConfig.counterNumber" required/>
            <span ng-show="submitted && vmcBasicConfig.counterNumber.$error.required ||
             vmcBasicConfig.counterNumber.$invalid && !vmcBasicConfig.counterNumber.$pristine" class="help-block">Counter Number is Required.</span>
        </div>


        <div id="utsDumb" class="col-md-6 col-xs-12 min-height-80" ng-show="vmcConfig.counterType=='UTS'">
            <span class="color-blue">UTS DUMB</span>
            <div class="pull-right btn-group" data-ng-init="vmcConfig.utsDumb='Left'">
                <span class="btn btn-default yesToggle" data-ng-model="vmcConfig.utsDumb" uib-btn-radio="'Left'">YES</span>
                <span class="btn btn-default noToggle" data-ng-model="vmcConfig.utsDumb" uib-btn-radio="'Right'">NO</span>
            </div>
        </div>

        <div id="autoSleepMode" class="col-md-6 col-xs-12 min-height-80">
            <span class="color-blue">AUTO SLEEP MODE</span>
            <div class="pull-right" data-ng-init="vmcConfig.autoSleepMode=true">
                <switch id="enabled" name="autoSleepMeeting" data-ng-model="vmcConfig.autoSleepMode" on="Enable" off="Disable" class="green"></switch>
            </div>
        </div>

        <?php
        $startTimeInHrs = date('H', strtotime($arrToBind->StartTime));
        $startTimeInMins = date('i', strtotime($arrToBind->StartTime));
        $endTimeInHrs = date('H', strtotime($arrToBind->EndTIme));
        $endTimeInMins = date('i', strtotime($arrToBind->EndTIme));
        ?>
        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0">
            <div id="counterStartTime" class="col-md-6 col-xs-12 min-height-80">
                <label class="color-blue smallFont12" for="bayCode">COUNTER START TIME</label>
            <span class="form-control" ng-click="counterStartTimeActive=!counterStartTimeActive"
                  ng-init="vmcConfig.counterStartTime=formatTimeToBindInTimePickers('{{$startTimeInHrs}}','{{$startTimeInMins}}')">@{{vmcConfig.counterStartTime | date:'HH:mm a'}}</span>
                <div ng-show="counterStartTimeActive" ng-init="counterStartTimeChanged()"
                     uib-timepicker
                     ng-model="vmcConfig.counterStartTime"
                     show-meridian="ismeridian"
                     ng-change="counterStartTimeChanged()">
                </div>
            </div>
            <div id="counterEndTime" class="col-md-6 col-xs-12  min-height-80">
                <label class="color-blue smallFont12" for="bayCode">COUNTER END TIME</label>
            <span class="form-control" ng-click="counterEndTimeActive=!counterEndTimeActive"
                  ng-init="vmcConfig.counterEndTime=formatTimeToBindInTimePickers('{{$endTimeInHrs}}','{{$endTimeInMins}}')">@{{vmcConfig.counterEndTime | date:'HH:mm a'}}</span>
                <div ng-show="counterEndTimeActive" ng-init="counterEndTimeChanged()"
                     uib-timepicker
                     ng-model="vmcConfig.counterEndTime"
                     show-meridian="ismeridian"
                     ng-change="counterEndTimeChanged()">
                </div>
            </div>
        </div>

        <div class="display-block overflow-auto col-xs-12 col-md-12 padding-0 bottomBtns">
            <div id="nextBtn" class="col-md-6 col-xs-12 pull-right">
                <button id="submitButton" class="btn btn-default submitButton pull-right col-md-6 col-xs-12"
                        type="button" ng-click="vmcTabService.goToNextAndPrevTab(2)" ng-disabled="vmcBasicConfig.$invalid">Next</button>
            </div>
        </div>
    </div>
</div>
