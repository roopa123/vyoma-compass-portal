@extends('layouts.ccctemplate')

@section('title')
    Advertiser
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
    @include('layouts.validationAlert')
    @can('Show Advertiser')
    <div class="adminPanel form-group NameRole rolesTable">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <span class="text-center color-gray gridCaption customerGridCaption">{{"List of Advertisers "}} </span>
        <!-- Check permission to create Advertiser -->
        @can('Create Advertiser')
        <a href="{{url('advertiser/create')}}" class="btn btn-success customerGridAdd"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;&nbsp;&nbsp;Add Advertiser</a>
        @endcan
        <div class="col-md-12 col-xs-12 padding0">
            <table id="AdvertiserGrid" class="col-md-12 col-xs-12 table-bordered table-hover table-condensed table-responsive table"><tr></tr></table>
            <div id="AdvertiserGridPager"></div>
            <script type="text/javascript">

                /*Delete advertiser row through ajax call*/
                function deleteRow(id){
                    message = "Are you sure, you want to delete the Advertiser?";
                    bootbox.confirm(message, function(result) {
                        if(result == true) {
                            $.ajax({type: "DELETE", url: "advertiser/"+id,
                                success: function(results){
                                    if(results == 'success'){
                                        displayMessage("Advertiser deleted successfully", true, 'AdvertiserGrid');
                                    }else if(results == 'error'){
                                        displayMessage("Cannot delete Advertiser, its associated with Creatives", false, 'AdvertiserGrid');
                                    }
                                    //location.reload();
                                }});
                        }
                    });
                }

                $.ajax({
                    type: "POST",
                    url: "/advertiser-grid",
                    dataType:"json",
                    success: function (dataJson) {
                        var oldFrom = $.jgrid.from,
                                lastSelected;

                        $.jgrid.from = function (source, initalQuery) {
                            var result = oldFrom.call(this, source, initalQuery),
                                    old_select = result.select;
                            result.select = function (f) {
                                lastSelected = old_select.call(this, f);
                                return lastSelected;
                            };
                            return result;
                        };
                        jQuery("#AdvertiserGrid").jqGrid(
                                {
                                    "datatype": "local",
                                    "mtype": "POST",
                                    "treeGridModel": "adjacency",
                                    "data": dataJson.rows,
                                    "loadonce": true,
                                    "rowNum": 10,
                                    "rownumbers": true, "autowidth": true, "height": 'auto',
                                    "viewrecords": true,
                                    "pager": '#AdvertiserGridPager',
                                    "recreateFilter": true,
                                    "altRows":true,
                                    "altclass":'myAltRowClass',
                                    "colModel": [
                                        {
                                            "label": "Advertiser Id",
                                            "index": "AdvertiserId",
                                            "name": "AdvertiserId",
                                            "search": false,
                                            "key": true,
                                            'hidden': true
                                        },
                                        {
                                            "label": "Advertiser Name",
                                            "index": "AdvertiserName",
                                            "name": "AdvertiserName",
                                            "search": true,
                                            "resizable":false
                                        },
                                        {"label": "Sector", "index": "Sector", "name": "Sector", "search": true,"resizable":false},

                                        {"label": "Paying Client", "index": "IsPaying", "name": "IsPaying", "search": true,"resizable":false, formatter: function(cellvalue, options, rowobject){
                                            if(cellvalue=='Y'){
                                                cellvalue='Yes';
                                            }
                                            if(cellvalue=='N'){
                                                cellvalue='No';
                                            }
                                            return cellvalue;
                                        }},
                                        {"label": "Industry Name", "index": "IndustryName", "name": "IndustryName", "search": true,"resizable":false,
                                            formatter:nullFormatter},

                                        {"label": "Status", "index": "Status", "name": "Status", "search": true,"resizable":false, formatter: function(cellvalue, options, rowobject){
                                            if(cellvalue=='A'){
                                                cellvalue='Active';
                                            }
                                            if(cellvalue=='D'){
                                                cellvalue='Delete';
                                            }
                                            return cellvalue;
                                        }},
                                        {"label": "Actions", "name": "Actions", "formatter": "actions","resizable":false}],
                                    beforeRequest: function () {
                                        modifySearchingFilter.call(this, ',');
                                    },
                                    loadComplete: function () {
                                        /* replacing industry column null values with 'Blank' */
                                        $.each($("#AdvertiserGrid").jqGrid('getGridParam','data'),function(index,value){
                                            if(value.IndustryName==null){
                                                value.IndustryName='(Blank)';
                                            }
                                            if(value.Status=='A'){
                                                value.Status='Active';
                                            }
                                            if(value.Status=='D'){
                                                value.Status='Delete';
                                            }
                                            if(value.IsPaying=='Y'){
                                                value.IsPaying='Yes';
                                            }
                                            if(value.IsPaying=='N'){
                                                value.IsPaying='No';
                                            }
                                        });

                                        this.p.lastSelected = lastSelected; // set this.p.lastSelected
                                        highlightFilteredColumns.call(this); //highlightFilteredColumns

                                        var iCol = columnIndexForGridBasedOnTableIdAndColumnName('#AdvertiserGrid', 'Actions');
                                        var iColStatus = columnIndexForGridBasedOnTableIdAndColumnName('#AdvertiserGrid', 'Status');
                                        var id, message;
                                        var statusValue=[];

                                        /*** status col***/
                                        $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColStatus + 1) + ")")
                                                .each(function(value,key) {
                                                    statusValue.push($(this).text());
                                                });

                                        $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iCol + 1) + ")").each(function (value,key) {
                                            if(statusValue[value] == 'Delete') {
                                                $("<div>", {
                                                    mouseover: function (e) {
                                                        id = $(e.target).closest("tr.jqgrow").attr("id");
                                                        $(this).children('.update-link').attr("href", "/advertiser/" + id + "/edit");
                                                    }}).css({cursor: "pointer"})
                                                        .addClass("col-md-12 col-xs-12 wrapperForButtons")

                                                        /*Check permission to update advertiser*/
                                                        @can('Update Advertiser')
                                                          .append('<a href="" class="col-md-5 col-xs-12 update-link">Edit</a>')
                                                @endcan
                                                .prependTo($(this).children("div"));
                                            }else{
                                                $("<div>", {
                                                    mouseover: function (e) {
                                                        id = $(e.target).closest("tr.jqgrow").attr("id");
                                                        $(this).children('.update-link').attr("href", "/advertiser/" + id + "/edit");
                                                        $(this).children('.delete-link').attr("onclick", "deleteRow('" + id + "')");
                                                    }}).css({cursor: "pointer"})
                                                        .addClass("col-md-12 col-xs-12 wrapperForButtons")

                                                        /*Check permission to update advertiser*/
                                                        @can('Update Advertiser')
                                                          .append('<a href="" class="col-md-5 col-xs-12 update-link">Edit</a>')
                                                @endcan
                                                /*Check permission to Delete advertiser*/
                                                @can('Delete Advertiser')
                                                .append('<span class="col-md-5 col-xs-12 delete-link">Delete</span>')
                                                @endcan
                                                .prependTo($(this).children("div"));
                                            }
                                        });

                                        /* hiding actions column based on the roles and permissions of users*/
                                        if(statusValue.length > 0){
                                            if ( $('#AdvertiserGrid .wrapperForButtons').children().length <= 0 ) {
                                                $(this).jqGrid('hideCol',["Actions"]);
                                            }
                                        }

                                        /* not showing blank values in the grid */
                                        $("#AdvertiserGrid td[title='(Blank)']").css("color","transparent");

                                        setMultiSearchSelect('AdvertiserName', jQuery("#AdvertiserGrid"), $("#AdvertiserGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('Sector', jQuery("#AdvertiserGrid"), $("#AdvertiserGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('IsPaying', jQuery("#AdvertiserGrid"), $("#AdvertiserGrid").jqGrid('getGridParam','data'));
//                                        setMultiSearchSelectForStatus('IsPaying', jQuery("#AdvertiserGrid"), $("#AdvertiserGrid").jqGrid('getGridParam','data'), ":All;Y:Yes;N:No");
                                        setMultiSearchSelect('IndustryName', jQuery("#AdvertiserGrid"), $("#AdvertiserGrid").jqGrid('getGridParam','data'));
//                                        setMultiSearchSelectForStatus('Status', jQuery("#AdvertiserGrid"), $("#AdvertiserGrid").jqGrid('getGridParam','data'), ":All;A:Active;D:Delete");
                                        setMultiSearchSelect('Status', jQuery("#AdvertiserGrid"), $("#AdvertiserGrid").jqGrid('getGridParam','data'));

                                       /* changeStatusValue('AdvertiserGrid', 'AdvertiserGrid_Status');
                                        changeIspayingValue('AdvertiserGrid', 'AdvertiserGrid_IsPaying');*/

                                        /* set max height for all grids based on the window height */
                                        var height = $(window).height();
                                        $('.ui-jqgrid .ui-jqgrid-bdiv').css("max-height",height/2+"px");
                                    }
                                });

                        jQuery("#AdvertiserGrid").jqGrid('navGrid', '#AdvertiserGridPager', {edit: false, add: false, del: false, search: false}, {}, {}, {}, {
                            multipleSearch: true,
                            multipleGroup: true,
                            recreateFilter: true,
                            overlay: 0
                        });

                        $('#jqgh_AdvertiserGrid_AdvertiserName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                        $('#jqgh_AdvertiserGrid_Sector').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                        $('#jqgh_AdvertiserGrid_IsPaying').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                        $('#jqgh_AdvertiserGrid_IndustryName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                        $('#jqgh_AdvertiserGrid_Status').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');

                        jQuery("#AdvertiserGrid").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch: myDefaultSearch});

                        $('#AdvertiserGrid_AdvertiserName .vSymbol').on('click',function(){
                            $('.ui-search-toolbar #gsh_AdvertiserGrid_AdvertiserName button.ui-multiselect.ui-widget.ui-state-default').click();
                            removeUnWantedSearchOptions(0,'AdvertiserName',$('#AdvertiserGrid').jqGrid('getGridParam', 'lastSelected'),'AdvertiserGrid');
                            return false;
                        });
                        $('#AdvertiserGrid_Sector .vSymbol').on('click',function(){
                            $('.ui-search-toolbar #gsh_AdvertiserGrid_Sector button.ui-multiselect.ui-widget.ui-state-default').click();
                            removeUnWantedSearchOptions(1,'Sector',$('#AdvertiserGrid').jqGrid('getGridParam', 'lastSelected'),'AdvertiserGrid');
                            return false;
                        });
                        $('#AdvertiserGrid_IsPaying .vSymbol').on('click',function(){
                            $('.ui-search-toolbar #gsh_AdvertiserGrid_IsPaying button.ui-multiselect.ui-widget.ui-state-default').click();
                            removeUnWantedSearchOptions(2,'IsPaying',$('#AdvertiserGrid').jqGrid('getGridParam', 'lastSelected'),'AdvertiserGrid');
                            return false;
                        });
                        $('#AdvertiserGrid_IndustryName .vSymbol').on('click',function(){
                            $('.ui-search-toolbar #gsh_AdvertiserGrid_IndustryName button.ui-multiselect.ui-widget.ui-state-default').click();
                            removeUnWantedSearchOptions(3,'IndustryName',$('#AdvertiserGrid').jqGrid('getGridParam', 'lastSelected'),'AdvertiserGrid');
                            return false;
                        });
                        $('#AdvertiserGrid_Status .vSymbol').on('click',function(){
                            $('.ui-search-toolbar #gsh_AdvertiserGrid_Status button.ui-multiselect.ui-widget.ui-state-default').click();
                            removeUnWantedSearchOptions(4,'Status',$('#AdvertiserGrid').jqGrid('getGridParam', 'lastSelected'),'AdvertiserGrid');
                            return false;
                        });
                    }
                });
            </script>
        </div>
        @endcan
    </div>
@stop



