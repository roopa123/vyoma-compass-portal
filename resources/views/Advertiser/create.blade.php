@extends('layouts.ccctemplate')
@section('title')
    Industry
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="logo"></div>
@can('Create Advertiser')
<div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
  <div class="industryContainer col-md-8 col-xs-12">
    <form method="POST" action="/advertiser">
      {!! Form::hidden('redirect', $redirect, ['id'=>'redirect', 'class'=>'form-control']) !!}

      {!! Form::hidden('redirectCampaign', $redirectId, ['id'=>'redirectCampaign', 'class'=>'form-control']) !!}

      {!! Form::hidden('redirectPOCampaign', $redirectPoCamId, ['id'=>'redirectPOCampaign', 'class'=>'form-control']) !!}

      <fieldset>
        {!! csrf_field() !!}
        <h4 class="col-md-12 col-xs-12 color-gray">{{"Add Advertiser "}}&nbsp;:</h4>
        <div class="clearfix fieldsWrapper col-md-8 col-xs-12">
            <div class="min-height-100 col-md-6 col-xs-12">
              {!! Form::label('advertiserName','Advertiser Name',array('class' => 'color-blue')) !!}
              {!! Form::text('advertiserName', Input::old('advertiserName'), ['class'=>'form-control']) !!}
            </div>

            <div class="min-height-100 col-md-6 col-xs-12">
              {!! Form::label('Sector','Sector',array('class' => 'color-blue')) !!}
              {!! Form::select('sector', $sectors, Input::old('sector') , ['class'=>'form-control', 'id' => 'sector']) !!}
            </div>            

            <div class="min-height-100 col-md-6 col-xs-12">
              <label class="color-blue" for="payingWrapper">Paying</label>
              <div id="payingWrapper" class="col-md-12 col-xs-12 padding-0">
                <a class="payingYesBtn cursor-pointer col-md-6 col-xs-12">
                  <input name="ispaying" type="text" class="IsPaying_yes" readonly value="Y"/>
                    Yes
                </a>
                <a class="payingNoBtn payingActive cursor-pointer col-md-6 col-xs-12">
                  <input name="ispaying" type="text" class="IsPaying_no" readonly value="N"/>
                    No
                </a>
              </div>
            </div>

            <div class="min-height-100 col-md-6 col-xs-12" id='industryDiv'>
              {!! Form::label('IndustryId','Industry',array('class' => 'color-blue')) !!}
              <select name='industry' id='industry' class='form-control'>
                <option></option>
              </select>
            </div>

            <div class="col-md-12 col-xs-12">
              <div class="col-md-6 col-xs-12 text-center">
              @if(Request::segment(3) == 'redirect' && Request::segment(4) == 'creative')
                  <a href="{{{ URL::route('creative.create') }}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
                @elseif(Request::segment(3) == 'redirect' && Request::segment(4) == 'purchaseorder')
                  <a href="{{{ URL::route('purchaseorder.create') }}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
                @elseif(Request::segment(3) == 'redirect' && Request::segment(4) == 'purchaseorderAdd')                
                    <a href="/purchaseorder/{{Request::segment(5)}}/add/campaign" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
                @elseif(Request::segment(3) == 'redirect' && Request::segment(4) == 'purchaseorderEdit')                
                    <a href="/purchaseorder/{{Request::segment(5)}}/edit/campaign/{{Request::segment(8)}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
                @elseif(Request::segment(3) == 'redirect' && Request::segment(4) == 'pocampaignClone')                
                    <a href="/purchaseorder/pocampaign/{{Request::segment(5)}}/clone/campaign/{{Request::segment(8)}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
                @elseif(Request::segment(3) == 'redirect' && Request::segment(4) == 'creativeEdit' && !is_null(Request::segment(5)))                
                    <a href="/creative/{{Request::segment(5)}}/edit" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
                @else                               
                  <a href="{{{ URL::route('advertiser.index') }}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
                @endif                  
                
              </div>
              <div class="col-md-6 col-xs-12 text-center">
                <button class="btn btn-default IndustrySubmitBtn col-md-12 col-xs-12 formSubmitUpdatePreloader">Submit</button>
              </div>
            </div>
          </div>
      </fieldset>
    </form>
    </div>
    @endcan

    <script type="text/javascript">
      $("#industryDiv").hide();
      $(document).on("change","#sector", function (e) {
        $("#industryDiv").show();

        if($("#sector").val() == 'Private')
        {
          industryDropdown($("#sector").val(), false);
        }else{
          $("#industryDiv").hide();
        }

      });

      $(document).ready(function() {
        $("#industryDiv").show();
        if($("#sector").val() == 'Private')
        {
          industryDropdown($("#sector").val(), false);
        }else{
          $("#industryDiv").hide();
        }
      });

      /** Toggle button functionality**/
      $('.payingYesBtn').on('click', function(){
          $('.payingNoBtn').removeClass('payingActive').addClass('color-gray');
          $('.payingYesBtn').addClass('payingActive');
          $('.payingNoBtn .IsPaying_no').removeAttr("name");
          $('.payingYesBtn .IsPaying_yes').attr("name","ispaying");

      });
      $('.payingNoBtn').on('click', function(){
          $('.payingYesBtn').removeClass('payingActive').addClass('color-gray');
          $('.payingNoBtn').addClass('payingActive');
          $('.payingYesBtn .IsPaying_yes').removeAttr("name");
          $('.payingNoBtn .IsPaying_no').attr("name","ispaying");

      });

      
    </script>
@stop
