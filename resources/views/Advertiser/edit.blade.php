@extends('layouts.ccctemplate')

@section('title')
    Advertiser
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
  <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
  <div class="row">
    <div class="logo"></div>
    @can('Update Advertiser')
      <div class="industryContainer col-md-8 col-xs-12">
        {!! Form::open(['method' => 'PATCH','route'=>['advertiser.update',$Advertiser->AdvertiserId]]) !!}
          {!! csrf_field() !!}
        <h4 class="col-md-12 col-xs-12 color-gray">{{"Edit Advertiser "}}&nbsp;:</h4>
        <div class="clearfix fieldsWrapper col-md-8 col-xs-12">
          <div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('advertiserName','Advertiser Name',array('class' => 'color-blue')) !!}
            {!! Form::hidden('advertiserId', Input::old('advertiserId', $Advertiser->AdvertiserId),['class'=>'form-control']) !!}
            {!! Form::text('advertiserName', Input::old('advertiserName', $Advertiser->AdvertiserName),['class'=>'form-control']) !!}
          </div>
          <div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('Sector','Sector',array('class' => 'color-blue')) !!}
            {!! Form::select('sector', $sectors, $Advertiser->Sector , ['class'=>'form-control', 'id' => 'sector']) !!}
          </div>
          {{--<div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('IsPaying','IsPaying',array('class' => 'color-blue')) !!}
            {!! Form::select('ispaying', $isPayings, $Advertiser->IsPaying , ['class'=>'form-control']) !!}
          </div>--}}
          <div class="min-height-100 col-md-6 col-xs-12">
            <label class="color-blue" for="payingWrapper">Paying</label>
            <div id="payingWrapper" class="col-md-12 col-xs-12 padding-0">

            @if($Advertiser->IsPaying=="Y")
              <a class="payingYesBtn payingActive cursor-pointer col-md-6 col-xs-12">
                  <input name="ispaying" type="text" class="IsPaying_yes" readonly value="Y"/>
                  Yes
              </a>
            @else
              <a class="payingYesBtn cursor-pointer col-md-6 col-xs-12">
                  <input type="text" class="IsPaying_yes" readonly value="Y"/>
                  Yes
              </a>
            @endif

            @if($Advertiser->IsPaying=="N")
              <a class="payingNoBtn payingActive cursor-pointer col-md-6 col-xs-12">
                <input name="ispaying" type="text" class="IsPaying_no" readonly value="N"/>
                  No
              </a>
            @else
              <a class="payingNoBtn cursor-pointer col-md-6 col-xs-12">
                <input type="text" class="IsPaying_no" readonly value="N"/>
                  No
              </a>
            @endif
            </div>
          </div>
          <div class="min-height-100 col-md-6 col-xs-12" id='industryDiv'>
            {!! Form::label('IndustryId','Industry',array('class' => 'color-blue')) !!}
            {!! Form::select('industry', $industrys, $Advertiser->IndustryId, ['class'=>'form-control']) !!}
          </div>

          <div class="min-height-100 col-md-6 col-xs-12">
            {!! Form::label('Status','Status',array('class' => 'color-blue')) !!}
            {!! Form::select('status', $status, $Advertiser->Status , ['class'=>'form-control']) !!}
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="col-md-6 col-xs-12 text-center">
              <a href="{{{ URL::route('advertiser.index') }}}" class="btn btn-default cancelBtn color-blue col-md-12 col-xs-12">Cancel</a>
            </div>
            <div class="col-md-6 col-xs-12 text-center">
              <button class="btn btn-default IndustrySubmitBtn col-md-12 col-xs-12 formSubmitUpdatePreloader">Update</button>
            </div>
          </div>
        </form>
      @endcan
    </div>
  </div>
  </div>
  <script type="text/javascript">
    if($("#sector").val() == 'Private')
    {
        $("#industryDiv").show();
    }else{
        $("#industryDiv").hide();
    }

    $(document).on("change","#sector", function (e) {
        $("#industryDiv").show();

        if($("#sector").val() == 'Private')
        {
            industryDropdown($("#sector").val(), $("#sector").val());
        }else{
            $("#industryDiv").hide();
        }

    });

    $('.payingYesBtn').on('click', function(){
      $('.payingNoBtn').removeClass('payingActive').addClass('color-gray');
      $('.payingYesBtn').addClass('payingActive');
      $('.payingNoBtn .IsPaying_no').removeAttr("name");
      $('.payingYesBtn .IsPaying_yes').attr("name","ispaying");
    });
    $('.payingNoBtn').on('click', function(){
      $('.payingYesBtn').removeClass('payingActive').addClass('color-gray');
      $('.payingNoBtn').addClass('payingActive');
      $('.payingYesBtn .IsPaying_yes').removeAttr("name");
      $('.payingNoBtn .IsPaying_no').attr("name","ispaying");
  });


   
  </script>
@stop
