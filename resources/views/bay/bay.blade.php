@extends('layouts.apptemplate')

@section('title')
    Bay
@endsection

@section('menu')
@include('layouts.menu')
@endsection

@section('content')
@include('layouts.validationAlert')
<div class="">
<script>



$(document).ready(function(){
  // Prevent Actions Column Export
  bayAddHostExportFix();
  $('.ui-icon-refresh').click(function() { location.reload();}) ;
});

function setPermission() {
  accessPermissionHost();
}

function editSelectForDropdown() {
  var starttime = $('#StartTime').val();
  var endtime = $('#EndTIme').val();
  if(starttime.length == 0 && endtime.length==0) {
    $('#StartTime').val('00:00');
    $('#EndTIme').val('00:00');
   }
   else {     
       $('#StartTime').val(starttime);
       $('#EndTIme').val(endtime);
       $('#StartTime').datetimepicker({
           format: 'HH:mm',
                  pickDate: false,
                  pickSeconds: false,
                  pick12HourFormat: false
        });
        $('#EndTIme').datetimepicker({
            format: 'HH:mm',
                   pickDate: false,
                   pickSeconds: false,
                   pick12HourFormat: false

        });
   }
   dropdownSort();  
}

function dropdownSort(){      
      // Get the deafult selected values
      stationval = $("#StationName").val();
      bayval = $("#BayLocationDesc").val();

      // Sort options in alphabetical order
      $('#StationName').sortOptions(stationval);
      $('#BayLocationDesc').sortOptions(bayval);   
  }

function addBefore(formId){
   $('#StartTime').val('00:00');
   $('#EndTIme').val('00:00');
   

   $('#StartTime').datetimepicker({
                        format: 'HH:mm',
                               pickDate: false,
                               pickSeconds: false,
                               pick12HourFormat: false

                     });
                     $('#EndTIme').datetimepicker({
                         format: 'HH:mm',
                                pickDate: false,
                                pickSeconds: false,
                                pick12HourFormat: false
                     });
    //Sorting for Station name //
    $('#StationName').append($("#StationName option").sort(function(a, b) {
        var getFirstValueText = $(a).text().toUpperCase(), getSecondValueText = $(b).text().toUpperCase();
        return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
    })).prepend("<option value='' selected>Select</option>");
    // Sorting for Bay Location //
    $('#BayLocationDesc').append($("#BayLocationDesc option").sort(function(a, b) {
        var getFirstValueText = $(a).text().toUpperCase(), getSecondValueText = $(b).text().toUpperCase();
        return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
    })).prepend("<option value='' selected>Select</option>");
    $('#App').prepend("<option value='' selected>Select</option>");
}


      var myfilter = { groupOp: "AND", rules: []};
      var ftype ='' , fvalue = '', url = '';

        //@if(isset($status))
          //ftype = 'Bay.Status';
          //fvalue = '{{$status}}';
          //myfilter.rules.push({field:ftype,op:"eq",data:fvalue});
        //@endif
        @if(isset($searchStation))
          ftype = 'StationName';
          fvalue = "{{$searchStation}}";
          myfilter.rules.push({field:ftype,op:"eq",data:fvalue});
        @endif
</script>
    <div class="">
        <table id="jqGridBay" class="table "></table>
    </div>
    @can('Show Bay')
      {!!
        GridRender::setGridId("jqGridBay")
          ->enablefilterToolbar()
          ->setGridOption('url',$buildUrl)
          ->setGridOption('editurl',URL::to('/bay/crud'))
          ->setGridOption('rowNum', 50)
          ->setGridOption('rownumbers', true)
          ->setGridOption('autowidth', true)
          ->setGridOption('height', 500)
          ->setGridOption('caption','List of Bays')
          ->setGridOption('viewrecords',true)
           ->setGridOption('altRows',true)
          ->setGridOption('altclass','myAltRowClass')

          ->setGridOption('postData',array("filters"=>"{'groupOp':'AND','rules':$searchJsonStr}"))
          
          ->setGridOption('cmTemplate', array('resizable' => false))
          ->setGridEvent('gridComplete', 'addButtonHostEvent')
          ->setGridEvent('loadComplete', 'disableInactiveAddHosts')

          ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => false, 'view' => true, 'refresh' => false, 'search' => true))
          ->setNavigatorOptions('add', array('closeAfterAdd' => true, 'addCaption' => 'Add Bay', 'resize' => false, 'drag' => false, 'left' => '387', 'top' => '193'))
          ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('edit', array('closeAfterEdit' => true, 'editCaption' => 'Edit Bay Details', 'resize' => false, 'drag' => false, 'left' => '387', 'top' => '193'))
          ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
          ->setNavigatorOptions('view', array('closeAfterEdit' => true, 'caption' => 'View Bay Info', 'resize' => false, 'drag' => false, 'left' => '387', 'top' => '193'))
          ->setNavigatorOptions('del', array('closeAfterEdit' => true, 'caption' => 'Delete Bay Info', 'resize' => false, 'drag' => false, 'left' => '502', 'top' => '223'))
          ->setNavigatorEvent('del', 'afterSubmit', 'afterSubmitEvent')

          ->setFileProperty('FileName', 'Bays')
          ->addColumn(array('index' => 'BayId', 'hidden' => true, 'editable' => true, 'key' => true))

          ->addColumn(array('label' => 'Station Name', 'index'=>'StationName', 'editable' => true, 'editrules' => array('required' => true), 'edittype'=>'select','align' => 'left','editoptions' => array( 'value' => $listOfStations ), 'search' => true, 'searchoptions' => array( 'defaultValue' => $searchStation)))

          ->addColumn(array('label' => 'Bay Location Name','index' => 'BayLocationDesc', 'align' => 'left','editable' => true,'edittype'=>'select','editoptions' => array( 'value' => $listOfBayLocations ), 'formatter' => 'showlink',
             'formatoptions' => array('baseLinkUrl'=>'/host', 'idName' => 'baylocation'), 'searchoptions' => array( 'defaultValue' => $searchBay)))

          ->addColumn(array('label' => 'App','index' => 'App', 'editable' => true, 'editrules' => array('required' => true), 'align' => 'left', 'edittype'=>'select','editoptions' => array( 'value' => 'DDIS:DDIS;VMC:VMC'), 'searchoptions' => array('defaultValue' => $searchApp)))

          ->addColumn(array('label' => 'Start Time','hidden' => true,'index' => 'StartTime', 'editable' => true, 'editrules' => array('required' => true, 'align' => 'left', 'edithidden' => true), 'width' => '60px'))

          ->addColumn(array('label' => 'End TIme','hidden' => true,'index' => 'EndTIme',  'editable' => true, 'align' => 'center', 'width' => '60px','editrules' => array('required' => true, 'edithidden' => true)))

          ->addColumn(array('label' => 'Status','index' => 'Bay.Status', 'hidden' => false, 'edittype'=>'select', 'align' => 'left','editoptions' => array( 'value' => 'A:Active;I:Inactive'),'editable' => true, 'editrules' => array('required' => true, 'edithidden' => false), 'width' => '90px', 'formatter' => 'statusDisplay', 'stype'=> 'select', 'searchoptions' => array('sopt'=> ['eq', 'ne'], 'value' => $searchOption, 'defaultValue' => $searchStatus)))    

          ->addColumn(array('label' => 'Actions', 'name'=>'addHost', 'sortable' => false,'editable' => false,
            'search'=>false, 'formatter' => 'showlink',
             'formatoptions' => array('baseLinkUrl'=>'/host', 'addParam' => '/redirect', 'idName' => 'bay')))
          ->setNavigatorEvent('add','beforeShowForm', 'addBefore')
          ->setNavigatorEvent('edit','beforeShowForm', 'editSelectForDropdown')
          ->setNavigatorEvent('view','beforeShowForm', 'setPermission')         
          ->renderGrid();
        !!}
      @endcan
 </div>

  <!-- Role based Permissions -->
  <script type="text/javascript">
    $('#add_jqGridBay').hide();
    $('#edit_jqGridBay').hide();
    $('#view_jqGridBay').hide();    

      @can('Create Bay')
        $('#add_jqGridBay').show();
      @endcan
      @can('Update Bay')
        $('#edit_jqGridBay').show();
      @endcan
      @can('Show Bay')
        $('#view_jqGridBay').show();
      @endcan       
      
        // Hide based on Role Permissions
        accessPermissionHost = function(){    
            $('td[aria-describedby=jqGridBay_addHost]').hide();
            $('#gsh_jqGridBay_addHost').hide();
            $('#jqGridBay_addHost').hide();
            $('#ViewTbl_jqGridBay #trv_addHost').hide();
          
          @can('Create Host')
            $('td[aria-describedby=jqGridBay_addHost]').show();
            $('#gsh_jqGridBay_addHost').show();
            $('#jqGridBay_addHost').show();
            $('#ViewTbl_jqGridBay #trv_addHost').show();
          @endcan
            
        }  
    </script>
@stop
