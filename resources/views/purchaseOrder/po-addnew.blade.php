@extends('layouts.polayouts')
@section('title')
    Customer Commitment Crud
@endsection
@section('menu')
    @include('layouts.menu')
@endsection
@section('pomenu')
    @include('layouts.pomenu')
@endsection
@section('content')
    {{-- ajax form submission of po insert --}}
    <script defer src="/assets/poassets/js/po-form-insertion-ajaxApi.js"></script>
    <script defer src="/assets/poassets/js/po-top-steps-navigation-validation.js"></script>
    <!-- PO steps navigation page -->
    <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
    <div class="poContainer col-md-10 col-xs-12">
        <form  name="purchaseorderform" id="purchaseorderform">
            <input type="hidden"  name="mode"  id="mode" value="1">
            <input type="hidden" name="postjsondata" id="postjsondata" value="">
            <input type="hidden" name="creativeValues" id="creativeValues" value="">
            <input type="hidden" name="po-location" id="po-location" value="">
            <input type="hidden" name="stepsComplete" id="stepsComplete" value="0">
            <input type="hidden" name="purchaseorder_id" id="purchaseorder_id" value="0">
            <input type="hidden" name="originalpoid" id="originalpoid" value="0">
            <input type="hidden" name="campaignid" id="campaignid" value="0">
            <input type="hidden" name="formurl" id="formurl" value="/purchaseorderstore">
            <input type="hidden" name="binding" id="binding" value="0">
            <!-- Step1 PO Form-->
            <div class="row setup-content po-block" id="step-1">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-purchaseorder')
                </fieldset>
            </div>
            <!-- Step2 Campaign Form-->
            <div class="row setup-content campaign-block" id="step-2">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-campaign')
                </fieldset>
            </div>
            <!-- Step3 Creative Form-->
            <div class="row setup-content creative-block" id="step-3">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-creative')
                </fieldset>
            </div>
            <!-- Step4 Location Form-->
            <div class="row setup-content location-block" id="step-4">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset>
                    @include('purchaseOrder.partials.po-form-location')
                </fieldset>
            </div>
            <!-- Step5 Preview Page-->
            <div class="row setup-content preview-block" id="step-5">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-preview')
                </fieldset>
            </div>
        </form>
    </div>
@stop

