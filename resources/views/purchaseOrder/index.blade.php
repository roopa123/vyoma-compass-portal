@extends('layouts.ccctemplate')

@section('title')
    Purchase Order
@endsection

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
    @include('layouts.validationAlert')

    <div class="adminPanel form-group NameRole rolesTable" style="width: 98%;">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <span class="text-center color-gray gridCaption customerGridCaption">{{"List of Purchase Orders "}} </span>
                    <!-- Check permission to create PO -->
            @can('Create PO')
            <a href="{{url('purchaseorder/create')}}" class="btn btn-success purchaseorder customerGridAdd"><i class="fa fa-plus-circle plus-icon"></i>&nbsp;&nbsp;&nbsp;Add Purchase Order</a>
            @endcan
            @can('Show PO')
            <div class="col-md-12 col-xs-12 padding0">
                <table id="POGrid" class="col-md-12 col-xs-12 table-bordered table-hover table-striped table-condensed table-responsive table"><tr></tr></table>
                <div id="POGridPager"></div>
                <script type="text/javascript">
                    $.ajax({
                        type: "POST",
                        url: "/po-grid",
                        dataType:"json",
                        success: function (dataJson) {
                            var oldFrom = $.jgrid.from,
                                    lastSelected;

                            $.jgrid.from = function (source, initalQuery) {
                                var result = oldFrom.call(this, source, initalQuery),
                                        old_select = result.select;
                                result.select = function (f) {
                                    lastSelected = old_select.call(this, f);
                                    return lastSelected;
                                };
                                return result;
                            };
                            jQuery("#POGrid").jqGrid(
                                    {
                                        "datatype": "local",
                                        "mtype": "POST",
                                        "treeGridModel": "adjacency",
                                        "data": dataJson.rows,
                                        "loadonce": true,
                                        "rowNum": 10,
                                        "rownumbers": false, "autowidth": true, "height": 'auto',
                                        "viewrecords": true,
                                        "pager": '#POGridPager',
                                        "altRows":true,
                                        "altclass":'myAltRowClass',
                                        "recreateFilter": true,
                                        beforeRequest: function () {
                                            modifySearchingFilter.call(this, ',');
                                        },

                                        loadComplete: function () {
                                            this.p.lastSelected = lastSelected; // set this.p.lastSelected
                                            highlightFilteredColumns.call(this); //highlightFilteredColumns

                                            /* set max height for all grids based on the window height */
                                            var height = $(window).height();
                                            $('.ui-jqgrid .ui-jqgrid-bdiv').css("max-height",height/2+"px");

                                            $.each($("#POGrid").jqGrid('getGridParam','data'),function(index,value){ /* replacing industry column null values with 'Blank' */
                                                if(value.IndustryName==null){
                                                    value.IndustryName='(Blank)';
                                                }
                                            });

                                            $("#POGrid td[title='(Blank)']").css("color","transparent"); /* not showing blank values in the grid */

{{--<<<<<<< HEAD--}}
                                            var iColStatus = columnIndexForGridBasedOnTableIdAndColumnName('#POGrid', 'Status');
                                            var statusValue=[];
                                            /*** status col***/
                                            $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColStatus + 1) + ")")
                                                    .each(function(value,key) {
                                                        statusValue.push($(this).children('.cellValue').text());
                                                    });
                                            setMultiSearchSelect('SaleRapeName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelect('BranchName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelect('AdvertiserName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelect('Sector', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelect('IndustryName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelect('PODate', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelect('StartDate', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelect('EndDate', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                            setMultiSearchSelectForStatus('Status', jQuery("#POGrid"), jQuery("#POGrid").jqGrid('getGridParam','data'), ":All;A:Active;P:Partial;D:Deleted;U:Uploaded");
/*=======
                                        var iColStatus = columnIndexForGridBasedOnTableIdAndColumnName('#POGrid', 'Status');
                                        var statusValue=[];
                                        /!*** status col***!/
                                        $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColStatus + 1) + ")")
                                                .each(function(value,key) {
                                                    statusValue.push($(this).children('.cellValue').text());
                                                });
                                        setMultiSearchSelect('SaleRapeName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('BranchName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('AdvertiserName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('Sector', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('IndustryName', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('PODate', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('StartDate', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelect('EndDate', jQuery("#POGrid"), $("#POGrid").jqGrid('getGridParam','data'));
                                        setMultiSearchSelectForStatus('Status', jQuery("#POGrid"), jQuery("#POGrid").jqGrid('getGridParam','data'), ":All;A:Active;P:Partial;D:Deleted;U:Uploaded");
                                    },
                                    "colModel": [
                                        {
                                            "label": "PO ID",
                                            "index": "PurchaseOrder",
                                            "name": "PurchaseOrder",
                                            "search": false,
                                            "key": true,
                                            'hidden': false,
                                            'editable' : true,
                                            sorttype: function (cellValue,rowObject)
                                            {
                                                return parseInt(cellValue);
                                            },
                                            'formatter' : poLinkFormat,
                                            'unformat' : poLinkUnFormat,
                                            "resizable":false
                                        },
                                        {
                                            "label": "Sales Rep",
                                            "index": "SaleRapeName",
                                            "name": "SaleRapeName",
                                            "search": true,
                                            "key": false,
                                            'hidden': false,
                                            "resizable":false
                                        },
                                        {
                                            "label": "Sales Branch",
                                            "index": "BranchName",
                                            "name": "BranchName",
                                            "search": true,
                                            "key": false,
                                            'hidden': false,
                                            "resizable":false
                                        },
                                        {
                                            "label": "Advertiser Name",
                                            "index": "AdvertiserName",
                                            "name": "AdvertiserName",
                                            "search": true,
                                            "resizable":false,
                                            "width":'170px'
>>>>>>> bd23fb8099b521120dc88c46d4a05da58edb7052*/
                                        },
                                        "colModel": [
                                            {
                                                "label": "PO ID",
                                                "index": "PurchaseOrder",
                                                "name": "PurchaseOrder",
                                                "search": false,
                                                "key": true,
                                                'hidden': false,
                                                'editable' : true,
                                                'formatter' : poLinkFormat,
                                                'unformat' : poLinkUnFormat,
                                                "resizable":false,
                                                sorttype: function (cellValue,rowObject)
                                                {
                                                    return parseInt(cellValue);
                                                }
                                            },
                                            {
                                                "label": "Sales Rep",
                                                "index": "SaleRapeName",
                                                "name": "SaleRapeName",
                                                "search": true,
                                                "key": false,
                                                'hidden': false,
                                                "resizable":false
                                            },
                                            {
                                                "label": "Sales Branch",
                                                "index": "BranchName",
                                                "name": "BranchName",
                                                "search": true,
                                                "key": false,
                                                'hidden': false,
                                                "resizable":false
                                            },
                                            {
                                                "label": "Advertiser Name",
                                                "index": "AdvertiserName",
                                                "name": "AdvertiserName",
                                                "search": true,
                                                "resizable":false,
                                                "width":'170px'
                                            },
                                            {
                                                "label": "PO Date",
                                                "index": "PODate",
                                                "name": "PODate",
                                                "search": true,
                                                "key": false,
                                                'hidden': false,
                                                "resizable":false
                                            },
                                            {
                                                "label": "Start Date",
                                                "index": "StartDate",
                                                "name": "StartDate",
                                                "search": true,
                                                "key": false,
                                                'hidden': false,
                                                "resizable":false
                                            },
                                            {
                                                "label": "End Date",
                                                "index": "EndDate",
                                                "name": "EndDate",
                                                "search": true,
                                                "key": false,
                                                'hidden': false,
                                                "resizable":false
                                            },
                                            {"label": "Sector", "index": "Sector", "name": "Sector", "search": true,"resizable":false},
                                            {"label": "Industry Name", "index": "IndustryName", "name": "IndustryName", "search": true,"resizable":false, 'width':'160px' ,formatter:nullFormatter},
                                            {"label": "Status", "index": "Status", "name": "Status", "search": true,"resizable":false,
                                                formatter: function(cellvalue, options, rowobject){
                                                    if(cellvalue == 'A'){
                                                        return 'Active';
                                                    }else if(cellvalue == 'P') {
                                                        return 'Partial';
                                                    }else if(cellvalue == 'D') {
                                                        return 'Delete';
                                                    }
                                                }}
                                        ]
                                    });

                            jQuery("#POGrid").jqGrid('navGrid', '#POGridPager', {edit: false, add: false, del: false, search: false, refresh:false}, {}, {}, {}, {
                                multipleSearch: true, multipleGroup: true, recreateFilter: true, overlay: 0
                            });

                            $('#jqgh_POGrid_AdvertiserName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_Sector').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_Status').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_IndustryName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_SaleRapeName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_BranchName').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_PODate').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_StartDate').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');
                            $('#jqgh_POGrid_EndDate').append('<img class="vSymbol" src="/assets/ccc-app/img/dropDownIcon.png"/>');

                            jQuery("#POGrid").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: true, defaultSearch: myDefaultSearch});

                            $('#POGrid_SaleRapeName .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(2) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(0,'SaleRapeName',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_BranchName .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(3) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(1,'BranchName',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_AdvertiserName .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(4) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(2,'AdvertiserName',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_PODate .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(5) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(3,'PODate',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_StartDate .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(6) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(4,'StartDate',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_EndDate .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(7) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(5,'EndDate',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_Sector .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(8) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(6,'Sector',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_IndustryName .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(9) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(7,'IndustryName',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                            $('#POGrid_Status .vSymbol').on('click',function(){
                                $('.ui-search-toolbar th:nth-child(10) button.ui-multiselect.ui-widget.ui-state-default').click();
                                removeUnWantedSearchOptions(8,'Status',$('#POGrid').jqGrid('getGridParam', 'lastSelected'),'POGrid');
                                return false;
                            });
                        }
                    });
                    var poLinkFormat = function(cellvalue, options, rowObject) {

                        if(rowObject.Status=='A'){
                            return "<a class='activePO' style='color: #9EC93F !important;font-weight:bold;border-bottom: 1px solid #9EC93F !important;" +
                                    "padding: 5px !important;' href='http://"+baseUrl+"/purchaseorder/"+rowObject.PurchaseOrder+"'>"+rowObject.PurchaseOrder+"</a>";
                        }else if(rowObject.Status=='P'){
                            return "<a class='partialPO' style='color: #ffbf00 !important;font-weight:bold;border-bottom: 1px solid #ffbf00 !important;" +
                                    " padding: 5px !important;'; href='http://"+baseUrl+"/purchaseorder/"+rowObject.PurchaseOrder+"'>"+rowObject.PurchaseOrder+"</a>";
                        }
                    };
                    var poLinkUnFormat = function(cellvalue, options, cell) {
                        return cell;
                    };
                </script>
            </div>
            @endcan
    </div>
@stop

