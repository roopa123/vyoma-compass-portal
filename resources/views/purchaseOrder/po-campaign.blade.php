<!--campaign block-->
<section class="col-md-12 col-xs-12">

    <h3 class="col-xs-4 campaignLabel">Campaign Details</h3>
    <div class="border-light-gray col-md-12 col-xs-12 padding-0">

        <ul class="nav nav-tabs col-md-12 col-xs-12 historyCampaign">
        <?php $counter = 1;$count=0;?>
            @foreach($purchaseOrders as $poCampaign)
                <?php //dd($poCampaign->POCampaignId);
                $campaignStartTime = strtotime($poCampaign->campaign->CampaignStartDate);
                $campaignStartDate = date("d-m-Y", $campaignStartTime);
                $campaignEndTime = strtotime($poCampaign->campaign->CampaignEndDate);
                $campaignEndDate = date("d-m-Y", $campaignEndTime);

                $classname = "";
                if($poCampaign->Status == 'D')
                $classname = "color-red";
                else
                {   
                    if($poCampaign->campaign->Status == 'P')
                    $classname = "amber-color";  
                    else if($poCampaign->campaign->Status == 'A')
                    $classname = "green-color";  
                    else if($poCampaign->campaign->Status == 'H')
                    $classname = "color-black";
                    else
                    {}
                }
                if($counter != 1 && $poCampaign->Status == 'D'){}
                else{ 
                ?>
            <li class="poCampaign po-class-for-camp-id" id="{{$poCampaign->campaign->CampaignId}}">
                <a data-toggle="tab" class="amber-color" href="#campaign{{$poCampaign->campaign->CampaignId}}" 
                onclick="gotoPOCampaignFn('campaign{{$poCampaign->campaign->CampaignId}}')">
                            <span class="{{$classname}}">{{$poCampaign->campaign->CampaignName}}</span></a>
                </li> <?php } $counter++;?>
            @endforeach
           
        </ul>
        <?php $counter = 1;?>
        @foreach($purchaseOrders as $poCampaign)
            <?php if(!isset($editCampaignId))
                $editCampaignId = $poCampaign->campaign->CampaignId;
                $classname = "";
                $status = "";
                if($poCampaign->Status == 'D'){
                 $classname = "color-red";
                 $status = 'Deleted';   
                }
                else
                {
                    if($poCampaign->campaign->Status == 'P'){
                    $classname = "amber-color"; 
                    $status = 'Partial';}
                    else if($poCampaign->campaign->Status == 'A'){
                    $classname = "green-color";
                    $status = 'Active';}
                    else
                    {}
                }
                if($counter != 1 && $poCampaign->Status == 'D'){}
                else
                { 
            ?>
            <div class="tab-pane col-md-12 col-xs-12 campaignTab padding-0" id="campaign{{$poCampaign->campaign->CampaignId}}">
                <section class="col-md-6 col-xs-12">
                    <ul class="po-history-campign list-unstyled">
                        <li class="padding-bottom"><strong>Campaign Name:</strong>
                            <span class="{{$classname}}"> {{ $poCampaign->campaign->CampaignName }}</span>
                        </li>
                        <li class="padding-bottom"><strong>Start Date:</strong><span> {{$campaignStartDate}}</span></li>
                    </ul>
                </section>
                <section class="col-md-6 col-xs-12">
                    <ul class="list-unstyled">
                        <li class="padding-bottom "><strong>Spots/Day:</strong><span> {{ $poCampaign->campaign->SpotsPerDay }}</span></li>
                        <li class="padding-bottom"><strong>End Date:</strong><span> {{$campaignEndDate}}</span></li>
                        <li class="padding-bottom"><strong>Status:</strong><span class="{{$classname}}"> {{$status}}</span></li>
                       
                    </ul>
                </section>

                <?php
                $poCampaignDetails = array('purchaseOrders' => $purchaseOrders,
                        'poCampaign' => $poCampaign->campaign);
                ?>
                @if(!is_null($poCampaign->CreativeCampaignId))
                    @include('purchaseOrder.po-creatives', $poCampaignDetails)
                @endif
                @if(!is_null($poCampaign->LocCampaignId))
                    @include('purchaseOrder.po-locations', $poCampaignDetails)
                @endif
            </div>

<?php } $counter++;?>



        @endforeach
    </div>

</section>
