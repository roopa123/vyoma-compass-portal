@extends('layouts.polayouts')
@section('title')
    Customer Commitment Crud
@endsection
@section('menu')
    @include('layouts.menu')
@endsection
@section('content')
    <div class="poContainer col-md-11 col-xs-12 po-history-container">
        <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
        <fieldset>
            <h3 class="col-md-12 col-xs-12 colornew-gray">Purchase Order :</h3>
            <div class="fieldsWrapper po-preview gray-bgr col-md-12 col-xs-12">
                <div class="col-md-3 col-xs-6 min-height-100"> <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-left historyTimeStamp">
                        @foreach($poHistory as $purchaseOrder)
                            <?php
                            $purchaseOrderKeys = array_keys($purchaseOrder);
                            $firstRecordKey = $purchaseOrderKeys[0];
                            $poItemOne = $purchaseOrder[$firstRecordKey];
                            if(!isset($editPOCampaignId))
                            {
                                $editPOCampaignId = $poItemOne->POCampaignId;
                                $campaignCount=0;
                                $operationButtonsEnable = 1;
                                $counter = 1;
                                foreach ($purchaseOrder as $poItem)
                                {
                                    if ($poItem->Status =='A' || $poItem->Status =='P')
                                    {
                                        $campaignCount++;
                                    }
                                    if ($counter == 1)
                                    {
                                        if ($poItem->Status == 'D')
                                        {
                                            $operationButtonsEnable = 0;
                                        } 
                                    }
                                    $counter++;
                                } 
                            }
                            $time = strtotime($poItemOne->CreatedAt);
                            $dateFormatForView = date("d-m-Y", $time);
                            $time = date("g:i A", $time);
                            ?>
                            <li  class="poClass" id="{{$poItemOne->POCampaignId}}">
                                <a href="#{{$poItemOne->POCampaignId}}" onclick="gotoDiv('{{$poItemOne->POCampaignId}}')" id="{{$poItemOne->POCampaignId}}_history" data-toggle="tab"><span>Date: {{$dateFormatForView}} <br> Time: {{$time}}</span></a>
                            </li>
                            <!-- Active Purchase Order Date By Desc First -->
                        @endforeach
                    </ul>
                </div>
                {{-- hidden filed to store active campaig id --}}
                <input type="hidden" id="pocampaignid" value="{{$editPOCampaignId}}">
                 <input type="hidden" id="campaignCount" value="{{$campaignCount}}">
                <input type="hidden" id="campaignenable" value="1">
                <input type="hidden" id="operationButtonsEnable" value="{{$operationButtonsEnable}}">
                <div class="col-md-9 col-xs-12 min-height-100">
                    <!-- Tab panes -->
                    <div class="tab-content preview-tab-content">
                        @foreach($poHistory as $purchaseOrder)
                        <?php
                        // find  MAX key Value
                        $purchaseOrderKeys = array_keys($purchaseOrder);
                        $firstRecordKey = $purchaseOrderKeys[0];
                        // Active Purchase Order By Desc First
                        $poItemOne = $purchaseOrder[$firstRecordKey];
                        //echo "<pre>";print_r($poItemOne);exit;
                        ?>
                                <!--BOF Timestamp -->
                        <div class="tab-pane po-tab" id="{{$poItemOne->POCampaignId}}">
                            <?php
                            // Latest PO for INFO
                            $PO = $poItemOne->purchaseOrder;
                            // Timestamp Campaigns
                            $purchaseOrders = $purchaseOrder;
                            ?>
                                    <!-- Render PO Information -->
                            @include('purchaseOrder.po-info', [$PO])
                                    <!-- Render PO Campaigns Details -->
                            @include('purchaseOrder.po-campaign', [$purchaseOrders])

                        </div>
                        <!--EOF Timestamp-->
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="min-height-100 col-md-12 col-xs-12">
                @can('Update PO')
                <div class="col-md-4 col-xs-12 text-center">
                    <button class="btn btn-default submitButton col-md-12 col-xs-12 previous cloneBtn" onclick="cloneCampaignFn()" type="submit">Clone</button>
                @endcan
                </div>
                @can('Update PO')
                <div class="col-md-4 col-xs-12 text-center">
                    <button id="submitButton" class="btn btn-default submitButton col-md-12 col-xs-12 previewEditBtn"
                            onclick="editCampaignFn()" type="submit">Edit</button>
                </div>
                @endcan
                @can('Delete PO')
                <div class="col-md-4 col-xs-12 text-center">
                    <button name="addAnother" id="addAnother" class="btn btn-default canselAndResetButton col-md-12 col-xs-12 nextBtn previewDelbtn" onclick="deleteCampaignFn()" type="button">Delete</button>
                </div>
                @endcan
            </div>
        </fieldset>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.poCampaign').first().addClass( "active" );
            $('.campaignTab').first().addClass('active-campaign');
            if($('.poCampaign.active a span').hasClass('color-red')){
                $('.cloneBtn').hide();
                $('.previewEditBtn').hide();
                $('.previewDelbtn').hide();
            }
            else
            {
                $('.cloneBtn').show();
                $('.previewEditBtn').show();
                $('.previewDelbtn').show();
            }
            $('.poClass').first().addClass( "active" );
            $('.tab-content.preview-tab-content .tab-pane').first().addClass('active');
            var poCampaignId;
            var campId;
        });

        gotoPOCampaignFn = function(id) {
            $(".poCampaign").removeClass('active');
            $(this).addClass('active');
            $('.campaignTab').removeClass('active-campaign');
            $('#'+id+'.campaignTab').addClass('active-campaign');
            setTimeout(function(){
                if($('.poCampaign.active a span').hasClass('color-red')){
                    $('.cloneBtn').hide();
                    $('.previewEditBtn').hide();
                    $('.previewDelbtn').hide();
                }else{
                    if($('#campaignenable').val() == 1)
                    {
                        $('.cloneBtn').show();
                        $('.previewEditBtn').show();
                        $('.previewDelbtn').show();
                    }
                }
            },200);
        };
        function getPoDeatils(poid, status){
            $.ajax({type: "GET", url: "/purchaseOrder/"+poid+"/"+status,
                success: function(results){
                    console.log(results);
                }
            });
        }

        function getTrimmedval(element,timeval) {
            var updateTime = timeval.replace(/\s/g, '');
            $(element).attr('href', '#'+updateTime);
        }

        function gotoDiv(status){

            /* hide del,edit,clone btns */
            if($('#pocampaignid').val() != status){
                $('.cloneBtn').hide();
                $('.previewEditBtn').hide();
                $('.previewDelbtn').hide();
                $('#campaignenable').val(0);
            }
            else
            {     
                if ($('#operationButtonsEnable').val() == 1)
                {
                    $('.cloneBtn').show();
                    $('.previewEditBtn').show();
                    $('.previewDelbtn').show();
                    $('#campaignenable').val(1);
                }
                $('#campaignenable').val(1);
            }
            $(".poClass").removeClass('active');
            $(this).addClass('active');
            $('.tab-content.preview-tab-content .po-tab').removeClass('active');
            $('#'+status+'.tab-pane').addClass('active');

            var campaignId_For_CampaignTab = $('#'+status+'.tab-pane  .historyCampaign li:first-child').attr('id');
            $('#'+status+'.tab-pane  .historyCampaign li:first-child').addClass('active');
            $('#'+status+'.tab-pane  .historyCampaign li').removeClass( "active" );
            $('#'+status+'.tab-pane  .campaignTab').removeClass('active-campaign')
            $('#'+campaignId_For_CampaignTab+'.poCampaign').addClass('active');
            $('#campaign'+campaignId_For_CampaignTab+'.campaignTab').addClass('active-campaign');
        }

        function cloneCampaignFn() {
            var poCampId = $('.historyTimeStamp li:first-child').attr('id');
            var campaignId = $("ul.historyCampaign li.active").attr("id")
            document.location ='/purchaseorder/pocampaign/'+poCampId+'/clone/campaign/'+campaignId;
        }

        function deleteCampaignFn() {
            var message = "Are you sure, you want to delete the Campaign?";
            if ($('#campaignCount').val() == 1 )
            {
                message = " Are you sure, you want to delete the Campaign? You have only one Campaign associated with this PO. If you delete this, entire PO data will be lost.";
            }
            
            bootbox.confirm(message, function(result) {
                if(result == true) {
                    var poCampId = $('.historyTimeStamp li:first-child').attr('id');
                    var campaignId = $("ul.historyCampaign li.active").attr("id");
                    document.location ='/purchaseorder/pocampaign/'+poCampId+'/delete/campaign/'+campaignId;
                }
            });
        }

        function editCampaignFn() {
            var poCampId = $('.historyTimeStamp li:first-child').attr('id');
            //alert(poCampId);
            //var campaignId = $('.historyCampaign li:first-child').attr('id');
            var campaignId = $("ul.historyCampaign li.active").attr("id")
            console.log(poCampId+ " " + campaignId);
            document.location ='/purchaseorder/'+poCampId+'/edit/campaign/'+campaignId;
        }
    </script>
@stop
