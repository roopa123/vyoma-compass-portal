<section class="col-md-6 col-xs-12">
<?php //dd($PO->advertiser->industry)
$POTime = strtotime($PO->PODate);
$PODate = date("d-m-Y", $POTime);

$POStartTime = strtotime($PO->StartDate);
$POStartDate = date("d-m-Y", $POStartTime);


$POEndTime = strtotime($PO->EndDate);
$POEndDate = date("d-m-Y", $POEndTime);
                    

?>
    <ul class="list-unstyled">
        
        @if($PO->ROReferenceNo != NULL)
            <li class="padding-bottom"><strong>RO Reference:</strong><span>{{ $PO->ROReferenceNo }}</span></li>
        @endif
        <li class="padding-bottom"><strong>Sale Rep:</strong><span>{{$PO->salesRep->name }}</span></li>
        <li class="padding-bottom"><strong>Sales Branch:</strong><span>{{$PO->branch->BranchName}} </span></li>
        <li class="padding-bottom"><strong>Advertiser:</strong><span>{{ $PO->advertiser->AdvertiserName}}</span></li>  
        <li class="padding-bottom"><strong>Sector:</strong><span>{{$PO->advertiser->Sector }}</span></li>      
      </ul>
</section>
<section class="col-md-6 col-xs-12">
    <ul class="list-unstyled">
        <li class="padding-bottom"><strong>P.O Date:</strong><span>{{$PODate}}</span></li>
        <li class="padding-bottom"><strong>Start Date:</strong><span>{{$POStartDate }}</span></li>
        <li class="padding-bottom"><strong>End Date:</strong><span>{{$POEndDate}}</span></li>        
        @if(!is_null($PO->advertiser->industry))
            <li class="padding-bottom"><strong>Industry:</strong><span>{{$PO->advertiser->industry->IndustryName }}</span></li>
        @endif
    </ul>
</section>