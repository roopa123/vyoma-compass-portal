<div class="col-md-4 col-xs-12 padding0">
<div class="selected-list-in-collapse">
<span class="col-md-12 col-xs-12 pull-left">Region</span>
</div>
<ul class="selected-lists list-unstyled">
<?php
  foreach ($regionArray as $regionId => $regionName) { ?>
  <li class="selected-stations9">
    <span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">
    <span class="margin-left-10">
    <span class="stationNameHdr" title="{{$regionName}}">{{$regionName}}</span>
    <span class="collapse-mode" onclick="toggleCollapseClass(this)"></span>
    </span>
    </span>
    <ul class="selected-list-items9 list-unstyled" style="padding: 18px;">
    <?php 
    foreach ($regionStationArray[$regionId] as $stationId => $stationName) { ?>
        <li class="">{{$stationName}}</li>
    <?php } ?>
    </ul>
  </li>
<?php } ?>
</ul>
</div>