@extends('layouts.polayouts')
@section('title')
    Customer Commitment Crud
@endsection
@section('menu')
    @include('layouts.menu')
@endsection
@section('pomenu')
    @include('layouts.poeditmenu')
@endsection
@section('content')
    {{-- ajax form submission of po update --}}
    <script defer src="/assets/poassets/js/po-form-update-ajaxApi.js"></script>
    <script defer src="/assets/poassets/js/po-top-steps-navigation-validation.js"></script>
    <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
    <!-- PO steps navigation page -->
    <div class="poContainer col-md-10 col-xs-12">
        <?php
        foreach ($POCampaignObj as $POCampaign) {}
        foreach ($POObj as $PO) {}
        foreach ($campaignObj as $campaign){}
        ?>
        <form  name="purchaseorderform" id="purchaseorderform">
            <input type="hidden" name="mode" id="mode" value="2">

            <!-- POId, CampaignId, CreativeCampaignId, LocationCampaignId, POCampaignId-->

            <input type="hidden" name="purchaseorder_id" id="purchaseorder_id"
                   value="{{$PO->POId}}">
            <input type="hidden" name="campaignid" id="campaignid"
                   value="{{$campaign->CampaignId}}">
            <input type="hidden" name="pocampaignid" id="pocampaignid"
                   value="{{$POCampaign->POCampaignId}}">
            <input type="hidden" name="creativecampaignid" id="creativecampaignid"
                   value="{{$POCampaign->CreativeCampaignId}}">
            <input type="hidden" name="locationcampaignid" id="locationcampaignid"
                   value="{{$POCampaign->LocCampaignId}}">

            <input type="hidden" name="originalpoid" id="originalpoid"
                   value="{{$POCampaign->OriginalPOId}}">
            <input type="hidden" name="originalcampaignid" id="originalcampaignid"
                   value="{{$POCampaign->OriginalCampaignId}}">

            <!-- PO, POCampaign Status-->
            <input type="hidden" name="editpocampaignstatus" id="editpocampaignstatus"
                   value="{{$POCampaign->Status}}">
            <input type="hidden" name="postatus" id="postatus"
                   value="{{$PO->Status}}">

            <!-- Creative Details -->
            <input type="hidden" name="creativeValues" id="creativeValues"
                   value="{{$creativeDetails}}">
            <input type="hidden" name="oldcreativeValues" id="oldcreativeValues"
                   value="{{$creativeDetails}}">
            <input type="hidden" name="creativeValuesJson" value="" id="creativeValuesJson">

            <!-- Location Details -->
            <input type="hidden" name="oldlocation" id="oldlocation"
                   value='{{$locationDetailsJson}}'>
            <input type="hidden" name="location" id="po-location"
                   value='{{$locationDetailsJson}}'>
            <input type="hidden" name="locationDifference" id="locationDifference"
                   value=''>

            <!-- Json Form Data-->
            <input type="hidden" name="postjsondata" id="postjsondata">

            <!-- Steps Complete-->

            <?php if ($POCampaign->Status == 'P') { ?>
            <input type="hidden" name="stepsComplete" id="stepsComplete" value="0">
            <?php } else { ?>
            <input type="hidden" name="stepsComplete" id="stepsComplete" value="1">
            <?php } ?>

                    <!-- Binding Data to Preview-->
            <input type="hidden" name="previewData" value="" id="previewData">

            <!-- Step1 PO Form-->
            <div class="row setup-content po-block" id="step-1">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset>
                    @include('purchaseOrder.partials.po-form-edit-purchaseorder')
                </fieldset>
            </div>
            <!-- Step2 Campaign Form-->
            <div class="row setup-content campaign-block" id="step-2">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-edit-campaign')
                </fieldset>
            </div>
            <!-- Step3 Creative Form-->
            <div class="row setup-content creative-block" id="step-3">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-creative')
                </fieldset>
            </div>
            <!-- Step4 Location Form-->
            <div class="row setup-content location-block" id="step-4">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset>
                    @include('purchaseOrder.partials.po-form-location')
                </fieldset>
            </div>
            <!-- Step5 Preview Page-->
            <div class="row setup-content preview-block" id="step-5">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-preview')
                </fieldset>
            </div>
        </form>
    </div>
@stop

