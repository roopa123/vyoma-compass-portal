@extends('layouts.polayouts')
@section('title')
    Customer Commitment Crud
@endsection
@section('menu')
    @include('layouts.menu')
@endsection
@section('pomenu')
    @include('layouts.pomenu')
@endsection
@section('content')
    {{-- ajax form submission of po insert --}}
    <script defer src="/assets/poassets/js/po-form-insertion-ajaxApi.js"></script>
    <!-- PO steps navigation page -->
    <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
    <div class="poContainer col-md-10 col-xs-12">
        <form  name="purchaseorderform" id="purchaseorderform">
            <div class="row setup-content po-block" id="step-1">
            <p ><i class="fa fa-times-circle close" aria-hidden="true" onclick="redirectFn('{{{ URL::route("purchaseorder.index") }}}')" ></i></p>
                <fieldset><br>
                    <h4 class="col-md-12 col-xs-12 colornew-gray"> Add Purchase Order :</h4>
                    <input type="hidden"  name="mode"  id="mode" value="1">
                    <input type="hidden" name="postjsondata" id="postjsondata" value="">
                     <input type="hidden" name="creativeValues" id="creativeValues" value="">
                      <input type="hidden" name="po-location" id="po-location" value="">
                      <input type="hidden" name="stepsComplete" id="stepsComplete" value="0">

                    <div class="fieldsWrapper col-md-8 col-xs-8">
                        <!--PO Errors-->
                        <div id="poerrors" class="col-md-12 col-xs-12 ">
                            <ul class="list-unstyled"></ul>

                        </div>
                        <!-- PO Errors End -->
                        <div class="min-height-90 col-md-6 col-xs-12">
                            {!! Form::label('Sales Rep','Sales Rep',array('class' => 'color-blue')) !!}
                            {!! Form::select('salesrep', $salesRep, Input::old('salesrep'), ['class'=>'col-md-12 col-xs-12 form-control', 'id' => 'sector']) !!}
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="purchase_order_date">Purchase Order Date</label>
                            <div class='input-group date col-md-12 col-xs-12' >
                                <input id="purchase_order_date" type='text' class="form-control" name="purchase_order_date" readonly required/>
                                    <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="startDate">Start Date</label>
                            <div class='input-group date col-md-12 col-xs-12' >
                                <input id="startDate" type='text' class="form-control" name="startDate" readonly required/>
                               <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="endDate">End Date</label>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="endDate" type='text' class="form-control" name="endDate" readonly required/>
                                    <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            {!! Form::label('advertiser','Advertiser Name',array('class' => 'color-blue col-md-6 padding-0')) !!}
                            @can('Create Advertiser')
                            <a href="/advertiser/create/redirect/purchaseorder" class="btn addAdvertiserBtn col-md-6 pull-right">
                                <i class="fa fa-plus-circle plus-icon"></i>&nbsp;Add Advertiser</a>
                            @endcan
                            {!! Form::select('advertiser', $activeAdvertiser, Input::old('advertiser'), ['class'=>'col-md-12 col-xs-12 form-control', 'id' => 'advertiser']) !!}
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            {!! Form::label('Branch','Branch',array('class' => 'color-blue')) !!}
                            {!! Form::select('branch', $branches, Input::old('Branch'), ['class'=>'col-md-12 col-xs-12 form-control', 'id' => 'branch']) !!}

                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="roreference">RO Reference</label>
                            <input  name="roreference" id="roreference" class="col-md-12 col-xs-12 form-control"  type="text" />
                        </div>
                        <div class="min-height-70 col-md-12 col-xs-12">
                            <div class="col-md-6 col-xs-12 text-center">
                                <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" onclick="resetPOForm()">Reset</a>
                            </div>

                            <div class="col-md-6 col-xs-12 text-center">
                                <button id="goToCampign" class="btn btn-default submitButton col-md-12 col-xs-12"  style="width: 100%;"   value="continue">Campaign</button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="row setup-content campaign-block" id="step-2">
            <p ><i class="fa fa-times-circle close" aria-hidden="true" onclick="redirectFn('{{{ URL::route("purchaseorder.index") }}}')" ></i></p>
                <fieldset><br>
                    <h4 class="col-md-12 col-xs-12 colornew-gray">Add Campaign :</h4>
                    <div class="fieldsWrapper col-md-8 col-xs-8">

                        <div id="campaignerrors" class="col-md-12 col-xs-12 ">
                            <ul class="list-unstyled"></ul>
                        </div>

                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="CompaignName">Campaign Name</label>
                            <input id="CompaignName" name="CompaignName" type="text" class="col-md-12 col-xs-12 form-control" required />
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="SpotsPerDay">Spots/Day</label>
                            <input id="SpotsPerDay" name="SpotsPerDay" class="col-md-12 col-xs-12 form-control" type="text" required/>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="start-date-campaign">Start Date</label>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="start-date-campaign" type='text' class="form-control" name="start_date_campaign" readonly required/>
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="end-date-campaign">End Date</label>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="end-date-campaign" type='text' class="form-control" name="end_date_campaign" readonly required/>
                               <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-70 col-md-12 col-xs-12">
                            <div class="col-md-4 col-xs-12 text-center">
                                <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" onclick="resetCampaignForm()">Reset</a>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button id="campaignbtn" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;" >Save & Exit</button>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button type="button" name="goToCreative" id="goToCreative" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;">Creative</button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="row setup-content creative-block" id="step-3">
            <p ><i class="fa fa-times-circle close" aria-hidden="true" onclick="redirectFn('{{{ URL::route("purchaseorder.index") }}}')" ></i></p>
                <fieldset><br>
                    <h4 class="col-md-12 col-xs-12 colornew-gray">Add Creative:</h4>
                    <div class="fieldsWrapper col-md-8 col-xs-8">
                        <div class="col-md-4 col-xs-12">
                            <label class="color-blue">Creative Name</label>
                            <select class="col-md-12 col-xs-12 form-control" id="Creative" name="Creative" required></select>
                        </div>
                        <div id="creativeerrors" class="col-md-4 col-xs-12 ">
                            <ul class="list-unstyled"></ul>
                        </div>

                        <div class="margin-bottom-30 min-height-90 col-md-12 col-xs-12 POCreativeGridBlock">
                            <table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped table-condensed table-responsive table"></table>
                            <div id="CreativeGridPager"></div>
                        </div>

                        <div id="creativegriderrors" class="col-md-12 col-xs-12 ">
                        <ul class="list-unstyled"></ul></div>


                        <input type="hidden" class="hiddenInputFileCreative"/>
                        <div class="min-height-70 col-md-12 col-xs-12">
                            <div class="col-md-4 col-xs-12 text-center">
                                <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" onclick="resetCreativesForm()">Reset</a>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button id="creativebtn" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;" type="submit">Save & Exit</button>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button type="button" name="goToLocation" id="goToLocation" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;">Location</button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="row setup-content location-block" id="step-4">
            <p ><i class="fa fa-times-circle close" aria-hidden="true" onclick="redirectFn('{{{ URL::route("purchaseorder.index") }}}')" ></i></p>
                <fieldset>
                    <div class="fieldsWrapper col-md-12 col-xs-12">

                        <div id="locationerrors" class="col-md-12 col-xs-12">
                            <ul class="list-unstyled"></ul></div>

                        <section class="location-tabs-blocks gray-bgr">
                            <ul class="nav nav-tabs col-md-12 col-xs-12">
                                <li class="active"><a data-toggle="tab" href="#Region">Region</a></li>
                                <li><a data-toggle="tab" href="#State">State</a></li>
                                <li><a data-toggle="tab" href="#Station">Station</a></li>
                                <li><a data-toggle="tab" href="#Summary">Summary</a></li>
                            </ul>
                            <!--region block-->
                            <div class="tab-pane fade in active col-md-12 col-xs-12" id="Region">
                                <div class="col-md-4 col-xs-12 padding-left">
                                    <div class="padding10-grayBG">Region</div>
                                    <ul id="region-" class="region-devisions-ul-list list-unstyled mCustomScrollbar"></ul>
                                </div>
                                <div class="col-md-3 col-xs-12 padding-0">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 regionNameHdr">Stations For Region</span>
                                        <span class="padding-0 selectAll-clearBtns">
                                            <i class="fa fa-check-circle-o" title="Select All" aria-hidden="true" onclick="chooseAllRegionStations()"></i>
                                           <i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clearAllRegionStations()"></i>
                                          {{--<button type="button" class="btn btn-default" onclick="chooseAllRegionStations()">Select All</button>
                                          <button type="button" class="btn btn-default" onclick="clearAllRegionStations()">Clear</button>--}}
                                        </span>
                                    </div>
                                    <ul class="region-station-list list-unstyled"></ul>
                                </div>
                                <div class="col-md-1 col-xs-4 padding-0">
                                    <img class="img-responsive proccedSelectionBtn" onclick="moveAllRegionStationsToSelectedList()" src="/assets/poassets/images/proccedSelectionBtn.png">
                                </div>
                                <div class="col-md-4 col-xs-12 padding-0">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 regionStationHdr">Stations</span>
                                    </div>
                                    <ul class="selected-lists list-unstyled"></ul>
                                </div>
                            </div>

                            <!--state block-->
                            <div class="tab-pane fade col-md-12 col-xs-12" id="State">
                                <div class="col-md-4 col-xs-12 padding-left">
                                    <div class="padding10-grayBG">State</div>
                                    <ul class="state-devisions-ul-list list-unstyled"></ul>
                                </div>
                                <div class="col-md-3 col-xs-12 padding-0">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 stateNameHdr">Stations For State</span>
                                        <span class="padding-0 selectAll-clearBtns">
                                            <i class="fa fa-check-circle-o" title="Select All" aria-hidden="true" onclick="chooseAllStateStations()"></i>
                                           <i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clearAllStateStations()"></i>
                                          {{--<button type="button" class="btn btn-default" onclick="chooseAllStateStations()">Select All</button>
                                          <button type="button" class="btn btn-default" onclick="clearAllStateStations()">Clear</button>--}}
                                        </span>
                                    </div>
                                    <ul class="state-station-list list-unstyled"></ul>
                                </div>
                                <div class="col-md-1 col-xs-4 padding-0">
                                    <img class="img-responsive proccedSelectionBtn" onclick="moveAllStateStationsToSelectedList()" src="/assets/poassets/images/proccedSelectionBtn.png">
                                </div>
                                <div class="col-md-4 col-xs-12 padding-0">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 regionStationHdr">Stations</span>
                                    </div>
                                    <ul class="selected-state-station-lists list-unstyled"></ul>
                                </div>
                            </div>

                            <!--station block-->
                            <div class="tab-pane fade col-md-12 col-xs-12" id="Station">
                                <div class="col-md-7 col-xs-12 padding-0">
                                    <div class="station-block-sorting-selectAll-clear-btns selectAll-clearBtns">
                                       {{-- <i class="fa fa-check-circle-o" title="Select All" aria-hidden="true" onclick="chooseAllStationStations()"></i>
                                        <i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="unCheckAllStationStationsCheckBoxs()"></i>--}}
                                        <button type="button" class="btn btn-default" onclick="chooseAllStationStations()">Select All</button>
                                        <button type="button" class="btn btn-default" onclick="unCheckAllStationStationsCheckBoxs()">Clear</button>
                                    </div>
                                    <div class="padding-0 stationSortContainer">
                                        <ul class="station-sortings-list list-unstyled col-md-12 col-xs-12"></ul>
                                    </div>
                                </div>
                                <div class="col-md-1 col-xs-4 padding-0">
                                    <img class="img-responsive proccedSelectionBtn" onclick="moveAllStationStationsToSelectedList()" src="/assets/poassets/images/proccedSelectionBtn.png">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">
                                                <span class="pull-left">Stations</span>
                                                <span class="pull-right">
                                                    <span class="selectAll-clearBtns">
                                                        <i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrFinalStationStationItems()"></i>
                                                        {{--<button type="button" class="btn btn-default" onclick="clrFinalStationStationItems()">Clear</button>--}}
                                                    </span>
                                                </span>
                                            </span>
                                    </div>
                                    <ul class="selected-station-station-lists list-unstyled">
                                        <li>
                                            <ul class="selected-station-station-list-items list-unstyled"></ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!--Summary block-->
                            <div class="tab-pane fade col-md-12 col-xs-12" id="Summary">
                                <div class="col-md-4 col-xs-12">
                                    <div class="selected-list-in-collapse">
                                        <span class="col-md-7 col-xs-12 pull-left">Region</span>
                                        <span class="col-md-5 col-xs-12"></span>
                                    </div>
                                    <ul class="selected-lists list-unstyled"></ul>
                                    {{--<div class="selected-list-in-collapse text-center"></div>--}}
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="selected-list-in-collapse">
                                        <span class="col-md-7 col-xs-12 pull-left">State</span>
                                        <span class="col-md-5 col-xs-12"></span>
                                    </div>
                                    <ul class="selected-state-station-lists list-unstyled"></ul>
                                    {{--<div class="selected-list-in-collapse text-center"></div>--}}
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">
                                                <span class="pull-left">Stations</span>
                                                <span class="pull-right">
                                                    <span class="selectAll-clearBtns">
                                                        <i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrFinalStationStationItems()"></i>
                                                        {{--<button type="button" class="btn btn-default" onclick="clrFinalStationStationItems()">Clear</button>--}}
                                                    </span>
                                                </span>
                                            </span>
                                    </div>
                                    <ul class="selected-station-station-lists list-unstyled">
                                        <li>
                                            <ul class="selected-station-station-list-items list-unstyled"></ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </section>

                        <section class="min-height-70 col-md-12 col-xs-12">
                            <div class="col-md-4 col-xs-12 text-center">
                                <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous"
                                        onclick="resetLocForm()">Reset</a>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button id="locationbtn" class="btn btn-default submitButton col-md-12 col-xs-12"   type="button">Save & Exit</button>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button type="button" class="btn btn-default submitButton col-md-12 col-xs-12" id="goToPreview" onclick="goToPreviewWithLocationData()">Preview</button>
                            </div>
                        </section>
                    </div>
                </fieldset>
            </div>
            <div class="row setup-content preview-block" id="step-5">
                <fieldset><br>
                    <h4 class="col-md-12 col-xs-12 colornew-gray">Purchase Order Details :</h4>
                    <div class="fieldsWrapper filedwrapper-for-po-preview display-inline-block col-md-8 col-xs-12">
                        <div id="allerrors">
                            <ul class="list-unstyled"></ul></div>
                        <section class="col-md-6 col-xs-12">
                            <ul class="list-unstyled">
                                <li class="padding-bottom"><strong>RO Reference:</strong>
                                    <span class="roreference"> </span></li>
                                <li class="padding-bottom"><strong>Sale Rep:</strong><span class="salesrep"></span></li>
                                <li class="padding-bottom"><strong>Sales Branch:</strong><span class="branchname"> </span></li>
                                <li class="padding-bottom"><strong>Advertiser:</strong><span class="advertisername"></span></li>
                            </ul>
                        </section>
                        <section class="col-md-6 col-xs-12">
                            <ul class="list-unstyled">
                                <li class="padding-bottom"><strong>P.O Date:</strong><span class="podate"></span></li>
                                <li class="padding-bottom"><strong>Start Date:</strong><span class="postart"></span></li>
                                <li class="padding-bottom"><strong>End Date:</strong><span class="poend"></span></li>
                            </ul>
                        </section>
                        <section class="col-md-12 col-xs-12">
                            <!--campaign block-->
                            <h3 class="col-xs-4 campaignLabel">Campaign Details</h3>
                            <section class="col-md-12 col-xs-12 po-summary-campaign-block">
                                <div class="tab-pane fade in active col-md-12 col-xs-12" id="1234567">
                                    <section class="col-md-6 col-xs-12">
                                        <ul class="list-unstyled">
                                            <li class="padding-bottom"><strong>Campaign Name:</strong><span class="campaignname"> </span></li>
                                            <li class="padding-bottom"><strong>Start Date:</strong><span class="campaignstart"> </span></li>
                                        </ul>
                                    </section>
                                    <section class="col-md-6 col-xs-12">
                                        <ul class="list-unstyled">
                                            <li class="padding-bottom"><strong>Spots/Day:</strong><span class="slots"> </span></li>
                                            <li class="padding-bottom"><strong>End Date:</strong><span class="campaignend"> </span></li>
                                        </ul>
                                    </section>
                                    <!--creatives block-->
                                    <div id="creativePreview">
                                    <h3 class="text-muted col-md-12 col-xs-12 padding-0">Creative</h3>
                                    <ul class="list-unstyled col-md-12 col-xs-12 padding-0">
                                        <li class="padding-bottom"><strong>Creative:</strong><span class="creativeNamesInPreview"></span></li>
                                    </ul>
                                    </div>
                                </div>
                                <!--location block-->
                                <div id="locationPreview">
                                <h3 class="text-muted col-md-12 col-xs-12">Location</h3>
                                <ul class="list-unstyled col-md-12 col-xs-12">
                                    <li class="padding-bottom"><strong>Region:</strong><span class="preview-selected-regions"></span></li>
                                    <li class="padding-bottom"><strong>State:</strong><span class="preview-selected-states"></span></li>
                                    <li class="padding-bottom"><strong>Station:</strong><span class="preview-selected-stations"></span></li>
                                </ul>
                                </div>
                            </section>
                        </section>
                    </div>
                    <section class="min-height-70 col-md-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 text-center">
                            <button id="previewsavebtn" class="btn btn-default submitButton col-md-12 col-xs-12"   type="button">Save & Exit</button>
                        </div>
                        <div class="col-md-4 col-xs-12 text-center">
                            <button id="addcampaignbtn" class="btn btn-default submitButton col-md-12 col-xs-12"   type="button">Add Another Campaign</button>
                        </div>
                        <div class="col-md-4 col-xs-12 text-center">
                            <button id="posubmitbtn" class="btn btn-default submitButton col-md-12 col-xs-12"   type="button">Submit</button>
                        </div>
                    </section>
                </fieldset>
            </div>
        </form>
    </div>
@stop

