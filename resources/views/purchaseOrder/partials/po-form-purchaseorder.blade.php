 <h4 class="col-md-12 col-xs-12 colornew-gray"> Add Purchase Order :</h4>
                     <div class="fieldsWrapper col-md-8 col-xs-8">
                        <!--PO Errors-->
                        <div id="poerrors" class="col-md-12 col-xs-12 ">
                            <ul class="list-unstyled"></ul>
                        </div>
                        <!-- PO Errors End -->
                        <div class="min-height-90 col-md-6 col-xs-12">
                        {!! Form::label('Sales Rep','Sales Rep',array('class' => 'color-blue')) !!}
                        {!! Form::select('salesrep', $salesRep, Input::old('salesrep'), ['class'=>'col-md-12 col-xs-12 form-control', 'name' => 'salesrep',
                        'id' => 'salesrep','required']) !!}
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="purchase_order_date">Purchase Order Date</label>
                            <div class='input-group date col-md-12 col-xs-12' >
                                <input id="purchase_order_date" type='text' class="form-control" name="purchase_order_date" readonly required/>
                                    <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="startDate">Start Date</label>
                            <div class='input-group date col-md-12 col-xs-12' >
                                <input id="startDate" type='text' data-format="DD-MM-YYYY" class="form-control" name="startDate" readonly required/>
                               <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="endDate">End Date</label>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="endDate" type='text' data-format="DD-MM-YYYY" class="form-control" name="endDate" readonly required/>
                                    <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            {!! Form::label('Advertiser Name','Advertiser Name',array('class' => 'color-blue col-md-6 padding-0"')) !!}
                            @can('Create Advertiser')
                            <a href="/advertiser/create/redirect/purchaseorder"
                            class="btn addAdvertiserBtn col-md-6 pull-right"
                            onclick="javascript:return redirectAdvertiser('/advertiser/create/redirect/purchaseorder');">
                                <i class="fa fa-plus-circle plus-icon"></i>&nbsp;Add Advertiser</a>
                            @endcan
                        {!! Form::select('advertiser', $activeAdvertiser, Input::old('advertiser'), ['class'=>'col-md-12 col-xs-12 form-control', 'name' => 'advertiser',
                        'id' => 'advertiser', 'onchange' => 'checkAdvertiserHasCreative(this.value,0)','required']) !!}
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                        {!! Form::label('branch','Sales Branch',array('class' => 'color-blue')) !!}
                        {!! Form::select('branch', $branchList, Input::old('branch'), ['class'=>'col-md-12 col-xs-12 form-control', 'name' => 'branch', 
                        'id' => 'branch','required']) !!}
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="roreference">RO Reference</label>
                            <input  name="roreference" id="roreference" class="col-md-12 col-xs-12 form-control"  type="text" />
                        </div>
                        <div class="min-height-70 col-md-12 col-xs-12">
                            <div class="col-md-6 col-xs-12 text-center">
                                <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" onclick="resetPOForm()">Reset</a>
                            </div>

                            <div class="col-md-6 col-xs-12 text-center">
                                <button id="goToCampign" class="btn btn-default submitButton col-md-12 col-xs-12"  style="width: 100%;"   value="continue">Campaign</button>
                            </div>
                        </div>
                    </div>
