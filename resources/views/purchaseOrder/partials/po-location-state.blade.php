<div class="col-md-4 col-xs-12 padding-left">
                                    <div class="padding10-grayBG">
                                        <span class="padding-0">State</span>
                                        <span class="padding-0 selectAll-clearBtns">
                                            <i class="fa fa-check-circle-o" aria-hidden="true" title="Select All States"
                                               onclick="toggleClassStateAddBtn('','','','true')"></i>
                                            <i class="fa fa-times-circle" aria-hidden="true" title="Clear All States" onclick="clearAllStates()"></i>
                                        </span>
                                    </div>
                                    <ul class="state-devisions-ul-list list-unstyled"></ul>
                                </div>
                                <div class="col-md-3 col-xs-12 padding-0">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 stateNameHdr">Stations for State</span>
                                        <span class="padding-0 selectAll-clearBtns">
                                            <i class="fa fa-check-circle-o" aria-hidden="true" onclick="chooseAllStateStations()"></i>
                                            <i class="fa fa-times-circle" aria-hidden="true" onclick="clearAllStateStations()"></i>
                                            {{-- <button type="button" class="btn btn-default" onclick="chooseAllStateStations()">Select All</button>
                                             <button type="button" class="btn btn-default" onclick="clearAllStateStations()">Clear</button>--}}
                                        </span>
                                    </div>
                                    <ul class="state-station-list list-unstyled"></ul>
                                </div>
                                <div class="col-md-1 col-xs-4 padding-0">
                                    <img class="img-responsive proccedSelectionBtn" onclick="moveAllStateStationsToSelectedList()" src="/assets/poassets/images/proccedSelectionBtn.png">
                                </div>
                                <div class="col-md-4 col-xs-12 padding-left">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 regionStationHdr">Stations</span>
                                    </div>
                                    <ul class="selected-state-station-lists list-unstyled"></ul>
                                </div>