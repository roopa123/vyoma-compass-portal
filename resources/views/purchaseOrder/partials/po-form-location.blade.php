<div class="fieldsWrapper col-md-12 col-xs-12">
                        <div id="locationerrors" class="errorsDisplay">
                            <ul class="list-unstyled"></ul></div>
                        <section class="location-tabs-blocks gray-bgr">
                            <ul class="nav nav-tabs col-md-12 col-xs-12">
                                <li class="active"><a data-toggle="tab" href="#Region">Region</a></li>
                                <li><a data-toggle="tab" href="#State">State</a></li>
                                <li><a data-toggle="tab" href="#Station">Station</a></li>
                                <li><a data-toggle="tab" href="#Summary">Summary</a></li>
                            </ul>
                            <!--region block-->
                            <div class="tab-pane fade in active col-md-12 col-xs-12" id="Region">
                               @include('purchaseOrder.partials.po-location-region')
                            </div>

                            <!--state block-->
                            <div class="tab-pane fade col-md-12 col-xs-12" id="State">
                                @include('purchaseOrder.partials.po-location-state')
                            </div>

                            <!--station block-->
                            <div class="tab-pane fade col-md-12 col-xs-12" id="Station">
                               @include('purchaseOrder.partials.po-location-station')
                            </div>

                            <!--Summary block-->
                            <div class="tab-pane fade col-md-12 col-xs-12" id="Summary">
                                <div class="col-md-4 col-xs-12">
                                    <div class="selected-list-in-collapse">
                                        <span class="col-md-7 col-xs-12 pull-left">Region</span>
                                        <span class="col-md-5 col-xs-12"></span>
                                    </div>
                                    <ul class="selected-lists list-unstyled"></ul>
                                    {{--<div class="selected-list-in-collapse text-center"></div>--}}
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="selected-list-in-collapse">
                                        <span class="col-md-7 col-xs-12 pull-left">State</span>
                                        <span class="col-md-5 col-xs-12"></span>
                                    </div>
                                    <ul class="selected-state-station-lists list-unstyled"></ul>
                                    {{--<div class="selected-list-in-collapse text-center"></div>--}}
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">
                                                <span class="pull-left">Stations</span>
                                                <span class="pull-right">
                                                    <span class="selectAll-clearBtns">
                                                        <i class="fa fa-times-circle stationClearBtn" title="Clear All" aria-hidden="true" onclick="clrFinalStationStationItems()"></i>
                                                        {{--<button type="button" class="btn btn-default" onclick="clrFinalStationStationItems()">Clear</button>--}}
                                                    </span>
                                                </span>
                                            </span>
                                    </div>
                                    <ul class="selected-station-station-lists list-unstyled">
                                        <li>
                                            <ul class="selected-station-station-list-items list-unstyled"></ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </section>

                        <section class="min-height-100 col-md-12 col-xs-12">
                            <div class="col-md-4 col-xs-12 text-center">
                                <button class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" type="button" onclick="resetLocForm();">Reset</button>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button id="locationbtn" class="btn btn-default submitButton col-md-12 col-xs-12" type="button">Save & Exit</button>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button type="button" class="btn btn-default submitButton col-md-12 col-xs-12" id="goToPreview" onclick="goToPreviewWithLocationData()">Preview</button>
                            </div>
                        </section>
                    </div>