<h4 class="col-md-12 col-xs-12 colornew-gray">Purchase Order Details :</h4>
<div class="fieldsWrapper filedwrapper-for-po-preview col-md-8 col-xs-12">
    <div id="allerrors">
        <ul class="list-unstyled"></ul></div>
    <section class="col-md-6 col-xs-12">
        <ul class="list-unstyled">
            <li class="padding-bottom"><strong>RO Reference:</strong>
                <span class="roreference"> </span></li>
            <li class="padding-bottom"><strong>Sale Rep:</strong><span class="salesrep"></span></li>
            <li class="padding-bottom"><strong>Sales Branch:</strong><span class="branchname"> </span></li>
            <li class="padding-bottom"><strong>Advertiser:</strong><span class="advertisername"></span></li>
        </ul>
    </section>
    <section class="col-md-6 col-xs-12">
        <ul class="list-unstyled">
            <li class="padding-bottom"><strong>P.O Date:</strong><span class="podate"></span></li>
            <li class="padding-bottom"><strong>Start Date:</strong><span class="postart"></span></li>
            <li class="padding-bottom"><strong>End Date:</strong><span class="poend"></span></li>
        </ul>
    </section>
    <section class="col-md-12 col-xs-12">
        <!--campaign block-->
        <h3 class="col-xs-4 campaignLabel">Campaign Details</h3>
        <section class="col-md-12 col-xs-12 po-summary-campaign-block">
            <div class="tab-pane fade in active col-md-12 col-xs-12" id="1234567">
                <section class="col-md-6 col-xs-12">
                    <ul class="list-unstyled">
                        <li class="padding-bottom"><strong>Campaign Name:</strong><span class="campaignname"> </span></li>
                        <li class="padding-bottom"><strong>Start Date:</strong><span class="campaignstart"> </span></li>
                    </ul>
                </section>
                <section class="col-md-6 col-xs-12">
                    <ul class="list-unstyled">
                        <li class="padding-bottom"><strong>Spots/Day:</strong><span class="slots"> </span></li>
                        <li class="padding-bottom"><strong>End Date:</strong><span class="campaignend"> </span></li>
                    </ul>
                </section>
                <!--creatives block-->
                <h3 class="text-muted col-md-12 col-xs-12 padding-0">Creative</h3>
                <ul class="list-unstyled col-md-12 col-xs-12 padding-0">
                    <li class="padding-bottom"><strong>Creative:</strong><span class="creativeNamesInPreview"></span></li>
                    {{--<li class="padding-bottom"><strong>End Date:</strong><span class ="creativeEndDate"></span></li>--}}
                </ul>
            </div>
            <!--location block-->
            <h3 class="text-muted col-md-12 col-xs-12">Location</h3>

            <!--Summary block-->
            <div class="tab-pane col-md-12 col-xs-12 padding0" id="SummaryPreview">
                <div class="col-md-4 col-xs-12 regionSection">
                    <div class="selected-list-in-collapse"><span class="col-md-12 col-xs-12 pull-left">Region</span></div>
                    <ul class="selected-lists list-unstyled"></ul>
                </div>
                <div class="col-md-4 col-xs-12 stateSection">
                    <div class="selected-list-in-collapse"><span class="col-md-12 col-xs-12 pull-left">State</span></div>
                    <ul class="selected-state-station-lists list-unstyled"></ul>
                </div>
                <div class="col-md-4 col-xs-12 stationSection">
                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                        <span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">
                            <span class="pull-left">Stations</span></span>
                    </div>
                    <ul class="selected-station-station-lists">
                        <li>
                            <ul class="selected-station-station-list-items"></ul>
                        </li>
                    </ul>
                </div>
            </div>
            {{--  <li class="padding-bottom col-md-12 col-xs-12 padding-0"><strong>Region:</strong><span class="preview-selected-regions long-text"></span></li>
              <li class="padding-bottom col-md-12 col-xs-12 padding-0"><strong>State:</strong><span class="preview-selected-states long-text"></span></li>
              <li class="padding-bottom col-md-12 col-xs-12 padding-0"><strong>Station:</strong><span class="preview-selected-stations long-text"></span></li>--}}
        </section>
    </section>
</div>
<section class="min-height-70 col-md-12 col-xs-12">
    <div class="col-md-4 col-xs-12 text-center">
        <button id="previewsavebtn" class="btn btn-default submitButton col-md-12 col-xs-12"   type="button">Save & Exit</button>
    </div>
    <div class="col-md-4 col-xs-12 text-center">
        <button id="addcampaignbtn" class="btn btn-default submitButton col-md-12 col-xs-12"   type="button">Add Another Campaign</button>
    </div>
    <div class="col-md-4 col-xs-12 text-center">
        <button id="posubmitbtn" class="btn btn-default submitButton col-md-12 col-xs-12"   type="button">Submit</button>
    </div>
</section>