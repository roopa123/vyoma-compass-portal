<div class="col-md-4 col-xs-12 padding-left">
                                    <div class="padding10-grayBG">
                                        <span class="padding-0">Region</span>
                                        <span class="padding-0 selectAll-clearBtns">
                                            <i class="fa fa-check-circle-o" aria-hidden="true" title="Select All Regions"
                                               onclick="toggleClassAddBtn('','','','true')"></i>
                                            <i class="fa fa-times-circle" aria-hidden="true" title="Clear All Regions" onclick="clearAllRegions()"></i>
                                        </span>
                                    </div>
                                    <ul class="region-devisions-ul-list list-unstyled"></ul>
                                </div>
                                <div class="col-md-3 col-xs-12 padding-0">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 regionNameHdr">Stations for Region</span>
                                        <span class="padding-0 selectAll-clearBtns">
                                            <i class="fa fa-check-circle-o" aria-hidden="true" onclick="chooseAllRegionStations()"></i>
                                            <i class="fa fa-times-circle" aria-hidden="true" onclick="clearAllRegionStations()"></i>
                                            {{-- <button type="button" class="btn btn-default" onclick="chooseAllRegionStations()">Select All</button>
                                             <button type="button" class="btn btn-default" onclick="clearAllRegionStations()">Clear</button>--}}
                                        </span>
                                    </div>
                                    <ul class="region-station-list list-unstyled"></ul>
                                </div>
                                <div class="col-md-1 col-xs-4 padding-0">
                                    <img class="img-responsive proccedSelectionBtn" onclick="moveAllRegionStationsToSelectedList()" src="/assets/poassets/images/proccedSelectionBtn.png">
                                </div>
                                <div class="col-md-4 col-xs-12 padding-left">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="padding-0 regionStationHdr">Stations</span>
                                    </div>
                                    <ul class="selected-lists list-unstyled"></ul>
                                </div>