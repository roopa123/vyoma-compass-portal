  <h4 class="col-md-12 col-xs-12 colornew-gray">Add Campaign :</h4>
                    <div class="fieldsWrapper col-md-8 col-xs-8">

                        <div id="campaignerrors" class="col-md-12 col-xs-12 ">
                            <ul class="list-unstyled"></ul>
                        </div>

                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="CompaignName">Campaign Name</label>
                            <input id="CompaignName" name="CompaignName" type="text" class="col-md-12 col-xs-12 form-control" required />
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="SpotsPerDay">Spots/Day</label>
                            <input id="SpotsPerDay" name="SpotsPerDay" class="col-md-12 col-xs-12 form-control" type="text" required/>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="start-date-campaign">Start Date</label>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="start-date-campaign" type='text' class="form-control" name="start_date_campaign" readonly required/>
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="end-date-campaign">End Date</label>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="end-date-campaign" type='text' class="form-control" name="end_date_campaign" readonly required/>
                               <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-70 col-md-12 col-xs-12">
                            <div class="col-md-4 col-xs-12 text-center">
                                <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" onclick="resetCampaignForm()">Reset</a>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button id="campaignbtn" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;" >Save & Exit</button>
                            </div>
                            <div class="col-md-4 col-xs-12 text-center">
                                <button type="button" name="goToCreative" id="goToCreative" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;">Creative</button>
                            </div>
                        </div>
                    </div>