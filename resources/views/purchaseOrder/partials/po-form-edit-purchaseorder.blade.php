 <h4 class="col-md-12 col-xs-12 colornew-gray"> Edit Purchase Order :</h4>
                <div class="fieldsWrapper col-md-8 col-xs-8">
                      <input type="hidden" id="oldAdvertiserId" value="{{$PO->AdvertiserId}}">
                        <div id="poerrors" class="col-md-12 col-xs-12 errorsDisplay">
                            <ul class="list-unstyled"></ul></div>
                            
                        <div class="min-height-90 col-md-6 col-xs-12">
                        {!! Form::label('Sales Rep','Sales Rep',array('class' => 'color-blue')) !!}
                        {!! Form::select('salesrep', $salesRep, $PO->SalesRepId, ['class'=>'col-md-12 col-xs-12 form-control', 'name' => 'salesrep', 'id' => 'salesrep', 'required']) !!}
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="purchase_order_date">Purchase Order Date</label>
                            <?php $poDate = date('d-m-Y', strtotime($PO->PODate)); ?>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="purchase_order_date" type='text' class="form-control" name="purchase_order_date" value="{{$poDate}}" readonly required/>
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="startDate">Start Date</label>
                            <?php $startDate = date('d-m-Y', strtotime($PO->StartDate)); ?>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input  id="startDate" type='text' class="form-control" name="startDate" value="{{$startDate}}"  readonly required/>
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="endDate">End Date</label>
                            <?php $endDate = date('d-m-Y', strtotime($PO->EndDate)); ?>
                            <div class='input-group date col-md-12 col-xs-12'>
                                <input id="endDate" type='text' class="form-control" name="endDate" value="{{$endDate}}"  readonly required/>
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>
                        <?php 
                            if(Request::segment(2) == 'pocampaign') {
                                $poCampaignId = Request::segment(3);
                                $campaignId = Request::segment(6); ?>
                            <?php }
                            else {
                                $poCampaignId = Request::segment(2);
                                $campaignId = Request::segment(5);
                            }
                        ?>
                        <div class="min-height-90 col-md-6 col-xs-12">
                        {!! Form::label('Advertiser Name','Advertiser Name',array('class' => 'color-blue col-md-6 padding-0"')) !!}
                        <?php 

                            if(Request::segment(2) === 'pocampaign') {
                                $poCampaignId = Request::segment(3);
                                $campaignId = Request::segment(6); ?>
                            @can('Create Advertiser')
                            <a href="/advertiser/create/redirect/purchaseorder" class="btn addAdvertiserBtn col-md-6 pull-right" onclick="javascript:return redirectAdvertiser('/advertiser/create/redirect/pocampaignClone/{{$poCampaignId}}/clone/campaign/{{$campaignId}}');">
                                <i class="fa fa-plus-circle plus-icon"></i>&nbsp;Add Advertiser</a>
                            @endcan
                            <?php }
                            else {
                                $poCampaignId = Request::segment(2);
                                $campaignId = Request::segment(5); ?>
                                @can('Create Advertiser')
                                <a href="/advertiser/create/redirect/purchaseorder" class="btn addAdvertiserBtn col-md-6 pull-right" onclick="javascript:return redirectAdvertiser('/advertiser/create/redirect/purchaseorderEdit/{{$poCampaignId}}/edit/campaign/{{$campaignId}}');">
                                <i class="fa fa-plus-circle plus-icon"></i>&nbsp;Add Advertiser</a>
                                @endcan
                            <?php }
                            
                        ?>
                         {!! Form::select('advertiser', $activeAdvertiser, 
                        $PO->AdvertiserId, ['class'=>'col-md-12 col-xs-12 form-control', 'name' => 'advertiser', 'id' => 'advertiser', 'onchange' => 'checkAdvertiserHasCreative(this.value, 1)', 'required']) !!}
                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">

                         {!! Form::label('branch','Sales Branch',array('class' => 'color-blue')) !!}
                         {!! Form::select('branch', $branchList, $PO->SalesBranchId, ['class'=>'col-md-12 col-xs-12 form-control', 'name' => 'branch', 'id' => 'branch']) !!}

                        </div>
                        <div class="min-height-90 col-md-6 col-xs-12">
                            <label class="color-blue" for="roreference">RO Reference</label>
                            <input  name="roreference" id="roreference" class="col-md-12 col-xs-12 form-control" type="text" value="{{$PO->ROReferenceNo}}" />
                        </div>
                        <div class="min-height-70 col-md-12 col-xs-12">
                            <div class="col-md-6 col-xs-12 text-center">
                                <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" onclick="resetPOForm()">Reset</a>
                            </div>
                            <div class="col-md-6 col-xs-12 text-center">
                                <button id="goToCampign" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;"   value="continue">Campaign</button>
                            </div>
                        </div>
                    </div>
