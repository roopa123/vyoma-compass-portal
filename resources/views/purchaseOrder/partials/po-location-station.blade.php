 <div class="col-md-7 col-xs-12 padding-0">
                                    <div class="station-block-sorting-selectAll-clear-btns selectAll-clearBtns">
                                        <i class="fa fa-check-circle-o" aria-hidden="true" onclick="chooseAllStationStations()"></i>
                                        <i class="fa fa-times-circle" aria-hidden="true" onclick="unCheckAllStationStationsCheckBoxs()"></i>
                                        {{--<button type="button" class="btn btn-default" onclick="chooseAllStationStations()">Select All</button>
                                        <button type="button" class="btn btn-default" onclick="unCheckAllStationStationsCheckBoxs()">Clear</button>--}}
                                    </div>
                                    <div class="padding-0">
                                        <ul class="station-sortings-list list-unstyled col-md-12 col-xs-12"></ul>
                                    </div>
                                </div>
                                <div class="col-md-1 col-xs-4 padding-0">
                                    <img class="img-responsive proccedSelectionBtn" onclick="moveAllStationStationsToSelectedList()"
                                         src="/assets/poassets/images/proccedSelectionBtn.png">
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="col-md-12 col-xs-12 padding10-grayBG padding-0">
                                        <span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">
                                                <span class="pull-left">Stations</span>
                                                <span class="pull-right">
                                                    <span class="selectAll-clearBtns">
                                                        <i class="fa fa-times-circle stationClearBtn" title="Clear All" aria-hidden="true" onclick="clrFinalStationStationItems()"></i>
                                                        {{--<button type="button" class="btn btn-default" onclick="clrFinalStationStationItems()">Clear</button>--}}
                                                    </span>
                                                </span>
                                            </span>
                                    </div>
                                    <ul class="selected-station-station-lists station-block-final list-unstyled">
                                        <li>
                                            <ul class="selected-station-station-list-items list-unstyled"></ul>
                                        </li>
                                    </ul>
                                </div>