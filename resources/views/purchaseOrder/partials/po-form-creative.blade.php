 <h4 class="col-md-12 col-xs-12 colornew-gray">Add Creative:</h4>
                <div class="fieldsWrapper col-md-8 col-xs-8">
                    <div class="col-md-4 col-xs-12">
                        <label class="color-blue" for="creative-name">Creative Name</label>
                    <select class="col-md-12 col-xs-12 form-control" id="Creative" name="Creative" required>
                        </select>
                    </div>

                    <div id="creativeerrors" class="col-md-4 col-xs-12 errorsDisplay">
                        <ul class="list-unstyled"></ul>
                    </div>

                    <div class="margin-bottom-30 min-height-90 col-md-12 col-xs-12 POCreativeGridBlock">
                        <table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped table-condensed table-responsive table"></table>
                        <div id="CreativeGridPager"></div>
                    </div>

                    <div id="creativegriderrors" class="col-md-12 col-xs-12 errorsDisplay">
                        <ul class="list-unstyled"></ul></div>

                    <div class="min-height-70 col-md-12 col-xs-12">
                        <div class="col-md-4 col-xs-12 text-center">
                            <a class="btn btn-default canselAndResetButton col-md-12 col-xs-12 previous" onclick="resetCreativesForm()">Reset</a>
                        </div>
                        <div class="col-md-4 col-xs-12 text-center">
                            <button id="creativebtn" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;" >Save & Exit</button>
                        </div>
                        <div class="col-md-4 col-xs-12 text-center">
                            <button type="button" name="goToLocation" id="goToLocation" class="btn btn-default submitButton col-md-12 col-xs-12" style="width: 100%;">Location</button>
                        </div>
                    </div>
                </div>