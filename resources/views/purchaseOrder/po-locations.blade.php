<h4 class="text-muted col-md-12 col-xs-12">Locations</h4>
<section class="col-md-12 col-xs-12">
    <ul class="list-unstyled">
    @foreach($poCampaignDetails['purchaseOrders'] as $poLocations)
      @if($poLocations->CampaignId == $poCampaignDetails['poCampaign']->CampaignId)
        <?php
        $stateArray =array();
        $stateStationArray =array();
        $regionArray =array();
        $regionStationArray =array();
        $stationArray =array();
        ?>
        @foreach($poLocations->campaignLocation as $locations)
          @if(!is_null($locations->LocationType))
            @if($locations->LocationType == App\Services\Constants\AppConstants::STATE_GROUP)
            <?php 
              $stateArray[$locations->station->State->StateId] = 
                  $locations->station->State->StateName;
              $stateStationArray[$locations->station->State->StateId][$locations->station->StationId] = $locations->station->StationName;
            ?>
            @elseif($locations->LocationType == App\Services\Constants\AppConstants::REGION_GROUP)
            <?php 
              $regionArray[$locations->station->Region->RegionId] = $locations->station->Region->RegionName;
              $regionStationArray[$locations->station->Region->RegionId][$locations->station->StationId] = $locations->station->StationName;
            ?>
            @elseif($locations->LocationType == App\Services\Constants\AppConstants::STATION_GROUP)
            <?php 
              $stationArray[$locations->station->StationId] = $locations->station->StationName; 
            ?>
            @endif
          @endif
        @endforeach
          <div class="tab-pane col-md-12 col-xs-12 padding0" id="SummaryPreview">
            <!-- Region Type -->
            <?php if (!empty($regionArray)) { ?>
            @include('purchaseOrder.po-locations-region')
            <?php } ?>
            <!-- State Type -->
            <?php if (!empty($stateArray)) { ?>
            @include('purchaseOrder.po-locations-state')
            <?php } ?>
            <!-- Station Type -->
            <?php if (!empty($stationArray)) { ?>
            @include('purchaseOrder.po-locations-station')
            <?php } ?>
          </div>
      @endif
    @endforeach
  </ul>
</section>
