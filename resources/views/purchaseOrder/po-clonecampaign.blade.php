@extends('layouts.polayouts')
@section('title')
    Customer Commitment Crud
@endsection
@section('menu')
    @include('layouts.menu')
@endsection
@section('pomenu')
    @include('layouts.poeditmenu')
@endsection
@section('content')
    {{-- ajax form submission of po update --}}
    <script defer src="/assets/poassets/js/po-form-insertion-ajaxApi.js"></script>
    <script defer src="/assets/poassets/js/po-top-steps-navigation-validation.js"></script>
    <div id="preLoaderBlock" style="display:none"><img class="preLoader" src="/assets/poassets/images/loader.gif"></div>
    <!-- PO steps navigation page -->
    <div class="poContainer col-md-10 col-xs-12">
        <?php
        foreach ($POCampaignObj as $POCampaign) {}
        foreach($POObj as $PO) {}
        foreach($campaignObj as $campaign) {}
        ?>

        <form  name="purchaseorderform" id="purchaseorderform"  >
            <input type="hidden" name="purchaseorder_id" id="purchaseorder_id"
                   value="{{$PO->POId}}">
            <input type="hidden" name="creativeValues" value="{{$creativeDetails}}"
                   id="creativeValues">
            <input type="hidden" name="oldcreativeValues" value="{{$creativeDetails}}"
                   id="oldcreativeValues">
            <input type="hidden" name="creativeValuesJson" value="" id="creativeValuesJson">
            <input type="hidden" name="oldlocation" value='{{$locationDetailsJson}}'
                   id="oldlocation">
            <input type="hidden" name="location" value='{{$locationDetailsJson}}'
                   id="po-location">
            <input type="hidden" name="locationDifference" value=''
                   id="locationDifference">
            <input type="hidden" name="postjsondata" id="postjsondata">
            <input type="hidden" name="stepsComplete" id="stepsComplete" value="0">
            <input type="hidden" name="previewData" value="" id="previewData">
            <input type="hidden" name="mode" id="mode" value="3">
            <input type="hidden" name="originalpoid" id="originalpoid"
                   value="{{$POCampaign->OriginalPOId}}">
            <input type="hidden" name="campaignid" id="campaignid"
                   value="0">
            <input type="hidden" name="formurl" id="formurl" value="/addcampaign">
            <input type="hidden" name="binding" id="binding" value="1">



            <!-- Step1 PO Form-->
            <div class="row setup-content po-block" id="step-1">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset>
                    @include('purchaseOrder.partials.po-form-edit-purchaseorder')
                </fieldset>
            </div>
            <!-- Step2 Campaign Form-->
            <div class="row setup-content campaign-block" id="step-2">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-edit-campaign')
                </fieldset>
            </div>
            <!-- Step3 Creative Form-->
            <div class="row setup-content creative-block" id="step-3">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-creative')
                </fieldset>
            </div>
            <!-- Step4 Location Form-->
            <div class="row setup-content location-block" id="step-4">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset>
                    @include('purchaseOrder.partials.po-form-location')
                </fieldset>
            </div>
            <!-- Step5 Preview Page-->
            <div class="row setup-content preview-block" id="step-5">
                {{-- close back to grid --}}
                <a href="{{ URL::to('/purchaseorder') }}" title="go to PO grid"><i class="fa fa-times-circle close-back-to-grid" aria-hidden="true"></i></a>
                <fieldset><br>
                    @include('purchaseOrder.partials.po-form-preview')
                </fieldset>
            </div>
        </form>
    </div>
@stop

