<h4 class="text-muted col-md-12 col-xs-12">Creatives</h4>
<section class="col-md-12 col-xs-12">
	<ul class="list-unstyled">
		@foreach($poCampaignDetails['purchaseOrders'] as $poCreative)
			@if($poCreative->CampaignId == $poCampaignDetails['poCampaign']->CampaignId)
				<?php
				$creativeNames = "";
				?>
				<li class="padding-bottom"><strong>Creative:</strong>
			<span class="creativeNamesInPreview">
		@foreach($poCreative->campaignCreative as $creatives)
					<?php $creativeNames .=  $creatives->creative->CreativeName."," ?>
					<span>{{$creatives->creative->CreativeName}}<img class="creativeVidPlayIcon" src='/assets/poassets/images/locationCreativePreview.png' onclick='getCreativeUrl({{$creatives->creative->FileMetaData}})'></span>
				@endforeach
			</span>
				</li>
			@endif
		@endforeach
	</ul>
</section>
