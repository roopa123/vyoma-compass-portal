<div class="col-md-4 col-xs-12 padding-right-0">
<div class="selected-list-in-collapse">
<span class="col-md-12 col-xs-12 pull-left">State</span>
</div>
<ul class="selected-state-station-lists list-unstyled">
<?php
foreach ($stateArray as $stateId => $stateName) { ?>
<li class="state-selected-stations3">
<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">
<span class="margin-left-10">
<span class="stationNameHdr" title="{{$stateName}}">{{$stateName}} </span>
<span class="collapse-mode" onclick="toggleCollapseClass(this)"></span>
</span>
</span>
<ul class="state-selected-list-items3 list-unstyled" style="padding: 18px;">
<?php foreach($stateStationArray[$stateId] as $stationId => $stationName) { ?>
<li class="">{{$stationName}}</li>
<?php } ?>
</ul>
</li>
<?php } ?>
</ul>
</div>