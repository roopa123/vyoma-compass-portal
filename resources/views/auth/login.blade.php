@extends('layouts.apptemplate')

@section('title')
    Login
@endsection

@section('menu')
    @include('layouts.staticHeader')
@endsection

@section('content')
    <br><br>

    <div class="logo"></div>

    <div class="loginContainer col-md-4 col-xs-12">

        <form method="POST" action="/auth/login">
            {!! csrf_field() !!}
            <fieldset>
                <h2 class="loginLegend text-center">Login</h2>

                <div class="form-group label-floating">
                    <span class="col-md-2 col-xs-2 usernameLogo"></span>
                    <div class="col-md-9 col-xs-9 min-height-100">
                        <label class="usernameLabel control-label" for="username">User Name</label>
                        <div class="input-group col-md-12 col-xs-12">
                            <input id="username" class="form-control" placeholder ="Username" name="email" value="{{ old('email') }}" type="email">
                        </div>
                    </div>
                </div>

                <div class="form-group label-floating min-height-100">
                    <span class="col-md-2 col-xs-2 passwordLogo"></span>
                    <div class="col-md-9 col-xs-9">
                        <label class="passwordLabel control-label" for="password">Password</label>
                        <div class="input-group col-md-12 col-xs-12">
                            <input name="password" id="password" class="form-control" type="password" placeholder ="Password">
                        </div>
                    </div>
                </div>

                {{--<div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder ="Username">
                </div>--}}
                <div class="form-group text-center ">
                  <h5 class="col-md-12 col-xs-12 forgotPassLink"><a href="{{  url('password/email') }}" >Forgot Password?</a></h5>
                    <button class="btn btn-default loginSubmitBtn" type="submit">Login</button>
                </div>
            </fieldset>
        </form>
    </div>
@stop
