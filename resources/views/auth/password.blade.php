@extends('layouts.apptemplate')

@section('title')
    Forgot Password
@endsection

@section('menu')
    @include('layouts.staticHeader')
@endsection

@section('content')
    <br><br>

    <div class="logo"></div>

    <div class="loginContainer col-md-4 col-xs-12">
        <form method="POST" action="/password/email">
            <fieldset>
                {!! csrf_field() !!}

                @if (count($errors) > 0)
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                <div class="form-group min-height-100">
                    <label class="usernameLabel control-label" for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group text-center min-height-100">
                    <button class="btn btn-default loginSubmitBtn passwordResetBtn" type="submit">Send Password Reset Link</button>
                </div>
            </fieldset>
        </form>
    </div>
@stop
