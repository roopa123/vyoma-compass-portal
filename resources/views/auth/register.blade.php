@extends('layouts.apptemplate')

@section('title')
    Register
@endsection

@section('menu')
    @include('layouts.staticHeader')
@endsection

@section('content')
    <br><br>

    <div class="adminPanel form-group NameRole usersTable">
        <form method="POST" action="/auth/register">
            {!! csrf_field() !!}
            <fieldset>

                <div class="form-group min-height-100 col-md-6 col-xs-12">
                    <label class="usernameLabel control-label" for="name">Name</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>

                <div class="form-group min-height-100 col-md-6 col-xs-12">
                    <label class="usernameLabel control-label" for="Email">Email</label>
                    <input id="Email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group min-height-100 col-md-6 col-xs-12">
                    <label class="usernameLabel control-label" for="password">Password</label>
                    <input id="password" type="password" class="form-control" name="password">
                </div >

                <div class="form-group min-height-100 col-md-6 col-xs-12">
                    <label class="usernameLabel control-label" for="password_confirmation">Confirm Password</label>
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">
                </div>

                <div class="form-group text-center min-height-100">
                    <button class="btn btn-default loginSubmitBtn" type="submit">Register</button>
                </div>
            </fieldset>
        </form>
    </div>
@stop
