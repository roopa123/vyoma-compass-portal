@extends('layouts.apptemplate')

@section('title')
  List of Stations
@endsection

@section('menu')
  @include('layouts.menu')
@endsection

@section('content')
  @include('layouts.validationAlert')

  <div class="">
    <div class="">
      <table id="jqGridStation" class="table "></table>
    </div>

    <script>
      $(document).ready(function(){
        // Prevent Actions Column Export
        $('.ui-icon-refresh').click(function() { location.reload();}) ;
      });

      var myfilter = { groupOp: "AND", rules: []};
      var branchName = '{{$requestType->BranchName or NULL}}';
      var regionName = '{{$requestType->RegionName or NULL}}';
      var ftype ='' , fvalue = '', url = '';
      @if(isset($requestType->BranchName))
              ftype = 'BranchName';
      fvalue = '{{$requestType->BranchName}}';
      myfilter.rules.push({field:ftype,op:"eq",data:fvalue},{field:'Station.Status',op:"eq",data:"A"});
      @elseif(isset($requestType->RegionName))
              ftype = 'RegionName';
      fvalue = '{{$requestType->RegionName}}';
      myfilter.rules.push({field:ftype,op:"eq",data:fvalue},{field:'Station.Status',op:"eq",data:"A"});
      @endif
    </script>
    @can('Show Station')
    <form method="POST" action="/station-grid" accept-charset="UTF-8" id="jqGridStationExportForm">
      {{ csrf_field() }}
      <input id="jqGridStationName" name="name" type="hidden" value="jqGridStation">
      <input id="jqGridStationModel" name="model" type="hidden">
      <input id="jqGridStationExportFormat" name="exportFormat" type="hidden" value="xls">
      <input id="jqGridStationFilters" name="filters" type="hidden">
      <input id="jqGridStationPivotFlag" name="pivot" type="hidden" value="">
      <input id="jqGridStationRows" name="pivotRows" type="hidden">
      <input name="fileProperties" type="hidden" value='{"FileName":"Stations"}'>
      <input name="sheetProperties" type="hidden" value='[]'>
      <input name="groupingView" type="hidden" value='[]'>
      <input name="groupHeaders" type="hidden" value='[]'>
    </form>
    <table id="jqGridStation"></table><div id="jqGridStationPager"></div>
    <script type="text/javascript">
      jQuery("#jqGridStation").jqGrid(
              {
                "datatype":"json",
                "mtype":"POST",
                "treeGridModel":"adjacency",
                "url": '{{ $buildUrl }}' ,
                "editurl":"/station/crud",
                "rowNum":20,"rownumbers":true,
                "autowidth": true,
                "height": 477,
                "caption":"List of Stations",
                "viewrecords":true,
                "altRows":true,
                "altclass":'myAltRowClass',
                cmTemplate:{resizable:false},
                "gridComplete":stationAutoComplete,
                "loadComplete": dynamicHeight,
                "colModel":[
                  {"searchoptions":{"sopt":["cn"]},"index":"StationId","hidden":true, "editable":true,"key":true,"name":"StationId"},
                  {"searchoptions":{"sopt":["cn"]},"label":"Station Name","index":"StationName", "align":'left',"editable":true,
                    "editrules":{"required":true},"formatter":"showlink",
                    "formatoptions":{"baseLinkUrl":"/host",
                      "idName":"station"},"name":"StationName"},
                  {@if(isset($requestType->BranchName)) 'searchoptions':{'defaultValue':branchName},@endif
                  "label":"Branch Name","index":"BranchName","editable":true,"edittype":"select", "align":'left',
                    "editoptions":{"value": {!!json_encode($listOfBranches)!!}},
                    "name":"BranchName"},
                  {@if(isset($requestType->RegionName)) 'searchoptions':{'defaultValue':regionName},@endif
                  "label":"Region Name", "index":"RegionName","editable":true,"edittype":"select", "align":'left',
                    "editoptions":{"value":{!!json_encode($listOfRegions)!!}},"name":"RegionName"},
                  {"searchoptions":{"sopt":["cn"]},
                    "label":"State Name","index":"StateName","editable":true,"edittype":"select", "align":'left',
                    "editoptions":{"value":{!!json_encode($listOfStates)!!},"sortname":"StateName","sortorder":"asc"},"name":"StateName"},
                  {"stype": '{{ $stype }}',
                    "searchoptions":{"sopt":["eq", "ne"], "value":{!!json_encode($searchOption)!!}, 'defaultValue':{!!json_encode($search)!!}},
                    "label":"Status","index":"Station.Status","editable":true,"edittype":"select", "align":'left', 'formatter':statusDisplay,
                    "editoptions":{"value":'A:Active;I:Inactive'},"name":"Station.Status"}
                ],
                "pager":"jqGridStationPager",
                'postData': {
                  filters: JSON.stringify(myfilter)
                },

              }).navGrid("#jqGridStationPager",
              {"add":true,"edit":true,"del":false,"search":false, 'view':true,"refresh":false},

              {"closeAfterEdit":true, "editCaption":"Edit Station Details","afterSubmit":afterSubmitEvent, "drag":false, "resize":false, "left":400, "top":193, "beforeShowForm":editSelectForDropdown},

              {"closeAfterAdd":true,"addCaption":"Add Station","afterSubmit":afterSubmitEvent, "drag":false, "resize":false, "left":400, "top":193, "beforeShowForm":addSelectForDropdown},

              {"closeAfterEdit":true,"caption":"Delete Station Info","afterSubmit":afterSubmitEvent, "drag":false, "resize":false, "left":400, "top":193},
              {},
              {"closeAfterEdit":true,"caption":"View Station Details", "drag":false, "resize":false, "left":400, "top":193} );

      jQuery("#jqGridStation").jqGrid("navButtonAdd", "#jqGridStationPager",
              {
                "id": "jqGridStationXlsButton", "caption":"xls", "buttonicon":"ui-icon-arrowthickstop-1-s",
                onClickButton:function() {
                  var headers = [], rows = [], row, cellCounter, postData;
                  jQuery("#jqGridStationModel").val(JSON.stringify(jQuery("#jqGridStation").getGridParam("colModel")));
                  postData = jQuery("#jqGridStation").getGridParam("postData");

                  if(postData["filters"] != undefined)
                  {
                    jQuery("#jqGridStationFilters").val(postData["filters"]);
                  }

                  jQuery("#jqGridStationExportFormat").val("xls");
                  jQuery("#jqGridStationExportForm").submit();
                }
              });
      jQuery("#jqGridStation").jqGrid("navButtonAdd", "#jqGridStationPager",
              {"id": "jqGridStationCsvButton", "caption":"csv", "buttonicon":"ui-icon-arrowthickstop-1-s",
                "onClickButton":function(){
                  var headers = [], rows = [], row, cellCounter, postData;
                  jQuery("#jqGridStationModel").val(JSON.stringify(jQuery("#jqGridStation").getGridParam("colModel")));
                  postData = jQuery("#jqGridStation").getGridParam("postData");
                  if(postData["filters"] != undefined)
                  {
                    jQuery("#jqGridStationFilters").val(postData["filters"]);
                  }
                  jQuery("#jqGridStationExportFormat").val("csv");
                  jQuery("#jqGridStationExportForm").submit();}
              });
      jQuery("#jqGridStation").jqGrid("navSeparatorAdd", "#jqGridStationPager");
      jQuery("#jqGridStation").jqGrid("filterToolbar", {"stringResult":true});
      jQuery("#jqGridStation").jqGrid("navButtonAdd", "#jqGridStationPager",{"caption":"", "buttonicon":"ui-icon-pin-s",
        "onClickButton":function(){ jQuery("#jqGridStation")[0].toggleToolbar();} });
      jQuery("#jqGridStation").jqGrid("navButtonAdd", "#jqGridStationPager",
              {"caption":"", "buttonicon":"ui-icon-refresh",
                "onClickButton":function(){ jQuery("#jqGridStation")[0].clearToolbar();} });
      jQuery("#jqGridStation").jqGrid("navSeparatorAdd", "#jqGridStationPager");
      jQuery("#jqGridStation").jqGrid("setGroupHeaders", {"useColSpanStyle":true});
      jQuery("#jqGridStation").jqGrid("setFrozenColumns");
      jQuery("#jqGridStation").jqGrid();

      function addSelectForDropdown() {
        // Sorting for Branch name //
        $('#BranchName').append($('#BranchName option').sort(function(a, b) {
          var getFirstValueText = $(a).text().toUpperCase(), getSecondValueText = $(b).text().toUpperCase();
          return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
        })).prepend("<option value='' selected>Select</option>");
        // Sorting for Region name //
        $('#RegionName').append($('#RegionName option').sort(function(a, b) {
          var getFirstValueText = $(a).text().toUpperCase(), getSecondValueText = $(b).text().toUpperCase();
          return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
        })).prepend("<option value='' selected>Select</option>");
        //Sorting for State name //
        $('#StateName').append($('#StateName option').sort(function(a, b) {
          var getFirstValueText = $(a).text().toUpperCase(), getSecondValueText = $(b).text().toUpperCase();
          return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
        })).prepend("<option value='' selected>Select</option>");
      }

      function editSelectForDropdown() {
        // Get the deafult selected values
        branchval = $("#BranchName").val();
        regval = $("#RegionName").val();
        stateval = $("#StateName").val();

        // Sort options in alphabetical order
        $('#BranchName').sortOptions(branchval);
        $('#RegionName').sortOptions(regval);
        $('#StateName').sortOptions(stateval);

      }

      //Role based permissions
      $('#add_jqGridStation').hide();
      $('#edit_jqGridStation').hide();
      $('#view_jqGridStation').hide();
      @can('Create Station')
        $('#add_jqGridStation').show();
      @endcan
      @can('Update Station')
        $('#edit_jqGridStation').show();
      @endcan
      @can('Show Station')
        $('#view_jqGridStation').show();
      @endcan

    </script>
    @endcan
  </div>
@stop
