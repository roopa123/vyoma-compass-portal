<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'masterinvalid' => 'Requested URL needs Host to be Master',
    'querystringinvalid' => 'Query String is Invalid',
    'appversionalreadyupdated' => 'App version is already updated',
    'confversionalreadyupdated' => 'Config version is already updated',
    'invalidstatus' => 'Not able to Online Because not valid combination of Isregisterd and Isonline',
    'invalidregister' => 'Not able to set Register false because it is online',
    'invalidonline' => 'Not able to set Online true because it is not registered',
    'invalidtoken' => 'has invalid  Value.',
    'tokennotempty' => 'should not be empty.',
    'duplicateregister' => 'Duplicate Register of Host',
    'masteralreadypresent' => 'Master alreay present',
    'cantchangebothtoplayers' => 'Cannot change Both type to Player',
    'cantchangemastertoplayers' => 'Cannot change Master type to Player',
    'cantchangeplayertoboth' => 'Cannot Change Player to Both, it has Master',
    'tokenmissing' => 'Token is Missing',
    'invalidpostkeys' => 'Some not Allowable keys are present in POST JSON',
    'invalidpostjsonstring' => 'Invalid POST Json String',
    'notavalidip' => 'IP Address is not a valid',
    'notarballurl' => 'No Tarball URL is present',
    'invalidapp' => 'Invalid App Version',
    'notupdatepublicip' => 'you are not allowed to update public_ip for players',
    'cantchangemastertoplasma' => 'Cannot change Master type to Plasma',
    ];
