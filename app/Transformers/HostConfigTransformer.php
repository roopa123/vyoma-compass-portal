<?php

namespace App\Transformers;

use App\Models\Host;
use App\Models\Code;
use League\Fractal\TransformerAbstract;
use App\Services\Constants\ApiConstants;
use App\Commands\HostTransformerTrait;

class HostConfigTransformer extends TransformerAbstract
{
    use HostTransformerTrait;

    private $host;

    /**
     * Turn this item object into a generic array.
     *
     * @return array
     */
    public function transform(Host $host)
    {
        $localIp = $host->LocalIPAddress;
        
        // No need for Transforming the DB Field Values
        $hdmi = $host->HdmiMode;
        $screenRatio = $host->ScreenRatio;
        $cableType = $host->CableType;
        $logPush = $host->IsPlayLogUpdateEnabled;
        $contentUpdate = $host->IsContentUpdateEnabled;
        $readtty = $host->IsReadTTYLogEnabled;
        $uploadCSV = $host->IsTicketLogUpdateEnabled;
        $screenShot = $host->IsScreenShotEnabled;
        $routerReboot = $host->IsRouterRebootEnabled;
        $serverIP = $this->getCodeValue('SERVER_IP');
        $serverPort = $this->getCodeValue('SERVER_PORT');
        // Need Transformation of DB Field Values
        $masterIp = $this->setMasterIpAddress($host);
        $deployState = $this->getStateCodeforHost($host);
        $role = $this->setHostRole($host);
        $countertype = $this->setCounterType($host);
        $playerWinSize = $this->getPlayerWinSize($host);
        list($rbtHour, $rbtMin, $screenOffHour, $screenOffMin) = $this->getRebootTimings($host);
        //only for PRS counters
        if ($countertype == ApiConstants::COUNTER_PRS) {
            if (is_null($host->SunScreenEndTIme) ? $sunScreenOffHour = $host->SunScreenEndTIme : $sunScreenOffHour = date('H', strtotime($host->SunScreenEndTIme)));

            if (is_null($host->SunScreenEndTIme) ? $sunScreenOffMin = $host->SunScreenEndTIme : $sunScreenOffMin = date('i', strtotime($host->SunScreenEndTIme)));
            //$sunScreenOffHour = date('H', strtotime($host->SunScreenEndTIme));
            //$sunScreenOffMin = date('i', strtotime($host->SunScreenEndTIme));
        }
        //PlaylogupdateSchedule
        if ($logPush == ApiConstants::ENABLE) {
            list($logPushMin, $logPushHour) = $this->getPlayLogSchedule($host);
        }
        //ContentUpdateSchedule
        if ($contentUpdate == ApiConstants::ENABLE) {
            $contentUpdateSchedule = $this->getContentSchedule($host);
        }
        //TicketLogUpdateSchedule
        if ($uploadCSV == ApiConstants::ENABLE) {
            $uploadCSVSchedule = $this->getTicketSchedule($host);
        }
        //ScreenShotSchedule
        if ($screenShot == ApiConstants::ENABLE) {
            list($screenShotMin, $screenShotHour) = $this->getScreenShotSchedule($host);
        }
        //RouterRebootSchedule
        if ($routerReboot == ApiConstants::ENABLE) {
            list($routerRebootMin, $routerRebootHour) = $this->getRouterRebootSchedule($host);
        }
        $response = [];
        $response['HOSTNAME'] = "'".$host->HostName."'";
        $response['IP_ADDRESS'] = $localIp;
        $response['MASTER_IP'] = $masterIp;
        $response['ROLE'] = "'".$role."'";
        $response['COUNTER_TYPE'] = "'".$countertype."'";
        $response['DEPLOY_STATE'] = $deployState;
        $response['PLAYER_WIN'] = $playerWinSize;
        
        /* Screen Start Time and End Time in DB*/
        $response['REBOOT_HOUR'] = $rbtHour;
        $response['REBOOT_MIN'] = $rbtMin;
        $response['SCREENOFF_HOUR'] = $screenOffHour;
        $response['SCREENOFF_MIN'] = $screenOffMin;
        
        /* Sunday Screen start and End Time */
        if ($countertype == ApiConstants::COUNTER_PRS) {
            $response['SCREENOFF_HOUR_SUN'] = $sunScreenOffHour;
            $response['SCREENOFF_MIN_SUN'] = $sunScreenOffMin;
        }
        if ($hdmi != ApiConstants::DISABLE) {
            $response['HDMI_MODE'] = $hdmi;
        }
        
        /* Router Details */
        $role = $host->MasterOrPlayer;
        if ($role == ApiConstants::HOST_MASTER || $role == ApiConstants::HOST_BOTH) {
            $response['ROUTER_REBOOT'] = $host->IsRouterRebootEnabled;
            $response['ROUTER_USERNAME'] = $host->RouterUserName;
            $response['ROUTER_PASSWORD'] = $host->RouterPassword;
            if ($routerReboot == ApiConstants::ENABLE) {
                $response['ROUTER_REBOOT_MIN'] = $routerRebootMin;
                $response['ROUTER_REBOOT_HOUR'] = $routerRebootHour;
            }
            $response['LOG_PUSH'] = $logPush;
            if ($logPush == ApiConstants::ENABLE) {
                $response['LOG_PUSH_MIN'] = $logPushMin;
                $response['LOG_PUSH_HOUR'] = $logPushHour;
            }
            $response['CONTENT_UPDATE'] = $contentUpdate;
            if ($contentUpdate == ApiConstants::ENABLE) {
                $response['UPDATE_SCHEDULE'] = $contentUpdateSchedule;
            }
        }
        if ($role == ApiConstants::HOST_PLAYER || $role == ApiConstants::HOST_BOTH) {
            $response['SCREEN_RATIO'] = $screenRatio;
            $response['READTTY_LOG'] = $readtty;
            if ($response['READTTY_LOG'] == ApiConstants::ENABLE) {
                 $response['UPLOAD_CSV'] = $uploadCSV;
                if ($uploadCSV == ApiConstants::ENABLE) {
                    $response['UPLOAD_CSV_SCHEDULE'] = $uploadCSVSchedule;
                }
            }
            $response['THANKS_MSG'] = $host->IsThxMessageEnabled;
        }
        //print_r($role); exit;
        $response['MMONIT_REPORT'] = $host->IsMonitEventEnabled;
        $response['MONIT_EVENTQUEUE'] = $host->IsMonitQueueEnabled;
        $response['ENABLE_CRON'] = $host->IsCronEnabled;
        $response['CABLE_TYPE'] = $cableType;
        $response['DEBUG_MODE'] = $host->IsDebugModeEnabled;
        $response['ENABLE_SYSSTAT'] = $host->IsSysStatEnabled;
        $response['SCREENSHOT'] = $screenShot;
        if ($screenShot == ApiConstants::ENABLE) {
            $response['SCREENSHOT_SCHEDULE_HOUR'] = $screenShotHour;
            $response['SCREENSHOT_SCHEDULE_MIN'] = $screenShotMin;
        }
        $response['SERVER_IP'] = $serverIP;
        $response['SERVER_PORT'] = $serverPort;

        return $response;
    }
}
