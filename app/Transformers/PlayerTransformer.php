<?php

namespace App\Transformers;

use App\Models\Host;
use League\Fractal\TransformerAbstract;
use App\Services\Constants\ApiConstants;
use App\Transformers\HostConfigTransformer;
use App\Transformers\HostTransformer;
 
class PlayerTransformer extends HostTransformer
{
    /**
     * @var Host
    */
    private $host;
    
    public function __construct($paramArray = array())
    {
        parent::__construct($paramArray);
    }
    
    public function transform(Host $host)
    {
        $response = [];
        $response['hostname'] = $host->HostName;
        $requestedParam =[];
        if (in_array(ApiConstants::PARAM_INLCUDE_PLAYERS, $this->paramArray)) {
            if (in_array(ApiConstants::PARAM_ALL, $this->paramArray)) {
                $configResponse = $this->createConfig($host);
                return $this->respondWithAllParams($host, $configResponse);
            }
            $requestedParam = $this->respondWithRequestedParams($host);
            if (in_array(ApiConstants::PARAM_CONFIG, $this->paramArray)) {
                $response[ApiConstants::PARAM_CONFIG] = $this->createConfig($host);
            }
        }
        return array_merge($response, $requestedParam);
    }

    /* To include Config object for Players */
    public function createConfig($host)
    {
        /* Create object for Config Tokens*/
        $hostConfigTransformer = new HostConfigTransformer();
        /* return the array of tokens */
        return $hostConfigTransformer->transform($host);
    }
}
