<?php

namespace App\Transformers;

use App\Models\Host;
use League\Fractal\TransformerAbstract;
use App\Services\Constants\ApiConstants;
use App\Commands\HostTransformerTrait;

class HostTransformer extends TransformerAbstract
{


    use HostTransformerTrait;
    /**
     * @var Host
    */
    private $host;

    protected $paramArray;
    
    protected $availableIncludes = [
        'config',
        'players_list',
    ];
    
    public function __construct($paramArray = array())
    {
        $this->paramArray = $paramArray;
    }

    public function transform(Host $host)
    {
        return $this->prepareResponse($host, $this->paramArray);
    }

    public function prepareResponse($host, $paramArray)
    {
        if (in_array(ApiConstants::PARAM_ALL, $this->paramArray)) {
            $hostConfigTransformer = new HostConfigTransformer();
            $configResponse = $hostConfigTransformer->transform($host);
            return $this->respondWithAllParams($host, $configResponse);
        }
        return $this->respondWithRequestedParams($host);
    }

    /* Include config object in the JsonResponse */
    public function includeConfig(Host $host)
    {
        return $this->item($host, new HostConfigTransformer());
    }

    /* Include players_list which holds array of json objects */
    public function includePlayersList(Host $host)
    {
        $hostCollection = Host::where('HostMasterId', $host->HostId)->where('MasterOrPlayer', 'P')->get();
        return $this->collection($hostCollection, new PlayerTransformer($this->paramArray));
    }
}
