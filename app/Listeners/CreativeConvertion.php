<?php

namespace App\Listeners;

use App\Events\CreativeWasUploaded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreativeConvertion implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreativeWasUploaded  $event
     * @return void
     */
    public function handle(CreativeWasUploaded $event)
    {
        //
    }
}
