<?php

namespace App\Jobs;

use App\Jobs\Job;
use Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Creative;
use App\Repositories\FfmpegRepository;
use App\Repositories\CreativeRepository;

class CreativeConveter extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreativeRepository $creativeRepo, FfmpegRepository $ffmpegRepo)
    {
       Log::info("Triggering Convertaion ");
        // load Creative list Asc
        //$creative = $creativeRepo->findCreativeConvertionItem();
        if(!isset($creative)) {
            echo 'No input file for proccess ';
            return false;
        }
        Log::info('Converting '. $creative->CreativeName. ' => Path => '. $creative->FileName);
        $converationType = \Config::get('ffmpeg.convert_into');
        // Trigger convertion process
        //$videoDetails = $ffmpegRepo->convert($creative->CreativeId, $creative->InputFileName, $converationType);

        $creativeArray = array();
        $creativeArray['Status'] = 'R';

        // Store converted video information
        /*$creativeRepo->updateCreative($creativeArray, $creative->CreativeId,
            $videoDetails);*/
        Log::info("Convertaion Completed ");
    }
}
