<?php

namespace App\Jobs;

use App\Jobs\Job;
use Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Creative;
use App\Repositories\GoogleDriveUpload;
use App\Repositories\CreativeRepository;

class GoogleDriveApi extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     * @param Repository $creativeRepo
     * @param GoogleDriveUpload $googleRepo
     * @return Array $googleUploadInfo
     */
    public function handle(CreativeRepository $creativeRepo, GoogleDriveUpload $googleRepo)
    {
       Log::info("Creative GDrive Started ");
        // load Creative list Asc
        $creative = $creativeRepo->findCreativeConvertionItem();
        if(!isset($creative)) {
            echo 'No input file for upload ';
            return false;
        }
        Log::info('Uploading '. $creative->CreativeName. ' => Path => '. $creative->FileName);
        $cretiveFileName = $creative->FileName;
        //Google drive upload
        $googleUploadInfo = $googleRepo->googleDriveUpload($creative->CreativeId, $cretiveFileName);
        
        Log::info("File upload Completed ");

        $creativeArray = array();
        $creativeArray['Status'] = 'U';
        $creativeArray['GStatus'] = 'U';
        $creativeArray['FileName'] = $cretiveFileName;
        Log::info("Update DB status ");

        // Store converted video information
        $creativeRepo->updateCreative($creativeArray, $creative->CreativeId,
            $googleUploadInfo);
        
        Log::info("Creative GDrive Completed ");
    }
}
