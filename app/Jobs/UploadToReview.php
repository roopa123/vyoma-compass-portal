<?php

namespace App\Jobs;

use App\Jobs\Job;
use Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Creative;
use App\Repositories\CreativeRepository;

class UploadToReview extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.

     * @param Repository $creativeRepo
     * @param GoogleDriveUpload $googleRepo
     * @return Array $googleUploadInfo
     */
    public function handle(CreativeRepository $creativeRepo)
    {
        $creative = $creativeRepo->findStatusForConvertion();
        $creativeArray = array();
        if(empty($creative)) {
            echo response()->json(['msg'=>'No Records found']);
            return false;
        }        
        foreach ($creative as $creativeArr) {
            $creativeArray['Status'] = 'R';
            $creativeArray['GStatus'] = 'R';
            // Store converted video information
            $creativeRepo->updateCreative($creativeArray, $creativeArr->CreativeId);
        } 
    }
}
