<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignCreative extends Model
{
    /**
    *The table name associated with model
    * @var string
    */
    protected $table = 'CampaignCreative';

    /**
    *The defining the primary key of the table
    * @var string
    */
    protected $primaryKey = 'CampaignCreativeId';

    /**
    * Defining the timestamps as false for the Compaign table
    */
    public $timestamps = false;

    protected $fillable = ['CampaignId', 'CreativeId', 'CreatedAt', 'UpdatedAt', 'Status', 'UserId'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'UserId');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'CampaignId');
    }

    public function creative()
    {
        return $this->belongsTo('App\Models\Creative', 'CreativeId');
    }
}