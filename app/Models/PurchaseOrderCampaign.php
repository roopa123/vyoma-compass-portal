<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Constants\AppConstants;

class PurchaseOrderCampaign extends Model
{
    /**
    *The table name associated with model
    * @var string
    */
    protected $table = 'POCampaign';

    /**
    *The defining the primary key of the table
    * @var string
    */
    protected $primaryKey = 'POCampaignId';

    /**
    * Defining the timestamps as false for the CompaignCreative table
    */
    public $timestamps = false;

    protected $fillable = ['OriginalPOId', 'OriginalCampaignId', 'POId', 'CampaignId',
        'CreativeCampaignId', 'LocCampaignId', 'CreatedAt', 'UpdatedAt', 'Status', 'UserId'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'UserId');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'CampaignId');
    }

    public function purchaseOrder()
    {
        return $this->belongsTo('App\Models\PurchaseOrder', 'POId');
    }

    public function campaignCreative()
    {
        return $this->hasMany('App\Models\CampaignCreative', 'CampaignId', 'CreativeCampaignId');
    }

    public function campaignLocation()
    {
        return $this->hasMany('App\Models\CampaignLocation', 'CampaignId', 'LocCampaignId');
    }


    public function scopeActive($query)
    {
        return $query->where('Status', 'A');
    }

    public function scopeStatusIn($query, $status)
    {
        return $query->whereIn('Status', $status);
    }

    public function scopeInProcess($query)
    {
        return $query->where('Status', 'P');
    }

    public function scopeHistory($query)
    {
        return $query->where('Status', 'H');
    }

    public function scopeRetrieveRecordStatusIn($query)
    {
        /*return $query->Where(function($query)
                    {
                        $query->where('Status', AppConstants::PURCHASE_ORDER_PARTIAL_STATUS)
                              ->orWhere('Status',AppConstants::PURCHASE_ORDER_ACTIVE);
                    });*/
        return $query->where('Status', AppConstants::PURCHASE_ORDER_PARTIAL_STATUS)
                              ->orWhere('Status',AppConstants::PURCHASE_ORDER_ACTIVE);
    }

}
