<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    /**
     * The database table used by the Branch model.
     *
     * @var string
     */
    protected $table = 'Branch';

        /**
    *The defining the primary key of the table
    * @var string
    */
    protected $primaryKey = 'BranchId';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['BranchName', 'Status'];

    public function Station()
    {
        return $this->hasMany('Station');
    }
}
