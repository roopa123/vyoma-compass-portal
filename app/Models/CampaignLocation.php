<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignLocation extends Model
{
    /**
    *The table name associated with model
    * @var string
    */
    protected $table = 'CampaignLocation';

    /**
    *The defining the primary key of the table
    * @var string
    */
    protected $primarykey = 'CampaignLocationId';

    /**
    * Defining the timestamps as false for the Compaign table
    */
    public $timestamps = false;

    protected $fillable = ['CampaignId', 'LocationType', 'LocationId', 'Status', 'UserId', 'CreatedAt', 'UpdatedAt'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'UserId');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'CampaignId');
    }

    public function station()
    {
        return $this->belongsTo('App\Models\Station', 'LocationId');
    }
    
}