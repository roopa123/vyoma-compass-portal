<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    /**
    *The table name associated with model
    * @var string
    */
    protected $table = 'PO';

    /**
    *The defining the primary key of the table
    * @var string
    */
    protected $primaryKey = 'POId';

    /**
    * defining the timestamps as false for the PO table
    */
    public $timestamps = false;

    /**
    * constant for the Updated at
    */
    const UPDATED_AT = 'UpdatedAt';

    protected $fillable = ['SalesRepId', 'PODate', 'StartDate', 'EndDate',
        'AdvertiserId', 'SalesBranchId', 'CreatedAt', 'UpdatedAt', 'Status', 'UserId'];

    public function advertiser()
    {
        return $this->belongsTo('App\Models\Advertiser', 'AdvertiserId');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'SalesBranchId');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'UserId');
    }

    public function salesRep()
    {
        return $this->belongsTo('App\Models\User', 'SalesRepId');
    }

}
