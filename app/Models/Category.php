<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Category';
    protected $primaryKey = 'CategoryId';

    /**
     * The name of the "Updated At " column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['CategoryName', 'CategoryType', 'UpdatedAt', 'Status', 'UpdatedBy'];

    public $timestamps = false;
}
