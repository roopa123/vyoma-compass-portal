<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Region';

    protected $primaryKey = 'RegionId';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['RegionName', 'Status'];

    public function Station()
    {
        return $this->hasMany('App\Models\Station', 'RegionId');
    }
}
