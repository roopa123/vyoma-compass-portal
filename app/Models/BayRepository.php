<?php
namespace App\Models;

use Illuminate\Database\DatabaseManager as DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use DB;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class BayRepository extends EloquentRepositoryAbstract
{

    public function __construct(Model $Model)
    {
        if (request()->segment(2) === 'region') {
            $RegionId = request()->segment(3);
            $branchId = request()->segment(5);
            $this->Database =  Bay::select('Bay.BayId', 'Station.StationName' ,'BayLocation.BayLocationDesc', 'Bay.App', 'Bay.StartTime', 'Bay.EndTIme', 
                            DB::raw('if(Bay.Status = "A", "Active", "Inactive") as "Bay.Status"'))
                        ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                        ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                        ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
                        ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                        ->where('Station.RegionId', $RegionId)
                        ->where('Station.BranchId', $branchId)
                        ->where('Station.Status', 'A')
                        ->where('Region.Status', 'A')
                        ->where('Bay.Status', 'A');
        }
        else if (request()->has('station_id')){
            $StationId = request()->get('station_id');
            $this->Database = DB::table('Bay')
                        ->select('Bay.BayId', 'Station.StationName' ,'BayLocation.BayLocationDesc', 'Bay.App', 'Bay.StartTime', 'Bay.EndTIme',
                            DB::raw('if(Bay.Status = "A", "Active", "Inactive") as "Bay.Status"'))
                        ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                        ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                        ->where('Station.StationId',$StationId);
        }
        else {
            $this->Database = DB::table('Bay')
                        ->select('Bay.BayId', 'Station.StationName' ,'BayLocation.BayLocationDesc', 
                            'Bay.App', 'Bay.StartTime', 'Bay.EndTIme', 
                            DB::raw('if(Bay.Status = "A", "Active", "Inactive") as "Bay.Status"'))
                        ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                        ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                        ->where('Station.Status','A');
        }
        $this->visibleColumns = array('Bay.BayId', 'Station.StationName' ,'BayLocation.BayLocationDesc', 'Bay.App', 'Bay.StartTime', 'Bay.EndTIme', 'Bay.Status AS Bay.Status');
        $this->orderBy = array(array('Station.StationName'), array('BayLocation.BayLocationDesc'));
    }
}
