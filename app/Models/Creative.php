<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Creative extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Creative';
    protected $primaryKey = 'CreativeId';

    /**
     * The name of the "Updated At " column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['CreativeName', 'AdvertiserId', 'FileName',
     'FileMetaData', 'Category1', 'Category2', 'UpdatedAt', 'Status', 'UpdatedBy'];

    public $timestamps = false;

    /**
    * Get user details, which belongs to Creative
    */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'UserId');
    }

    /**
    * Get Advertisers which are mapped with Creative
    */
    public function advertiser()
    {
        return $this->belongsTo('App\Models\Advertiser', 'AdvertiserId');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'CreativeId');
    }

     public function campaignCreative()
    {
        return $this->belongsTo('App\Models\CampaignCreative', 'CreativeId');
    }
}
