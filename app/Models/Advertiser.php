<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertiser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Advertiser';
    protected $primaryKey = 'AdvertiserId';

    /**
     * The name of the "Updated At " column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['AdvertiserName', 'Sector', 'IndustryId', 'IsPaying', 'UpdatedAt', 'Status', 'UpdatedBy'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User','UserId');
    }

    public function industry()
    {
        return $this->belongsTo('App\Models\Industry','IndustryId');
    }

}
