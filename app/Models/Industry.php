<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Industry';
    protected $primaryKey = 'IndustryId';

    /**
     * The name of the "Updated At " column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedAt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['IndustryName', 'UpdatedAt', 'Status', 'UpdatedBy'];

    public $timestamps = false;

	public function user()
	{
		return $this->belongsTo('App\Models\User','UserId');
	}

}
