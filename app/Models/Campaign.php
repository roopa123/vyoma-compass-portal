<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
    *The table name associated with model
    * @var string
    */
    protected $table = 'Campaign';

    /**
    *The defining the primary key of the table
    * @var string
    */
    protected $primaryKey = 'CampaignId';


    /**
    * Defining the timestamps as false for the Compaign table
    */
    public $timestamps = false;

    protected $fillable = ['CampaignName', 'CampaignStartDate', 'CampaignEndDate', 'SpotsPerDay', 'CreatedAt', 
    'UpdatedAt', 'Status', 'UserId'];

    public function purchaseorder()
    {
        return $this->belongsToMany('App\Models\PurchaseOrder');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'UserId');
    }
 
}
