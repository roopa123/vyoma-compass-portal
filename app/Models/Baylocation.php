<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Baylocation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'BayLocation';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['BayLocationCode','BayLocationDesc', 'Status'];

}
