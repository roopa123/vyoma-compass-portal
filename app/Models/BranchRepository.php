<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Branch;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class BranchRepository extends EloquentRepositoryAbstract
{
    protected $status;

    public function __construct(Model $Model)
    {

        $this->status = \Config::get('vyoma-constants.status');
        $this->Database = Branch::select('BranchId', 'BranchName',  DB::raw('if(Status = "A", "Active", "Inactive") as "Status"'));

        $this->visibleColumns = array('BranchId', 'BranchName', 'Status');

        $this->orderBy = array(array('BranchName'));


    }

   /* public function getTotalNumberOfRows(array $filters = array()) {
        $count = DB::table('Station')
                ->select('Branch.BranchId', 'Branch.BranchName',
                    DB::raw('COUNT(DISTINCT Station.StationId) as StationCount'),
                    DB::raw('COUNT(DISTINCT Region.RegionId) as RegionCount'),
                    DB::raw('COUNT(DISTINCT Bay.BayId) AS BayCount'),
                    DB::raw('COUNT(DISTINCT Host.HostId) AS HostCount'))
                ->join('Bay','Bay.StationId', '=', 'Station.StationId')
                ->join('Host','Host.BayId', '=', 'Bay.BayId')
                ->rightjoin('Region','Station.RegionId', '=', 'Region.RegionId')
                ->rightjoin('Branch','Station.BranchId', '=', 'Branch.BranchId')
                ->where('Branch.Status', '=', $this->status['Active'])
                ->groupBy('Branch.BranchId','Branch.BranchName')->get();

        return count($count);
    }*/
}
