<?php
namespace App\Models;

use Illuminate\Database\DatabaseManager as DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use DB;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class HostRepository extends EloquentRepositoryAbstract
{
    public function __construct(Model $Model)
    {
        $this->status = \Config::get('vyoma-constants.status');
        if (request()->has('station_id')) {

            $this->Database = DB::table('Host')
              ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'Bay.BayLocationCode', 'Bay.App', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime as Host.StartTime','Host.EndTIme As Host.EndTIme', DB::raw('if(Host.Status = "A", "Active", "Inactive") as "Host.Status"'))
              ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
              ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
              ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
              ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
              ->where('Bay.StationId', request()->get('station_id'));
              //->where('Host.status', $this->status["Active"]);
        } else if (request()->has('bay_id') && request()->has('app')){
            $app = request()->get('app');
            $bayid = request()->get('bay_id');
            $station =  DB::table('Bay')->select('Station.StationName','Station.StationId')
                        ->where('Bay.Status', $this->status["Active"])
                        ->where('Bay.BayId', $bayid)
                        ->where('Bay.app', $app)
                        ->join('Station', 'Bay.StationId' ,'=', 'Station.StationId')->first();
            $stationId = $station->StationId;
            $this->Database = DB::table('Host')
            ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'Bay.BayLocationCode', 'Bay.App', 'Bay.App','Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime As Host.StartTime','Host.EndTIme As Host.EndTIme', DB::raw('if(Host.Status = "A", "Active", "Inactive") as "Host.Status"'))
              ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
              ->join('Station', 'Bay.StationId', '=', 'Station.StationId')              
              ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
              ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
              ->where('Bay.StationId', $stationId)
              ->where('Bay.app', $app)
              ->where('Host.Status', $this->status["Active"]);
        }
        else if(request()->segment(2) === 'region'){
            $RegionId = request()->segment(3);
            $branchId = request()->segment(5);
            $this->Database  =  DB::table('Host')
                            ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'Bay.BayLocationCode', 'Bay.App','Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime As Host.StartTime','Host.EndTIme As Host.EndTIme', DB::raw('if(Host.Status = "A", "Active", "Inactive") as "Host.Status"'))
                            ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')                            
                            ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
                            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                            ->where('Station.RegionId', $RegionId)
                            ->where('Station.BranchId', $branchId)
                            ->where('Station.Status', 'A')
                            ->where('Region.Status', 'A')
                            ->where('Bay.Status', 'A')
                            ->where('Host.Status', 'A');
        }
        else if(request()->has('bay_location')){
            $BayCode = request()->get('bay_location');
            $this->Database = DB::table('Host')
                          ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'Bay.BayLocationCode', 'Bay.App', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime as Host.StartTime','Host.EndTIme as Host.EndTIme', DB::raw('if(Host.Status = "A", "Active", "Inactive") as "Host.Status"'))
                          ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                          ->join('Station', 'Bay.StationId', '=', 'Station.StationId')                       
                          ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                          ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
                          ->where('Bay.BayLocationCode', $BayCode);
          }
        else {
            $this->Database = DB::table('Host')
                          ->select('Host.HostId', 'Host.HostName' , 'Station.StationName', 'Bay.BayLocationCode', 'Bay.App', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName',
                            'Region.RegionName','Host.StartTime as Host.StartTime','Host.EndTIme as Host.EndTIme', DB::raw('if(Host.Status = "A", "Active", "Inactive") as "Host.Status"'))
                          ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                          ->join('Station', 'Bay.StationId', '=', 'Station.StationId')                       
                          ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                          ->join('Region', 'Station.RegionId', '=', 'Region.RegionId');

        }
        $this->visibleColumns = array('Host.HostId', 'Host.HostName' , 'Station.StationName', 'Bay.BayLocationCode', 'Bay.App', 'Host.MasterOrPlayer','Host.HostCounterType','Host.CounterNumber','Host.HostMasterName','Branch.BranchName','Region.RegionName', 'Host.StartTime As Host.StartTime', 'Host.EndTIme As Host.EndTIme','Host.Status AS Host.Status');
        $this->orderBy = array(array('Station.StationName'), array('Bay.BayLocationCode'), array('Host.HostName'));
    }
}
