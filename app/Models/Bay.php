<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bay extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Bay';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['StationId','BayLocationCode','App', 'Status'];

}
