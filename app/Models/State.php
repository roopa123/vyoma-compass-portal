<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'State';
    protected $primaryKey = 'StateId';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['StateName'];

}
