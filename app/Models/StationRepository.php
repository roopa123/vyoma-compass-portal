<?php
namespace App\Models;

use Illuminate\Database\DatabaseManager as DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use DB;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class StationRepository extends EloquentRepositoryAbstract
{
    public function __construct(Model $Model)
    {
        if (request()->has('branch_id')) {
            $this->Database = DB::table('Station')
                            ->select('Station.StationId', 'Station.StationName',
                            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.Status = "A", "Active", "Inactive") as "Station.Status"'))
                            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
                            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                            ->join('State', 'Station.StateId', '=', 'State.StateId')
                            ->where('Station.BranchId', request()->get('branch_id'));
        } else if (request()->has('region_id')) {
            $this->Database = DB::table('Station')
            ->select('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.Status = "A", "Active", "Inactive") as "Station.Status"'))
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId')
            ->where('Station.RegionId', request()->get('region_id'));
           }
           else if (request()->segment(2) === 'home_region_id') {
            $branchId = request()->segment(5);
            $regId = request()->segment(3);
            $this->Database = DB::table('Station')
            ->select('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.Status = "A", "Active", "Inactive") as "Station.Status"'))
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId')
            ->where('Station.RegionId', $regId)
            ->where('Station.BranchId', $branchId)
            ->where('Station.Status', 'A')
            ->where('Region.Status', 'A');
           } else {
            $this->Database = DB::table('Station')
            ->select('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', DB::raw('if(Station.Status = "A", "Active", "Inactive") as "Station.Status"'))
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId');
        }
        $this->visibleColumns = array('Station.StationId', 'Station.StationName',
            'Region.RegionName', 'Branch.BranchName', 'State.StateName', 'Station.Status AS Station.Status');
        $this->orderBy = array(array('StationName'));
    }
}
