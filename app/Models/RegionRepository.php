<?php
namespace App\Models;

use Illuminate\Database\DatabaseManager as DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Region;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class RegionRepository extends EloquentRepositoryAbstract
{
    public function __construct(Model $Model)
    {
        $this->Database = Region::select('RegionId', 'RegionName',  DB::raw('if(Status = "A", "Active", "Inactive") as "Status"'));

        $this->visibleColumns = array('RegionId', 'RegionName', 'Status');

        $this->orderBy = array(array('RegionName'));
    }
   
}
