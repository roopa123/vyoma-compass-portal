<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Station';
    protected $primaryKey = 'StationId';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['StationName','RegionId','BranchId','StateId', 'Status'];

    public function Branch()
    {
        return $this->belongsTo('Branch');
    }

    public function Region()
    {
        return $this->belongsTo('App\Models\Region', 'RegionId');
    }

    public function State()
    {
        return $this->belongsTo('App\Models\State', 'StateId');
    }
}
