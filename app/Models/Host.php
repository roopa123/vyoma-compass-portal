<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Host extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Host';

    protected $primaryKey = 'HostId';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['HostName','BayId','MasterOrPlayer','HostCounterType','HostMasterId','HostMachineId','CounterNumber','HostMasterName','StartTime','EndTIme', 'Status'];

}
