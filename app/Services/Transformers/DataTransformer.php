<?php
namespace App\Services\Transformers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class DataTransformer extends BaseTransformer
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var \Illuminate\Support\Collection
     */
    private $collectionToTransform;

    use Helpers;
    public function __construct(Request $request)
    {
        $this->collectionToTransform = [];
        $this->request = $request;
        $this->includes = [];
    }
    public function addData($data, $transformer, $dataType = 'item', $key = null, $includes = [])
    {
        $this->setIncludes($includes);
        switch ($dataType) {
            case 'item':
                $transformedData = $this->respondWithItem($data, $transformer, $key);
                break;
            case 'collection':
                $transformedData = $this->respondWithCollection($data, $transformer, $key);
                break;
        }
        return array_merge($this->collectionToTransform, $transformedData);
        //$this->collectionToTransform = array_merge($this->collectionToTransform, $transformedData);
    }

    public function getTransformedData()
    {
        return $this->collectionToTransform;
    }
}
