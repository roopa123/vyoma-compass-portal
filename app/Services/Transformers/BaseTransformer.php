<?php
namespace App\Services\Transformers;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use App\Serializer\NoDataArraySerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class BaseTransformer
{
    protected $includes;
    /**
     * Generates a Fractal Item response.
     *
     * @param object $item     Resource to be converted
     * @param object $callback Resource Transformer
     *
     * @return \League\Fractal\Reource\Item
     */
    protected function respondWithItem($item, $callback, $key = null)
    {
        $resource = new Item($item, $callback, $key);
        $transformedResource = $this->transform($resource);
        return $this->attachKeyToTransformedResource($transformedResource);
    }

    /**
     * Generates a Fractal Collection response.
     *
     * @param object $item     Resource to be converted
     * @param object $callback Resource Transformer
     *
     * @return \League\Fractal\Reource\Collection
     */
    protected function respondWithCollection($collection, $callback, $key = null)
    {
        $resource = new Collection($collection->getCollection(), $callback, $key);
        /* If pagination requires follwing line to be used */
        //$resource->setPaginator(new IlluminatePaginatorAdapter($collection));
        $transformedResource = $this->transform($resource);
        return $this->attachKeyToTransformedResource($transformedResource);
    }

    public function attachKeyToTransformedResource($transformedResource, $key = 'data')
    {
        $returnArray = [];
        if ($key) {
            $returnArray[$key] = $transformedResource;
        } else {
            $returnArray = $transformedResource;
        }

        return $returnArray;
    }

     
    /**
     * Transforms the response into array.
     *
     * @param League\Fractal\Resource $data
     *
     * @return League\Fractal\Manager
     */
    protected function transform($data)
    {
        $fractalManager = new Manager();
        $manager = $this->applyIncludes($fractalManager);
        $this->resetIncludes();
        /* serializing the Data */
        //A Serializer structures your Transformed data in certain ways.
        $manager->setSerializer(new NoDataArraySerializer());
        // Run all transformers
        return $manager->createData($data)->toArray();
    }

    protected function resetIncludes()
    {
        $this->includes = [];
    }

    protected function setIncludes($includes)
    {
        $this->includes = $includes;
    }

    protected function applyIncludes($fractalManager)
    {
        if ($this->request->has('include')) {
            $fractalManager->parseIncludes($this->request->get('include'));
        }
        if (count($this->includes) > 0) {
            $fractalManager->parseIncludes(implode(',', $this->includes));
        }
        return $fractalManager;
    }
}
