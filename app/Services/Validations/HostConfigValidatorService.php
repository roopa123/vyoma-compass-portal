<?php
namespace App\Services\Validations;

use App\Services\Constants\ApiConstants;
use App\Models\Code;
use App\Models\Host;
use App\Models\State;
use App\Services\Constants\AppErrorCodes;
use App\Commands\HostTransformerTrait;
use App\Repositories\HostRepository;
use App\Services\Validations\BaseValidationService;
 
class HostConfigValidatorService extends BaseValidationService
{
    use HostTransformerTrait;
    
    private $hostRepo;

    private $validUpdateData = array();

    public function __construct(AppErrorCodes $appErrorCodes, HostRepository $hostRepo)
    {
        parent::__construct($appErrorCodes);
        $this->hostRepo = $hostRepo;
    }

    /**
    * Validate Post Data for Updating Version for the Host
    * @param Object, $hostObject
    * @param Array $postData
    * @return Array
    */
    public function validateHostConfigTokens($hostObject, $postData)
    {
        $errorArray = array();
        if ($this->validateMandatoryTokens($postData, ApiConstants::MANDATORY_CONFIG_TOKENS)) {
            $validateEmptyTokens = $this->addDependencyFields($postData, ApiConstants::MANDATORY_CONFIG_TOKENS);
            $this->validateEmptyValue($postData, $validateEmptyTokens);
            $this->validateHostName($postData, $hostObject);
            $this->validateRole($postData, $hostObject);
            $this->validateIPAddress($postData, $hostObject);
            $this->validateDeployState($postData, $hostObject);
            $this->validateCounterType($postData['COUNTER_TYPE']);
            $this->doValidationForEnableDisableConfigPostParams($postData);
            $this->validateTimeTokens($postData, $hostObject);
            $this->validateCodeValues($postData);
            $this->validateScheduleData($postData);
            return array($this->validationStatus, $this->errorArray);
        }
        return array($this->validationStatus, $this->errorArray);
    }

    /**
    * To validate Whether All mandatory config tokens are present in POST JSON
    * @param Array, $postData
    * @return Bool True/False
    */
    public function validateMandatoryTokens($postData, $tokenArray)
    {
        $allFieldsPresent = true;
        $mandatoryTokenArray = $this->addDependencyFields($postData, $tokenArray);
        foreach ($mandatoryTokenArray as $key => $configToken) {
            $this->validationStatus[$configToken] = ApiConstants::VALIDATION_SUCCESS;
            if (!isset($postData[$configToken])) {
                $allFieldsPresent = false;
                $this->setErrorCodeAndMessage(
                    AppErrorCodes::TOKEN_MISSING_IN_POST_JSON,
                    $configToken,
                    true
                );
            }
        }
        return $allFieldsPresent;
    }

    /**
    * To Add Dependency Tokens Based on Host Role
    * @param Array, $tokenArray
    * @param STRING, $role
    * @return ARRAY $mandatoryTokenArray
    */
    public function addTokensBasedOnHostRole($postData, $tokenArray)
    {
        $mandatoryTokenArray = $tokenArray;
        $role = $this->removeSingleQuotes($postData['ROLE']);
        if ($role == ApiConstants::ROLE_MASTER || $role == ApiConstants::ROLE_BOTH) {
            $mandatoryTokenArray = array_merge($tokenArray, ApiConstants::MANDATORY_MASTER_TOKENS);
            if ($this->checkTokenSetAndEnabled('LOG_PUSH', $postData)) {
                array_push($mandatoryTokenArray, 'LOG_PUSH_MIN');
                array_push($mandatoryTokenArray, 'LOG_PUSH_HOUR');
            }
            if ($this->checkTokenSetAndEnabled('CONTENT_UPDATE', $postData)) {
                array_push($mandatoryTokenArray, 'UPDATE_SCHEDULE');
            }
            if ($this->checkTokenSetAndEnabled('ROUTER_REBOOT', $postData)) {
                array_push($mandatoryTokenArray, 'ROUTER_REBOOT_MIN');
                array_push($mandatoryTokenArray, 'ROUTER_REBOOT_HOUR');
            }
            if ($role == ApiConstants::ROLE_BOTH) {
                if($postData['READTTY_LOG'] == ApiConstants::ENABLE) {
                    array_push($mandatoryTokenArray, 'UPLOAD_CSV');
                    if ($this->checkTokenSetAndEnabled('UPLOAD_CSV', $postData)) {
                        array_push($mandatoryTokenArray, 'UPLOAD_CSV_SCHEDULE');
                    }
                }
                
            }
        } else if ($role == ApiConstants::ROLE_PLAYER || $role == ApiConstants::ROLE_PLASMA) {
            $mandatoryTokenArray = array_merge($tokenArray, ApiConstants::MANDATORY_PLAYER_TOKENS);
            if ($this->checkTokenSetAndEnabled('UPLOAD_CSV', $postData)) {
                array_push($mandatoryTokenArray, 'UPLOAD_CSV_SCHEDULE');
            }
        }
        return $mandatoryTokenArray;
    }

    /**
    * To Add Dependency Tokens Based on Enable/Disable to Mandatory POST Array
    * @param Array, $postData
    * @return ARRAY $mandatoryTokenArray
    */
    public function addDependencyFields($postData, $tokenArray)
    {
        $mandatoryTokenArray = $this->addTokensBasedOnHostRole($postData, $tokenArray);
        if ($this->checkTokenSetAndEnabled('SCREENSHOT', $postData)) {
            array_push($mandatoryTokenArray, 'SCREENSHOT_SCHEDULE_HOUR');
            array_push($mandatoryTokenArray, 'SCREENSHOT_SCHEDULE_MIN');
        }
        if (isset($postData['COUNTER_TYPE'])) {
            $counterType = $this->removeSingleQuotes($postData['COUNTER_TYPE']);
            if ($counterType == ApiConstants::COUNTER_PRS) {
                array_push($mandatoryTokenArray, 'SCREENOFF_HOUR_SUN');
                array_push($mandatoryTokenArray, 'SCREENOFF_MIN_SUN');
            }
        }
        if (isset($postData['HDMI_MODE'])) {
            array_push($mandatoryTokenArray, 'HDMI_MODE');
        }
        return $mandatoryTokenArray;
    }

    /**
    * To validate Enable/Disable Tokens
    * @param Array, $postData
    * @return Void
    */
    public function doValidationForEnableDisableConfigPostParams($postData)
    {
        $enableDisableArray = ApiConstants::CONFIG_ENABLE_DISABLE_TOKENS;
        $validArray = [ApiConstants::ENABLE, ApiConstants::DISABLE];
        $validValues = $this->doImplode($validArray);
        foreach ($enableDisableArray as $key => $configToken) {
            if (isset($postData[$configToken]) && !$this->checkValidationExists($configToken)) {
                $value = $postData[$configToken];
                $this->validateAllowableTokens($value, $validArray, $configToken, $validValues);
            }
        }
    }

    /**
    * To validate Token Values within the defined values
    * @param STRING $tokenValue
    * @param ARRAY $validArray
    * @param STRING $configTokenName
    * @return BOOL
    */
    public function validateAllowableTokens($tokenValue, $validArray, $configToken, $validValues)
    {
        $errMsg = ApiConstants::VALUES_SHOULD_BE_LIKE."[".$validValues. "]";
        $errCode = AppErrorCodes::INVALID_TOKEN;
        $tokenValue = $this->removeSingleQuotes($tokenValue);
        if (!in_array($tokenValue, $validArray)) {
            $this->setErrorCodeAndMessage($errCode, $configToken, true, $errMsg);
            return false;
        }
        return true;
    }

    /**
    * To validate Counter Type
    * @param Array, $postData
    * @return Void
    */
    public function validateCounterType($counterType)
    {
        if (!$this->checkValidationExists('COUNTER_TYPE')) {
            $validArray = ApiConstants::VALID_COUNTER_TYPE_TOKEN;
            $validValues = $this->doImplode(ApiConstants::VALID_COUNTER_TYPE_TOKEN);
            $this->validateAllowableTokens($counterType, $validArray, 'COUNTER_TYPE', $validValues);
        }
    }

    /**
    * To Set Error Codes for Cofig Tokens
    * @param Array, $tokensArray
    * @param String, $errMsg
    * @return Void
    */
    public function setErrorCodeForStartAndEndTime($tokensArray, $errMsg)
    {
        $errCode = AppErrorCodes::INVALID_TOKEN;
        foreach ($tokensArray as $key => $configToken) {
            $this->setErrorCodeAndMessage($errCode, $configToken, true, $errMsg);
        }
    }

    /**
    * To validate Hour/Minutes Range
    * @param Array, $timeTokenArray
    * @param Array, $postData
    * @param INT, $checkHourOrMinute
    * @return Void
    */
    public function validateHourMinuteRange($timeTokenArray, $postData, $checkHourOrMinute)
    {
        $errCode = AppErrorCodes::INVALID_TOKEN;
        $errMsg = ApiConstants::RANGE;
        foreach ($timeTokenArray as $key => $configToken) {
            if (!$this->checkValidationExists($configToken)) {
                if (!$this->validateHourMinute($postData[$configToken], $checkHourOrMinute)) {
                    $this->setErrorCodeAndMessage($errCode, $configToken, true, $errMsg);
                }
            }
        }
    }

    /**
    * Start Validate Time Related Tokens: REBOOT_HOUR, REBOOT_MIN, SCREENOFF_HOUR, SCREENOFF_MIN, SCREENOFF_HOUR_SUN, SCREENOFF_MIN_SUN
    * @param Array $postData
    * @param Object $hostObject
    * @return Void
    */
    public function validateTimeTokens($postData, $hostObject)
    {
        $hourArray = ApiConstants::TIME_HOUR_TOKENS;
        $minArray = ApiConstants::TIME_MIN_TOKENS;
        $screenStartToken = array('REBOOT_HOUR', 'REBOOT_MIN');
        $screenEndToken = array('SCREENOFF_HOUR', 'SCREENOFF_MIN');
        /* Hour, Minute Range Validation */
        $this->validateHourMinuteRange($hourArray, $postData, ApiConstants::CHECK_HOUR);
        $this->validateHourMinuteRange($minArray, $postData, ApiConstants::CHECK_MINUTE);
        if ((!$this->checkValidationExists('REBOOT_HOUR') && !$this->checkValidationExists('REBOOT_MIN')) && (!$this->checkValidationExists('SCREENOFF_HOUR') && !$this->checkValidationExists('SCREENOFF_MIN'))) {
            $onhour = $postData['REBOOT_HOUR'];
            $onmin = $postData['REBOOT_MIN'];
            $offhour = $postData['SCREENOFF_HOUR'];
            $offmin = $postData['SCREENOFF_MIN'];
            $startTime = $this->convertHourMinToDateTime($onhour, $onmin);

            $endTime = $this->convertHourMinToDateTime($offhour, $offmin);
            if ($this->clientTimeValidation($startTime, $endTime, $screenStartToken, $screenEndToken)) {
                $this->severTimeValidation($startTime, $endTime, $screenStartToken, $screenEndToken, $hostObject, $postData);
            }
        }
        /* If Counter Type is PRS */
        $counterType = $this->trimSingleQuotes($postData['COUNTER_TYPE']);
        if ($counterType == ApiConstants::COUNTER_PRS && $this->validationStatus['COUNTER_TYPE'] == ApiConstants::VALIDATION_SUCCESS) {
            $this->validatePSRCounterTokens($postData, $hostObject);
        } /*else {
            //Dependency Validation
            if ($this->validationStatus['COUNTER_TYPE'] == ApiConstants::VALIDATION_SUCCESS) {
                $this->validateEmptyCheck($postData, 'SCREENOFF_HOUR_SUN');
                $this->validateEmptyCheck($postData, 'SCREENOFF_MIN_SUN');
            }
        }*/
    }

    /**
    * To validate PSR Timing Tokens
    * @param Array, $postData
    * @param Object, $hostObject
    * @return Void
    */
    public function validatePSRCounterTokens($postData, $hostObject)
    {
        $hourArray = ApiConstants::PRS_TIME_HOUR_TOKENS;
        $minArray = ApiConstants::PRS_TIME_MIN_TOKENS;
        $this->validateHourMinuteRange($hourArray, $postData, ApiConstants::CHECK_HOUR);
        $this->validateHourMinuteRange($minArray, $postData, ApiConstants::CHECK_MINUTE);
        if ((!$this->checkValidationExists('REBOOT_HOUR') && !$this->checkValidationExists('REBOOT_MIN')) && (!$this->checkValidationExists('SCREENOFF_HOUR_SUN') && !$this->checkValidationExists('SCREENOFF_MIN_SUN'))) {
            $this->validateSundayTimings($postData, $hostObject);
        }
    }

    /**
    * To Validate sunday off timings only for PRS counters
    * @param Array, $postData
    * @param Object, $hostObject
    * @return Void
    */
    public function validateSundayTimings($postData, $hostObject)
    {
        $onhour = $postData['REBOOT_HOUR'];
        $onmin = $postData['REBOOT_MIN'];
        $offhour = $postData['SCREENOFF_HOUR_SUN'];
        $offmin = $postData['SCREENOFF_MIN_SUN'];
        $startTime = $this->convertHourMinToDateTime($onhour, $onmin);
        $endTime = $this->convertHourMinToDateTime($offhour, $offmin);
        $screenStartToken = array('REBOOT_HOUR', 'REBOOT_MIN');
        $screenEndToken = array('SCREENOFF_HOUR_SUN', 'SCREENOFF_MIN_SUN');
        if ($this->clientTimeValidation($startTime, $endTime, $screenStartToken, $screenEndToken)) {
            $this->severTimeValidation($startTime, $endTime, $screenStartToken, $screenEndToken, $hostObject, $postData);
        }
    }

    /**
    * To Validate Counter Timings
    * @param DateTime, $startTime
    * @param DateTime, $endTime
    * @param Array, $screenStartToken
    * @param Array, $screenEndToken
    * @return Bool
    */
    public function clientTimeValidation($startTime, $endTime, $screenStartToken, $screenEndToken)
    {
        $valid = true;
        /* Client Side Validation */
        if ($this->validateStartTime($startTime, $endTime)) {
            $errMsg = ApiConstants::SCREEN_START_TIME_MSG;
            $this->setErrorCodeForStartAndEndTime($screenStartToken, $errMsg);
            $valid = false;
        }
        if ($this->validateEndTime($startTime, $endTime)) {
            $errMsg = ApiConstants::SCREEN_END_TIME_MSG;
            $this->setErrorCodeForStartAndEndTime($screenEndToken, $errMsg);
            $valid =false;
        }
        return $valid;
    }

    /**
    * Server Side Time Validation
    * @param Time $startTime, $endTime, $screenStartToken, $screenEndToken
    * @param Obj $hostObject,
    * @param Array $postData, 
    * @return Bool
    */
    public function severTimeValidation($startTime, $endTime, $screenStartToken, $screenEndToken, $hostObject, $postData)
    {
        $startMsg = ApiConstants::SCREEN_START_TIME_SERVER_MSG;
        $endMsg = ApiConstants::SCREEN_END_TIME_SERVER_MSG;
        // If It is Master
        $roleType = $this->trimSingleQuotes($postData['ROLE']);
        if ($roleType == ApiConstants::ROLE_MASTER || $roleType == ApiConstants::ROLE_BOTH) {
            $startErrMsg = $startMsg." [ ".$hostObject->StartTime.",".$hostObject->EndTIme." ] ";
            $endErrMsg = $endMsg." [ ".$hostObject->StartTime.",".$hostObject->EndTIme." ] ";
        if (!(strtotime($startTime) >= strtotime($hostObject->StartTime))){
            $this->setErrorCodeForStartAndEndTime($screenStartToken, $startErrMsg);
        }
        if (!(strtotime($endTime) <= strtotime($hostObject->EndTIme))){
           $this->setErrorCodeForStartAndEndTime($screenEndToken, $endErrMsg);
        }
    } else {
            // If It is Player
            $masterHostObj = $this->hostRepo->getMasterHostObjForPlayer($hostObject->HostId);
            $startErrMsg = $startMsg." [ ".$masterHostObj->StartTime.",".$masterHostObj->EndTIme." ] ";
            $endErrMsg = $endMsg." [ ".$masterHostObj->StartTime.",".$masterHostObj->EndTIme." ] ";
            if (!(strtotime($startTime) >= strtotime($masterHostObj->StartTime))){
                $this->setErrorCodeForStartAndEndTime($screenStartToken, $startErrMsg);
            }
            if (!(strtotime($endTime) <= strtotime($masterHostObj->EndTIme))){
                $this->setErrorCodeForStartAndEndTime($screenEndToken, $endErrMsg);
            }
        }
    }

    /**
    * To validate Token Value with Defined Values for Tokens in Code Table
    * @param Array, $postData
    * @return Void
    */
    public function validateCodeValues($postData)
    {        
        /* Code type values in Code Table */
        $code['HDMI_MODE'] = "HdmiMode";
        $code['PLAYER_WIN'] = "PlayerWinSize";
        $code['SCREEN_RATIO'] = "ScreenRatio";
        $code['CABLE_TYPE'] = "CableType";
        $configTokenArray = ApiConstants::CODE_CONFIG_TOKENS;
        $errCode = AppErrorCodes::INVALID_TOKEN;
        //Check whether code is existed or not based on CodeType, If not throw error with valid values.
        foreach ($configTokenArray as $key => $configToken) {
            if (isset($code[$configToken]) && !$this->checkValidationExists($configToken) && isset($postData[$configToken])) {    
                $codeType = $code[$configToken];
                $codeValue = $postData[$configToken];
                $codeValueExists = $this->getCodeTableValue($codeType, $codeValue);
                if (!$codeValueExists) {
                    $validArray = $this->getCodeTableValue($codeType, $codeValue, false);
                    $implodeArray = $this->getImplodeArray($validArray);
                    $validValues = $this->doImplode($implodeArray);
                    $errMsg = ApiConstants::VALUES_SHOULD_BE_LIKE."[".$validValues. "]";
                    $this->setErrorCodeAndMessage($errCode, $configToken, true, $errMsg);
                }
            }
        }
    }

    /**
    * To validate Token Value is Empty or Not
    * @param Array, $postData
    * @param String, $token
    * @return Void
    */
    public function validateEmptyCheck($postData, $token)
    {
        if (isset($postData[$token])) {
            $value = $postData[$token];
            $errCode = AppErrorCodes::INVALID_TOKEN;
            $errMsg = ApiConstants::SCHEDULE_EMPTY;
            if (!$this->checkEmpty($value)) {
                $this->setErrorCodeAndMessage($errCode, $token, true, $errMsg);
            }
        }
    }

    /**
    * To validate Schedule Config Token Values with Code Table
    * @param String, $scheduleValue
    * @param String, $codeType
    * @param Array, $tokens
    * @param Array, $validValueArray
    * @return Void
    */
    public function validateScheduleCode($scheduleValue, $codeType, $tokens, $validValueArray)
    {
        $codeValueExists = $this->getCodeTableValue($codeType, $scheduleValue);
        if (!$codeValueExists) {
            foreach ($tokens as $key => $configToken) {
                if (!$this->checkValidationExists($configToken)) {
                    $validationMsg = ApiConstants::VALUES_SHOULD_BE_LIKE. "[".$validValueArray[$configToken]."]";
                    $this->setErrorCodeAndMessage(
                        AppErrorCodes::INVALID_TOKEN,
                        $configToken,
                        true,
                        $validationMsg
                    );
                }
            }
        }
    }

    /**
    * Validate Schedule values with the Code Table
    * @param Array, $postData
    */
    public function validateScheduleData($postData)
    {
        $role = $this->removeSingleQuotes($postData['ROLE']);
        if ($role == ApiConstants::ROLE_MASTER || $role == ApiConstants::ROLE_BOTH) {
            $this->validateRouterSchedule($postData);
            $this->validatePushLogSchedule($postData);
            $this->validateContentSchedule($postData);
            if ($role == ApiConstants::ROLE_BOTH) {
                $this->validateCsvSchedule($postData);
            }
        } else if ($role == ApiConstants::ROLE_PLAYER || $role == ApiConstants::ROLE_PLASMA) {
            $this->validateCsvSchedule($postData);
        }
        $this->validateScreenShotSchedule($postData);
    }

   /**
    * To validate PushLogSchedule
    * @param Array, $postData
    * @return Void
    */
    public function validatePushLogSchedule($postData)
    {
        if ($postData['LOG_PUSH'] == ApiConstants::ENABLE) {
            $valid['LOG_PUSH_MIN'] = ApiConstants::LOG_PUSH_MIN_VALID_VALUES;
            $valid['LOG_PUSH_HOUR'] = ApiConstants::LOG_PUSH_HOUR_VALID_VALUES;
            $playLogSchedule = $postData['LOG_PUSH_MIN']." ".$postData['LOG_PUSH_HOUR']." * * *";
            $this->validateScheduleCode($playLogSchedule, 'PlayLogUpdateSchedule', ApiConstants::PLAY_LOG_SCHEDULE, $valid);
        }
        //Dependency Validation
        /*if ($postData['LOG_PUSH'] == ApiConstants::DISABLE) {
            $this->validateEmptyCheck($postData, 'LOG_PUSH_MIN');
            $this->validateEmptyCheck($postData, 'LOG_PUSH_HOUR');
        }*/
    }

    /**
    * To validate ContentSchedule
    * @param Array, $postData
    * @return Void
    */
    public function validateContentSchedule($postData)
    {
        if ($postData['CONTENT_UPDATE'] == ApiConstants::ENABLE) {
            $valid['UPDATE_SCHEDULE'] = ApiConstants::UPDATE_SCHEDULE_VALID_VALUES;
            $contentSchedule = $postData['UPDATE_SCHEDULE']." * * * *";
            $this->validateScheduleCode($contentSchedule, 'ContentUpdateSchedule', ApiConstants::CONTENT_SCHEDULE, $valid);
        }
        //Dependency Validation
        /*if ($postData['CONTENT_UPDATE'] == ApiConstants::DISABLE) {
            $this->validateEmptyCheck($postData, 'UPDATE_SCHEDULE');
        }*/
    }

    /**
    * To validate CsvSchedule
    * @param Array, $postData
    * @return Void
    */
    public function validateCsvSchedule($postData)
    {
        if($postData['READTTY_LOG'] == ApiConstants::ENABLE) {
            if ($postData['UPLOAD_CSV'] == ApiConstants::ENABLE) {
                $valid['UPLOAD_CSV_SCHEDULE'] = ApiConstants::UPLOAD_CSV_SCHEDULE_VALID_VALUES;
                $csvSchedule = $postData['UPLOAD_CSV_SCHEDULE']." * * * *";
                $this->validateScheduleCode($csvSchedule, 'TicketLogUpdateSchedule', ApiConstants::TICKET_SCHEDULE, $valid);
            }
        }
        
        //Dependency Validation
        /*if ($postData['UPLOAD_CSV'] == ApiConstants::DISABLE) {
            $this->validateEmptyCheck($postData, 'UPLOAD_CSV_SCHEDULE');
        }*/
    }

    /**
    * To validate ScreenShotSchedule
    * @param Array, $postData
    * @return Void
    */
    public function validateScreenShotSchedule($postData)
    {
        if ($postData['SCREENSHOT'] == ApiConstants::ENABLE) {
            $valid['SCREENSHOT_SCHEDULE_MIN'] = ApiConstants::SCREENSHOT_SCHEDULE_MIN_VALID_VALUES;
            $valid['SCREENSHOT_SCHEDULE_HOUR'] = ApiConstants::SCREENSHOT_SCHEDULE_HOUR_VALID_VALUES;
            $screenShotSchedule = $postData['SCREENSHOT_SCHEDULE_MIN']." ".$postData['SCREENSHOT_SCHEDULE_HOUR']." * * *";
            $this->validateScheduleCode($screenShotSchedule, 'ScreenShotSchedule', ApiConstants::SCREENSHOT_SCHEDULE, $valid);
        }
        //Dependency Validation
        /*if ($postData['SCREENSHOT'] == ApiConstants::DISABLE) {
            $this->validateEmptyCheck($postData, 'SCREENSHOT_SCHEDULE_MIN');
            $this->validateEmptyCheck($postData, 'SCREENSHOT_SCHEDULE_HOUR');
        }*/
    }

    /**
    * To validate RouterSchedule
    * @param Array, $postData
    * @return Void
    */
    public function validateRouterSchedule($postData)
    {
        if ($postData['ROUTER_REBOOT'] == ApiConstants::ENABLE) {
            $valid['ROUTER_REBOOT_MIN'] = ApiConstants::ROUTER_REBOOT_MIN_VALID_VALUES;
            $valid['ROUTER_REBOOT_HOUR'] = ApiConstants::ROUTER_REBOOT_HOUR_VALID_VALUES;
            $routerRebootSchedule = $postData['ROUTER_REBOOT_MIN']." ".$postData['ROUTER_REBOOT_HOUR']." * * *";
            $this->validateScheduleCode($routerRebootSchedule, 'RouterRebootSchedule', ApiConstants::ROUTER_REBOOT_SCHEDULE, $valid);
        }
        //Dependency Validation
        /*if ($postData['ROUTER_REBOOT'] == ApiConstants::DISABLE) {
            $this->validateEmptyCheck($postData, 'ROUTER_REBOOT_MIN');
            $this->validateEmptyCheck($postData, 'ROUTER_REBOOT_HOUR');
        }*/
    }

    /**
    * To validate Host Role
    * @param Array, $postData
    * @param Object, $host
    * @return Void
    */
    public function validateRole($postData, $host)
    {
        $validValues = $this->doImplode(ApiConstants::VALID_ROLE_TOKEN);
        $validRole = $this->validateAllowableTokens($postData['ROLE'], ApiConstants::VALID_ROLE_TOKEN, 'ROLE', $validValues);
        if ($validRole) {
            $hostmasterId = Host::where('HostId', $host->HostMasterId)->get();
            /* Check validation for master*/
            $role = $this->removeSingleQuotes($postData['ROLE']);
            if ($role == ApiConstants::ROLE_MASTER) {
                if ($host->MasterOrPlayer == \Config::get('vyoma-constants.bothType')) {
                    $masterName = \Config::get('vyoma-constants.masterType');
                    $this->validUpdateData['MasterOrPlayer'] = $masterName;
                } else if ($host->MasterOrPlayer == \Config::get('vyoma-constants.playerType')) {
                    if ($hostmasterId != null) {
                        $this->setErrorCodeAndMessage(
                            AppErrorCodes::MASTER_ALREADY_PRESENT,
                            'ROLE'
                        );
                    } else {
                        $masterName = \Config::get('vyoma-constants.masterType');
                        $this->validUpdateData['MasterOrPlayer'] = $masterName;
                    }
                }
            }
            /* Check validation for player*/
            if ($role == ApiConstants::ROLE_PLAYER) {
                if ($host->MasterOrPlayer == \Config::get('vyoma-constants.bothType')) {
                    $this->setErrorCodeAndMessage(
                        AppErrorCodes::CANT_CHANGE_BOTH_TO_PLAYERS,
                        'ROLE'
                    );
                } else if ($host->MasterOrPlayer == \Config::get('vyoma-constants.masterType')) {
                    $this->setErrorCodeAndMessage(
                        AppErrorCodes::CANT_CHANGE_MASTER_TO_PLAYERS,
                        'ROLE'
                    );
                } else {
                    $playerName = \Config::get('vyoma-constants.playerType');
                    $this->validUpdateData['MasterOrPlayer'] = $playerName;
                }
            }
            /* Check validation for combi*/
            if ($role == ApiConstants::ROLE_BOTH) {
                if ($host->MasterOrPlayer == \Config::get('vyoma-constants.masterType')) {
                    $masterName = \Config::get('vyoma-constants.bothType');
                    $this->validUpdateData['MasterOrPlayer'] = $masterName;
                } else if ($host->MasterOrPlayer == \Config::get('vyoma-constants.playerType')) {
                    if ($hostmasterId != null) {
                        $this->setErrorCodeAndMessage(
                            AppErrorCodes::CANT_CHANGE_PLAYER_TO_BOTH,
                            'ROLE'
                        );
                    } else {
                        $masterName = \Config::get('vyoma-constants.bothType');
                        $this->validUpdateData['MasterOrPlayer'] = $masterName;
                    }
                }
            }

            /* Check validation for plasma*/
            if ($role == ApiConstants::ROLE_PLASMA) {
                if($host->MasterOrPlayer == ApiConstants::HOST_MASTER){
                    $this->setErrorCodeAndMessage(
                        AppErrorCodes::CANT_CHANGE_MASTER_TO_PLASMA,
                        'ROLE'
                    );
                }                           
            }
        }
    }

    /**
    * To validate IpAddress
    * @param Array, $postData
    * @param Object, $host
    * @return Void
    */
    public function validateIPAddress($postData, $hostObject)
    {
        if (!filter_var($postData['IP_ADDRESS'], FILTER_VALIDATE_IP) === false) {
             $this->validUpdateData['LocalIPAddress'] = $postData["IP_ADDRESS"];
        } else {
            $this->setErrorCodeAndMessage(
                AppErrorCodes::NOT_VALID_IP_ADDRESS,
                'IP_ADDRESS'
            );
        }
    }

    /**
    * To validate DeployState
    * @param Array, $postData
    * @param Object, $host
    * @return Void
    */
    public function validateDeployState($postData, $hostObject)
    {
        $statecode = $this->removeSingleQuotes($postData['DEPLOY_STATE']);
        if (!$this->hostRepo->validateStateCodeInState($statecode)) {
            $this->setErrorCodeAndMessage(
                AppErrorCodes::INVALID_TOKEN,
                'DEPLOY_STATE'
            );
        }
    }

    /**
    * To validate Host Name
    * @param Array, $postData
    * @param Object, $host
    * @return Void
    */
    public function validateHostName($postData, $hostObject)
    {
        $hostName = $this->removeSingleQuotes($postData['HOSTNAME']);
        if ($hostName == $hostObject->HostName) {
            $this->validUpdateData['HostName'] = $hostName;
        } else {
            $this->setErrorCodeAndMessage(
                AppErrorCodes::INVALID_TOKEN,
                'HOSTNAME'
            );
        }
    }

    /**
    * return the LocalIpAddress of Host players
    * @param Object $host
    * @return Array $hostMasterDetails
    */
    public function getLocalIp($host)
    {
        $hostMasterDetails = Host::where('HostId', $host->HostMasterId)->first();
        return $hostMasterDetails;
    }
}