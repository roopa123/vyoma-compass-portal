<?php
namespace App\Services\Validations;

use App\Services\Constants\AppErrorCodes;
use App\Services\Constants\ApiConstants;
use App\Models\Code;

class BaseValidationService
{
    protected $appErrorCodes;

    protected $errorArray = array();

    protected $validationStatus = array ();

    public function __construct(AppErrorCodes $appErrorCodes)
    {
        $this->appErrorCodes = $appErrorCodes;
    }

    /* To set code and msg for every parameters */
    public function setErrorCodeAndMessage(
        $errorCode,
        $key,
        $concatKeyToMessage = false,
        $addtionalMsg = ""
    ) {
        $message = $this->appErrorCodes->getMessageForCode($errorCode);
        if ($concatKeyToMessage) {
            $message = $key." ".$message;
            if ($addtionalMsg != "") {
                $message =$message." ".$addtionalMsg;
            }
        }
        $this->errorArray[$key][$errorCode] = $message;
        $this->validationStatus[$key] = ApiConstants::VALIDATION_FAIL;
    }

    /* validate Empty Values */
    public function validateEmptyValue($postData, $configTokenArray)
    {
        $errCode = AppErrorCodes::TOKEN_NOT_EMPTY;
        foreach ($configTokenArray as $key => $configToken) {
            if (isset($postData[$configToken])) {
                $this->validationStatus[$configToken] = ApiConstants::VALIDATION_SUCCESS;
                if (trim($postData[$configToken]) == "") {
                    $this->setErrorCodeAndMessage($errCode, $configToken, true);
                }
            }
        }
    }

    /* Convert Error Response */
    public function convertErrorResponse($array)
    {
        $error = array();
        foreach ($array as $key1 => $value) {
            foreach ($value as $key => $val) {
                $error[$key1] = array('error_code' => $key, 'error_msg' => $val);
            }
        }
        return $error;
    }

    /* Convert Success Response */
    public function convertSuccessResponse($array, $postData)
    {
        $success = array();
        foreach ($array as $key1 => $value) {
            if (isset($postData[$key1])) {
                $msg = ApiConstants::UPDATE_SUCCESS_MSG;
                if ($value == ApiConstants::REGISTER_YES_TO_NO) {
                    $msg = ApiConstants::REGISTER_YES_TO_NO_MSG;
                }
                if ($value == ApiConstants::VALIDATION_SUCCESS || $value == ApiConstants::REGISTER_YES_TO_NO) {
                    $success[$key1] = array('success' => 'true' , 'msg' => $msg);
                } else {
                    $success[$key1] = array('success' => 'false', 'msg' => ApiConstants::UPDATE_FAIL_MSG);
                }
            }
        }
        return $success;
    }

    /* implode Array to string */
    public function doImplode($arrayToString)
    {
        return implode(",", $arrayToString);
    }

    /* Covert hour, min to DateTime*/
    public function convertHourMinToDateTime($hour, $min)
    {
        $fullTime = $hour.":".$min;
        return date('H:i:s', strtotime($fullTime));
    }

    /* Trim Singlequotes in string */
    public function removeSingleQuotes($tokenValue)
    {
        $tokenValue = str_replace("'", "", $tokenValue);
        return $tokenValue;
    }

    /* Validate Hour, Minute Range */
    public function validateHourMinute($value, $checkHourOrMinute)
    {
        if ($checkHourOrMinute == ApiConstants::CHECK_HOUR) {
            return $this->validateHourRange($value);
        }
        if ($checkHourOrMinute == ApiConstants::CHECK_MINUTE) {
            return $this->validateMinutesRange($value);
        }
    }

    /* Validate Hour Range */
    public function validateHourRange($hour)
    {
        if (!in_array($hour, range(0, 23))) {
            return false;
        }
        return true;
    }

    /* Validate Minute Range */
    public function validateMinutesRange($minute)
    {
        if (!in_array($minute, range(0, 59))) {
            return false;
        }
        return true;
    }

    /* To remove single in begining and end of the string */
    public function trimSingleQuotes($value)
    {
        return trim($value, "'");
    }

    /* To check given value is empty or not */
    public function checkEmpty($value)
    {
        if (trim($value) != "") {
            return false;
        }
        return true;
    }

    //Validate Start Time
    public function validateStartTime($startTime, $endTime)
    {
        if (strtotime($startTime) >= strtotime($endTime)) {
            return true;
        }
        return false;
    }

    //Validate End Time
    public function validateEndTime($startTime, $endTime)
    {
        if (strtotime($endTime) <= strtotime($startTime)) {
            return true;
        }
        return false;
    }

    /* To check Field is Set or Not in POST Data */
    public function checkTokenSetAndEnabled($token, $postData)
    {
        if (isset($postData[$token]) && $postData[$token] == ApiConstants::ENABLE) {
            return true;
        }
        return false;
    }

    /**
    * Generic Function To Check Validation Exists for Token
    * @param STRING $ConfigTokenKey
    * @return BOOL
    */
    public function checkValidationExists($ConfigTokenKey)
    {
        if (isset($this->validationStatus[$ConfigTokenKey])) {
            if ($this->validationStatus[$ConfigTokenKey] == ApiConstants::VALIDATION_FAIL) {
                return true;
            }
        }
        return false;
    }

     /* To check Given Value with Code Table Values */
    public function getCodeTableValue($codeType, $code, $check = true)
    {
        if ($check) {
            return Code::where('CodeType', $codeType)->where('code', $code)->exists();
        }
        return Code::where('CodeType', $codeType)->get()->toArray();
    }
     
    /* To convert DB Result to Array */
    public function getImplodeArray($codeArray)
    {
        foreach ($codeArray as $codeValue) {
            $codes[] = $codeValue['Code'];
        }
        return $codes;
    }
}