<?php

namespace App\Services\Validations;

use App\Services\Constants\ApiConstants;
use Illuminate\Http\Request;
use App\Http\Requests;
 
class RequestValidatorService
{
    /**
    * To return QueryString as Array
    * @param OBJECT REQUEST $requestObject
    * @return ARRAY $params
    */
    public function getParamsArray($requestObject)
    {
        $params = array();
        $queryString = $this->getQueryString($requestObject);
        if ($queryString !="") {
            $queryString  = explode('&', $queryString);
            foreach ($queryString as $param) {
                list ($name, $value) = explode('=', $param);
                array_push($params, $value);
            }
            return $params;
        }
        return $params;
    }

    /* To check Querystring is Valid or Not
    * @param ARRAY $paramsArray
    * @return BOOL $paramValid
    */
    public function validQueryString($paramsArray)
    {
        $allowedParams = ApiConstants::API_REQUESTED_PARAMS;
        return $this->checkValidParams($paramsArray, $allowedParams);
    }

    /**
    * To check POSTKey is Valid or Not
    * @param ARRAY $paramsArray
    * @return BOOL $paramValid
    */
    public function validPostKey($paramsArray, $allowableKeysArray)
    {
        return $this->checkValidParams($paramsArray, $allowableKeysArray);
    }

    /**
    * To check valid elements in the array
    * @param ARRAY $paramsArray
    * @param ARRAY $allowedParams
    * @return BOOL $paramValid
    */
    public function checkValidParams($testParam, $allowedParams)
    {
        $paramValid = true;
        foreach ($testParam as $key => $val) {
            if (!$this->checkElementsInArray($val, $allowedParams)) {
                $paramValid = false;
            }
        }
        return $paramValid;
    }
    
    /* To check whether additional to add along with player hostname */
    public function checkToAddAddtionalInfoWithPlayers($testParam)
    {
        $paramValid = false;
        foreach ($testParam as $key => $val) {
            if ($this->checkElementsInArray($val, ApiConstants::ADD_PARAMS_TO_PLAYER)) {
                $paramValid = true;
            }
        }
        return $paramValid;
    }

    /**
    * To return QueryString from the API Request
    * @param OBJECT REQUEST $requestObject
    * @return STRING
    */
    public function getQueryString($requestObject)
    {
        // To get the Query String
        //Example :{{api_base_url}}/api/host/test?data=app_version&data=config_version
        //Query_String : data=app_version&data=config_version
        return $requestObject->server->get('QUERY_STRING');
    }

    public function checkElementsInArray($value, $paramsArray)
    {
        return in_array($value, $paramsArray);
    }
}
