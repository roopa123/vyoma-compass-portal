<?php

namespace App\Services\Validations;

use App\Services\Constants\ApiConstants;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Services\Constants\AppErrorCodes;
 
class HostVersionService extends BaseValidationService
{
    private $dbNotifyServerUpdateFields = array();
    
    public function __construct(AppErrorCodes $appErrorCodes)
    {
        parent::__construct($appErrorCodes);
        $this->setDbFieldNames();
    }

    /* Validate Post Data for Updating Version for the Host */
    public function validateServerParameters($hostObject, $postData)
    {
        $errorArray = array();
        $this->validateEmptyValue($postData, ApiConstants::NOTIFY_SERVER_API_POST_JSON_KEYS);
        if (isset($postData['app_version'])) {
            $this->validateAppVersion($hostObject, $postData, 'app_version');
        }
        if (isset($postData['conf_version'])) {
            $this->validateConfVersion($hostObject, $postData, 'conf_version');
        }
        if (isset($postData['registered']) || isset($postData['online'])) {
            $this->validateStatus($hostObject, $postData);
            if (isset($postData['registered'])) {
                $this->setAlreadyRegisteredStatus($hostObject, $postData);
            }
        }
        if (isset($postData['public_ip'])) {
            $this->validatePublicIp($postData);
            if ($hostObject->MasterOrPlayer == ApiConstants::HOST_PLAYER) {
                $errorCode = AppErrorCodes::NOT_ALLOW_TO_UPDATE_PUBLIC_IP_FOR_PLAYERS;
                $this->setErrorCodeAndMessage($errorCode, 'public_ip');
            }
        }
        return array($this->validationStatus, $this->errorArray);
    }

    /* Validate AppVersion*/
    public function validateAppVersion($hostObject, $postData, $errorKey)
    {
        if (is_null($hostObject->NewAppVersion) && !is_null($hostObject->CurrentAppVersion)) {
            $errorCode = AppErrorCodes::APP_VERSION_ALREADY_UPDATED;
            $this->setErrorCodeAndMessage($errorCode, $errorKey);
        } else {
            if ($postData['app_version'] != $hostObject->NewAppVersion) {
                $errorCode = AppErrorCodes::INVALID_TOKEN;
                $this->setErrorCodeAndMessage($errorCode, $errorKey);
            }
        }
    }

    /* validate ConfVersion */
    public function validateConfVersion($hostObject, $postData, $errorKey)
    {
        if (is_null($hostObject->NewConfigVersion) && !is_null($hostObject->CurrentConfigVersion)) {
            $errorCode = AppErrorCodes::CONF_VERSION_ALREADY_UPDATED;
            $this->setErrorCodeAndMessage($errorCode, $errorKey);
        } else {
            if ($postData['conf_version'] != $hostObject->NewConfigVersion) {
                $errorCode = AppErrorCodes::INVALID_TOKEN;
                $this->setErrorCodeAndMessage($errorCode, $errorKey);
            }
        }
    }

    /* Validate Status for the Host [Resgistered, Online] */
    public function validateStatus($hostObject, $postData)
    {
        if (isset($postData['registered']) && isset($postData['online'])) {
            if ($postData['registered'] == ApiConstants::REGISTERED_NO && $postData['online'] == ApiConstants::ONLINE_YES) {
                    $errorCode = AppErrorCodes::INVALID_COMBINATION_VALUES_FOR_REGISTER_ONLINE;
                    $this->setErrorCodeAndMessage($errorCode, 'registered');
                    $this->setErrorCodeAndMessage($errorCode, 'online');
            }
        } else if (isset($postData['registered']) && !isset($postData['online'])) {
            // Post Data has only Registered Key
            if ($postData['registered'] == ApiConstants::REGISTERED_YES && $hostObject->IsRegistered == ApiConstants::YES_CHAR) {
                $this->setDuplicateErrorCode();
            } else if ($postData['registered'] == ApiConstants::REGISTERED_NO && $hostObject->IsOnline == ApiConstants::YES_CHAR) {
                $errorCode = AppErrorCodes::NOT_ALLOWED_TO_SET_REGISTER_FALSE_BECAUSE_HOST_ALREADY_ONLINE;
                $this->setErrorCodeAndMessage($errorCode, 'registered');
            } else {
            }
        } else if (isset($postData['online']) && !isset($postData['registered'])) {
            // Post Data has only online Key
            if ($postData['online'] == ApiConstants::ONLINE_YES && $hostObject->IsRegistered == ApiConstants::NO_CHAR) {
                $errorCode = AppErrorCodes::NOT_ALLOWED_TO_SET_ONLINE_TRUE_BECAUSE_HOST_NOT_REGISTERED;
                $this->setErrorCodeAndMessage($errorCode, 'online');
            }
        }
    }
    
    /* Set Duplicate Status */
    public function setAlreadyRegisteredStatus($hostObject, $postData)
    {
        if ($this->validationStatus['registered'] == ApiConstants::VALIDATION_SUCCESS) {
            if ($postData['registered'] == ApiConstants::REGISTERED_NO && $hostObject->IsRegistered == ApiConstants::YES_CHAR) {
                $this->validationStatus['registered'] = ApiConstants::REGISTER_YES_TO_NO;
            }
        }
    }

    //Return valid Post array to update DB
    public function getValidVersionArray($validatorArray, $postData)
    {
        $validUpdateData = array();
        foreach ($validatorArray as $key => $value) {
            if (isset($postData[$key]) && $value == ApiConstants::VALIDATION_SUCCESS) {
                $postValue = $postData[$key];
                if ($postValue == ApiConstants::YES_STRING) {
                    $postValue = ApiConstants::YES_CHAR;
                } else if ($postValue == ApiConstants::NO_STRING) {
                    $postValue = ApiConstants::NO_CHAR;
                }
                if ($key == 'app_version') {
                    $validUpdateData['NewAppVersion'] = null;
                } else if ($key == 'conf_version') {
                    $validUpdateData['NewConfigVersion'] = null;
                }
                $validUpdateData[$this->dbNotifyServerUpdateFields[$key]] = $postValue;
            }
        }
        return $validUpdateData;
    }

    // To validate Ip_Address
    public function validatePublicIp($postData)
    {
        if (filter_var($postData['public_ip'], FILTER_VALIDATE_IP) === false) {
            $this->setErrorCodeAndMessage(
                AppErrorCodes::NOT_VALID_IP_ADDRESS,
                'public_ip'
            );
        }
    }

    // To set DB Field Names
    public function setDbFieldNames()
    {
        $this->dbNotifyServerUpdateFields['app_version'] = "CurrentAppVersion";
        $this->dbNotifyServerUpdateFields['conf_version'] = "CurrentConfigVersion";
        $this->dbNotifyServerUpdateFields['mac_address'] = "MacAddress";
        $this->dbNotifyServerUpdateFields['public_ip'] = "PublicIPAddress";
        $this->dbNotifyServerUpdateFields['online'] = "IsOnline";
        $this->dbNotifyServerUpdateFields['registered'] = "IsRegistered";
    }

    // To set Duplicate
    public function setDuplicateErrorCode()
    {
        $this->setErrorCodeAndMessage(
            AppErrorCodes::DUPLICATE_REGISTER,
            'registered'
        );
    }
}
