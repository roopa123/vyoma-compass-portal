<?php

namespace App\Services\ApiResponse;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\Constants\AppSuccessCodes;
use App\Services\Constants\AppErrorCodes;
 
class ApiResponse
{
    /**
     * @var AppSuccessCodes
     */
    private $appSuccessCodes;

    /**
     * @var AppErrorCodes
     */
    private $appErrorCodes;

    public function __construct(AppSuccessCodes $appSuccessCodes, AppErrorCodes $appErrorCodes)
    {
        $this->appSuccessCodes = $appSuccessCodes;
        $this->appErrorCodes = $appErrorCodes;
    }

    public function response($data = null, $httpStatusCode = 200, $addStatusInResponse = false)
    {
        $response['response'] = $data;
        if ($addStatusInResponse) {
            $response['status'] = ['success' => 'true'];
        }
        return new JsonResponse($response, $httpStatusCode);
    }

    /**
    * Return Error Response in GET API call
    * @param int $errorCode
    * @return JSON response
    */
    public function createErrorResponse($errorCode, $message = "")
    {
        if ($message == "") {
            $message = $this->appErrorCodes->getMessageForCode($errorCode);
        }
        $errordata[] = array ('error_code' => $errorCode, 'error_msg' => $message);
        $error_response['status'] = [
            'success' => false];
        $error_response['error'] = $errordata;
        $reponse['data'] = $error_response;
        return $this->response($reponse);
    }

    /**
    * Return Response for POST API call
    * @param Array $errorData
    * @param Array $successData
    * @return JSON response
    */
    public function postUpdateResponse($errorData, $successData, $httpStatusCode = 200)
    {
        $error_response['status'] = $successData;
        $error_response['error'] = [];
        if (!empty($errorData)) {
            $error_response['error'] = $errorData;
        }
        $reponse['data'] = $error_response;
        return $this->response($reponse, $httpStatusCode);
    }

    /**
    * Return Error and Success Object
    * @param char $statusKey
    * @param INT $errorCode
    * @return JSON response
    */
    public function returnErrorResponseForPostAPI($statusKey, $errorCode)
    {
        $status[$statusKey] = array('success' => false);
        $message = $this->appErrorCodes->getMessageForCode($errorCode);
        $errorObject[] = array('error_code' => $errorCode, 'error_msg' => $message);
        $successObject[] = $status;
        return $this->postUpdateResponse($errorObject, $successObject);
    }
}
