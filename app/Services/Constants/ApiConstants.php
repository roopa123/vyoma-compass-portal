<?php

namespace App\Services\Constants;
 
class ApiConstants
{
    const PARAM_HOST_NAME = "hostname";

    const PARAM_ALL = "all";

    const PARAM_CONFIG = "config";

    const PARAM_PLAYERLIST = "playerlist";

    const PARAM_APP_VERSION = "app_version";

    const PARAM_CONF_VERSION = "conf_version";

    const PARAM_MAC_ADDRESS = "mac_address";

    const PARAM_STATUS = "status";

    const PARAM_REGISTERED = "registered";

    const PARAM_ONLINE = "online";

    const PARAM_IP_ADDRESS = "ip_address";

    const PARAM_ROLE = "role";

    const PARAM_INLCUDE_PLAYERS = "incl_players";

    const YES_CHAR = 'Y';

    const NO_CHAR = 'N';

    const YES_STRING = 'yes';

    const NO_STRING = 'no';
    
    const APP_PAGINATION = 50;

    const ENABLE = 1;

    const DISABLE = 0;

    const VALIDATION_FAIL = 0;

    const VALIDATION_SUCCESS =1;

    //Host Roles in DB

    const HOST_MASTER = 'M';

    const HOST_PLAYER = 'P';

    const HOST_BOTH = 'B';

    const HOST_PLASMA = 'S';


    // Host Roles in Config Tokens
    const ROLE_MASTER = 'master';

    const ROLE_PLAYER = 'player';

    const ROLE_BOTH = 'combi';

    const ROLE_PLASMA = 'plasma';

    const VALID_ROLE_TOKEN = array('master', 'player', 'combi', 'plasma');

    // Counters for HOST in DB
    const HOST_COUNTER_PRS = 'PRS';

    const HOST_COUNTER_UTS = 'UTS';

    // Config Tokens
    const COUNTER_PRS = 'prs';

    const COUNTER_UTS = 'uts';

    const COUNTER_UTS_DB = 'uts-db';

    const VALID_COUNTER_TYPE_TOKEN = array('prs', 'uts', 'uts-db');

    
    // POST json Keys
    const NOTIFY_SERVER_API_POST_JSON_KEYS = array('app_version', 'conf_version', 'mac_address', 'public_ip', 'online', 'registered');

    const REGISTERED_YES = 'yes';
    const REGISTERED_NO = 'no';
    const ONLINE_YES = 'yes';
    const ONLINE_NO = 'no';
    const REGISTER_YES_TO_NO = 2;
    const REGISTER_YES_TO_NO_MSG = "You Unregistered the Host";
    const DUPLICATE_REGISTER = 409;
       
    // Common Message For Updation
    const UPDATE_SUCCESS_MSG = "Updated Successfuly";
    const UPDATE_FAIL_MSG = "Updation Failed";
    
    const API_REQUESTED_PARAMS = array('mac_address', 'config', 'app_version', 'conf_version', 'status', 'online', 'ip_address', 'incl_players', 'playerlist', 'all', 'role');

    const ADD_PARAMS_TO_PLAYER = array('mac_address', 'config', 'app_version', 'conf_version', 'status', 'online', 'ip_address', 'all', 'role');

    //const API_REQUESTED_PARAMS = array(self::PARAM_MAC_ADDRESS,self::PARAM_CONFIG, self::PARAM_APP_VERSION , self::PARAM_CONF_VERSION, self::PARAM_STATUS, self::PARAM_ONLINE, self::PARAM_IP_ADDRESS, self::PARAM_ROLE, self::PARAM_PLAYERLIST, self::PARAM_INLCUDE_PLAYERS, self::PARAM_ALL);

    //const ADD_PARAMS_TO_PLAYER = array(self::PARAM_MAC_ADDRESS,self::PARAM_CONFIG, self::PARAM_APP_VERSION , self::PARAM_CONF_VERSION, self::PARAM_STATUS, self::PARAM_ONLINE, self::PARAM_IP_ADDRESS, self::PARAM_ROLE, self::PARAM_ALL);

    const CHECK_HOUR = 'HOUR';

    const CHECK_MINUTE = 'MINUTE';

    //All Config Tokens 38
    const  ALL_CONFIG_TOKENS = array('HOSTNAME', 'IP_ADDRESS', 'MASTER_IP', 'ROLE', 'COUNTER_TYPE', 'DEPLOY_STATE', 'PLAYER_WIN', 'REBOOT_HOUR', 'REBOOT_MIN', 'SCREENOFF_HOUR', 'SCREENOFF_MIN', 'SCREENOFF_HOUR_SUN', 'SCREENOFF_MIN_SUN', 'HDMI_MODE', 'SCREEN_RATIO', 'ROUTER_REBOOT', 'ROUTER_USERNAME', 'ROUTER_PASSWORD', 'ROUTER_REBOOT_MIN', 'ROUTER_REBOOT_HOUR', 'MMONIT_REPORT', 'MONIT_EVENTQUEUE', 'ENABLE_CRON', 'CABLE_TYPE', 'LOG_PUSH', 'LOG_PUSH_MIN', 'LOG_PUSH_HOUR', 'CONTENT_UPDATE', 'UPDATE_SCHEDULE', 'READTTY_LOG', 'UPLOAD_CSV', 'UPLOAD_CSV_SCHEDULE', 'DEBUG_MODE', 'ENABLE_SYSSTAT', 'SCREENSHOT', 'SCREENSHOT_SCHEDULE_HOUR', 'SCREENSHOT_SCHEDULE_MIN', 'THANKS_MSG');

    // Mandatory Config Tokens
    const  MANDATORY_CONFIG_TOKENS = array('HOSTNAME', 'IP_ADDRESS', 'MASTER_IP', 'ROLE', 'COUNTER_TYPE', 'DEPLOY_STATE', 'PLAYER_WIN', 'REBOOT_HOUR', 'REBOOT_MIN', 'SCREENOFF_HOUR', 'SCREENOFF_MIN', 'MMONIT_REPORT', 'MONIT_EVENTQUEUE', 'ENABLE_CRON', 'CABLE_TYPE', 'DEBUG_MODE', 'ENABLE_SYSSTAT', 'SCREENSHOT');

    // Tokens only for Master
    const  MANDATORY_MASTER_TOKENS = array('ROUTER_REBOOT', 'ROUTER_USERNAME', 'ROUTER_PASSWORD', 'ROUTER_REBOOT_MIN', 'ROUTER_REBOOT_HOUR', 'LOG_PUSH', 'CONTENT_UPDATE');

    // Tokens only for Both
    const  MANDATORY_BOTH_TOKENS = array('ROUTER_REBOOT', 'ROUTER_USERNAME', 'ROUTER_PASSWORD', 'ROUTER_REBOOT_MIN', 'ROUTER_REBOOT_HOUR', 'LOG_PUSH', 'CONTENT_UPDATE', 'SCREEN_RATIO', 'READTTY_LOG', 'THANKS_MSG');

    // Tokens only for Player
    const  MANDATORY_PLAYER_TOKENS = array('SCREEN_RATIO', 'READTTY_LOG', 'THANKS_MSG');

    // Enable/Disable Tokens Count: 12
    const  CONFIG_ENABLE_DISABLE_TOKENS = array('ROUTER_REBOOT', 'MMONIT_REPORT', 'MONIT_EVENTQUEUE', 'ENABLE_CRON', 'LOG_PUSH', 'CONTENT_UPDATE', 'READTTY_LOG', 'UPLOAD_CSV', 'DEBUG_MODE', 'ENABLE_SYSSTAT', 'SCREENSHOT', 'THANKS_MSG');

    // Token Values comes fron Code Table
    const  CODE_CONFIG_TOKENS = array('PLAYER_WIN', 'HDMI_MODE', 'SCREEN_RATIO', 'CABLE_TYPE');

    // Tokens Does not need Transformation of data before store it in DB
     const UPDATE_TOKENS_WITH_NO_TRANSFORMATION = array('PLAYER_WIN', 'HDMI_MODE', 'MMONIT_REPORT', 'MONIT_EVENTQUEUE', 'ENABLE_CRON', 'CABLE_TYPE', 'DEBUG_MODE', 'ENABLE_SYSSTAT', 'SCREENSHOT');

     const UPDATE_TOKENS_WITH_NO_TRANSFORMATION_MASTER = array('ROUTER_REBOOT', 'ROUTER_USERNAME', 'ROUTER_PASSWORD', 'LOG_PUSH', 'CONTENT_UPDATE');

     const UPDATE_TOKENS_WITH_NO_TRANSFORMATION_BOTH = array('ROUTER_REBOOT', 'ROUTER_USERNAME', 'ROUTER_PASSWORD', 'LOG_PUSH', 'CONTENT_UPDATE', 'READTTY_LOG', 'UPLOAD_CSV', 'THANKS_MSG', 'SCREEN_RATIO');

     const UPDATE_TOKENS_WITH_NO_TRANSFORMATION_PLAYER = array('READTTY_LOG', 'UPLOAD_CSV', 'THANKS_MSG', 'SCREEN_RATIO');

    // Cron Entry Tokens
    const SCHEDULE_TOKENS = array('ROUTER_REBOOT_MIN', 'ROUTER_REBOOT_HOUR', 'LOG_PUSH_MIN', 'LOG_PUSH_HOUR', 'UPDATE_SCHEDULE', 'UPLOAD_CSV_SCHEDULE','SCREENSHOT_SCHEDULE_HOUR', 'SCREENSHOT_SCHEDULE_MIN');

    const PLAY_LOG_SCHEDULE = array('LOG_PUSH_HOUR', 'LOG_PUSH_MIN');

    const CONTENT_SCHEDULE = array('UPDATE_SCHEDULE');

    const TICKET_SCHEDULE = array('UPLOAD_CSV_SCHEDULE');

    const SCREENSHOT_SCHEDULE = array('SCREENSHOT_SCHEDULE_HOUR', 'SCREENSHOT_SCHEDULE_MIN');

    const ROUTER_REBOOT_SCHEDULE = array('ROUTER_REBOOT_MIN', 'ROUTER_REBOOT_HOUR');

    const TIME_HOUR_TOKENS = array('REBOOT_HOUR', 'SCREENOFF_HOUR');

    const TIME_MIN_TOKENS = array('REBOOT_MIN', 'SCREENOFF_MIN');

    const PRS_TIME_HOUR_TOKENS = array('SCREENOFF_HOUR_SUN');

    const PRS_TIME_MIN_TOKENS = array('SCREENOFF_MIN_SUN');

    const ROUTER_REBOOT_SCHEDULE_HOUR = array('ROUTER_REBOOT_HOUR');

    const ROUTER_REBOOT_SCHEDULE_MIN = array('ROUTER_REBOOT_MIN');

    const PRS_TOKENS = array('SCREENOFF_HOUR_SUN', 'SCREENOFF_MIN_SUN');

    const VALUES_SHOULD_BE_LIKE = "The Valid values should be in any one of the following ";

    const RANGE = "Valid values for Hour Between 0-23 And Minute Between 0-59";

    const SCREEN_START_TIME_MSG = "Screen start time should be less than screen end time.";

    const SCREEN_END_TIME_MSG = "Screen End time should be greater than screen Start time";

    const SCREEN_START_TIME_SERVER_MSG = "Screen start time should be greater than or equal to counters start time.Start time Range between ";

    const SCREEN_END_TIME_SERVER_MSG = "Screen End time should be less than or equal to counters End  time.End time Range between ";

    const SCHEDULE_EMPTY = "This Token should be Empty because Schedule for this token is disabled";

    const LOG_PUSH_MIN_VALID_VALUES = "*/30,0";

    const LOG_PUSH_HOUR_VALID_VALUES = "*";

    const UPDATE_SCHEDULE_VALID_VALUES = "*/10,*/30,0";

    const UPLOAD_CSV_SCHEDULE_VALID_VALUES = "*/30,0";

    const SCREENSHOT_SCHEDULE_MIN_VALID_VALUES = "00";

    const SCREENSHOT_SCHEDULE_HOUR_VALID_VALUES = "12";

    const ROUTER_REBOOT_MIN_VALID_VALUES = "*/30,0";

    const ROUTER_REBOOT_HOUR_VALID_VALUES = "*,*/2";
}
