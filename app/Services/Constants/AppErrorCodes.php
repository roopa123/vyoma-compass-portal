<?php
namespace App\Services\Constants;

class AppErrorCodes extends AbstractConstant
{
    const QUERY_STRING_INVALID = 2;
    //Host is not a Master
    const HOST_NOT_MASTER = 3;

    const APP_EXCEPTION = 111;

    //General Error Codes for POST JSON
    const TOKEN_NOT_EMPTY = 100;
    const INVALID_TOKEN = 99;
    const TOKEN_MISSING_IN_POST_JSON = 98;
    const INVALID_POST_KEYS_IN_JSON = 97;
    const INVALID_POST_JSON_STRING = 96;

    //Notify Server Update Codes
    const APP_VERSION_ALREADY_UPDATED = 1001;
    const CONF_VERSION_ALREADY_UPDATED = 1004;
    const NOT_ALLOWED_TO_SET_REGISTER_FALSE_BECAUSE_HOST_ALREADY_ONLINE = 1007;
    const INVALID_COMBINATION_VALUES_FOR_REGISTER_ONLINE = 1008;
    const NOT_ALLOWED_TO_SET_ONLINE_TRUE_BECAUSE_HOST_NOT_REGISTERED = 1009;
    const DUPLICATE_REGISTER = 409;
    
    //Update Host Config Tokens
    const MASTER_ALREADY_PRESENT = 1014;
    const CANT_CHANGE_BOTH_TO_PLAYERS = 1015;
    const CANT_CHANGE_MASTER_TO_PLAYERS = 1016;    
    const CANT_CHANGE_PLAYER_TO_BOTH = 1017;
    const NOT_VALID_IP_ADDRESS = 1018;
    const NO_TARBALL_URL = 1019;
    const INVALID_APP = 1020;
    const NOT_ALLOW_TO_UPDATE_PUBLIC_IP_FOR_PLAYERS = 1021;
    const CANT_CHANGE_MASTER_TO_PLASMA = 1022;
    
    
    protected function setMessages()
    {
        $this->messages = [
            self::QUERY_STRING_INVALID => $this->lan->get('api_error.querystringinvalid'),
            self::HOST_NOT_MASTER => $this->lan->get('api_error.hostnotmaster'),
            
            /* Error Message for Notify Server Update */
            self::APP_VERSION_ALREADY_UPDATED => $this->lan->get('api_error.appversionalreadyupdated'),
            self::CONF_VERSION_ALREADY_UPDATED => $this->lan->get('api_error.confversionalreadyupdated'),
            self::NOT_ALLOWED_TO_SET_REGISTER_FALSE_BECAUSE_HOST_ALREADY_ONLINE => $this->lan->get('api_error.invalidregister'),
            self::INVALID_COMBINATION_VALUES_FOR_REGISTER_ONLINE => $this->lan->get('api_error.invalidstatus'),
            self::NOT_ALLOWED_TO_SET_ONLINE_TRUE_BECAUSE_HOST_NOT_REGISTERED => $this->lan->get('api_error.invalidonline'),
            self::DUPLICATE_REGISTER => $this->lan->get('api_error.duplicateregister'),
            /* Common Validation in POST */
            self::INVALID_TOKEN => $this->lan->get('api_error.invalidtoken'),
            self::TOKEN_NOT_EMPTY => $this->lan->get('api_error.tokennotempty'),
            self::TOKEN_MISSING_IN_POST_JSON => $this->lan->get('api_error.tokenmissing'),
            self::INVALID_POST_KEYS_IN_JSON => $this->lan->get('api_error.invalidpostkeys'),
            self::INVALID_POST_JSON_STRING => $this->lan->get('api_error.invalidpostjsonstring'),
            /* Update config tokens error messages for Role */
            self::MASTER_ALREADY_PRESENT => $this->lan->get('api_error.masteralreadypresent'),
            self::CANT_CHANGE_BOTH_TO_PLAYERS => $this->lan->get('api_error.cantchangebothtoplayers'),
            self::CANT_CHANGE_MASTER_TO_PLAYERS => $this->lan->get('api_error.cantchangemastertoplayers'),
            self::CANT_CHANGE_PLAYER_TO_BOTH => $this->lan->get('api_error.cantchangeplayertoboth'),
            self::NOT_VALID_IP_ADDRESS => $this->lan->get('api_error.notavalidip'),
            self::NO_TARBALL_URL => $this->lan->get('api_error.notarballurl'),
            self::INVALID_APP => $this->lan->get('api_error.invalidapp'),
            self::NOT_ALLOW_TO_UPDATE_PUBLIC_IP_FOR_PLAYERS => $this->lan->get('api_error.notupdatepublicip'),
            self::CANT_CHANGE_MASTER_TO_PLASMA => $this->lan->get('api_error.cantchangemastertoplasma')
        ];
    }
}
