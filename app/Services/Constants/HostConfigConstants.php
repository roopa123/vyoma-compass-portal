<?php

namespace App\Services\Constants;
 
class HostConfigConstants
{ 
	//Code Constants

    const CODE_PLAYER_WINSIZE = 'PlayerWinSize';
    const CODE_HDMI_MODE = 'HdmiMode';
    const CODE_CABLE_TYPE = 'CableType';
    const CODE_DISPLAY_TYPE = 'DisplayType';
    const CODE_DISPLAY_SIZE = 'DisplaySize';
    const CODE_PIMODEL = 'PiModel';
    const CODE_POWER_ADAPTER = 'PowerAdapterModel';
    const CODE_SCREEN_RATIO = 'ScreenRatio';
    const CODE_DEFAULT = 'Default'; 
    const STATUS_ACTIVE = 'A';  
    const VIEW_MODE_ADD = 'add';
    const VIEW_MODE_EDIT = 'edit';

    const DROPDOWN_LISTS = array('PlayerWinSize', 'HdmiMode', 'ScreenRatio', 'RouterRebootSchedule', 'CableType', 'PlayLogUpdateSchedule', 'ContentUpdateSchedule', 'TicketLogUpdateSchedule', 'DisplayType', 'DisplaySize', 'PiModel', 'PowerAdapterModel'); 
}