<?php
namespace App\Services\Constants;

class AppSuccessCodes extends AbstractConstant
{
    const TEST = 200;
    
    protected function setMessages()
    {
        $this->messages = [
            self::TEST => "success"
        ];
    }
}
