<?php

namespace App\Services\Constants;
 
class HttpStatusCodes
{
    const HTTP_SUCCESS_CODE = 200;

    const HTPP_NOT_FOUND = 404;
 
    const DUPLICATE_REGISTER = 409;
    
    const HTTP_SEE_OTHER = 303;
}
