<?php
namespace App\Services\ConfigTokenCreation;

use App\Services\Constants\ApiConstants;
use App\Models\State;
use App\Commands\HostTransformerTrait;
use App\Repositories\HostRepository;
use App\Services\Validations\BaseValidationService;
 
class ConfigTokenCreateService extends BaseValidationService
{
    use HostTransformerTrait;
    
    private $dbConfigTokenFieldName = array();

    private $validUpdateData = array();
    
    private $hostRepo;

    public function __construct(HostRepository $hostRepo)
    {
        $this->setDbFieldNames();
        $this->hostRepo = $hostRepo;
    }

    /**
    * To Form POST Data Array to update 
    * @param Array $postData
    * @return Array $validUpdateData
    */
    public function getConfigPostData($postData)
    {
        
        $configConstants = ApiConstants::UPDATE_TOKENS_WITH_NO_TRANSFORMATION;
        $configTokenArray = $this->getConstantsBasedOnHost($postData, $configConstants);
        foreach ($configTokenArray as $key => $configToken) {
            if(isset($postData[$configToken])) {
                $this->validUpdateData[$this->dbConfigTokenFieldName[$configToken]] = $postData[$configToken];
            }            
        }
        $this->setEmptyValuesForConfigTokens();
        $this->setRole($postData['ROLE']);
        $this->setCounterType($postData['COUNTER_TYPE']);
        $this->setStateCode($postData['DEPLOY_STATE']);
        $this->setTimeTokens($postData);
        $this->setIPAddress($postData['IP_ADDRESS']);
        $this->setScheduleValues($postData);
        $this->setCSVBasedOnReadttyLog($postData);
        return $this->validUpdateData;
    }

    /**
    * Get transormation Array based on Role 
    * @param Array $postData, $configArray
    * @return Array $configArray
    */
    public function getConstantsBasedOnHost($postData, $configArray)
    {
        $role = $this->removeSingleQuotes($postData['ROLE']);
        if ($role == ApiConstants::ROLE_MASTER){
            $configArray = array_merge($configArray, ApiConstants::UPDATE_TOKENS_WITH_NO_TRANSFORMATION_MASTER);
        }
        if($role == ApiConstants::ROLE_BOTH) {
            $configArray = array_merge($configArray, ApiConstants::UPDATE_TOKENS_WITH_NO_TRANSFORMATION_BOTH);
        }
        if ($role == ApiConstants::ROLE_PLAYER){
            $configArray = array_merge($configArray, ApiConstants::UPDATE_TOKENS_WITH_NO_TRANSFORMATION_PLAYER);
        }

        return $configArray;
    }

    /** 
    *To set null for dependency fields 
    * @param Array $postData
    * @return Array $validUpdateData
    */
    public function setEmptyValuesForConfigTokens()
    {
        /* If Counter Type is Not PRS */
        $this->validUpdateData['SunScreenEndTIme'] = null;
        /* If Schedule Tokens Disabled */
        $this->validUpdateData['PlayLogUpdateSchedule'] = null;
        $this->validUpdateData['ContentUpdateSchedule'] = null;
        $this->validUpdateData['TicketLogUpdateSchedule'] = null;
        $this->validUpdateData['ScreenShotSchedule'] = null;
        $this->validUpdateData['RouterRebootSchedule'] = null;
    }

    /** 
    *To set null for uploadcsv if readtty log is disabled.
    * @param Array $postData
    * @return Array $validUpdateData
    */
    public function setCSVBasedOnReadttyLog($postData)
    {
        $role = $this->trimSingleQuotes($postData['ROLE']);
        if ($role == ApiConstants::ROLE_BOTH || $role == ApiConstants::ROLE_PLAYER || $role == ApiConstants::ROLE_PLASMA) {
            $this->validUpdateData['IsReadTTYLogEnabled'] = $postData['READTTY_LOG'];
            if ($postData['READTTY_LOG'] == ApiConstants::DISABLE) {
                $this->validUpdateData['IsTicketLogUpdateEnabled'] = 0;
                $this->validUpdateData['TicketLogUpdateSchedule'] = null;
            }
        }
    }

    /**
    *To set Host Role 
    *@param String $role
    *@return Array $validUpdateData
    */
    public function setRole($role)
    {
        $role = $this->trimSingleQuotes($role);
        if ($role == ApiConstants::ROLE_MASTER) {
            $this->validUpdateData['MasterOrPlayer'] = \Config::get('vyoma-constants.masterType');
        }
        if ($role == ApiConstants::ROLE_BOTH) {
            $this->validUpdateData['MasterOrPlayer'] = \Config::get('vyoma-constants.bothType');
        }
        if ($role == ApiConstants::ROLE_PLAYER) {
            $this->validUpdateData['MasterOrPlayer'] = \Config::get('vyoma-constants.playerType');
        }   
        if($role == ApiConstants::ROLE_PLASMA) {
            $this->validUpdateData['ShowTicketData'] = ApiConstants::NO_CHAR;
        }  
    }

    /**
    *To set IP Address 
    *@param int $ip
    *@return Array $validUpdateData
    */
    public function setIPAddress($ip)
    {  
        $this->validUpdateData['LocalIPAddress'] = $ip;         
    }

    /**
    *To set State Code
    *@param String $deployState
    *@return Array $validUpdateData
    */
    public function setStateCode($deployState)
    {
        $statecode = $this->trimSingleQuotes($deployState);
        $stateId = $this->hostRepo->validateStateCode($statecode);
        $this->validUpdateData['DeployStateId'] = $stateId;
    }

    /**
    *To set Counter Type
    *@param String $deployState
    *@return Array $validUpdateData
    */
    public function setCounterType($counterType)
    {
        $counterType = $this->trimSingleQuotes($counterType);
        if ($counterType == ApiConstants::COUNTER_PRS) {
            $this->validUpdateData['HostCounterType'] = ApiConstants::HOST_COUNTER_PRS;
        }
        if ($counterType == ApiConstants::COUNTER_UTS) {
            $this->validUpdateData['HostCounterType'] = ApiConstants::HOST_COUNTER_UTS;
        }
        if ($counterType == ApiConstants::COUNTER_UTS_DB) {
            $this->validUpdateData['HostCounterType'] = ApiConstants::HOST_COUNTER_UTS;
            $this->validUpdateData['IsUTSDumb'] = ApiConstants::YES_CHAR;
        }
    }

    /**
    *To set ScreenStartTime, ScreenEndTIme, SunScreenEndTIme
    *@param Array $postData
    *@return Array $validUpdateData
    */
    public function setTimeTokens($postData)
    {
        /* ScreenStartTime, ScreenEndTime Calculation*/
        $rebotTime = $postData['REBOOT_HOUR'].":".$postData['REBOOT_MIN'];
        $rebootStartTime = date('H:i:s', strtotime($rebotTime));
        $rebotOffTime = $postData['SCREENOFF_HOUR'].":".$postData['SCREENOFF_MIN'];
        $rebootEndTime = date('H:i:s', strtotime($rebotOffTime));
        $this->validUpdateData['ScreenStartTime'] = $rebootStartTime;
        $this->validUpdateData['ScreenEndTIme'] = $rebootEndTime;
        
        $counterType = $this->trimSingleQuotes($postData['COUNTER_TYPE']);
        if ($counterType == ApiConstants::COUNTER_PRS) {
            $sunOffTime = $postData['SCREENOFF_HOUR_SUN'].":".$postData['SCREENOFF_MIN_SUN'];
            $sunTime = date('H:i:s', strtotime($sunOffTime));
            $this->validUpdateData['SunScreenEndTIme'] = $sunTime;
        }
    }

    
    /**
    *To set PlayLogUpdateSchedule, ContentUpdateSchedule, TicketLogUpdateSchedule,     SCREENSHOT_SCHEDULE_MIN, RouterRebootSchedule
    *@param Array $postData
    *@return Array $validUpdateData
    */   
    public function setScheduleValues($postData)
    {
        $role = $this->removeSingleQuotes($postData['ROLE']);
        if ($role == ApiConstants::ROLE_MASTER || $role == ApiConstants::ROLE_BOTH){
            if ($postData['LOG_PUSH'] == ApiConstants::ENABLE) {
                $playLogSchedule = $postData['LOG_PUSH_MIN']." ".$postData['LOG_PUSH_HOUR']." * * *";
                $this->validUpdateData['PlayLogUpdateSchedule'] = $playLogSchedule;
            }
            if ($postData['CONTENT_UPDATE'] == ApiConstants::ENABLE) {
                $contentSchedule = $postData['UPDATE_SCHEDULE']." * * * *";
                $this->validUpdateData['ContentUpdateSchedule'] = $contentSchedule;
            }
            if ($postData['ROUTER_REBOOT'] == ApiConstants::ENABLE) {
                $routerRebootSchedule = $postData['ROUTER_REBOOT_MIN']." ".$postData['ROUTER_REBOOT_HOUR']." * * *";
                $this->validUpdateData['RouterRebootSchedule'] = $routerRebootSchedule;
            }
        }
        if ($role == ApiConstants::ROLE_PLAYER || $role == ApiConstants::ROLE_BOTH){   
                if ($postData['READTTY_LOG'] == ApiConstants::ENABLE) {
                    if ($postData['UPLOAD_CSV'] == ApiConstants::ENABLE) {
                        $csvSchedule = $postData['UPLOAD_CSV_SCHEDULE']." * * * *";
                        $this->validUpdateData['TicketLogUpdateSchedule'] = $csvSchedule;
                    } 
               }
        }
        if ($postData['SCREENSHOT'] == ApiConstants::ENABLE) {
            $screenShotSchedule = $postData['SCREENSHOT_SCHEDULE_MIN']." ".$postData['SCREENSHOT_SCHEDULE_HOUR']." * * *";
            $this->validUpdateData['ScreenShotSchedule'] = $screenShotSchedule;
        }        
    }

    /**
    *Set DB FieldNames which needs no transformation of data before storing it in DB
    *@param Array $postData
    *@return Array $validUpdateData
    */     
    public function setDbFieldNames()
    {
        $this->dbConfigTokenFieldName['PLAYER_WIN'] = "PlayerWinSize";
        $this->dbConfigTokenFieldName['HDMI_MODE'] = "HdmiMode";
        $this->dbConfigTokenFieldName['SCREEN_RATIO'] = "ScreenRatio";
        $this->dbConfigTokenFieldName['ROUTER_REBOOT'] = "IsRouterRebootEnabled";
        $this->dbConfigTokenFieldName['ROUTER_USERNAME'] = "RouterUserName";
        $this->dbConfigTokenFieldName['ROUTER_PASSWORD'] = "RouterPassword";
        $this->dbConfigTokenFieldName['MMONIT_REPORT'] = "IsMonitEventEnabled";
        $this->dbConfigTokenFieldName['MONIT_EVENTQUEUE'] = "IsMonitQueueEnabled";
        $this->dbConfigTokenFieldName['ENABLE_CRON'] = "IsCronEnabled";
        $this->dbConfigTokenFieldName['CABLE_TYPE'] = "CableType";
        $this->dbConfigTokenFieldName['LOG_PUSH'] = "IsPlayLogUpdateEnabled";
        $this->dbConfigTokenFieldName['CONTENT_UPDATE'] = "IsContentUpdateEnabled";
        $this->dbConfigTokenFieldName['READTTY_LOG'] = "IsReadTTYLogEnabled";
        $this->dbConfigTokenFieldName['UPLOAD_CSV'] = "IsTicketLogUpdateEnabled";
        $this->dbConfigTokenFieldName['DEBUG_MODE'] = "IsDebugModeEnabled";
        $this->dbConfigTokenFieldName['ENABLE_SYSSTAT'] = "IsSysStatEnabled";
        $this->dbConfigTokenFieldName['SCREENSHOT'] = "IsScreenShotEnabled";
        $this->dbConfigTokenFieldName['THANKS_MSG'] = "IsThxMessageEnabled";
    }
}