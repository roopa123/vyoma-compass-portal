<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidationExtensionServiceProvider extends ServiceProvider {

    public function register() {}

    public function boot() {
        $this->app->validator->resolver( function( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
            return new \App\validator\customValidatorForHostName( $translator, $data, $rules, $messages, $customAttributes );
        } );

        /**
        * Validator for video upload
        */
        $this->app->validator->resolver( function( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
            return new \App\validator\creativeVideoValidator( $translator, $data, $rules, $messages, $customAttributes );
        } );

        /**
        * Validator for video upload
        */
        $this->app->validator->resolver( function( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
            return new \App\validator\CampaignDateValidator( $translator, $data, $rules, $messages, $customAttributes );
        } );

        /**
        * Validator for Host Vmc
        */
        $this->app->validator->resolver( function( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
            return new \App\validator\HostVmcValidator( $translator, $data, $rules, $messages, $customAttributes,  new \App\Repositories\HostRepository);
        } );
    }

}
