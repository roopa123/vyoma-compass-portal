<?php
namespace App\Repositories;

use App\Models\Branch;
use App\Models\Region;
use App\Models\Station;
use App\Models\Bay;
use App\Models\Host;
use App\Models\User;
use DB;
use Carbon\Carbon;

class BranchRepositories extends BaseRepository
{
    /* Validation for assigning people to project and manager */
    public $validationRules = [
            'BranchName' => 'required|unique:Branch|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/',
            'Status' => 'required'
        ];

    public function editValidationRules($branch)
    {
        $rules = array('BranchName' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/|unique:Branch,BranchName,'.$branch['BranchId'].',BranchId');

        return $rules;
    }

    /* Insert data to DB table */
    public function insertData($inputData)
    {
        $branch_name = $inputData['BranchName'];
        $dataToInsert = array(
          'BranchName' => $branch_name,
          'Status' => $inputData['Status']
        );
        $inserted = DB::table('Branch')->insertGetId($dataToInsert);
        return $inserted;
    }
    
    /* Fetch list of branches */
    public function branchlist()
    {
        return Branch::all();
    }
    
    /* Count of Branches */
    public function branchCount()
    {
        return Branch::where('Status', 'A')->count();
    }

    /* Count of Regions */
    public function regionCount()
    {
        return Region::where('Status', 'A')->count();
    }

    /* Count of Stations */
    public function stationCount()
    {
        return Station::where('Status', 'A')->count();
    }

    /* Count of Bays */
    public function bayCount()
    {
        return Bay::where('Status', 'A')->count();
    }

    /* Count of Hosts */
    public function hostCount()
    {
        return Host::where('Status', 'A')->count();
    }

    /* Fetch Details of branch based on id */
    public function getBranchDetails($branchId)
    {
        return Branch::where('BranchId', $branchId)->get();
    }

    /* Update Details of branch based on id */
    public function updateBranchDetails($branchId)
    {
        return DB::table('Branch')->where('BranchId', $branchId)
                ->update(array('BranchName' => \Input::get('BranchName'),
                                'Status' => \Input::get('Status')
                        ));
    }

    /* Autocomplete search for Branch */
    public function autocompleteSearch($searchterm)
    {
        $term = $searchterm;
        $results = array();
        $queries = DB::table('Branch')
                    ->where('BranchName', 'LIKE', $term.'%')
                    ->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();
        foreach ($queries as $query) {
            if (\Request::segment(3) == 'BranchName') {
                $results[] = [ 'id' => $query->BranchId, 'value' => $query->BranchName];
            } else if (\Request::segment(3) == 'Status') {
                $results[] = [ 'id' => $query->BranchId, 'value' => $query->Status];
            }
        }
        return \Response::json($results);
    }

    /* To Get Active Branches */
    public function getActiveBranches($isArray = false)
    {
        $branches = Branch::select('BranchId', 'BranchName')->orderby('BranchName', 'ASC')
                     ->where('Status', self::STATUS_ACTIVE)->get();
        if ($isArray) {
            return $this->convertIntoArray($branches);
        }
        return $branches;
    }

    /**
     * To Get Active Branches
     * @param Object $Branches
     * @return Array Branches
     */
    private function convertIntoArray($branchArray)
    {
        $branches[null] = 'Select';
        if (is_object($branchArray)) {
            foreach ($branchArray as $branch) {
                $branches[$branch->BranchId] = $branch->BranchName;
            }
            return $branches;
        }
        return $branches;
    }

    /* Delete row of branch based on id */
    public function fetchBranches($isArray = false)
    {
        $branches = Branch::select('BranchId', 'BranchName')
            ->orderby('BranchName', 'ASC')
            ->where('Status', self::STATUS_ACTIVE)
            ->get();
        if ($isArray) {
            return $this->creativeArrayType($branches);
        }
        return $branches;
    }

    private function creativeArrayType($branches)
    {
        $brancheArray[] = 'Select';
        if (is_object($branches)) {
            foreach ($branches as $branche) {
                $brancheArray[$branche->BranchId] = $branche->BranchName;
            }
            return $brancheArray;
        }
        return $brancheArray;
    }
}
