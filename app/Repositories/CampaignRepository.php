<?php
namespace App\Repositories;

use App\Models\Campaign;
use DB;
use App\Repositories\UserRepository;
use App\Repositories\PoCampaignRepository;
use App\Services\Constants\AppConstants;

class CampaignRepository extends BaseRepository
{
    private $campaign;

    private $userRepository;

    private $poCampaignRepository;

    public function __construct(
        Campaign $campaign,
        UserRepository $userRepository,
        PoCampaignRepository $poCampaignRepository
    ) {
        $this->campaign = $campaign;
        $this->userRepository = $userRepository;
        $this->poCampaignRepository = $poCampaignRepository;
    }

    /**
    * Create Campaign
    * @param Array $inputData
    * @param Char $POStatus
    * @return object $this->campaign
    */
    public function createCampaign($inputData, $POStatus)
    {
        $this->campaign->CampaignName = $inputData['campaignName'];
        $this->campaign->CampaignStartDate = $inputData['campaignStartDate'];
        $this->campaign->CampaignEndDate = $inputData['campaignEndDate'];
        $this->campaign->SpotsPerDay = $inputData['spotsPerDay'];
        $this->campaign->Status =  $POStatus;
        $this->campaign->UserId = $this->userRepository->getCurrentUserLoginId();
        $this->campaign->save();
        return $this->campaign;
    }

    /**
    * Check whether Campagin Form Data Changed
    * @param Array $inputData
    * @param INT $status
    * @return INT CAMPAIGN_FORM_CHANGED:2,NO_CHANGE:1
    */
    public function changesInCampaign($inputData, $status)
    {
        $updateCampaignObj = $this->findCampaign($inputData['campaignId']);
        $updateCampaignObj->CampaignName = $inputData['campaignName'];
        $updateCampaignObj->CampaignStartDate = $inputData['campaignStartDate'];
        $updateCampaignObj->CampaignEndDate = $inputData['campaignEndDate'];
        $updateCampaignObj->SpotsPerDay = $inputData['spotsPerDay'];
        $updateCampaignObj->Status = $status;
        $updateCampaignObj->UserId = $this->userRepository->getCurrentUserLoginId();
        if (count($updateCampaignObj->getDirty()) > 0) {
            return AppConstants::CAMPAIGN_FORM_CHANGED;
        }
        return AppConstants::NO_CHANGE;
    }

    /**
    * Update Campaign Status
    * @param INT $campaignId
    * @param CHAR $status
    * @return INT Affected Rows
    */
    public function updateStatus($campaignId, $status)
    {
        return Campaign::where('CampaignId', $campaignId)->update(['Status' => $status]);
    }
   
    /**
    * To Find Campaign Object For Updating
    * @param INT $POId
    * @return Object Campaign
    */
    public function findCampaign($campaignId)
    {
        return Campaign::find($campaignId);
    }

    /**
    * To create Multiple Campaigns in a single time
    * @param Object $POCampaignObj
    * @param INT $POId
    * @return
    */
    public function createCampaignsAndPOCampaignAsCreativeNull($POCampaignObj, $POId)
    {
        foreach ($POCampaignObj as $POCampaign) {
        /*Creating Campaign*/
            $campaign = Campaign::where('CampaignId', $POCampaign->CampaignId)->get();
            $campaignObj = $this->createPartialCampaign($campaign);
            $POCampaignParameterArray = $this->getPOCampaignParameterArray(
                $POCampaign,
                $POId,
                $campaignObj->CampaignId
            );
            $this->poCampaignRepository->createMultiplePOCampaignAndUpdateStatus(
                $POCampaignParameterArray,
                $POCampaign->POCampaignId
            );
        }
    }

    /**
    * To Create Partial Campaign
    * @param Object $campaign
    * @return Object Campaign
    */
    public function createPartialCampaign($campaign)
    {
        $campaignObj = $this->campaign->newInstance();
        $campaignObj->CampaignName = $campaign[0]->CampaignName;
        $campaignObj->CampaignStartDate = $campaign[0]->CampaignStartDate;
        $campaignObj->CampaignEndDate = $campaign[0]->CampaignEndDate;
        $campaignObj->SpotsPerDay = $campaign[0]->SpotsPerDay;
        $campaignObj->Status = self::PURCHASE_ORDER_PARTIAL_STATUS;
        $campaignObj->UserId = $this->userRepository->getCurrentUserLoginId();
        $campaignObj->save();
        return $campaignObj;
    }

    /**
    * To get POCampaignParameterArray
    * @param Object $POCampaign
    * @param INT $POId
    * @param INT $campaignId
    * @return Array
    */
    public function getPOCampaignParameterArray($POCampaign, $POId, $campaignId)
    {
        $creativeCampaignId = null;
        $POStatus = self::PURCHASE_ORDER_PARTIAL_STATUS;
        $originalPOId = $POCampaign->OriginalPOId;
        $originalCampaignId = $POCampaign->OriginalCampaignId;
        $locationCampaignId = $POCampaign->LocCampaignId;
        return $this->poCampaignRepository->getPOCampaignParameterArray(
            $originalPOId,
            $originalCampaignId,
            $POId,
            $campaignId,
            $creativeCampaignId,
            $locationCampaignId,
            $POStatus
        );
    }

    /**
    * To Get Campaign Object
    * @param INT $campaignId
    * @return Object Campaign
    */
    public function getCampaign($campaignId)
    {
        return Campaign::where('CampaignId', $campaignId)->get();
    }
}
