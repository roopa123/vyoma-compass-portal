<?php

namespace App\Repositories;


class MPEG extends \FFMpeg\Format\Video\DefaultVideo
{
    public function __construct($audioCodec = 'libmp3lame', $videoCodec = 'mpeg2video')
    {
        $this
            ->setAudioCodec($audioCodec)
            ->setVideoCodec($videoCodec);
    }

    public function supportBFrames()
    {
        return false;
    }

    public function getAvailableAudioCodecs()
    {
        return array('libmp3lame');
    }

    public function getAvailableVideoCodecs()
    {
        return array('mpeg2video');
    }
}

?>