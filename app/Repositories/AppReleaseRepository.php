<?php
namespace App\Repositories;

use App\Models\AppRelease;
use DB;

class AppReleaseRepository
{
    public function getAppReleaseDetails($appVersion)
    {
        return AppRelease::where('VersionNo', $appVersion)->first();
    }

    //Dingo api functions
    public function validVersion($appVersion)
    {
        if (AppRelease::where('VersionNo', $appVersion)->exists()) {
            return true;
        }
        return false;
    }
}
