<?php
namespace App\Repositories;

use App\Models\Branch;
use App\Models\Region;
use App\Models\Station;
use App\Models\State;
use App\Models\User;
use App\Models\Bay;
use DB;
use Carbon\Carbon;

class StationRepository extends BaseRepository
{
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
            'StationName' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/|unique:Station',
            'BranchName' => 'required',
            'RegionName' => 'required',
            'StateName' => 'required'
        ];

    // Update validation rule
    public function editValidationRules($station)
    {
        $rules = array('StationName' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/|unique:Station,StationName,'.$station['StationId'].',StationId');
        return $rules;

    }

    /* Get all the state details */
    public function statelist()
    {
        return State::lists('StateName', 'StateId');
    }

    /* Get all the station details */
    public function stationlist()
    {
        return Station::where('Status', 'A')
                ->orderBy('StationName', 'ASC')
                ->lists('StationName', 'StationId');
    }

    public function stationNameWithBay($bayId)
    {
        return Bay::select('Station.StationName', 'Station.StationId')
                        ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                        ->where('Bay.BayId', $bayId)->first();
    }

    /*
    * Insert row details to DB table
    */
    public function insertData($inputData)
    {
        $station_name = $inputData['StationName'];
        $region_name = $inputData['RegionName'];
        $branch_name = $inputData['BranchName'];
        $state_name = $inputData['StateName'];
        $dataToInsert = array(
            'StationName' => $station_name,
            'RegionId' => $region_name,
            'BranchId' => $branch_name,
            'StateId' => $state_name,
            'Status' => $inputData['Station_Status']
        );
        $inserted = DB::table('Station')->insertGetId($dataToInsert);
        return $inserted;
    }

    /*
    * Get region name based on id
    */
    public function getRegionName($id)
    {
        return Region::select('RegionName')->where('RegionId', $id)->get();
    }

    /*
    * Get state name based on id
    */
    public function getStateName($id)
    {
        return State::select('StateName')->where('StateId', $id)->get();
    }

    /*
    * Get Station details based on id
    */
    public function getStationDetails($id)
    {
        return Station::where('StationId', $id)->get();
    }

    /*
    * Update Station details based on id
    */
    public function updateStationData($id)
    {
        DB::table('Station')->where('StationId', $id)
            ->update(array('StationName' => \Input::get('StationName'),
                            'RegionId' => \Input::get('RegionName'),
                            'BranchId' => \Input::get('BranchName'),
                            'StateId' => \Input::get('StateName'),
                            'Status' => \Input::get('Station_Status')));
    }

    /*
    * Delete Station details based on id
    */
    /*public function deleteStationrow($stationid)
    {
        $result = DB::table('Station')
            ->join('Bay', 'Station.StationId', '=', 'Bay.StationId')
            ->join('Host', 'Bay.BayId', '=', 'Host.BayId')
            ->where('Station.StationId', $stationid)
            ->update(array( 'Station.status' => 'D',
                            'Bay.status' => 'D',  'Host.status' => 'D'));
        if($result){
                return true;
        }
        else {
            $result = DB::table('Station')
            ->where('Station.StationId', $stationid)
            ->update(array('Station.status' => 'D'));
            if($result){
                return true;
            }

        }
    }*/

    public function findRequestType($type, $Id)
    {
        if ($type == 'branch' && $Id) {
            return Branch::where('BranchId', '=', $Id)->first();
        } else if ($type == 'region' && $Id) {
            return Region::where('RegionId', '=', $Id)->first();
        } else if ($type == 'regionHome' && $Id) {
            return Region::where('RegionId', '=', $Id)->first();
        }
        return null;
    }

    public function autocompleteSearch($searchterm)
    {
        $term = $searchterm;
        $colValue = \Request::segment(3);
        $results = array();
        $queries = DB::table('Station')
            ->join('Region', 'Station.RegionId', '=', 'Region.RegionId')
            ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
            ->join('State', 'Station.StateId', '=', 'State.StateId');

        switch ($colValue) {
            case 'BranchName':
                $queries->where('Branch.BranchName', 'LIKE', $term.'%');
                break;
            case 'StationName':
                $queries->where('StationName', 'LIKE', $term.'%');
                break;
            case 'RegionName':
                $queries->where('Region.RegionName', 'LIKE', $term.'%');
                break;
            case 'StateName':
                $queries->where('State.StateName', 'LIKE', $term.'%');
                break;
        }
            $queries->groupBy(\Request::segment(3));
            $res = $queries->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();
        foreach ($res as $query) {
            if (\Request::segment(3) == 'StationName') {
                $results[] = [ 'id' => $query->StationId, 'value' => $query->StationName];
            } else if (\Request::segment(3) == 'BranchName') {
                $results[] = [ 'id' => $query->StationId, 'value' => $query->BranchName];
            } else if (\Request::segment(3) == 'RegionName') {
                $results[] = [ 'id' => $query->StationId, 'value' => $query->RegionName];
            } else if (\Request::segment(3) == 'StateName') {
                $results[] = [ 'id' => $query->StationId, 'value' => $query->StateName];
            }
        }
        return \Response::json($results);
    }
    /*
     * Api Call to get Station Details
     */
    public function getStateDetails()
    {
        return State::where('Status', self::STATUS_ACTIVE)->orderBy('StateName', 'asc')->get();
    }
    
     /*
   * Api Call to get Station By Region Details
   */
    public function getStationByRegionDetails($regionId)
    {
        $stationDetails = DB::table('Station')->leftjoin(
            'Region',
            'Station.RegionId',
            '=',
            'Region.RegionId'
        )
        ->whereIn('Station.RegionId', explode(',', $regionId))
        ->where('Station.Status', self::STATUS_ACTIVE)->orderBy('RegionName', 'asc')->get();
        return $stationDetails;
    }
    /*
   * Api Call to get Station By State Details
   */
    public function getStationByStateDetails($stateId)
    {
        $stationDetails = DB::table('Station')->leftjoin(
            'State',
            'Station.StateId',
            '=',
            'State.StateId'
        )
        ->whereIn('Station.StateId', explode(',', $stateId))
        ->where('Station.Status', self::STATUS_ACTIVE)
        ->orderBy('StateName', 'asc')->get();
        return $stationDetails;
    }

    public function getStationsDetails()
    {
        return Station::where('Status', self::STATUS_ACTIVE)->orderBy('StationName', 'asc')->get();
    }
}
