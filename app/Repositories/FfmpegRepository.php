<?php
namespace App\Repositories;

use App\Repositories\Converter;
use FFMpeg\Media\Video\VideoFilters;


class FfmpegRepository extends BaseRepository implements Converter
{

   /**
     * convert
     * @param FileObject $creativeFile
     * @return Creative
    */
  public function convert($creativeId, $fileName, $converationType)
	{
        $path = \Config::get('ffmpeg.upload_user_path');

        $ffmpeg = \FFMpeg\FFMpeg::create([
            'ffmpeg.binaries'  => exec('which ffmpeg'),
            'ffprobe.binaries' => exec('which ffprobe'),
            'timeout' => 3000
        ]);

        $video = $ffmpeg->open($path . '/' . $fileName);

        $nameWithoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);

        $storeVMCVideoPath = \Config::get('ffmpeg.upload_path').$nameWithoutExt.'.'.$converationType[0];
        $storeDDisVideoPath = \Config::get('ffmpeg.upload_path').$nameWithoutExt.'.'.$converationType[1];
        //file name with mp4 and mp2 extensions
        $vmcFileName = $nameWithoutExt.'.'.$converationType[0];
        $ddisFileName = $nameWithoutExt.'.'.$converationType[1];

        // .mp4 coversion through php-ffmpeg
        $video->filters()
        ->framerate(new \FFMpeg\Coordinate\FrameRate('25'), 60)
        ->resize(new \FFMpeg\Coordinate\Dimension(1024, 960))
        ->synchronize();
        $video->save(new \App\Repositories\MPEG4(), $storeVMCVideoPath);

        // .mpg coversion through ffmpeg command
        $fullPath = $path . '/' . $fileName;
        // MPEG convertion
        $video = $this->getMpgConversionFile($fullPath,$storeDDisVideoPath);

        //file size for mp4 and mp2 files
        $vmcfilesize = filesize($storeVMCVideoPath);
        $vmcBytesToMb = $this->formatBytes($vmcfilesize);

        $ddisfilesize = filesize($storeDDisVideoPath);
        $ddisBytesToMb = $this->formatBytes($ddisfilesize);

       //file duration for mp4 and mp2 files
        $vmcduration = $this->getFileDuration($storeVMCVideoPath);
        $ddisduration = $this->getFileDuration($storeDDisVideoPath);

        //Json data for file metadata
        $videoDetails['VMC'] = array('url' => $storeVMCVideoPath, 'VMCFileName' => $vmcFileName, 'size' => $vmcBytesToMb, 'duration' => $vmcduration, 'extension' => $converationType[0]);
        $videoDetails['DDIS'] = array('url' => $storeDDisVideoPath, 'DDiSFileName' => $ddisFileName, 'size' => $ddisBytesToMb, 'duration' => $ddisduration, 'extension' => $converationType[1]);

        return $videoDetails;
	}

  

  /**
  * Converting bytes to mb
  */
	public function formatBytes($bytes, $decimals = 0)
  {
      return round(($bytes / 1024), $decimals).' MiB';
	}

  /**
  * Checking the failure conditions
  */
  public function failures()
  {
      return false;
  }

  /**
  * Remove the file extension from user input file
  */
  public function getFileNameWithOutExt($filename)
  {
      return preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
  }

  /*
  * Covert other format video files into .mpg
  */
  public function getMpgConversionFile($filePath, $storePath)
  {
    return exec('ffmpeg -i '.$filePath.'   -r 25  -c:v mpeg2video -qscale:v 2 -c:a mp2  -f vob -vf scale=1024x896 '.$storePath);

  }

  /**
  * Get file durations
  */
  public function getFileDuration($path, $fileName)
  {
    $tmpName = str_replace('&', ' ', $fileName);   
    $file = $path.$tmpName;
      $time =  exec("ffmpeg -i $file 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//");
      
      $duration = explode(":",$time);
      $duration_in_seconds = $duration[0]*3600 + $duration[1]*60+ round($duration[2]);

      return $duration_in_seconds;
  }

  /**
  * Delete the videos if already exists
  */
	/*public function deleteExistingVideo($file)
	{

		$path = \Config::get('ffmpeg.upload_path').$file;
		$res = \File::delete($path);

		return ($res)? true: false;
	}*/

  public function deleteExistingVideo($file)

  {
    $path = \Config::get('ffmpeg.upload_user_path').$file;
    $res = \File::delete($path);

    if($res === true) {
      $path = \Config::get('ffmpeg.upload_path').$file;
      $result = \File::delete($path);

      return ($result)? true: false;
    }
  }

}

