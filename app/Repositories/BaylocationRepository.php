<?php
namespace App\Repositories;

use App\Models\Baylocation;
use App\Models\User;
use DB;

class BaylocationRepository
{
    /**
     * Validation for assigning people to project and manager
     */
    public $validationRules = [
            'BayLocationCode' => 'required|unique:BayLocation|regex:/(^[A-Z]+$)+/',
            'BayLocationDesc' => 'required|unique:BayLocation|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/',
        ];

    // Update validation rule
    public function editValidationRules($bayLocation)
    {
            $rules = array('BayLocationDesc' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/|unique:BayLocation,BayLocationDesc,'.$bayLocation['BayLocationCode'].',BayLocationCode');
            return $rules;
    }

    /*
    * Insert data to DB table
    */
    public function insertData($inputData)
    {
        $baylocation_code = $inputData['BayLocationCode'];
        $baylocation_desc = $inputData['BayLocationDesc'];

        $dataToInsert = array(
            'BayLocationCode' => $baylocation_code,
            'BayLocationDesc' => $baylocation_desc,
            'Status' => $inputData['Status']
        );
        try {
            $insertId = DB::table('BayLocation')->insertGetId($dataToInsert);
        } catch (QueryException $e) {
            return json_encode(array('success' => false, 'message' => 'Something went wrong, please try again later.'));
        }
        return $insertId;
    }

    /*
    * Fetching Bay Location List
    */
    public function baylocationlist()
    {
        $list = Baylocation::where('Status', 'A')->lists('BayLocationDesc', 'BayLocationCode');
        return $list;
    }

    /*
    * Fetching Bay Location details with the id
    */
    public function getBayLocationDetails($code)
    {
        return Baylocation::where('BayLocationCode', $code)->get();
    }

    /*
    * Update Bay Location details with the id
    */
    public function updateBayLocation($code)
    {
        DB::table('BayLocation')->where('BayLocationCode', $code)
            ->update(array('BayLocationDesc' => \Input::get('BayLocationDesc'),
                            'Status' => \Input::get('Status')));
    }


    /*
    * Autocomplete search for bay location
    */
    public function autocompleteSearch($searchterm)
    {
        $term = $searchterm;
        $colval = \Request::segment(3);
        $results = array();
        $queries = DB::table('BayLocation');
        switch ($colval) {
            case 'BayLocationCode':
                $queries->where('BayLocationCode', 'LIKE', $term.'%');
                break;
            case 'BayLocationDesc':
                $queries->where('BayLocationDesc', 'LIKE', $term.'%');
                break;
        }
        $queries->groupBy(\Request::segment(3));
        $res = $queries->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();
        foreach ($res as $query) {
            if (\Request::segment(3) == 'BayLocationCode') {
                $results[] = ['id' => $query->BayLocationCode, 'value' => $query->BayLocationCode];
            } else if (\Request::segment(3) == 'BayLocationDesc') {
                $results[] = [ 'id' => $query->BayLocationCode, 'value' => $query->BayLocationDesc];
            }
        }
        return \Response::json($results);
    }

    /*
    * Delete Bay Location details with the id
    */
    /*public function deleteBayLocation($code)
    {
        DB::table('BayLocation')->where('BayLocationCode', $code)
            ->update(array('status' => 'D'));
    }*/
}
