<?php
namespace App\Repositories;

use App\Models\Role;
use App\Models\RolesPermission;
use App\Models\Permission;
use App\Models\User;
use App\Models\UserRole;
use DB;
use Carbon\Carbon;

use App\Repositories\RolePermissionRepository;

class UserRepository
{

    /**
     * Instances of Role Repository
     */
    protected $roleRepo;

    /**
     * Instances of User Role Repository
     */
    protected $userRoleRepo;

    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
        'name' => 'required',
        'email' => 'required|email|unique:Users',
        'password' => 'required|alpha_num|confirmed:password_confirmation',
        'role' => 'required|array|min:1'
    ];

    // Update validation rule
    public function editValidationRules($id)
    {
        $rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:Users,email,'.$id.',id',
            'password' => 'alpha_num|confirmed:password_confirmation',
            'role' => 'required|array|min:1'
        );
        return $rules;
    }

    /**
    * Access all methods and objects in Repository
    */
    public function __construct( RoleRepository $roleRepo, UserRoleRepository $userRoleRepo)
    {
        $this->roleRepo = $roleRepo;
        $this->userRoleRepo = $userRoleRepo;

    }

    public function find($userId)
    {
        return User::find($userId);
    }

    /**
     *  Save User
     * @param Array $userData
     * @return UserModel
     */
    public function saveUser($userData)
    {
        $user = new User();
        $user->name = $userData['name'];
        $user->email = $userData['email'];

        if (!empty($userData['password'])) {
            if ($userData['password'] === $userData['password_confirmation']) {
                $encrypt = (\Config::get('auth.encrypt_type'));
                $user->password = $encrypt($userData['password']);
            }
        }
        // User role save
        if ($user->save()) {
             // Updated all the selected roles
            foreach ($userData['role'] as $roleId) {
                $this->userRoleRepo->saveUserRole($user->id, $roleId);
            }
            //$this->userRoleRepo->saveUserRole($user->id, $userData['role']);
            return $user;
        }
    }

    /**
     *  Update User
     * @param Integer $userId
     * @param Array $userData
     * @return UserModel
     */
    public function updateUser($userId, $userData)
    {
        // Fetch User
        $user = User::find($userId);
        $user->name = $userData['name'];
        $user->email = $userData['email'];

        if (!empty($userData['password'])) {
            if ($userData['password'] === $userData['password_confirmation']) {
                $encrypt = (\Config::get('auth.encrypt_type'));
                $user->password = $encrypt($userData['password']);
            }
        }
        if ($user->save()) {
            // Remove exsisting User rolemapping
            $this->userRoleRepo->deleteUserRoleByUserId($user->id);
            // Updated all the selected roles
            foreach ($userData['role'] as $roleId) {
                $this->userRoleRepo->saveUserRole($user->id, $roleId);
            }
           // Update User Role
            return $user;
        }
    }

    /**
     *  Delete User
     * @param Integer $userId
     * @return Boolean
     */
    public function deleteUser($userId)
    {
        // delete all permissions belong with roles

        $res = User::find($userId)->delete();
        if($res) {
            return true;
        }
    }

    /*
    * Getting all user lists
    */
    public function all()
    {
        return User::all();

    }

    /**
    *   Fetch user details with the associated roles
    */
    public function userRoles($isRolesArray = false)
    {
        $roles = User::with('roles')->get();

        return (!$isRolesArray)?$roles:$roles->toArray();
    }

    /*
    * Getting user lists
    */
    public function UserList()
    {
        return  User::lists('email', 'id');

    }

    /*
    * Autocomplete search for user
    */
    public function autocompleteSearch($searchStr)
    {
        $results = array();
        $queries = DB::table('Users')
            ->where('email', 'LIKE', '%'.$searchStr.'%')
            ->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();
        foreach ($queries as $query) {
            if(\Request::segment(3) == 'email') {
                $results[] = [ 'id' => $query->id, 'value' => $query->email];
            }
        }
        return \Response::json($results);
    }
    /**
     * To Get Users based on role
     * @param Integer $roleId
     * @return Object Users
     */
    public function getUsersByRole($roleId)
    {
            $users = User::whereHas('Roles', function($q) use($roleId){
                    $q->where('Roles.RoleId',$roleId);
                })->whereNull('deleted_at')
                  ->orderby('name', 'ASC')->get();
            return $users;

    }

    /**
     * To Get Users based on role
     * @param Integer $roleId
     * @param $isArray Optional for return Type
     * @return Object/Array Users
     */
    public function fetchUserNameandUserIDByRole($roleId, $isArray = false)
    {
            $users = User::whereHas('Roles', function($q) use($roleId) {
                    $q->where('Roles.RoleId',$roleId);
                })->select('id','name')
                ->whereNull('deleted_at')->select('id','name')
                ->orderby('name', 'ASC')
                ->get();
            // check for return type
            if ($isArray) {
                return $this->convertIntoArray($users);
            }
            return $users;
    }

    /**
     * To Get Users based on role
     * @param Object $Users
     * @return Array Users
     */
    private function convertIntoArray($usersArray)
    {
        $users[NULL] = 'Select';
        if(is_object($usersArray)) {
            foreach($usersArray as $user) {
                $users[$user->id] = $user->name;
            }
            return $users;
        }
        return $users;
    }

    public function getCurrentUserLoginId()
    {
        return \Auth::user()->id;
    }
}
