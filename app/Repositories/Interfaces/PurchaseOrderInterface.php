<?php

namespace App\Repositories\Interfaces;

interface PurchaseOrderInterface
{
    public function createPo();
}
