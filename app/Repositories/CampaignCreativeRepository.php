<?php
namespace App\Repositories;

use App\Models\CampaignCreative;
use App\Models\PurchaseOrderCampaign;
use DB;
use App\Repositories\UserRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\PoCampaignRepository;
use App\Repositories\CampaignLocationRepository;
use App\Services\Constants\AppConstants;

class CampaignCreativeRepository extends BaseRepository
{
    private $campaignCreative;

    private $userRepository;

    private $campaignRepository;

    private $poCampaignRepository;

    private $campaignLocationRepository;

    public function __construct(
        CampaignCreative $campaignCreative,
        UserRepository $userRepository,
        CampaignRepository $campaignRepository,
        PoCampaignRepository $poCampaignRepository,
        CampaignLocationRepository $campaignLocationRepository
    ) {
        $this->campaignCreative = $campaignCreative;
        $this->userRepository = $userRepository;
        $this->campaignRepository = $campaignRepository;
        $this->poCampaignRepository = $poCampaignRepository;
        $this->campaignLocationRepository = $campaignLocationRepository;
    }
 

    /**
    * Create CampaignCreative
    * @param Array $inputData
    * @param INT $campaignId
    * @return BOOL
    */
    public function createCampaignCreative($inputData, $campaignId)
    {
        $creativeInputs = $inputData['creativeValues'];
        if ($this->checkCreativeIsNotEmpty($creativeInputs)) {
        //For creation of more than one creatives for the campaign
            if (strpos($creativeInputs, ',') !== false) {
                $creativeArray = explode(',', $creativeInputs);
                foreach ($creativeArray as $key => $creativeId) {
                    $creativeCampaignId = $this->createCreative($creativeId, $campaignId);
                }
                return true;
            }
        // For creation of single Creative
            $creativeCampaignId = $this->createCreative($creativeInputs, $campaignId);
            return true;
        }
        return false;
    }

    /**
    * To Check Creative is not empty
    * @param String $creativeInputs
    * @return Boolean
    */
    public function checkCreativeIsNotEmpty($creativeInputs)
    {
        if (trim($creativeInputs) != "") {
            return true;
        }
        return false;
    }

    /**
    * Create Creative
    * @param INT $creativeId
    * @param INT $campaignId
    * @return Object $campaignCreativeObj
    */
    public function createCreative($creativeId, $campaignId)
    {
        $campaignCreativeObj = $this->campaignCreative->newInstance();
        $campaignCreativeObj->CampaignId = $campaignId;
        $campaignCreativeObj->CreativeId = $creativeId;
        $campaignCreativeObj->Status = self::PURCHASE_ORDER_ACTIVE;
        $campaignCreativeObj->UserId = $this->userRepository->getCurrentUserLoginId();
        $campaignCreativeObj->save();
        return $campaignCreativeObj;
    }
    
    /**
    * Check whether Campagin Form Data Changed
    * @param Array $inputData
    * @return INT CREATIVE_FORM_CHANGED:3,NO_CHANGE:1
    */
    public function changesInCreative($inputData)
    {
        $oldCreativeArray = explode(",", $inputData['oldCreatives']);
        $newCreativeArray = explode(",", $inputData['creativeValues']);
        if (!array_diff($oldCreativeArray, $newCreativeArray) && !array_diff(
            $newCreativeArray,
            $oldCreativeArray
        )) {
            return AppConstants::NO_CHANGE;
        }
        return AppConstants::CREATIVE_FORM_CHANGED;
    }
    
    /**
    * Update Creative Status
    * @param INT $creativeCampaignId
    * @param CHAR $status
    * @return INT Affected Rows
    */
    public function updateStatus($creativeCampaignId, $status)
    {
        return CampaignCreative::where('CampaignId', $creativeCampaignId)
                                 ->update(['Status' => $status]);
    }

    /**
    * Create Campaign And Creative on creative Form Change
    * @param Array $inputData
    * @param CHAR $POStatus
    * @return Array $campaignId, $creativeCampaignId
    */
    public function createCampaignAndCreative($inputData, $POStatus)
    {
        $creativeCampaignId = null;
        $campaignObj = $this->campaignRepository->createCampaign($inputData, $POStatus);
        $campaignId = $campaignObj->CampaignId;
        $creativeBoolean = $this->createCampaignCreative($inputData, $campaignId);
        if ($creativeBoolean) {
            $creativeCampaignId = $campaignId;
        }
        return array($campaignId, $creativeCampaignId);
    }

    /**
    * To get Creatives
    * @param Int $creativeCampaignId
    * @return String $creatives
    */
    public function getCreatives($creativeCampaignId)
    {
        $creatives = "";
        if (!is_null($creativeCampaignId)) {
            $creatives = DB::select(DB::raw("select GROUP_CONCAT(DISTINCT CreativeId SEPARATOR ',') as creativesId from CampaignCreative where CampaignId = ". $creativeCampaignId." and (Status = '".self::PURCHASE_ORDER_PARTIAL_STATUS."' or Status = '".self::PURCHASE_ORDER_ACTIVE."')"));
            return $creatives[0]->creativesId;
        }
        return $creatives;
    }
}
