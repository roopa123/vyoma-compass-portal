<?php

namespace App\Repositories;


class MPEG4 extends \FFMpeg\Format\Video\DefaultVideo
{
    public function __construct($audioCodec = 'libfdk_aac', $videoCodec = 'mpeg4')
    {
        $this
            ->setAudioCodec($audioCodec)
            ->setVideoCodec($videoCodec);
    }

    public function supportBFrames()
    {
        return false;
    }

    public function getAvailableAudioCodecs()
    {
        return array('libfdk_aac');
    }

    public function getAvailableVideoCodecs()
    {
        return array('mpeg4');
    }
}

?>