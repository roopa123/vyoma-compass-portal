<?php
namespace App\Repositories;

use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderCampaign;
use App\Models\Campaign;
use App\Models\CampaignCreative;
use App\Models\CampaignLocation;
use DB;
use App\Repositories\PoCampaignRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\CampaignCreativeRepository;
use App\Repositories\CampaignLocationRepository;
use App\Repositories\UserRepository;
use App\Services\Constants\AppConstants;
use Illuminate\Database\Eloquent\Collection;

class PurchaseOrderRepository extends BaseRepository
{
    private $purchaseOrder;

    private $poCampaignRepository;

    private $campaignRepository;

    private $userRepository;

    private $campaignCreativeRepository;

    private $campaignLocationRepository;

    /*Validation Rules */
    public $poValidationRules = [
            'salesrep' => 'required',
            'poDate'=>'required|date|date_format:Y-m-d',
            'startDate'=>'required|date|date_format:Y-m-d|po_date:poDate',
            'endDate'=>'required|date|date_format:Y-m-d|po_date:poDate|po_start_date:startDate',
            'advertiser'=>'required',
            'branch'=>'required'];

    public $campaignValidationRules = [
            'startDate'=>'required',
            'endDate'=>'required',
            'campaignName'=>'required',
            'campaignStartDate'=>'required|date|date_format:Y-m-d|start_date_lesser_equal:startDate',
            'campaignEndDate'=>'required|date|date_format:Y-m-d|end_date_lesser_equal:endDate|end_date:campaignStartDate',
            'spotsPerDay'=>'required|numeric',];

    public $creativeValidationRules = ['creativeValues' => 'required'];

    
    public $locationValidationRules = ['location' => 'required'];

    /*Validation Messages */
    public $poValidationMessages = ['salesrep.required' => 'PO Sales Representative Required',
                                    'advertiser.required'=>'PO Advertiser  is Required',
                                    'branch.required'=>'PO Branch Required',];

    public $campaignMessages = ['campaignName.required'=>'Campaign Campaign Name Required',
                                'spotsPerDay.required'=>'Campaign Spots Per Day Required',];

    public $creativeMessages = ['creativeValues.required'=>'Creative Creative Required'];

    public $locationMessages = ['location.required'=>'Location Location Required'];

    public function __construct(
        PurchaseOrder $purchaseOrder,
        PoCampaignRepository $poCampaignRepository = null,
        CampaignRepository $campaignRepository = null,
        UserRepository $userRepository = null,
        CampaignCreativeRepository $campaignCreativeRepository = null,
        CampaignLocationRepository $campaignLocationRepository = null
    ) {
        $this->purchaseOrder = $purchaseOrder;
        $this->poCampaignRepository = $poCampaignRepository;
        $this->campaignRepository = $campaignRepository;
        $this->userRepository = $userRepository;
        $this->campaignCreativeRepository = $campaignCreativeRepository;
        $this->campaignLocationRepository = $campaignLocationRepository;
        $this->getGridData();
    }

    /**
    * Get Campaign Validation Rules while editing Campaign
    * @param Array $inputData
    * @return Array $rules
    */
    public function editCampaignValidationRules($inputData)
    {
        $rules = [
            'campaignName'=>'required|edit_campaign_name:campaignName,'.$inputData['originalPOId'].
                            ','.$inputData['campaignId'],
            'startDate'=>'required',
            'endDate'=>'required',
            'campaignStartDate'=>'required|date|date_format:Y-m-d|start_date_lesser_equal:startDate',
            'campaignEndDate'=>'required|date|date_format:Y-m-d|end_date_lesser_equal:endDate|end_date:campaignStartDate',
            'spotsPerDay'=>'required|numeric',
                ];
        return $rules;
    }

    /**
    * Get Campaign Validation Rules while Addign Campaign
    * @param Array $inputData
    * @return Array $rules
    */
    public function addCampaignValidationRules($inputData)
    {
        $rules = [
            'campaignName'=>'required|add_campaign_name:campaignName,'.$inputData['originalPOId'],
            'startDate'=>'required',
            'endDate'=>'required',
            'campaignStartDate'=>'required|date|date_format:Y-m-d|start_date_lesser_equal:startDate',
            'campaignEndDate'=>'required|date|date_format:Y-m-d|end_date_lesser_equal:endDate|end_date:campaignStartDate',
            'spotsPerDay'=>'required|numeric',
                ];
        return $rules;
    }

    /**
    * Validating PO Forms
    * @param Array $inputData
    * @return object $validator
    */
    public function validateForms($inputData)
    {
        switch ($inputData['stepsComplete']) {
            case AppConstants::PO_PARTIAL:
                $validator = $this->validatePoForm($inputData);
                if ($validator->fails()) {
                    return $validator;
                }
                return $this->validateCampaignForm($inputData);
                        break;
            case AppConstants::PO_ACTIVE_COMPLETED:
                $validator = $this->validatePoForm($inputData);
                if ($validator->fails()) {
                    return $validator;
                }
                $validator = $this->validateCampaignForm($inputData);
                if ($validator->fails()) {
                    return $validator;
                }
                $validator = \Validator::make(
                    $inputData,
                    $this->creativeValidationRules,
                    $this->creativeMessages
                );
                if ($validator->fails()) {
                    return $validator;
                }
                $validator = \Validator::make(
                    $inputData,
                    $this->locationValidationRules,
                    $this->locationMessages
                );
                return $validator;
                break;
            default:
        }
    }

    /**
    * Validating PO Form
    * @param Array $inputData
    * @return object $validator
    */
    public function validatePoForm($inputData)
    {
        $validator = \Validator::make(
            $inputData,
            $this->poValidationRules,
            $this->poValidationMessages
        );
        return $validator;
    }

    /**
    * Validating Campaign Form
    * @param Array $inputData
    * @return object $validator
    */
    public function validateCampaignForm($inputData)
    {
        if ($inputData['mode'] == AppConstants::ADD_NEW_PO_VALIDATION) {
            $validator = \Validator::make(
                $inputData,
                $this->campaignValidationRules,
                $this->campaignMessages
            );
        } elseif ($inputData['mode'] == AppConstants::EDIT_CAMPAIGN_VALIDATION) {
            $validator = \Validator::make(
                $inputData,
                $this->editCampaignValidationRules($inputData),
                $this->campaignMessages
            );
        } elseif ($inputData['mode'] == AppConstants::ADD_CAMPAIGN_VALIDATION) {
            $validator = \Validator::make(
                $inputData,
                $this->addCampaignValidationRules($inputData),
                $this->campaignMessages
            );
        }
        return $validator;
    }

    /**
    * Validating Creative Form
    * @param Array $inputData
    * @return object $validator
    */
    public function validateCreativeForm($inputData)
    {
        $validator = \Validator::make(
            $inputData,
            $this->creativeValidationRules,
            $this->creativeMessages
        );
        return $validator;
    }

    public function getGridData()
    {
        $this->Database = DB::select(DB::raw("select * from (select `POCampaign`.`OriginalPOId` As PurchaseOrder,`POCampaign`.`Status`, salesrep.name as SaleRapeName , Branch.BranchName, Advertiser.AdvertiserName,
            DATE_FORMAT(`PO`.`PODate`,'%d-%m-%Y') as `PODate`,
            DATE_FORMAT(`PO`.`StartDate`,'%d-%m-%Y') as `StartDate`,
            DATE_FORMAT(`PO`.`EndDate`,'%d-%m-%Y') as `EndDate`,
            Advertiser.Sector, Industry.IndustryName
          from `POCampaign` 
            left join `PO` on `PO`.`POId` = `POCampaign`.`POId` 
            left join `Users` as `salesrep` on `salesrep`.`id` = `SalesRepId` 
            left join `Advertiser` on `Advertiser`.`AdvertiserId` = `PO`.`AdvertiserId` 
            left join `Branch` on `Branch`.`BranchId` = `PO`.`SalesBranchId` 
            left join `Industry` on `Industry`.`IndustryId` = `Advertiser`.`IndustryId`
            left join (select a.POId from (select distinct(POId) from POCampaign  where POCampaign.Status NOT IN ('H', 'D')) as a group by a.POId) as b on b.POId=POCampaign.POId where POCampaign.Status NOT IN  ('H', 'D') group by POCampaign.OriginalPOId,POCampaign.Status asc) as c group by c.PurchaseOrder order by c.PurchaseOrder desc"));
       
        return $this->Database;
    }

    /* Set the Flas Message*/
    public function flashMessage($message)
    {
        \Session::flash('success', $message);
    }

    /**
    * Create PurchaseOrder
    * @param Array $inputData
    * @return object $POCampaignObject
    */
    public function createPurchaseOrder($inputData)
    {
        /*Setting the PurchaseOrder Status*/
        $POStatus = $this->getPOStatus($inputData['stepsComplete']);
        $creativeCampaignId = null;
        $locationCampaignId = null;
        /*Creation of PO*/
        $purchaseOrderObj = $this->createPO($inputData);
        $POId = $purchaseOrderObj->POId;
        $originalPOId = $purchaseOrderObj->POId;
        /*Creation of Campaign*/
        $campaignObj = $this->campaignRepository->createCampaign($inputData, $POStatus);
        $campaignId = $campaignObj->CampaignId;
        $originalCampaignId = $campaignObj->CampaignId;
        /*Creation of Creative*/
        $creativeBoolean =$this->campaignCreativeRepository->createCampaignCreative(
            $inputData,
            $campaignId
        );
        if ($creativeBoolean) {
            $creativeCampaignId = $campaignId;
        }
        /*Creation of Location */
        $locationBoolean = $this->campaignLocationRepository->createCampaignLocation(
            $inputData,
            $campaignId
        );
        if ($locationBoolean) {
            $locationCampaignId = $campaignId;
        }
        /*Creation of POCampaign*/
        $POCampaignParameterArray = $this->poCampaignRepository->getPOCampaignParameterArray(
            $originalPOId,
            $originalCampaignId,
            $POId,
            $campaignId,
            $creativeCampaignId,
            $locationCampaignId,
            $POStatus
        );
        return $this->poCampaignRepository->createPOCampaign($POCampaignParameterArray);
    }
    
    /**
    * To Get PO Status [Active/Partial]
    * @param INT $status
    * @return CHAR Either 'A' Or 'P'
    */
    public function getPOStatus($status)
    {
        if ($status == AppConstants::PO_ACTIVE_COMPLETED) {
            return self::PURCHASE_ORDER_ACTIVE;
        }
        return self::PURCHASE_ORDER_PARTIAL_STATUS;
    }

    /**
    * Create PO
    * @param Array $inputData
    * @return object $this->purchaseOrder
    */
    public function createPO($inputData)
    {
        $this->purchaseOrder->ROReferenceNo = $inputData['roReference'];
        $this->purchaseOrder->SalesRepId = $inputData['salesrep'];
        $this->purchaseOrder->PODate = $inputData['poDate'];
        $this->purchaseOrder->StartDate = $inputData['startDate'];
        $this->purchaseOrder->EndDate = $inputData['endDate'];
        $this->purchaseOrder->AdvertiserId = $inputData['advertiser'];
        $this->purchaseOrder->SalesBranchId = $inputData['branch'];
        $this->purchaseOrder->Status = self::PURCHASE_ORDER_ACTIVE;
        $this->purchaseOrder->UserId = $this->userRepository->getCurrentUserLoginId();
        $this->purchaseOrder->save();
        return $this->purchaseOrder;
    }

    /**
    * Update PurchaseOrder
    * @param Array $inputData
    * @return object $POCampaignObject
    */
    public function updatePurchaseOrder($inputData)
    {
        $changesInEdit = false;
        /* To Set the Status based on User click save&exit / Submit button*/
        $POStatus = $this->getPOStatus($inputData['stepsComplete']);
        /* To find Changes in PO*/
        $POChange = $this->checkChangesInPO($inputData);
        /* To Find Changes in PO elements [campaign, creative, location]*/
        $changesInForm = $this->getFormChanges($inputData, $POStatus);
        /* Setting Variable If any change in PO Elements*/
        if ($POChange || $changesInForm != AppConstants::NO_PO_ELEMENT_CHANGE) {
            list($POId, $campaignId, $creativeCampaignId, $locationCampaignId, $POCampaignId,
            $originalPOId, $originalCampaignId) = $this->setVariablesForPOUpdate($inputData);
        }
        /* Do Opearation When PO Changes*/
        if ($POChange) {
            $changesInEdit = true;
            $POObj = $this->createPO($inputData);
            $POId = $POObj->POId;
            $this->createPOCampaignForNewPOId(
                $originalPOId,
                $POId,
                $campaignId,
                $inputData
            );
        }
        /*Do Operation When PO elements Change [campaign, creative, location]*/
        if ($changesInForm != AppConstants::NO_CHANGE) {
            $changesInEdit = true;
            list($campaignId, $creativeCampaignId, $locationCampaignId) = $this->updatePOElements(
                $changesInForm,
                $creativeCampaignId,
                $locationCampaignId,
                $inputData,
                $POStatus
            );
        }
        /*Create and Update POCampaign When Any of the PO Elements Changed*/
        if ($changesInEdit) {
            $POCampaignParameterArray = $this->poCampaignRepository->getPOCampaignParameterArray(
                $originalPOId,
                $originalCampaignId,
                $POId,
                $campaignId,
                $creativeCampaignId,
                $locationCampaignId,
                $POStatus
            );
            $this->flashMessage('Purchase Order Updated Successfully !');
            return $this->poCampaignRepository->createPOCampaignAndUpdateStatus(
                $POCampaignParameterArray,
                $POCampaignId
            );
        }
        return $this->poCampaignRepository->findPOCampaign($inputData['POCampaignId']);
    }

    /**
    * Update PO Elements on Update
    * @param INT $changesInForm
    * @param INT $creativeCampaignId
    * @param INT $locationCampaignId
    * @param Array $inputData
    * @param CHAR $POStatus
    * @return Array $campaignId, $creativeCampaignId, $locationCampaignId
    */
    public function updatePOElements(
        $changesInForm,
        $creativeCampaignId,
        $locationCampaignId,
        $inputData,
        $POStatus
    ) {
        switch ($changesInForm) {
            case AppConstants::CAMPAIGN_FORM_CHANGED:
                $campaignObj = $this->campaignRepository->createCampaign(
                    $inputData,
                    $POStatus
                );
                $campaignId = $campaignObj->CampaignId;
                break;
            case AppConstants::CREATIVE_FORM_CHANGED:
            case AppConstants::CAMPAIGN_CREATIVE_CHANGED:
                list($campaignId, $creativeCampaignId) = $this->createCampaignAndCreative(
                    $inputData,
                    $POStatus
                );
                break;
            case AppConstants::LOCATION_FORM_CHANGED:
            case AppConstants::CAMPAIGN_LOCATION_CHANGED:
                list($campaignId, $locationCampaignId) = $this->createCampaignAndLocation(
                    $inputData,
                    $POStatus
                );
                break;
            case AppConstants::CREATIVE_LOCATION_CHANGED:
            case AppConstants::CAMPAIGN_CREATIVE_LOCATION_CHANGED:
                list($campaignId, $creativeCampaignId, $locationCampaignId) = $this->
                createCampaignAndCreativeAndLocation(
                    $inputData,
                    $POStatus
                );
                break;
            default:
                break;
        }
        return array($campaignId, $creativeCampaignId, $locationCampaignId);
    }

    /**
    * Create Campaign And Creative on creative Form Change
    * @param Array $inputData
    * @param CHAR $POStatus
    * @return Array $campaignId, $creativeCampaignId
    */
    public function createCampaignAndCreative($inputData, $POStatus)
    {
        return $this->campaignCreativeRepository->createCampaignAndCreative(
            $inputData,
            $POStatus
        );
    }

    /**
    * Create Campaign And Location on location Form Change
    * @param Array $inputData
    * @param CHAR $POStatus
    * @return Array $campaignId, $locationCampaignId
    */
    public function createCampaignAndLocation($inputData, $POStatus)
    {
        return $this->campaignLocationRepository->createCampaignAndlocation(
            $inputData,
            $POStatus
        );
    }

    /**
    * Create Campaign And Creative And Location
    * @param Array $inputData
    * @param CHAR $POStatus
    * @return Array $campaignId, $creativeCampaignId, $locationCampaignId
    */
    public function createCampaignAndCreativeAndLocation($inputData, $POStatus)
    {
        $creativeCampaignId = null;
        $locationCampaignId = null;
        $campaignObj = $this->campaignRepository->createCampaign($inputData, $POStatus);
        $campaignId = $campaignObj->CampaignId;
        $creativeBoolean = $this->campaignCreativeRepository->createCampaignCreative(
            $inputData,
            $campaignId
        );
        $locationBoolean = $this->campaignLocationRepository->createCampaignlocation(
            $inputData,
            $campaignId
        );
        if ($creativeBoolean) {
            $creativeCampaignId = $campaignId;
        }
        if ($locationBoolean) {
            $locationCampaignId = $campaignId;
        }
        return array($campaignId, $creativeCampaignId, $locationCampaignId);
    }

    /**
    * Create POCampagin with NewPOId and Update Status
    * @param INT $originalPOId
    * @param INT POId
    * @param INT $campaignId
    * @param Array $inputData
    * @return
    */
    public function createPOCampaignForNewPOId($originalPOId, $POId, $campaignId, $inputData)
    {
        $POCampaignObj = $this->poCampaignRepository->getPOCampaignsForUpdatingPOId(
            $originalPOId,
            $POId,
            $campaignId
        );
        if (count($POCampaignObj) > AppConstants::HAS_NO_CAMPAIGN) {
            $checkAdvertiserChangeInPO = $this->checkAdvertiserChangeInPO($inputData);
            if ($checkAdvertiserChangeInPO) {
                $this->campaignRepository->createCampaignsAndPOCampaignAsCreativeNull(
                    $POCampaignObj,
                    $POId
                );
                return $POCampaignObj;
            }
            return $this->poCampaignRepository->createPOCampaignForNewPOId(
                $POCampaignObj,
                $POId
            );
        }
    }

    /**
    * To Get Form Data Changes in updating PurchaseOrder
    * @param Array $inputData
    * @param CHAR $status
    * @return INT combination of changes [campaign, creative, location]
    */
    public function getFormChanges($inputData, $POStatus)
    {
        /*To Check Any Changes Mades in Campaign*/
        $campaignChange = $this->campaignRepository->changesInCampaign($inputData, $POStatus);
        /*To Check Any Changes Made in Creative*/
        $creativeChange = $this->campaignCreativeRepository->changesInCreative($inputData);
        /*To Check Any Changes Made in Location*/
        $locationChange = $this->campaignLocationRepository->changesInLocation($inputData);
        return $campaignChange.$creativeChange.$locationChange;
    }

    /**
    * Setting Varibles for Updating PurchaseOrder
    * @param Array $inputData
    * @return Array
    */
    public function setVariablesForPOUpdate($inputData)
    {
        $POCampaignId = $inputData['POCampaignId'];
        $originalPOId = $inputData['originalPOId'];
        $originalCampaignId = $inputData['originalCampaignId'];
        $POId = $inputData['POId'];
        $campaignId = $inputData['campaignId'];
        $creativeCampaignId = null;
        $locationCampaignId = null;

        if ($this->campaignCreativeRepository->checkCreativeIsNotEmpty($inputData['creativeValues'])) {
                $creativeCampaignId = $inputData['creativeCampaignId'];
        }
        if ($this->campaignLocationRepository->checkLocationIsNotEmpty($inputData['location'])) {
            $locationCampaignId = $inputData['locationCampaignId'];
        }
        return array($POId, $campaignId, $creativeCampaignId, $locationCampaignId, $POCampaignId,
        $originalPOId, $originalCampaignId);
    }

    /**
    * To Check Change of Advertiser in PO form
    * @param Array $inputData
    * @return BOOL
    */
    public function checkAdvertiserChangeInPO($inputData)
    {
        $updatePurchaseOrder = $this->findPO($inputData['POId']);
        $updatePurchaseOrder->AdvertiserId = $inputData['advertiser'];
        if (count($updatePurchaseOrder->getDirty()) > 0) {
            return true;
        }
        return false;
    }

    /**
    * To Check Form Data Changes in PO
    * @param Array $inputData
    * @return BOOL
    */
    public function checkChangesInPO($inputData)
    {
        $updatePurchaseOrder = $this->findPO($inputData['POId']);
        $updatePurchaseOrder->ROReferenceNo = $inputData['roReference'];
        $updatePurchaseOrder->SalesRepId = $inputData['salesrep'];
        $updatePurchaseOrder->PODate = $inputData['poDate'];
        $updatePurchaseOrder->StartDate = $inputData['startDate'];
        $updatePurchaseOrder->EndDate = $inputData['endDate'];
        $updatePurchaseOrder->AdvertiserId = $inputData['advertiser'];
        $updatePurchaseOrder->SalesBranchId = $inputData['branch'];
        $updatePurchaseOrder->UserId = $this->userRepository->getCurrentUserLoginId();
        if (count($updatePurchaseOrder->getDirty()) > 0) {
            return true;
        }
        return false;
    }

    /**
    * To Create New Campaign To the Existing PO
    * @param Array $inputData
    * @return Object POCampaign
    */
    public function addCampaign($inputData)
    {
        $POId = $inputData['POId'];
        $originalPOId = $inputData['originalPOId'];
        $creativeCampaignId = null;
        $locationCampaignId = null;
        $POStatus = $this->getPOStatus($inputData['stepsComplete']);
        $POChange = $this->checkChangesInPO($inputData);
        /*Create Campaign*/
        $campaignObj = $this->campaignRepository->createCampaign($inputData, $POStatus);
        $campaignId = $campaignObj->CampaignId;
        $originalCampaignId = $campaignObj->CampaignId;
        if ($POChange) {
            /*Create PO*/
            $POObj = $this->createPO($inputData);
            $POId = $POObj->POId;
            $this->createPOCampaignForNewPOId($originalPOId, $POId, $campaignId, $inputData);
        }
        /*Creation of Creative*/
        $creativeBoolean =$this->campaignCreativeRepository->createCampaignCreative(
            $inputData,
            $campaignId
        );
        if ($creativeBoolean) {
            $creativeCampaignId = $campaignId;
        }
        /*Creation of Location */
        $locationBoolean = $this->campaignLocationRepository->createCampaignLocation(
            $inputData,
            $campaignId
        );
        if ($locationBoolean) {
            $locationCampaignId = $campaignId;
        }
        /*Creation of POCampaign*/
        $POCampaignParameterArray = $this->poCampaignRepository->getPOCampaignParameterArray(
            $originalPOId,
            $originalCampaignId,
            $POId,
            $campaignId,
            $creativeCampaignId,
            $locationCampaignId,
            $POStatus
        );
        return $this->poCampaignRepository->createPOCampaign($POCampaignParameterArray);
    }

    /**
    * Delete PO
    * @param INT $POId
    * @return INT Affected Rows
    */
    public function deletePO($POId)
    {
        return $this->updateStatus($POId, self::PURCHASE_ORDER_DELETE);
    }

    /**
    * Update PO status
    * @param INT $POId
    * @param CHAR $status
    * @return INT Affected Rows
    */
    public function updateStatus($POId, $status)
    {
        return PurchaseOrder::where('POId', $POId)->update(['Status' => $status]);
    }

    /**
    * To Find PO Object For Updating
    * @param INT $POId
    * @return Object PurchaseOrder
    */
    public function findPO($POId)
    {
        return PurchaseOrder::find($POId);
    }

    /**
    * To Get PO Object
    * @param INT $POId
    * @return Object PO
    */
    public function getPO($POId)
    {
        return PurchaseOrder::where('POId', $POId)->get();
    }
 
    /**
    * Get Purchase order histor
    * @param Int $OriginalPOId
    * @param Array $history
    * @return Array $poHistory
    */
    public function getPOHistory($originalPOId)
    {
        $poHistory = $this->poCampaignRepository->getPOCampaignHistory($originalPOId);
        return $this->fetchHistoryByPO($poHistory);
    }

    /**
    * Get Purchase order history based on the timestamp
    * @param Array $history
    * @return Array $poData
    */
    public function fetchHistoryByPO($poHistory)
    {
        if ($poHistory) {
            $poData = array();
            $poDataTemp = array();
            $poDate = array();
            // Count the Number Items
            $poHistoryCount = count($poHistory);
            for ($poDecrement = 0; $poDecrement < $poHistoryCount; $poDecrement++) {
            // Itertae History Data
                foreach ($poHistory as $poHistoryData) {
                    if ($poHistoryData->POCampaignId <= $poHistory[$poDecrement]->POCampaignId &&
                    !isset($poDataTemp[$poHistoryData->OriginalCampaignId])) {
                        $poDataTemp[$poHistoryData->OriginalCampaignId] = $poHistoryData;
                    }
                }
                if (isset($poDate[$poHistory[$poDecrement]->CreatedAt])) {
                    $poDate[$poHistory[$poDecrement]->CreatedAt] = AppConstants::DUPLICATE_DATE;
                } else {
                    $poDate[$poHistory[$poDecrement]->CreatedAt] = AppConstants::NO_DUPLICATE_DATE;
                }
                if ($poDate[$poHistory[$poDecrement]->CreatedAt] == AppConstants::NO_DUPLICATE_DATE) {
                    $poData[$poHistory[$poDecrement]->POCampaignId] = $poDataTemp;
                }
                unset($poDataTemp);
            }
            return $poData;
        }
        return [];
    }

    /**
    * Get PO Elements Details [PO, Campaign, Creatives, Location For Editing]
    * @param Object $POCampaign
    * @return Array
    */
    public function getPOElements($POCampaign)
    {
        $POId = $POCampaign[0]->POId;
        $campaignId = $POCampaign[0]->CampaignId;
        $creativeCampaignId = $POCampaign[0]->CreativeCampaignId;
        $locCampaignId = $POCampaign[0]->LocCampaignId;
        $PO = $this->getPO($POId);
        $campaign = $this->campaignRepository->getCampaign($campaignId);
        $creatives = $this->campaignCreativeRepository->getCreatives($creativeCampaignId);
        $locations = $this->campaignLocationRepository->getLocations($locCampaignId);
        return array($PO, $campaign, $creatives, $locations);
    }

    /**
    * Get PO and POCampaign
    * @param INT $POCampaignId
    * @return Array
    */
    public function getPOAndPOCampaign($POCampaignId)
    {
        return DB::table('POCampaign')
            ->leftjoin('PO', 'POCampaign.POId', '=', 'PO.POId')
            ->where('POCampaignId', '=', $POCampaignId)
            ->get();
    }

    /**
    * Delete PO Elements [Campaign, Creatives, Locations]
    * @param Object $POCampaign
    * @return Object
    */
    public function deletePOElements($POCampaign)
    {
        return $this->poCampaignRepository->deletePOCampaign($POCampaign);
    }
}
