<?php
namespace App\Repositories;

use App\Models\User;
use App\Models\Code;

use DB;


class CodeRepository extends BaseRepository
{


    /**
    * Access all methods and objects in Repository
    */
    public function __construct()
    {
    }


    public function find($codeId)
    {
        return Code::find($codeId);
    }

    /**
     * Get category list dropdown values
     * @param String $codeType
     * @return Code
     */
    public function getCategoryByCodeType($codeType)
    {
       //return ['' => 'Select'] + Code::where('Code', $codeType)->lists('Desc', 'Desc')->all();
      return Code::where('Code', $codeType)
            ->OrderBy('Desc')
            ->lists('Desc', 'Desc')->all();

    }

    /**
     * Category lists
     * @param String $desc
     * @return Code
     */
    public function getCodeCategoryList($desc)
    {
       return Code::where('Desc',$desc)->lists('Desc', 'Desc');
    }

    /**
     * Getting Code lists
     * @return $codeObj
     */
    public function all()
    {
        return Code::all();
    }

    /**
    *Get PlayerWinSize values from Code 
    *@param String $codeType
    *@return Obj $codeObject
    */
    public function getValuesOfCode($codeType)
    {
        return Code::where('CodeType',$codeType)->get();
       
    }

    /**
    *Prepare array from Code Default values
    *@param Obj $codeObj
    *@return Array $codeDefaults
    */
    public function getPreparedDefaultArr($codeObj)
    {
        $codeDefaults = array();
        foreach($codeObj as $codeObjRs) {
            $codeDefaults[$codeObjRs->Desc] = $codeObjRs->Code;
        }
        return $codeDefaults;
    }

    /**
    *Prepare array from Code for DropDown values
    *@param Array $codeTypes
    *@return Array $codeDesc
    */
    public function getPreparedCodeValues($codeTypes)
    {
        $codeDesc = array();
        foreach($codeTypes as $key => $value) {
            $code = $this->getValuesOfCode($value);
            $prepareArray = array();
            foreach ($code as $res) {
                $prepareArray[$res->Code] = $res->Desc;
                $codeDesc[$value] = $prepareArray;
            }
        }
        return $codeDesc;
   }

}
