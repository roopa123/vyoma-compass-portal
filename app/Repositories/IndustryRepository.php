<?php
namespace App\Repositories;

use App\Models\Industry;
use App\Models\Advertiser;
use DB;

use App\Models\User;


class IndustryRepository extends BaseRepository
{

    public function __construct(\App\Models\Industry $Model)
    {
        $this->Database = DB::table('Industry');

        $this->visibleColumns = array('IndustryId', 'IndustryName' ,'Status');

        $this->orderBy = array(array('IndustryName'));
    }

    public $validationRules = [
            'industryName' => 'required|unique:Industry',
        ];

    // Update validation rule
    public function editValidationRules($industry) {
            return array('industryName' => 'required|unique:Industry,industryName,'.$industry['industryId'].',industryId',
                        'status' => 'required');

    }

    /**
    *  Get all the users which are mapped with Industry
    *
    */
	public function getIndustryValues()
	{
		return Industry::where('Status', '!=', self::STATUS_DELETE)->get();

	}

    public function findActiveIndustry()
    {
        return Industry::select('IndustryId', 'IndustryName')
            ->where('Status', self::STATUS_ACTIVE)->orderBy('IndustryName')->get();
    }

    public function activeIndustryList()
    {
        return Industry::where('Status', self::STATUS_ACTIVE)
        ->OrderBy('IndustryName')
        ->lists('IndustryName', 'IndustryId');
    }

    public function find($id)
    {
        return Industry::find($id);
    }

    /**
     * Store Industry Row
     */
    public function saveIndustry($industryArray)
    {
        $industry = ['IndustryName' => $industryArray['industryName'],
                        'Status' => self::STATUS_ACTIVE,
                        'UpdatedBy' => \Auth::user()->id
                        ];

        return Industry::create($industry);
    }

    /**
     * Update Industry Row
     */
    public function updateIndustry($id, $industryData)
    {
        $industry = Industry::find($id);
        $industry->IndustryName = $industryData['industryName'];
        $industry->Status = $industryData['status'];
        $industry->UpdatedBy = \Auth::user()->id;
        return ($industry->save()) ? $industry : false;
    }

    /**
    * Mark the Industry status to Delete
    */
    public function industryDelete($id)
    {
        $result = Industry::join('Advertiser', 'Advertiser.IndustryId', '=', 'Industry.IndustryId')
            ->where('Industry.IndustryId', $id)->get();

        if($result->isEmpty())
        {
            Industry::where('IndustryId', $id)
            ->update(array('status' => 'D'));
            return true;
        }
        return false;
    }
}
