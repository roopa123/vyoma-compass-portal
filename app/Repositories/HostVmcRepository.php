<?php
namespace App\Repositories;

use App\Repositories\CodeRepository;
use App\Repositories\HostRepository;
use App\Services\Constants\HostConfigConstants;
use DB;
use App\Models\Host;
use App\Models\Bay;

class HostVmcRepository extends BaseRepository
{
    /**
    * Instances of Code Repository
    */
    private $codeRepo;
    private $hostRepo;


    /**
    * Access all methods and objects in Repository
    */
    public function __construct(
        CodeRepository $codeRepo,
        HostRepository $hostRepo
    ) {
        $this->codeRepo = $codeRepo;
        $this->hostRepo = $hostRepo;
    }

    /*Validation Rules */
    public function validationRules() {
        return [
            'StartTime' => 'check_counter_start_time:EndTime|bay_start_time:BayStart',
            'EndTime'=>'check_counter_end_time:StartTime|bay_end_time:BayEnd',
            'HostType' => 'master_exists:BayId|check_player:BayId'
        ];        
    }     

    /*Validation Rules */
    public function editValidationRules($data) {
        return [
            'StartTime' => 'check_counter_start_time:EndTime|bay_start_time:BayStart',
            'EndTime'=>'check_counter_end_time:StartTime|bay_end_time:BayEnd',
            'HostType' => 'master_exists:BayId|check_player:BayId'
        ];        
    }   

    /**
    * Get MasterHost Object of Bay
    * @param int $bayId
    * @return Object
    */
    public function getMasterObjOfBay($bayId)
    {
        return Host::where('BayId', $bayId)->Where(function ($query) {
                $query->where('MasterOrPlayer', \Config::get('vyoma-constants.masterType'))->orWhere('MasterOrPlayer', \Config::get('vyoma-constants.bothType'));
        })->get();
    }
}