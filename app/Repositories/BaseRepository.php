<?php
namespace App\Repositories;

use DB;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
//use App\Repositories\UserRepository;


class BaseRepository extends EloquentRepositoryAbstract
{

    const STATUS_DELETE = 'D';

    const STATUS_ACTIVE = 'A';

    const STATUS_INACTIVE = 'I';
    // Creative Status - uplaoded
    const STATUS_UPLOADING = 'U';
    // Creative Status - Ready for Review
    const STATUS_READY_FOR_REVIEW = 'R';
    // Creative Status - uplaoded
    public $sectors = array('' => 'Select', 'Govt' => 'Govt', 'Private' => 'Private' , 'PSU'=>'PSU');

    public $isPaying = array('' => 'Select', 'Y' => 'Yes', 'N' => 'No');

    public $status = array('' => 'Select', 'A' => 'Active');

    public $Creativestatus = array('' => 'Select', 'R' => 'Review','A' => 'Active');

    public $poCloneStatus = array('A', 'P');

    CONST PO_CLONE_STATUS = '"A","P"';
    
    const PURCHASE_ORDER_PARTIAL_STATUS = 'P';

    const PURCHASE_ORDER_ACTIVE = 'A';

    const PURCHASE_ORDER_HISTORY = 'H';

    const PURCHASE_ORDER_DELETE = 'D';

    const PURCHASE_ORDER_COMPAIGN_NOT_PLAYED = 'N';

    const PURCHASE_ORDER_COMPAIGN_PLAYED = 'Y';

    const PURCHASE_ORDER_COMPAIGN_HISTORY = 'Y';

    const PURCHASE_ORDER_COMPAIGN_ACTIVE = 'N';
 
}
