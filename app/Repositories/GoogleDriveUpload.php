<?php
namespace App\Repositories;


class GoogleDriveUpload
{
    // Variable for Local file Path
    private $localPath;

    private $clienEmail;

    private $privateKey;

    private $scopes;

    public function __construct()
    {
        $this->localPath = \Config::get('gdrive.upload_user_path');

        $this->clienEmail =  \Config::get('gdrive.client_account');

        $this->privateKey = file_get_contents(\Config::get('gdrive.private_key'));

        $this->scopes = array(\Config::get('gdrive.drive_url'));
    }

    /**
    * Get Authentication credentials for File Upload in Google Drive
    * @param $credentials
    * @return boolean true 
    */
    public function GoogleClientAuth($credentials=null)
    {
        $credentials = new \Google_Auth_AssertionCredentials(
          $this->clienEmail,
          $this->scopes,
          $this->privateKey
        );

        $client = new \Google_Client();
        $client->setAssertionCredentials($credentials);
        if ($client->getAuth()->isAccessTokenExpired()) {
        $client->getAuth()->refreshTokenWithAssertion();
        }
        return $client;
    }

    /**
    * File uploading via Google Drive upload
    * @param Int $creativeId
    * @param String $filename
    * @return Int $googleFileId
    */
	public function GoogleDriveUpload($creativeid, $filename)
    {
        $filePath = $this->localPath . $filename;

        $client = $this->GoogleClientAuth();

        $service = new \Google_Service_Drive($client);
        $client->setDefer(true);

        $chunkSizeBytes = 1 * \Config::get('gdrive.chunk_size') * \Config::get('gdrive.chunk_size');

        $file = new \Google_Service_Drive_DriveFile(array('name' => $filename));
        $request = $service->files->create($file);

        $media = new \Google_Http_MediaFileUpload(
            $client,
            $request,
            \Config::get('gdrive.codec'),
            null,
            true,
            $chunkSizeBytes
        );

        $media->setFileSize(filesize($filePath));
        // Upload the various chunks. $status will be false until the process is
        // complete.
        $status = false;
        $handle = fopen($filePath, "rb");
        while (!$status && !feof($handle)) {
            // read until you get $chunkSizeBytes from TESTFILE
            // fread will never return more than 8192 bytes if the stream is read buffered and it does not represent a plain file
            // An example of a read buffered file is when reading from a URL
            $chunk = $this->readVideoChunk($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
        }
        // The final value of $status will be the data from the API for the object
        // that has been uploaded.
        $result = false;
        if ($status != false) {
            $result = $status;
        }
        fclose($handle);
        $client->setDefer(false);

        $fileId = $result['id'];
        if($this->insertPermission($service, $fileId, 'anyone', 'reader'))
        {
            return $result['id'];
        }
        return false;
  }

    /**
    *   If file size is large, file will be uploaded as chunk by chunk, Reading all the chunks and make it as Gaintchunk.
    * @param File $handle
    * @param Int $chunkSize
    * @return object $giantChunk
    */
    public function readVideoChunk ($handle, $chunkSize)
    {
        $byteCount = 0;
        $giantChunk = "";
        while (!feof($handle)) {
            // fread will never return more than 8192 bytes if the stream is read buffered and it does not represent a plain file
            $chunk = fread($handle, 8192);
            $byteCount += strlen($chunk);
            $giantChunk .= $chunk;
            if ($byteCount >= $chunkSize)
                return $giantChunk;
        }
      return $giantChunk;
    }

    /**
    * Download file from google drive to local through API
    * @param Int $fileId
    * @return Object $fileDetails
    */
    public function googleVideoDownload($fileId)
    {
        $client = $this->GoogleClientAuth();
        $service = new \Google_Service_Drive($client);

        return $service->files->get($fileId);

        $response = $service->files->listFiles();

        print "Files:\n";
          foreach ($response->getFiles() as $file) {
            printf("%s (%s)\n", $file->getName(), $file->getId());
          }
    }   

    /**
    * Get the sharable link through file uploaded id
    * @param Array $service
    * @param Int $fileId
    * @param String $Accesstype
    * @param String $Roletype
    * @return Boolean true
    */
    public function insertPermission($service, $fileId, $type, $role)
    {
    	$newPermission = new \Google_Service_Drive_Permission();
  		$newPermission->setType($type);
  		$newPermission->setRole($role);
  		try {
  		    $service->permissions->create($fileId, $newPermission);
  		} catch (Exception $e) {
  		   print "An error occurred: " . $e->getMessage();
  		}
  		return true;
	}

}