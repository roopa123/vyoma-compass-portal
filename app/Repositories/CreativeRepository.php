<?php
namespace App\Repositories;

use App\Models\Advertiser;
use App\Models\Creative;
use DB;
use App\Models\User;
use App\Models\CampaignCreative;
use App\Repositories\FfmpegRepository;
use App\Repositories\CodeRepository;
use App\Models\Code;
use App\Models\Category;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Response;
use Carbon\Carbon;


class CreativeRepository extends BaseRepository
{

    use DispatchesJobs;

    /**
     * Instances of FFMPEG Repository
     */
    protected $ffmpegRepo;

    /**
     * Instances of Code Repository
     */
    protected $codeRepo;


   /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
            'creativeName' => 'required|unique:Creative',
            'advertiser' => 'required',
            'file' => 'required|video_size|mime_type',
            'Category1' => 'required',
            'Category2' => 'required'
        ];


    /**
     * Update validation rule
     * @param Int $creativeId
     * @return Array Rules
     */
    public function editValidationRules($creative, $filedata)
    {
        if($filedata != ""){
            $result = array('creativeName' => 'required|unique:Creative,creativeName,'.$creative['creativeId'].',creativeId',
                'Status' => 'required');
        }
        else {
            $result = array('file' => 'required|video_size|mime_type','creativeName' => 'required|unique:Creative,creativeName,'.$creative['creativeId'].',creativeId',
                'Status' => 'required');
        }
        return $result;
    }

    /**
     * Class Init
     * @param \App\Models\Creative $Model
     * @param \App\FfmpegRepository $ffmpegRepo
     * @return Creative Josn Data
     */

    public function __construct(\App\Models\Creative $Model, FfmpegRepository $ffmpegRepo,
            CodeRepository $codeRepo)
    {
        $this->Database = DB::table('Creative')->select('Creative.CreativeId', 'Creative.CreativeName',
                'Advertiser.AdvertiserName', 'Creative.Category1',
                'Creative.Category2','Creative.FileName', 'Creative.FileMetaData', 'Users.name as UpdatedBy',DB::raw('DATE_FORMAT(`Creative`.`UpdatedAt`,"%d-%m-%Y %h.%i %p") as `UpdatedAt`'),
                DB::raw('FileMetaData  as FileDuration'),
                'Advertiser.Sector', 'Industry.IndustryName', 'Creative.Status')
                            ->join('Advertiser', 'Advertiser.AdvertiserId', '=', 'Creative.AdvertiserId')
                            ->leftjoin('Industry', 'Advertiser.IndustryId', '=', 'Industry.IndustryId')
                            ->join('Users', 'Users.id', '=', 'Creative.UpdatedBy');

        $this->visibleColumns = array('Creative.CreativeId', 'Creative.CreativeName',
                'Advertiser.AdvertiserName', 'Creative.Category1',
                'Creative.Category2', 'Creative.FileName','Creative.FileMetaData', 'FileDuration', 'Users.name as UpdatedBy','UpdatedAt',
                'Advertiser.Sector', 'Industry.IndustryName', 'Creative.Status');

        $this->orderBy = array(array('CreativeName'));

        $this->ffmpegRepo = $ffmpegRepo;

        $this->codeRepo = $codeRepo;
    }



    /**
     * Gett all active creatives with associated with advertisers
     * @param Boolean $isResponseArray
     */
    public function all($isResponseArray = false)
    {
        $creatives = Creative::with('advertiser')->where('Status', '!=', self::STATUS_DELETE)->get();
        return (!$isResponseArray)?$creatives:$creatives->toArray();
    }

    /**
     * Find creatives
     * @param Int $id
     * @return Creative Object
    */
    public function find($id)
    {
        return Creative::find($id);
    }

    /**
     * Find CreativeConvertion Item
     * @return Creative Object
    */
    public function findCreativeConvertionItem()
    {
        return Creative::where('Status', '=', self::STATUS_UPLOADING )
        ->where('GStatus', '=', NULL)
        ->orderBy('CreativeId', 'ASC')->first();
    }

    /**
     * Find CreativeConvertion Item
     * @return Creative Object
    */
    public function findStatusForConvertion()
    {
        return DB::table('Creative')        
            ->where('GStatus', '=', self::STATUS_UPLOADING )
            ->where('Status', '=', self::STATUS_UPLOADING )  
            ->whereRaw('UpdatedAt < DATE_SUB(NOW(),INTERVAL '.\Config::get('gdrive.required_time_for_update_creativeStatus').' MINUTE)')            
            ->orderBy('CreativeId', 'ASC')->get();        
    }

    /**
     * Fetch last created record in Creatives
     * @return Creative Object
     */
    public function getLastCreatedCreatives()
    {
        return Creative::orderBy('CreativeId', 'desc')->first()->toArray();
    }

    /**
     * Store Creative row
     * @param Array $creativeData
     * @param FileObject $creativeFile
     * @return Creative
    */
    public function storeCreative($creativeData, $creativeFile)
    {

        $creativeArray = ['CreativeName' => $creativeData['creativeName'],
                        'AdvertiserId' => $creativeData['advertiser'],
                        'Category1' => $creativeData['Category1'],
                        'Category2' => $creativeData['Category2'],
                        'Status' => self::STATUS_UPLOADING,
                        'UpdatedBy' => \Auth::user()->id,
                        ];
        // Add creative
        $creative = Creative::create($creativeArray);
        //genrateCreativeFileName
        $cretiveFileName = $this->genrateCreativeFileName($creative, $creativeFile);

        // update the CreativeFilename with creative id and crative name
        $creative->FileName = $cretiveFileName;


        // Move The File into converation queue directory
        $creativeFile->move(\Config::get('ffmpeg.upload_user_path'), $cretiveFileName);

        $uploadPath = "";
        $filemetadata= $this->getInputFileMetadata($uploadPath, $creativeFile, $cretiveFileName);
        $creative->FileMetaData = json_encode($filemetadata);
        $creative->save();

        return $creative;
    }


     /**
     * Create FileName according to Format
     * @param Int $creativeId
     * @param FileObject $creativeFile
     * @return CreativeFileName
    */
    public function genrateCreativeFileName($creative, $creativeFile)
    {
         // genrate creative_filename
        $creativeExt = $creativeFile->getClientOriginalExtension();
        //
        $cretiveFileName = $creative->CreativeId.'_'.$creative->CreativeName.'.'.$creativeExt;
        $cretiveFileName = str_replace(' ', '_', $cretiveFileName);
        return $cretiveFileName;
    }

    /**
     * Video file Metadata Forming
     * @param String $uploadpath
     * @param FileObject $fileValue
     * @return Array $videoDetails
    */
    public function getInputFileMetadata($uploadpath, $fileValue, $fileName)
    {
        $path = \Config::get('ffmpeg.upload_user_path').$fileName;
        $pathWithoutName = \Config::get('ffmpeg.upload_user_path');
        $filesize = filesize($path);
        $fileduration = $this->ffmpegRepo->getFileDuration($pathWithoutName, $fileName);
        $fileBytesToMb = $this->ffmpegRepo->formatBytes($filesize);
        $fileExtn = 'mp4';

        if($uploadpath != null) {
           $path = $uploadpath;
        }

        $videoDetails = array('url' => $path, 'FileName' => $fileName, 'size' => $fileBytesToMb, 'duration' => $fileduration, 'extension' => $fileExtn);

        return $videoDetails;
    }

    /**
     * Update Creative row
     * @param Array $input
     * @param Integre $id
     * @return Creative
    */
    public function updateCreative($input, $id, $videoDetails = null)
    {
        $creative = Creative::find($id);
        if(isset($input['creativeName']))
            $creative->CreativeName = $input['creativeName'];
        if(isset($input['advertiser']))
            $creative->AdvertiserId = $input['advertiser'];
        if(isset($input['Category1']))
            $creative->Category1 = $input['Category1'];
        if(isset($input['Category2']))
            $creative->Category2 = $input['Category2'];
        if(isset(\Auth::user()->id)) {
            // Updated User
            $creative->UpdatedBy = \Auth::user()->id;
        }
        // check the file is present
        if(isset($input['file'])) {
            //dd($input);
            $creative->Status = self::STATUS_UPLOADING;
            $creative->GStatus = NULL;
            //$fileName = $input['file']->getClientOriginalName();
            $fileName = $this->genrateCreativeFileName($creative, $input['file']);
            $creative->FileName = $fileName;
            // Move The File into converation queue directory
            $input['file']->move(\Config::get('ffmpeg.upload_user_path'), $fileName);
            $filemetadata= $this->getInputFileMetadata(null, $input['file'], $fileName);
            $creative->FileMetaData = json_encode($filemetadata);

        } else 
        {
            $creative->Status = $input['Status'];
            $creative->GStatus = $input['GStatus'];            
            // Check Video convertion
            if($videoDetails) {
                $filemetadata= $this->getInputFileMetadata($videoDetails, null, $input['FileName']);
                $creative->FileMetaData = json_encode($filemetadata);
            }
        }               
        return ($creative->save()) ? $creative : false;
    }

    /**
    * Mark creative status as Delete
    * @param Int $id
    * @return Creative Object
    */
    public function statusDeleteForCreative($id)
    {
        /*$creative = Creative::findOrFail($id);
        $creative->Status = self::STATUS_DELETE;
        $creative->UpdatedBy = \Auth::user()->id;
        return ($creative->save())? true : false;*/

        $result = Creative::join('CampaignCreative', 'Creative.CreativeId', '=', 'CampaignCreative.CreativeId')
            ->where('Creative.CreativeId', $id)->get();

        if($result->isEmpty())
        {
            Creative::where('CreativeId', $id)
            ->update(array('Status' => self::STATUS_DELETE,
                            'UpdatedBy' => \Auth::user()->id));

            return true;
        }
        return false;
    }

    /**
    * Update Delete status to Review status, when user request for Undo
    * @param Int $creativeId
    * @return \Illuminate\Http\Response
    **/
    public function updateStatus($value)
    {
        $creative = Creative::findOrFail($value);
        $urlInfo = json_decode($creative->FileMetaData);
        if(strpos($urlInfo->url, $urlInfo->FileName)){
           $creative->Status = self::STATUS_UPLOADING;
        }
        else {
            $creative->Status = self::STATUS_READY_FOR_REVIEW;
        }
        $creative->UpdatedBy = \Auth::user()->id;
        return ($creative->save())? true : false;
    }

    /**
    * Update status value for creative, From Review to Active or vice versa
    * @param Int $CreativeId
    * @param Char $status
    * @return \Illuminate\Http\Response
    */
    public function updateStatusValues($id, $status)
    {
        $creative = Creative::findOrFail($id);
        if($status === self::STATUS_READY_FOR_REVIEW) {
            $result = Creative::join('CampaignCreative', 'Creative.CreativeId', '=', 'CampaignCreative.CreativeId')
            ->where('Creative.CreativeId', $id)->get();

            if(!$result->isEmpty())
            {
                return 'associatedWithPo';
            }
            $creative->Status = $status;
        }
        else {

            $creative->Status = $status;

        }
        $creative->UpdatedBy = \Auth::user()->id;
        return ($creative->save())? true : false;
    }

    /**
     * Delete video files from Local Path
     * @param String $filename
     * @param Int $id
     * @return boolean
     */
    public function deleteInputFile($file, $id)
    {
        $creative = Creative::findOrFail($id);
        $path = \Config::get('ffmpeg.upload_user_path').$file;
        $res = \File::delete($path);
        if($res == true) {
            $creative->FileName = '';
            $creative->FileMetaData = '';
            $creative->UpdatedBy = \Auth::user()->id;
            $creative->save();

            return true;
        }
        else {
            return false;
        }
    }

    public function getCreativesByAdvertiser($advertiserId)
    {
        return  Creative::where('AdvertiserId', $advertiserId)
        ->Where(function($query)
            {
                $query->where('Status', '=', self::STATUS_ACTIVE)
                      ->orWhere('Status', '=', self::STATUS_READY_FOR_REVIEW);
            })
        ->select('CreativeId', 'CreativeName')->get();


    }

    public function getCreativeDetailsById($creativeId)
    {
       return DB::select(DB::raw("select FileMetaData,CreativeId,CreativeName,Status, FileMetaData as FileDuration from Creative where (Status = '".self::STATUS_ACTIVE."' or Status='".self::STATUS_READY_FOR_REVIEW."') and CreativeId in ($creativeId)"));
    }

    public function checkCreativeToAddForCampaign($creativeId)
    {
        return DB::select(DB::raw("select count(*) as CreativeCount from Creative where Status='".self::STATUS_READY_FOR_REVIEW."' and CreativeId in ($creativeId)"));

    }
    public function checkAdvertiserHasCreatives($advertiserId)
    {

        return Creative::where('AdvertiserId', $advertiserId)
               ->Where(function($query)
                    {
                        $query->where('Status', '=', self::STATUS_ACTIVE)
                              ->orWhere('Status', '=', self::STATUS_READY_FOR_REVIEW);
                    })
               ->count();

    }

    public function checkCampaignCreative($id)
    {
        return Creative::join('CampaignCreative', 'Creative.CreativeId', '=', 'CampaignCreative.CreativeId')
            ->where('Creative.CreativeId', $id)->first();
    }

}
