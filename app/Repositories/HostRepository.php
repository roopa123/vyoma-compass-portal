<?php
namespace App\Repositories;

use App\Models\User;
use App\Models\Bay;
use App\Models\Host;
use App\Models\State;
use App\Services\Constants\ApiConstants;

use DB;

class HostRepository
{

    public  $validationRules = [
            //'HostName' => 'required|state_code:StateCode|station_name|counter_type|bay_location_code|hyphen|add_number|unique:Host',
            'HostName' => 'required|unique:Host',
            'CounterNumber' => 'required|regex:/^[A-Za-z0-9_]+$/',
            'HostMachineId' => 'numeric',
            'HostMasterId'  => 'numeric',
            'BayId'  => 'required_if:BayLocationBayId,""',
            'Host_StartTime' => 'required',
            'Host_EndTIme' => 'required'
        ];

    // Update validation rule
    public function editValidationRules($host) {
        //dd($host);
             $rules = array(//'HostName' => 'required|state_code:StateCode|station_name|counter_type|bay_location_code|hyphen|add_number|unique:Host,HostName,'.$host['HostId'].',HostId',
                            'HostName' => 'required | unique:Host,HostName,'.$host['HostId'].',HostId',
                            'CounterNumber' => 'required|regex:/^[A-Za-z0-9_]+$/',
                            'HostMachineId' => 'numeric',
                            'HostMasterId'  => 'numeric',
                            'BayId'  => 'required_if:BayLocationBayId,""',
                            'Host_StartTime' => 'required',
                            'Host_EndTIme' => 'required'
                            );
             return $rules;
        }

    /*
    * Insert host details to DB
    */
    public function insertData($inputData)
    {
        if (isset($inputData['BayLocationBayId']) && $inputData['BayLocationBayId'] != null){
            $inputData['BayId'] = $inputData['BayLocationBayId'];
        }
        if (isset($inputData['MasterOrPlayer'])) {
            $checkMaster = Host::where('BayId', $inputData['BayId'])->Where(function($query)
            {
                $query->where('MasterOrPlayer', '=', \Config::get('vyoma-constants.masterType'))
                      ->orWhere('MasterOrPlayer', '=', \Config::get('vyoma-constants.bothType'));
            })->first();

            $startEndTime = Bay::where('BayId', $inputData['BayId'])->first();
            
            if($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                $masterPlayerBoth = \Config::get('vyoma-constants.masterType');
                $masterId = $inputData['HostId'];
                $masterName = $inputData['HostName'];
                if (strtotime($inputData['Host_StartTime']) >= strtotime($startEndTime['StartTime'])  &&  strtotime($inputData['Host_EndTIme']) <= strtotime($startEndTime['EndTIme'])) {
                    $sTime = $inputData['Host_StartTime'];
                    $eTime = $inputData['Host_EndTIme'];
                }
                else {
                    $strtTim = date('H:i', strtotime($startEndTime['StartTime']));
                    $endtime = date('H:i', strtotime($startEndTime['EndTIme']));
                    $error = "Please select time between Bay's StartTime ".$strtTim." & EndTime ".$endtime;
                    throw new \Exception($error);
                }
                
            }
            else if ($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                $masterPlayerBoth = \Config::get('vyoma-constants.bothType');
                $masterId = $inputData['HostId'];
                $masterName = $inputData['HostName'];
                if (strtotime($inputData['Host_StartTime']) >= strtotime($startEndTime['StartTime'])  &&  strtotime($inputData['Host_EndTIme']) <= strtotime($startEndTime['EndTIme'])) {
                    $sTime = $inputData['Host_StartTime'];
                    $eTime = $inputData['Host_EndTIme'];
                }
                else {
                    $strtTim = date('H:i', strtotime($startEndTime['StartTime']));
                    $endtime = date('H:i', strtotime($startEndTime['EndTIme']));
                    $error = "Please select time between Bay's StartTime ".$strtTim." & EndTime ".$endtime;
                    throw new \Exception($error);
                }
            }
            else if ($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')) {
                $error = 'Cannot create Players until create Master';
                throw new \Exception($error);
            }            
            else {   
                  
                if($checkMaster->MasterOrPlayer == \Config::get('vyoma-constants.bothType') && $inputData['MasterOrPlayer'] == $checkMaster->MasterOrPlayer) {
                    $error = 'Host Type is Both, already Present!';
                    throw new \Exception($error);
                }
                else if($checkMaster->MasterOrPlayer == \Config::get('vyoma-constants.bothType') && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                    $error = 'Host Type is Both, already Present!';
                    throw new \Exception($error);
                }
                else if($checkMaster->MasterOrPlayer == \Config::get('vyoma-constants.masterType') && $inputData['MasterOrPlayer'] == $checkMaster->MasterOrPlayer)
                {
                    
                    $error = 'Host Type is Master, already Present!';
                    throw new \Exception($error);
                }
                elseif ($checkMaster->MasterOrPlayer == \Config::get('vyoma-constants.masterType') && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                    
                    $error = 'Host Type is Master, already Present!';
                    throw new \Exception($error);
                }
                else {
                    if (strtotime($inputData['Host_StartTime']) >= strtotime($checkMaster->StartTime)  &&  strtotime($inputData['Host_EndTIme']) <= strtotime($checkMaster->EndTIme)) 
                        {
                            $sTime = $inputData['Host_StartTime'];
                            $eTime = $inputData['Host_EndTIme'];
                        }
                        else {
                            $strtTim = date('H:i', strtotime($checkMaster->StartTime));
                            $endtime = date('H:i', strtotime($checkMaster->EndTIme));
                            $error = "Please select time between Master's StartTime ".$strtTim." & EndTime ".$endtime;
                            throw new \Exception($error);
                        }
                    $masterPlayerBoth = \Config::get('vyoma-constants.playerType');
                    $masterId = $checkMaster->HostMasterId;
                    $masterName = $checkMaster->HostMasterName;
                }
            }
        }
        $endTIme = $this->timeValidation( $eTime, $sTime);
        $dataToInsert = array(
            'HostName' => $inputData['HostName'],
            'BayId' => $inputData['BayId'],
            'MasterOrPlayer' => $masterPlayerBoth,
            'HostCounterType' => $inputData['HostCounterType'],
            'HostMasterId' => $masterId,
            'HostMachineId' => "",
            'CounterNumber' => $inputData['CounterNumber'],
            'HostMasterName' => $masterName,
            'StartTime' =>$sTime,
            'EndTIme' => $endTIme,
            'Status' => $inputData['Host_Status']
        );
        $insertedId = DB::table('Host')->insertGetId($dataToInsert);
        if($insertedId){
            if($checkMaster == null && $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType') || $inputData['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                $result = DB::table('Host')->where('HostId', $insertedId)
                ->update(array('HostMasterId' => $insertedId));

                return $result;
            }
        }
       return  $insertedId;
    }

    public function timeValidation($endTime, $startTime) {
        if(strtotime($endTime) > strtotime($startTime) && strtotime($endTime) <= strtotime('23:59'))
             {
                 return $endTime;
             }
             else {
                $error = 'Please give valid StartTime and EndTime';
                throw new \Exception($error);
            }
    }

    public function getBayApp($bayId){
        $app = Bay::select('App')->where('BayId', $bayId)->first();

        return $app;
    }

    /*
    * Fetch bay list info with bay location code
    */
    public function baylist()
    {
        $status = \Config::get('vyoma-constants.status');
        $list = DB::table('Bay')->distinct()->where('Bay.Status', $status['Active']) 

                ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                ->lists('BayLocation.BayLocationDesc', 'Bay.BayId');
        return $list;
    }

    /*
    * Bay location Info
    */
    public function getBayLocationInfo($bayId)
    {
        return Bay::where('Bay.BayId', $bayId)->first();
        
    }

    public function bayDetail($bayid)
    {
        $status = \Config::get('vyoma-constants.status');
        $list = Bay::where('Bay.Status', $status['Active'])
                ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                ->where('BayId', $bayid)->lists('BayLocation.BayLocationDesc', 'Bay.BayId');
        return $list;
    }

    public function bayDetailTest($bayid)
    {
        $status = \Config::get('vyoma-constants.status');
        $list = Bay::where('Bay.Status', $status['Active'])
                ->select('BayLocation.BayLocationDesc', 'BayLocation.BayLocationCode','Bay.BayId', 'Bay.App', 'Bay.StartTime', 'Bay.EndTIme')
                ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                ->where('BayId', $bayid)->get();
        return $list;
    }

    public function stationDetail($bayid)
    {
        $station = Bay::select('Station.StationName','Station.StationId')->where('Bay.Status', 'A')->where('Bay.BayId', $bayid)
                ->join('Station', 'Bay.StationId' ,'=', 'Station.StationId')->first();


        return $station->toArray();
    }

    /*
    * Master or player information
    */
    public function getPlayerOrMasterInfo($bayid)
    {
        $checkMaster = Host::where('BayId', $bayid)->get();
        return $checkMaster->toArray();
    }

    public function getHostId($host) {
        $host = Host::select('BayId')->where('HostName', $host)->first();
        return $host;
    }

    public function getHostName($hostid) {
        $host = Host::select('HostName')->where('HostId', $hostid)->first();
        return $host;
    }

    public function getAppInfoWithBay($bayid)
    {
        $checkMaster = Bay::where('BayId', $bayid)->first();
        return $checkMaster;
    }

    public function getHostIdwithName($host) {
        $host = Host::select('HostId', 'BayId', 'StartTime', 'EndTIme')->where('HostName', $host)->first();
        return $host;
    }

    public function getLocationInfo($bay, $host)
    {
        $bayLocation = DB::table('Host')
                            ->distinct()
                            ->select('BayLocation.BayLocationDesc', 'BayLocation.BayLocationCode')
                            ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                            ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                            ->where('Host.BayId', $bay)
                            ->where('Host.HostId', $host)
                            ->first();

        return $bayLocation;
    }

    public function getBayinfoWithStations($station)
    {
        $status ='A';
           $detailsOfBay = DB::Select(DB::raw('SELECT BayLocation.BayLocationCode as BayLocationCode, Bay.App, Bay.BayId FROM `BayLocation`
           JOIN `Bay` ON Bay.BayLocationCode = BayLocation.BayLocationCode
           WHERE Bay.StationId=:station AND Bay.status =:status
           '),array('station' => $station , 'status' =>$status));

            // $detailsOfBay = DB::SELECT()('BayLocation')
            // ->select('Bay.BayId', 'CONCAT(BayLocation.BayLocationDesc, Bay.app) AS BayLocationDesc')
            // ->join('Bay', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
            //                 ->where('Bay.StationId', $station)
            //                 ->where('Bay.status', 'A')
            //                 ->groupBy('BayLocation.BayLocationCode')->get();
            $bays = $detailsOfBay;


        return $bays;
    }

    /*
    * Update host info with id
    */
    public function updateHostDetails($hostid)
    {
        $status = \Config::get('vyoma-constants.status');
        $masterName = \Input::get('MasterOrPlayer');
        $stationId = \Input::get('StationName');
        $bayid = \Input::get('BayId');
        $sTime = \Input::get('Host_StartTime');
        $eTime = \Input::get('Host_EndTIme');
        $startEndTime = Bay::where('BayId', \Input::get('BayId'))->first();
        $changeOfPlayers = Host::select('MasterOrPlayer', 'HostMasterId', 'BayId')
                                ->where('HostId', $hostid)->first();
        if($changeOfPlayers['BayId'] != \Input::get('BayId')) {
            $getBay = Host::select('MasterOrPlayer', 'HostMasterId', 'HostMasterName')
                            ->where('BayId', $changeOfPlayers['BayId'])->first();
            if(\Input::get('MasterOrPlayer') == \Config::get('vyoma-constants.playerType')) {

                $result = DB::table('Host')->where('HostId', $hostid)
                            ->update(array('HostMasterId' => \Input::get('HostMasterId'),
                                           'HostMasterName' =>$getBay['HostMasterName']));

            }
        }
        if(\Input::get('MasterOrPlayer') == \Config::get('vyoma-constants.masterType')) {
            $hostMasterName = \Input::get('HostName');   
            $masterId = Host::where('HostId', $hostid)->first();
            /*Check whether has any players*/
            $players = Host::where('HostMasterId', $masterId['HostMasterId'])->count();
            /* if Master is assigning to other Station, check whether station has master/both*/
            $stationMasterExists = $this->checkMasterForStations($stationId, $bayid);   

            if($players > 1 && $stationMasterExists == "") {
                $error = 'Players are present, Cannot assign Master to other Station';
                throw new \Exception($error);
            }

            if($masterId['BayId'] != $bayid) {
                if($stationMasterExists != "" && $stationMasterExists['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                        $error = 'For Station '.$stationMasterExists['StationName'].' , Master already present! Cannot assign Master';
                        throw new \Exception($error);
                    }
                if($stationMasterExists != "" && $stationMasterExists['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                    $error = 'For Station '.$stationMasterExists['StationName'].', Both already present! Cannot assign Master';
                    throw new \Exception($error);
                }    
            }    
            if (strtotime($sTime) >= strtotime($startEndTime['StartTime'])  &&  strtotime($eTime) <= strtotime($startEndTime['EndTIme'])) {
                    $sTime = \Input::get('Host_StartTime');
                    $eTime = \Input::get('Host_EndTIme');
                }
                else {
                    $strtTim = date('H:i', strtotime($startEndTime['StartTime']));
                    $endtime = date('H:i', strtotime($startEndTime['EndTIme']));
                    $error = "Please select time between Bay's StartTime ".$strtTim." & EndTime ".$endtime;
                    throw new \Exception($error);
                }
            
            if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                $masterName = \Config::get('vyoma-constants.masterType');
            }
            else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')){
               $count = Host::select('HostMasterId', 'MasterOrPlayer')
                            ->where('HostId', $hostid)->first();

                $hostmasterId = Host::where('HostId', $count['HostMasterId'])->get();
                if($hostmasterId != null){
                    $error = "Master already Present";
                    throw new \Exception($error);
                }

            }
            else {
                $count = Host::select('HostMasterId', 'MasterOrPlayer')
                                ->where('HostId', $hostid)->first();
                $hostmasterId = Host::where('HostId', $count['HostMasterId'])->get();
                $result = DB::table('Host')->where('HostMasterId', $hostid)->first();
                if($hostMasterName != $result->HostMasterName) {                
                    DB::table('Host')->where('HostMasterId', $hostid)->update(array('HostMasterName' =>$hostMasterName));
                }
               /* if($hostmasterId != null){
                    $error = "Master already Present";
                    throw new \Exception($error);

                }*/
            }

            if(\Input::get('Host_Status') == $status['Inactive']) {
                $inactiveHost = DB::table('Host')->where('HostMasterId', $masterId['HostMasterId'])
                                ->update(array('Status' => $status['Inactive']));
            }
            else {
                $inactiveHost = DB::table('Host')->where('HostMasterId', $masterId['HostMasterId'])
                                ->update(array('Status' => $status['Active']));
            }
        }
        else if(\Input::get('MasterOrPlayer') == \Config::get('vyoma-constants.playerType')){
            $hostMasterName = \Input::get('HostMasterName');
            $hostMaster = DB::table('Host')->where('HostMasterName', \Input::get('HostMasterName'))->first();
            $idOfhostmaster = $hostMaster->HostMasterId;

            $result = DB::table('Host')->where('HostId', $hostid)
                            ->update(array('HostMasterId' => $idOfhostmaster,
                                           'HostMasterName' =>$hostMasterName));

            $masterId = Host::select('HostMasterId', 'MasterOrPlayer')
                                ->where('HostId', $hostid)->first();
            if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')) {
            $hostmasterTime = Host::select('StartTime', 'EndTIme')
                                ->where('HostId', $masterId['HostMasterId'])->first();
            if (strtotime($sTime) >= strtotime($hostmasterTime['StartTime'])  &&  strtotime($eTime) <= strtotime($hostmasterTime['EndTIme'])) {
                $sTime = \Input::get('Host_StartTime');
                $eTime = \Input::get('Host_EndTIme');
            }
            else {
                $sTime = date('H:i', strtotime($hostmasterTime['StartTime']));
                $eTime = date('H:i', strtotime($hostmasterTime['EndTIme']));
                $error = "Please select time between Master's StartTime ". $sTime. " & EndTime ".$eTime;
                throw new \Exception($error);
            }
        }
        else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                //$masterName = \Config::get('vyoma-constants.bothType');
                $error = 'Cannot change Both type to Player';
                throw new \Exception($error);
            }
            else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                //$masterName = \Config::get('vyoma-constants.masterType');
                $error = 'Cannot change Master type to Player';
                throw new \Exception($error);
            }

        }
        else if(\Input::get('MasterOrPlayer') == \Config::get('vyoma-constants.bothType')){                
                $hostMasterName = \Input::get('HostName');
                $masterId = Host::where('HostId', $hostid)->first();
                /*Check whether has any players*/
                $players = Host::where('HostMasterId', $masterId['HostMasterId'])->count();
                /* if Master is assigning to other Station, check whether station has master/both*/
                $stationMasterExists = $this->checkMasterForStations($stationId, $bayid);

                if($players > 1 && $stationMasterExists == "") {
                    $error = 'Players are present, Cannot assign Master to other Station';
                    throw new \Exception($error);
                }
                if($masterId['BayId'] != $bayid) {
                    if($stationMasterExists != "" && $stationMasterExists['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                        $error = 'For Station '.$stationMasterExists['StationName'].' , Master already present! Cannot assign Both';
                        throw new \Exception($error);
                    }
                    if($stationMasterExists != "" && $stationMasterExists['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')) {
                        $error = 'For Station '.$stationMasterExists['StationName'].', Both already present! Cannot assign Both';
                        throw new \Exception($error);
                    } 
                }                

                if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                $count = Host::select('HostMasterId', 'MasterOrPlayer')
                        ->where('HostMasterId', $masterId['HostMasterId'])->count();              
                if (strtotime($sTime) >= strtotime($startEndTime['StartTime'])  &&  strtotime($eTime) <= strtotime($startEndTime['EndTIme'])){
                    $sTime = \Input::get('Host_StartTime');
                    $eTime = \Input::get('Host_EndTIme');
                }
                else {
                    $strtTim = date('H:i', strtotime($startEndTime['StartTime']));
                    $endtime = date('H:i', strtotime($startEndTime['EndTIme']));
                    $error = "Please select time between Bay's StartTime ".$strtTim." & EndTime ".$endtime;
                    throw new \Exception($error);
                }
                $masterName = \Config::get('vyoma-constants.bothType');
               
            }
            else if($masterId['MasterOrPlayer'] == \Config::get('vyoma-constants.playerType')) {
                    $error = "Cannot Change Player to Both, it has Master";
                    throw new \Exception($error);
            }
            else {
                $result = Host::where('HostMasterId', $hostid)->first();

                if($hostMasterName != $result->HostMasterName) {                
                    DB::table('Host')->where('HostMasterId', $hostid)->update(array('HostMasterName' =>$hostMasterName));
                }
            }
        }
        $endTIme = $this->timeValidation($eTime, $sTime);

        $sTime = date('H:i:s', strtotime($sTime));
        $endTIme = date('H:i:s', strtotime($endTIme));

        $result = DB::table('Host')->where('HostId', $hostid)
            ->update(array('HostName' => \Input::get('HostName'),
                            'BayId' => \Input::get('BayId'),
                            'MasterOrPlayer' => $masterName,
                            'HostCounterType' => \Input::get('HostCounterType'),
                            //'HostMasterId' => \Input::get('HostMasterId'),
                            'HostMachineId' => \Input::get('HostMachineId'),
                            'CounterNumber' => \Input::get('CounterNumber'),                           
                            'StartTime' => $sTime,
                            'EndTIme' => $endTIme,
                            'Status' => \Input::get('Host_Status')
                            ));
    }

    /*
    * Autocomplete search for Host
    */

     public function autocompleteSearch($searchterm){
        $term = $searchterm;
        $colValue = \Request::segment(3);
        $results = array();
        $queries = DB::table('Host')
                    ->select('Station.StationName', 'BayLocation.BayLocationCode', 'Bay.App', 'Host.HostName', 'Host.HostMasterName', 'Host.HostCounterType', 'Host.CounterNumber', 'Branch.BranchName', 'Region.RegionName', 'Host.StartTime', 'Host.EndTIme', 'Host.HostId', 'Bay.BayId')
                    ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
                    ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                    ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
                    ->join('Branch', 'Station.BranchId', '=', 'Branch.BranchId')
                    ->join('Region', 'Station.RegionId', '=', 'Region.RegionId');
        
        switch ($colValue) {
            case 'StationName':
                $queries->where('Station.StationName', 'LIKE', $term.'%');
                $queries->groupBy('Station.StationName');
                break;
            case 'BayLocationCode':
                $queries->where('BayLocation.BayLocationCode', 'LIKE', $term.'%');
                $queries->groupBy('BayLocation.BayLocationCode');
                break;
            case 'App':
                $queries->where('Bay.App', 'LIKE', $term.'%');
                $queries->groupBy('Bay.App');
                break;
            case 'HostName':
                $queries->where('Host.HostName', 'LIKE', $term.'%');
                $queries->groupBy('Host.HostName');
                break;
            case 'HostMasterName':
                $queries->where('Host.HostMasterName', 'LIKE', $term.'%');
                $queries->groupBy('Host.HostMasterName');
                break;
            case 'HostCounterType':
                $queries->where('Host.HostCounterType', 'LIKE', $term.'%');
                $queries->groupBy('Host.HostCounterType');
                break;
            case 'CounterNumber':
                $queries->where('Host.CounterNumber', 'LIKE', $term.'%');
                $queries->groupBy('Host.CounterNumber');
                break;
            case 'BranchName':
                $queries->where('Branch.BranchName', 'LIKE', $term.'%');
                $queries->groupBy('Branch.BranchName');
                break;
            case 'RegionName':
                $queries->where('Region.RegionName', 'LIKE', $term.'%');
                $queries->groupBy('Region.RegionName');
                break;
            case 'StartTime':
                $queries->where('Host.StartTime', 'LIKE', $term.'%');
                $queries->groupBy('Host.StartTime');
                break;
            case 'EndTIme':
                $queries->where('Host.EndTIme', 'LIKE',  $term.'%');
                $queries->groupBy('Host.EndTIme');
                break;
        }
        //$queries->groupBy(\Request::segment(3));
        $res = $queries->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();

        foreach ($res as $query)
        {
            if(\Request::segment(3) == 'StationName'){
                $results[] = [ 'id' => $query->HostId, 'value' => $query->StationName];
            }
            else if(\Request::segment(3) == 'BranchName') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->BranchName];
            }
            else if(\Request::segment(3) == 'RegionName'){
                $results[] = [ 'id' => $query->HostId, 'value' => $query->RegionName];
            }
            else if(\Request::segment(3) == 'BayLocationCode') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->BayLocationCode];
            }
            else if(\Request::segment(3) == 'App') {
                $results[] = [ 'id' => $query->BayId, 'value' => $query->App];
            }
            else if(\Request::segment(3) == 'HostName') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->HostName];
            }
            else if(\Request::segment(3) == 'HostMasterName') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->HostMasterName];
            }
            else if(\Request::segment(3) == 'HostCounterType') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->HostCounterType];
            }
            else if(\Request::segment(3) == 'CounterNumber') {
                $results[] = [ 'id' => $query->HostId, 'value' => $query->CounterNumber];
            }
            else if(\Request::segment(3) == 'StartTime') {
                $startTime = substr($query->StartTime, 0, -3);
                $results[] = ['id' => $query->HostId, 'value' => $startTime];
            }
            else if(\Request::segment(3) == 'EndTIme') {
                $endTime = substr($query->EndTIme, 0, -3);
                $results[] = ['id' => $query->HostId, 'value' => $endTime];
            }
        }
        return \Response::json($results);
    }

    /*
    * Delete host info with id
    */
    public function deleteHost($id)
    {
        $status = \Config::get('vyoma-constants.status');
        Host::where('HostId', $id)
            ->update(array('Status' => $status['Delete']));
    }

    public function stationId($bayid)
    {
        $station = Bay::select('Station.StationId')->where('Bay.Status', 'A')->where('Bay.BayId', $bayid)
                ->join('Station', 'Bay.StationId' ,'=', 'Station.StationId')->first();


        return $station;
    }

    public function checkMasterForStations($stationId, $bayId)
    {
        $host = Host::select('Host.MasterOrPlayer','Bay.BayId','Station.StationId', 'Station.StationName')
              ->join('Bay', 'Host.BayId', '=', 'Bay.BayId')
              ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
              ->where('Bay.BayId', $bayId)
              ->where('Station.StationId', $stationId)
              ->Where(function($query)
            {
                $query->where('MasterOrPlayer', '=', \Config::get('vyoma-constants.masterType'))
                      ->orWhere('MasterOrPlayer', '=', \Config::get('vyoma-constants.bothType'));
            })->first();
         
        return $host;
    }

    //Dingo api functions
    public function validateHostname($hostName)
    {
        if (Host::where('HostName', $hostName)->exists()) {
            return true;
        }
        return false;
    }

    // Get Host Details By Name
    public function getHostByName($hostName)
    {
        return Host::where('HostName', $hostName)->first();
    }

    // Get HostObject By Name
    public function getHostObjectByName($hostName)
    {
        $hostDetails = $this->getHostByName($hostName);
        return $this->getHostObject($hostDetails->HostId);
    }

    public function getHostObject($hostId)
    {
        return Host::find($hostId);
    }

    public function getPlayersByHostId($hostId)
    {
        return Host::where('HostMasterId', $hostId)->where('MasterOrPlayer', \Config::get('vyoma-constants.playerType'))->paginate(ApiConstants::APP_PAGINATION);
    }

    public function updateTokens($hostObject, $updateData)
    {
        
        return DB::table('Host')->where('HostId', $hostObject->HostId)->update($updateData);
    }

    /* return the LocalIpAddress of Host players */
    public function getLocalIp($host)
    {
        $hostMasterDetails = Host::where('HostId', $host->HostMasterId)->first();
        return $hostMasterDetails;
    }

    public function validateStateCodeInState($statecode)
    {
        $status = \Config::get('vyoma-constants.status');
        if (State::where('StateCode', $statecode)->where('Status', $status['Active'])->exists()) {
            return true;
        }
        return false;
    }

    public function validateStateCode($statecode)
    {
        $state = State::select('StateId')->where('StateCode', $statecode)->first();
        return $state->StateId;
    }

    public function getMasterHostObjForPlayer($hostId)
    {
        $hostObj = Host::where('HostId', $hostId)->first();
        $masterHostId = $hostObj->HostMasterId;
        return $this->getHostObj($masterHostId);
    }

    public function getHostObj($hostId)
    {
        return Host::where('HostId', $hostId)->first();
    }


    public function getHostDetailsWithBay($hostId)
    {
        return Host::where('Host.HostId', $hostId)
                ->join('Bay', 'Bay.BayId' ,'=', 'Host.BayId')
                ->join('Station', 'Bay.StationId' ,'=', 'Station.StationId')
                ->first();
    }

    public function hostMasterExists($bayId)
    {
        return Host::where('BayId', $bayId)->Where(function($query)
            {
                $query->where('MasterOrPlayer', '=', \Config::get('vyoma-constants.masterType'))
                      ->orWhere('MasterOrPlayer', '=', \Config::get('vyoma-constants.bothType'));
            })->exists();       
    }
}
