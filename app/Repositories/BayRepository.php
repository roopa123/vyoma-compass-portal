<?php
namespace App\Repositories;

use App\Models\Branch;
use App\Models\Region;
use App\Models\Station;
use App\Models\State;
use App\Models\User;
use App\Models\Bay;
use Carbon\Carbon;
use DB;

class BayRepository extends HostRepository
{
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
            'StationName' => 'required',
            'BayLocationDesc' => 'required',
            'App' => 'required',
            'StartTime' => 'required',
            'EndTIme' => 'required'
        ];

    /*
    * Insert data to DB table
    */
    public function insertData($inputData)
    {
        $station_name = $inputData['StationName'];
        $baylocation_name = $inputData['BayLocationDesc'];
        $app_name = $inputData['App'];
        $starttime = $inputData['StartTime'];
        $endtime = $inputData['EndTIme'];
        $etime = $this->timeValidation($endtime, $starttime);
        $detailsBay = Bay::select('Bay.App')
                    ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                    ->where('Bay.BayLocationCode', $baylocation_name)
                    ->where('Bay.StationId', $station_name)
                    ->where('Bay.App', $inputData['App'])->first();
        if ($detailsBay==null) {
            $dataToInsert = array(
            'StationId' => $station_name,
            'BayLocationCode' => $baylocation_name,
            'App' => $app_name,
            'StartTime' => $starttime,
            'EndTIme' => $etime,
            'Status' => $inputData['Bay_Status']
            );
            $inserted = DB::table('Bay')->insertGetId($dataToInsert);
            return $inserted;
        } else {
            $error = 'Duplicate Entry';
            throw new \Exception($error);
        }
    }

    /*
    * Getting bay list
    */
    public function baylist()
    {
        $list = Bay::where('Status', 'A')->lists('BayId');
        return $list;
    }

    /*
    * Station Info
    */
    public function getStationDetail($bayId)
    {
        $details = Bay::select('Station.StationName')
            ->where('Bay.BayId', $bayId)
            ->join('Station', 'Bay.StationId', '=', 'Station.StationId')->first();

        return $details->toArray();
    }

    /*
    * Update Bay details with id
    */
    public function updateBayData($id)
    {
        $selectBayApp = Bay::select(
            'App',
            'StationId',
            'BayLocationCode',
            'Status',
            'StartTime',
            'EndTIme'
        )
        ->where('StationId', \Input::get('StationName'))
        ->where('BayLocationCode', \Input::get('BayLocationDesc'))
        ->where('App', \Input::get('App'))
        ->where('StartTime', \Input::get('StartTime'))
        ->where('EndTIme', \Input::get('EndTIme'))
        ->first();
        if ($selectBayApp==null) {
            $starttime = \Input::get('StartTime');
            $endtime =\Input::get('EndTIme');
            $etime = $this->timeValidation($endtime, $starttime);
            DB::table('Bay')->where('BayId', $id)
            ->update(array('StationId' => \Input::get('StationName'),
                            'BayLocationCode' => \Input::get('BayLocationDesc'),
                           'App' => \Input::get('App'),
                            'Status' => \Input::get('Bay_Status'),
                            'StartTime' => $starttime,
                            'EndTIme' => $etime));
        } else if ($selectBayApp['Status'] != \Input::get('Bay_Status')) {
             DB::table('Bay')->where('BayId', $id)
            ->update(array('Status' => \Input::get('Bay_Status')));
        } else {
             $error = 'Duplicate Entry';
            throw new \Exception($error);
        }
    }

    /*
    * Autocomplete search for bay
    */

    public function autocompleteSearch($searchterm)
    {
        $term = $searchterm;
        $colValue = \Request::segment(3);
        $results = array();
        $queries = DB::table('Bay')
                    ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
                    ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode');
        switch ($colValue) {
            case 'StationName':
                $queries->where('Station.StationName', 'LIKE', $term.'%');
                break;
            case 'BayLocationDesc':
                $queries->where('BayLocation.BayLocationDesc', 'LIKE', $term.'%');
                break;
            case 'App':
                $queries->where('Bay.App', 'LIKE', $term.'%');
                break;
        }
        $queries->groupBy(\Request::segment(3));
        $res = $queries->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();
        foreach ($res as $query) {
            if (\Request::segment(3) == 'StationName') {
                $results[] = [ 'id' => $query->BayId, 'value' => $query->StationName];
            } else if (\Request::segment(3) == 'BayLocationDesc') {
                $results[] = [ 'id' => $query->BayId, 'value' => $query->BayLocationDesc];
            } else if (\Request::segment(3) == 'App') {
                $results[] = [ 'id' => $query->BayId, 'value' => $query->App];
            }
        }
        return \Response::json($results);
    }

    /*
    * Delete Bay details with id
    */
    public function deleteBay($id)
    {
        $result = DB::table('Bay')
            ->join('Host', 'Bay.BayId', '=', 'Host.HostId')
            ->where('Bay.BayId', $id)
            ->update(array( 'Bay.Status' => 'D',  'Host.Status' => 'D'));
        if ($result) {
            return true;
        } else {
            $result = DB::table('Bay')
            ->where('Bay.BayId', $id)
            ->update(array('Bay.Status' => 'D'));
            if ($result) {
                return true;
            }
        }
    }

     /**
    * Return Bayobject which holds information of station,baylocation Code
    * @param  INT  $bayId
    * @return OBJECT
    */
    public function getBayObject($bayId)
    {
        $status = \Config::get('vyoma-constants.status');
        return DB::table('Bay')
            ->join('Station', 'Bay.StationId', '=', 'Station.StationId')
            ->join('BayLocation', 'Bay.BayLocationCode', '=', 'BayLocation.BayLocationCode')
            ->where('Bay.BayId', $bayId)
            ->where('Bay.Status', $status['Active'])
            ->first();
    }
}
