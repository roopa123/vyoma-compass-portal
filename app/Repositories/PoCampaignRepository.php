<?php
namespace App\Repositories;

use App\Models\PurchaseOrderCampaign;
use DB;
use App\Repositories\UserRepository;
use App\Services\Constants\AppConstants;

class PoCampaignRepository extends BaseRepository
{
    private $purchaseOrderCampaign;

    private $userRepository;

    public function __construct(
        PurchaseOrderCampaign $purchaseOrderCampaign,
        UserRepository $userRepository
    ) {
        $this->purchaseOrderCampaign = $purchaseOrderCampaign;
        $this->userRepository = $userRepository;
    }

    /**
    * Create PurchaseOrder Campaign
    * @param Array $POElements
    * @return object $this->purchaseOrderCampaign
    */
    public function createPOCampaign($POElements)
    {
        $this->purchaseOrderCampaign->OriginalPOId = $POElements['OriginalPOId'];
        $this->purchaseOrderCampaign->OriginalCampaignId = $POElements['OriginalCampaignId'];
        $this->purchaseOrderCampaign->POId = $POElements['POId'];
        $this->purchaseOrderCampaign->CampaignId = $POElements['CampaignId'];
        $this->purchaseOrderCampaign->CreativeCampaignId = $POElements['CreativeCampaignId'];
        $this->purchaseOrderCampaign->LocCampaignId = $POElements['LocCampaignId'];
        $this->purchaseOrderCampaign->Status = $POElements['Status'];
        $this->purchaseOrderCampaign->UserId = $this->userRepository->getCurrentUserLoginId();
        $this->purchaseOrderCampaign->save();
        return $this->purchaseOrderCampaign;
    }

    /**
    * Create POCampagin and update status
    * @param Array $POCampaignParameter
    * @param INT $POCampaignId
    * @return Object $POCampaignObject
    */
    public function createPOCampaignAndUpdateStatus($POCampaignParameter, $POCampaignId)
    {
        $POCampaignObject = $this->createPOCampaign($POCampaignParameter);
        $this->updateStatus($POCampaignId, self::PURCHASE_ORDER_HISTORY);
        return $POCampaignObject;
    }

    /**
    * Create Multiple POCampagin in a single Time
    * @param Array $POElements
    * @param INT $POCampaignId
    * @return Object $POCampaignObject
    */
    public function createMultiplePOCampaignAndUpdateStatus($POElements, $POCampaignId)
    {
        $POCampaignObj = $this->purchaseOrderCampaign->newInstance();
        $POCampaignObj->OriginalPOId = $POElements['OriginalPOId'];
        $POCampaignObj->OriginalCampaignId = $POElements['OriginalCampaignId'];
        $POCampaignObj->POId = $POElements['POId'];
        $POCampaignObj->CampaignId = $POElements['CampaignId'];
        $POCampaignObj->CreativeCampaignId = $POElements['CreativeCampaignId'];
        $POCampaignObj->LocCampaignId = $POElements['LocCampaignId'];
        $POCampaignObj->Status = $POElements['Status'];
        $POCampaignObj->UserId = $this->userRepository->getCurrentUserLoginId();
        $POCampaignObj->save();
        $this->updateStatus($POCampaignId, self::PURCHASE_ORDER_HISTORY);
        return $POCampaignObj;
    }

    /**
    * To Delete POCampaign
    * @param Object $POCampaignObj
    * @return Object
    */
    public function deletePOCampaign($POCampaignObj)
    {
        $status = self::PURCHASE_ORDER_DELETE;
        foreach ($POCampaignObj as $POCampaign) {
        }
        $POId = $POCampaign->POId;
        $POCampaignParameterArray = $this->getCreateParameter($POCampaign, $POId, $status);
        return $this->createPOCampaignAndUpdateStatus(
            $POCampaignParameterArray,
            $POCampaign->POCampaignId
        );
    }

    /**
    * Create POCampagin with NewPOId and Update Status
    * @param INT $originalPOId
    * @param INT $POId
    * @param INT $campaignId
    * @return Object PurchaseOrderCampaign
    */
    public function getPOCampaignsForUpdatingPOId($originalPOId, $newPOId, $campaignId)
    {
        return PurchaseOrderCampaign::where('originalPOId', $originalPOId)
        ->where('POId', '!=', $newPOId)
        ->where('CampaignId', '!=', $campaignId)
        ->StatusIn([self::PURCHASE_ORDER_PARTIAL_STATUS,self::PURCHASE_ORDER_ACTIVE])
        ->get();
    }

    /**
    * To Create POCampaign For New POId when we change in PO Form
    * @param Object $POCampaign [Holds Multiple POCampaign Objects]
    * @param INT $POId
    */
    public function createPOCampaignForNewPOId($POCampaignObj, $POId)
    {
        $status = self::PURCHASE_ORDER_PARTIAL_STATUS;
        foreach ($POCampaignObj as $POCampaign) {
                $POCampaignParameterArray = $this->getCreateParameter($POCampaign, $POId, $status);
                $this->createMultiplePOCampaignAndUpdateStatus(
                    $POCampaignParameterArray,
                    $POCampaign->POCampaignId
                );
        }
    }

    /**
    * To get Parameter for Creating POCampaign
    * @param Object $POCampaign
    * @param INT $POId
    * @param CHAR $status
    * @return Array
    */
    public function getCreateParameter($POCampaign, $POId, $status)
    {
        $originalPOId = $POCampaign->OriginalPOId;
        $originalCampaignId = $POCampaign->OriginalCampaignId;
        $campaignId = $POCampaign->CampaignId;
        $locationCampaignId = $POCampaign->LocCampaignId;
        $creativeCampaignId = $POCampaign->CreativeCampaignId;
        $POStatus = $status;
        return $this->getPOCampaignParameterArray(
            $originalPOId,
            $originalCampaignId,
            $POId,
            $campaignId,
            $creativeCampaignId,
            $locationCampaignId,
            $POStatus
        );
    }

    /**
    * Return Parameter for Creating POCampaign
    * @param INT $originalPOId
    * @param INT $originalCampaignId
    * @param INT $POId
    * @param INT $campaignId
    * @param INT $creativeCampaignId
    * @param INT $locationCampaignId
    * @param CHAR $POStatus
    * @return Array
    */
    public function getPOCampaignParameterArray(
        $originalPOId,
        $originalCampaignId,
        $POId,
        $campaignId,
        $creativeCampaignId,
        $locationCampaignId,
        $POStatus
    ) {
        return array('OriginalPOId' => $originalPOId,
                     'OriginalCampaignId' => $originalCampaignId,
                     'POId' => $POId,
                     'CampaignId' => $campaignId,
                     'CreativeCampaignId' => $creativeCampaignId,
                     'LocCampaignId' => $locationCampaignId,
                     'Status' => $POStatus
                    );
    }

    /**
    * To Get History of PO
    * @param INT $OriginalPOId
    * @return Object POCampaign
    */
    public function getPOCampaignHistory($OriginalPOId)
    {
        return PurchaseOrderCampaign::where('OriginalPOId', $OriginalPOId)
                ->select(
                    'OriginalPOId',
                    'CreatedAt',
                    'UpdatedAt',
                    'POCampaignId',
                    'CampaignId',
                    'LocCampaignId',
                    'CreativeCampaignId',
                    'OriginalCampaignId',
                    'POId',
                    'Status'
                )->orderBy('POCampaignId', 'DESC')->get();
    }

    /**
    * Update POCampaign Status
    * @param INT $POCampaignId
    * @param CHAR $status
    * @return INT Affected Rows
    */
    public function updateStatus($POCampaignId, $status)
    {
        return PurchaseOrderCampaign::where('POCampaignId', $POCampaignId)
                                      ->update(['Status' => $status]);
    }

    /**
    * To Get POCampaign Object
    * @param INT $POId
    * @return Object POCampaign
    */
    public function getPOCampaign($POCampaignId)
    {
        return PurchaseOrderCampaign::where('POCampaignId', $POCampaignId)->get();
    }

    /**
    * To Get POCampaign details for Particular PO
    * @param INT $POId
    * @return Object POCampaign
    */
    public function getPOCampaignByPO($POId)
    {
        return PurchaseOrderCampaign::orderBy('POCampaignId', 'desc')->where('POId', $POId)->first();
    }

    /**
    * To Get POCampaign details for Particular Campaign
    * @param INT $campaignId
    * @return Object POCampaign
    */
    public function getPOCampaignByCampaign($campaignId)
    {
        return PurchaseOrderCampaign::where('CampaignId', $campaignId)
               ->StatusIn([self::PURCHASE_ORDER_PARTIAL_STATUS,self::PURCHASE_ORDER_ACTIVE])
               ->get();
    }

    /**
    * To Get Campaign Count for OriginalPO
    * @param INT $originalPOId
    * @return INT count of campaigns
    */
    public function getCampaignCount($originalPOId)
    {
        return PurchaseOrderCampaign::where('OriginalPOId', $originalPOId)
               ->StatusIn([self::PURCHASE_ORDER_PARTIAL_STATUS,self::PURCHASE_ORDER_ACTIVE])
               ->count();
    }

    /**
    * To Find POCampaign
    * @param INT $POCampaignId
    * @return Object PurchaseOrderCampaign
    */
    public function findPOCampaign($POCampaignId)
    {
        return PurchaseOrderCampaign::find($POCampaignId);
    }
}
