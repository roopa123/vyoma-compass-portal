<?php
namespace App\Repositories;

use App\Models\Advertiser;
use DB;
use App\Models\User;

class AdvertiserRepository extends BaseRepository
{
     public function __construct(\App\Models\Advertiser $Model)
    {
        $this->Database = DB::table('Advertiser')
                            ->leftJoin('Industry', 'Industry.IndustryId', '=', 'Advertiser.IndustryId');

        $this->visibleColumns = array('Advertiser.AdvertiserId', 'Advertiser.AdvertiserName' ,'Advertiser.Sector',
                                'Advertiser.IsPaying', 'Industry.IndustryName', 'Advertiser.Status');

        $this->orderBy = array(array('AdvertiserName'));
    }
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
            'advertiserName' => 'required|unique:Advertiser',
            'sector' => 'required',
            'ispaying' => 'required',
       ];

    /**
    * Validation for assigning people to project and manager
    */
    public $validationRulesIndustry = [
            'advertiserName' => 'required|unique:Advertiser',
            'sector' => 'required',
            'industry' => 'required',
            'ispaying' => 'required',
       ];

     // Update validation rule
    public function editValidationRules($adv) {
        $validationArray = array('advertiserName' => 'required|unique:Advertiser,advertiserName,'.$adv['advertiserId'].',advertiserId',
            'sector' => 'required',           
            'status' => 'required'
            );

         if($adv['sector'] === 'Private') {
                $industryValidation = array('industry' => 'required');
                return array_merge($validationArray, $industryValidation);
            }
            else {
                return $validationArray;
            }
        

        
    }

    /**
    * Get all the Users, who are mapped with Industry
    */
    public function all($isResponseArray = false)
    {
        $advertisers = Advertiser::with('industry')->where('Status', '!=', self::STATUS_DELETE )
            ->get();
        return (!$isResponseArray)?$advertisers:$advertisers->toArray();
    }

    public function find($advertiserId)
    {
        return Advertiser::find($advertiserId);
    }

    /**
     * Fetch advertiser details
     * @param Boolean $isArray
     * @return Array/Object Creative
     */
    public function findAllAdvertisersByActiveStatus()
    {
        $advertisers = Advertiser::where('Status', self::STATUS_ACTIVE)
            ->OrderBy('AdvertiserName')
            ->lists('AdvertiserName','AdvertiserId');

        return $advertisers;

    }

    /**
     * Fetch advertiser details
     * @param Boolean $isArray
     * @return Array/Object Creative
     */
    public function findAdvertisersByActiveStatus($isArray = false)
    {
        $advertisers = Advertiser::select('AdvertiserId', 'AdvertiserName')
            ->where('Status', self::STATUS_ACTIVE)->get();
        if ($isArray) {
            return $this->creativeArrayType($advertisers);
        }
        return $advertisers;

    }

    private function creativeArrayType($advertisers)
    {
        $advertiserArray[] = 'Select';
        if(is_object($advertisers)) {

            foreach ($advertisers as $advertiser) {
                $advertiserArray[$advertiser->AdvertiserId] = $advertiser->AdvertiserName;
            }
            return $advertiserArray;
        }
        return $advertiserArray;
    }

    /**
    * List of Advertisers based on Advertiser Id
    */
    public function listAdvertisers($advId)
    {
        return Advertiser::where('AdvertiserId',$advId)->lists('AdvertiserName', 'AdvertiserId');
    }

    /**
    * Storing Advertiser
    */
    public function saveAdvertiser($advertiser)
    {
        $industryId = null;

        if($advertiser['sector'] == 'Private'){
            $industryId = $advertiser['industry'];
        }
        $advertiserData = ['AdvertiserName' => $advertiser['advertiserName'],
                        'Sector' => $advertiser['sector'],
                        'IsPaying' => $advertiser['ispaying'],
                        'IndustryId' => $industryId,
                        'Status' => self::STATUS_ACTIVE,
                        'UpdatedBy' => \Auth::user()->id
                        ];
        return Advertiser::create($advertiserData);
    }

    /**
    * Update the advertiser details
    */
    public function updateAdvertiser($id, $advertiserData)
    {
        $industryId=null;

        if($advertiserData['sector'] == 'Private'){
            $industryId = $advertiserData['industry'];
        }
        $advertiser = Advertiser::find($id);
        $advertiser->AdvertiserName = $advertiserData['advertiserName'];
        $advertiser->Sector = $advertiserData['sector'];
        $advertiser->IsPaying = $advertiserData['ispaying'];
        $advertiser->IndustryId = $industryId;
        $advertiser->Status = $advertiserData['status'];
        $advertiser->UpdatedBy = \Auth::user()->id;

        return ($advertiser->save()) ? $advertiser : false;
    }

    /**
    * Mark status as Delete
    */
    public function deleteAdvertiser($id)
    {
       $result = Advertiser::join('Creative', 'Creative.AdvertiserId', '=', 'Advertiser.AdvertiserId')
            ->where('Advertiser.AdvertiserId', $id)->get();

        if($result->isEmpty())
        {
            Advertiser::where('AdvertiserId', $id)
            ->update(array('status' => self::STATUS_DELETE,
                            'UpdatedBy' => \Auth::user()->id));

            return true;
        }
        return false;
    }
    /**
    * To Get Active Advertisers
    */
    public function getActiveAdvertisers($isArray = false)
    {
         $advertisers = Advertiser::select('AdvertiserId', 'AdvertiserName')
            ->orderBy('AdvertiserName', 'ASC')->where('Status', self::STATUS_ACTIVE)->get();
         if($isArray) {
                return $this->convertIntoArray($advertisers);
            }

         return $advertisers;
    }


    /**
     * To Get Active Advertisers
     * @param Object $Advertisers
     * @return Array Advertisers
     */
    private function convertIntoArray($advertisersArray)
    {
        $advertisers[NULL] = 'Select';
        if(is_object($advertisersArray)) {
            foreach($advertisersArray as $advertiser) {
                $advertisers[$advertiser->AdvertiserId] = $advertiser->AdvertiserName;
            }
            return $advertisers;
        }
        return $advertisers;
    }

}