<?php
namespace App\Repositories;


use App\Models\Role;
use App\Models\RolesPermission;
use App\Models\Permission;
use App\Models\User;
use App\Models\UserRole;
use DB;
use Carbon\Carbon;

use App\Repositories\RolePermissionRepository;

class RoleRepository
{
    /**
    * Validation for assigning people to project and manager
    */
    public $validationRules = [
        'RoleName' => 'required|unique:Roles|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]*+$)+/'
      ];


    /**
    * Access all methods and objects in Repository
    */
    public function __construct( PermissionRepository $permissionRepo,
            RolePermissionRepository $rolePermissionRepo)
    {
        $this->permissionRepo = $permissionRepo;
        $this->rolePermissionRepo = $rolePermissionRepo;
    }

    // Update validation rule
    public function editValidationRules($role)
    {
        $rules = array('RoleName' => 'required|regex:/(^[a-zA-Z\s]*-?[-a-zA-Z\s]+$)+/|unique:Roles,RoleName,'.$role.',RoleId');
        return $rules;
    }

    public function findRole($roleId)
    {
        return Role::find($roleId);
    }

    /*
    * Insert row into Region table
    */
    public function saveRole($roleName, $permissions)
    {

        $role = new Role();
        $role->RoleName = $roleName;

        if ($role->save()) {
            //Update Permissions
            $this->rolePermissionRepo->saveRolePermission($role->RoleId, $permissions);

        }
        return $role;
    }

    public function updateRole($RoleId, $roleName, $permissions )
    {
        // Fetch Role
        $role = Role::find($RoleId);
        $role->RoleName = $roleName;

        if($role->save())
        {
            // Delete All the exsiting role permissions
            $this->deleteRolePermissionsByRoleId($role->RoleId);
            // Update with New permissions
            $this->rolePermissionRepo->saveRolePermission($role->RoleId, $permissions);
            return $role;
        }
        return false;
    }

    public function deleteRolePermissionsByRoleId($RoleId)
    {
        // Delete Exsisting Permissions
        return RolesPermission::where('RoleId', $RoleId)->delete();
    }

    public function deleteRole($RoleId)
    {
        // delete all permissions belong with roles
        $this->deleteRolePermissionsByRoleId($RoleId);
        return Role::find($RoleId)->delete();
    }

     /**
    * Mark the Industry status to Delete
    */
    public function roleDelete($id)
    {
        $result = UserRole::where('RoleId', $id)->get();

        if($result->isEmpty())
        {
            Role::find($id)->delete();
            return true;            
        }
        else {
            return false;            
        }


    }
        

    /*
    * Getting Role lists
    */
    public function all()
    {
        return Role::all();
    }


    /*
    * Getting region lists
    */
    public function roleList()
    {
        $list = Role::lists('RoleName', 'RoleId');

        return $list;
    }


    /*
    * Autocomplete search for Region
    */
    public function autocompleteSearch($searchStr)
    {

        $results = array();

        $queries = DB::table('Role')
            ->where('RoleName', 'LIKE', '%'.$searchStr.'%')
            ->take(\Config::get('vyoma-constants.noOfsuggestions'))->get();

        foreach ($queries as $query)
        {
            if(\Request::segment(3) == 'RoleName'){
                $results[] = [ 'id' => $query->RoleId, 'value' => $query->RoleName];
            }
        }
        return \Response::json($results);
    }

}
