<?php
namespace App\Repositories;

Interface Converter
{

   /**
     * Convert a A video into different formate.
     *
     * @param  string  $file
     * @param  Array  $convertInto
     * @return int
     */
    public function convert($id, $file, $convertInto);


     /**
     * Get failed Converters.
     *
     * @return array
     */
    public function failures();
}