<?php
namespace App\Repositories;

use App\Models\CampaignLocation;
use DB;
use App\Repositories\UserRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\PoCampaignRepository;
use App\Services\Constants\AppConstants;

class CampaignLocationRepository extends BaseRepository
{
    private $campaignLocation;

    private $userRepository;

    private $campaignRepository;

    private $poCampaignRepository;

    public function __construct(
        CampaignLocation $campaignLocation,
        UserRepository $userRepository,
        CampaignRepository $campaignRepository,
        PoCampaignRepository $poCampaignRepository
    ) {
        $this->campaignLocation = $campaignLocation;
        $this->userRepository = $userRepository;
        $this->campaignRepository = $campaignRepository;
        $this->poCampaignRepository = $poCampaignRepository;
    }

    /**
    * Create CampaignLocation
    * @param Array $inputData
    * @param INT $campaignId
    * @return BOOL
    */
    public function createCampaignLocation($inputData, $campaignId)
    {
        $location = $inputData['location'];
        if ($this->checkLocationIsNotEmpty($location)) {
            $locationArray = json_decode($location, true);
            $duplicateStationsArray = $this->findDuplicationOfStationInGroup($locationArray);
            foreach ($locationArray as $key => $value) {
                if ($duplicateStationsArray[$value['stationId']][0] == AppConstants::STATION_DUPLICATED) {
                    if ($duplicateStationsArray[$value['stationId']][1] == $value['locationType'])
                    {
                        $locationCampaignId = $this->createLocation(
                        $value['locationType'],
                        $value['stationId'],
                        $campaignId
                        ); 
                    }
                } else {
                  $locationCampaignId = $this->createLocation(
                    $value['locationType'],
                    $value['stationId'],
                    $campaignId
                    );  
                }
            }
            return true;
        }
        return false;
    }

    /* To Find Duplication Of Stations in Different Groups */ 
    public function findDuplicationOfStationInGroup($locationArray)
    {
        $locationTypeArray = array();
        foreach ($locationArray as $key => $value) {
            if (!isset($locationTypeArray[$value['stationId']])) {
                $locationTypeArray[$value['stationId']][0] = AppConstants::STATION_NO_DUPLICATED;
                $locationTypeArray[$value['stationId']][1] = $value['locationType'];
            } else {
                $valueToPush = $locationTypeArray[$value['stationId']][1].",".$value['locationType']; 
                $locationTypeArray[$value['stationId']][1] = $valueToPush;
                $locationTypeArray = $this->assignLocationTypeForDuplicateStations
                                     ($locationTypeArray, $value['stationId']);
                }
        }
        return $locationTypeArray;
    }

    /* To assgin Location Type For Duplication */
    public function assignLocationTypeForDuplicateStations($locationTypeArray, $stationId)
    {
        $valueToPush = "";
        $locationTypes = explode(',', $locationTypeArray[$stationId][1]);
        if (in_array(AppConstants::STATION_GROUP, $locationTypes))
        {
            $valueToPush = AppConstants::STATION_GROUP;  
        } else if (in_array(AppConstants::REGION_GROUP, $locationTypes)) {
            $valueToPush = AppConstants::REGION_GROUP;
        } else if (in_array(AppConstants::STATE_GROUP, $locationTypes)) {
            $valueToPush = AppConstants::STATE_GROUP;
        }
        $locationTypeArray[$stationId][0] = AppConstants::STATION_DUPLICATED;;
        $locationTypeArray[$stationId][1] = $valueToPush;
        return $locationTypeArray;
    }

    /**
    * To Check Location is not empty
    * @param String $location
    * @return Boolean
    */
    public function checkLocationIsNotEmpty($location)
    {
        if (trim($location) !="" && trim($location) != "[]") {
            return true;
        }
        return false;
    }

    /**
    * Create Location
    * @param CHAR $locationType
    * @param INT $stationId
    * @param INT $campaignId
    * @return Object $CampaignLocationObj
    */
    public function createLocation($locationType, $stationId, $campaignId)
    {
        $campaignLocationObj = $this->campaignLocation->newInstance();
        $campaignLocationObj->CampaignId = $campaignId;
        $campaignLocationObj->LocationType = $locationType;
        $campaignLocationObj->LocationId = $stationId;
        $campaignLocationObj->Status = self::PURCHASE_ORDER_ACTIVE;
        $campaignLocationObj->UserId = $this->userRepository->getCurrentUserLoginId();
        $campaignLocationObj->save();
        return $campaignLocationObj;
    }

    /**
    * Check whether Location Form Data Changed
    * @param Array $inputData
    * @return INT LOCATION_FORM_CHANGED:4,NO_CHANGE:1
    */
    public function changesInLocation($inputData)
    {
        $oldLocation = $inputData['oldLocation'];
        $updatedLocation = $inputData['location'];
        $locationDifference = $this->checkLocationDifference($updatedLocation, $oldLocation);
        if ($locationDifference) {
            return AppConstants::LOCATION_FORM_CHANGED;
        }
        return AppConstants::NO_CHANGE;
    }

    /**
    * Compare two arrays and Find Difference
    * @param Json string  $newLocation
    * @param Json string  $oldLocation
    * @return BOOL true/false
    */
    public function checkLocationDifference($newLocation, $oldLocation)
    {
        $differenceArray = array();
        $newLocArray = json_decode($newLocation, true);
        $oldLocArray = json_decode($oldLocation, true);
        if ((empty($oldLocArray) && count($newLocArray) > 0) || (empty($newLocArray) && count(
        $oldLocArray) > 0
        )) {
                return true;
        }
        foreach ($newLocArray as $newData) {
            $duplicate = false;
            foreach ($oldLocArray as $oldData) {
                if ($newData['groupId'] === $oldData['groupId'] && $newData['groupName'] === $oldData['groupName'] && $newData['locationType'] === $oldData['locationType'] && $newData['stationId'] && $oldData['stationId'] && $newData['stationName'] && $oldData['stationName']) {
                    $duplicate = true;
                }
            }
            if ($duplicate === false) {
                $differenceArray[] = $newData;
            }
        }
        if (count($differenceArray) > AppConstants::NO_DIFFERENCE_IN_LOCATION) {
            return true;
        } else if (count($newLocArray) != count($oldLocArray)) {
            return true;
        }
        return false;
    }

    /**
    * Update Location Status
    * @param INT $creativeLocationId
    * @param CHAR $status
    * @return INT Affected Rows
    */
    public function updateStatus($creativeLocationId, $status)
    {
        return CampaignLocation::where('CampaignId', $creativeLocationId)
                                 ->update(['Status' => $status]);
    }

    /**
    * Create Campaign And Location on location Form Change
    * @param Array $inputData
    * @param CHAR $POStatus
    * @return Array $campaignId, $locationCampaignId
    */
    public function createCampaignAndlocation($inputData, $POStatus)
    {
        $locationCampaignId = null;
        $campaignObj = $this->campaignRepository->createCampaign($inputData, $POStatus);
        $campaignId = $campaignObj->CampaignId;
        $locationBoolean = $this->createCampaignlocation($inputData, $campaignId);
        if ($locationBoolean) {
            $locationCampaignId = $campaignId;
        }
        return array($campaignId, $locationCampaignId);
    }

    /**
    * To get Locations
    * @param Int $locCampaignId
    * @return JsonString $locationJson
    */
    public function getLocations($locCampaignId)
    {
        $locationJson = '[]';
        if (!is_null($locCampaignId)) {
            $location = DB::select(DB::raw("SELECT LocationType,GROUP_CONCAT(LocationId SEPARATOR ',') as stationId FROM CampaignLocation where CampaignId = ".$locCampaignId." GROUP By locationType"));
            return $this->getLocationJson($location);
        }
        return $locationJson;
    }

    /**
    * To get Locations as Json
    * @param Array $locationDetails
    * @return JsonString $locationJson
    */
    public function getLocationJson($locationDetails)
    {
        $locationByStation = array();
        $locationByRegion = array();
        $locationByState = array();
        foreach ($locationDetails as $locArrayRs) {
            if ($locArrayRs->LocationType == AppConstants::REGION_GROUP) {
                $locationByRegion = DB::select(DB::raw("select Station.RegionId as groupId, RegionName as groupName,'".AppConstants::REGION_GROUP."' as locationType,StationId as stationId,StationName as stationName  from Station left join Region on Station.RegionId = Region.RegionId where StationId in($locArrayRs->stationId)"));
            }
            if ($locArrayRs->LocationType == AppConstants::STATE_GROUP) {
                $locationByState = DB::select(DB::raw("select Station.StateId as groupId, StateName as groupName,'".AppConstants::STATE_GROUP."' as locationType,StationId as stationId, StationName as stationName from Station left join State on Station.StateId = State.StateId where StationId in($locArrayRs->stationId)"));
            }
            if ($locArrayRs->LocationType == AppConstants::STATION_GROUP) {
                $locationByStation = DB::select(DB::raw("select StationId as groupId, StationName as groupName,'".AppConstants::STATION_GROUP."' as locationType,StationId as stationId, StationName as stationName from Station where StationId in($locArrayRs->stationId)"));
            }
        }
        return $this->convertLocationToJson($locationByRegion, $locationByState, $locationByStation);
    }

    /**
    * To convert Location Array To Json
    * @param Array $locationByRegion
    * @param Array $locationByState
    * @param Array $locationByStation
    * @return JsonString
    */
    public function convertLocationToJson($locationByRegion, $locationByState, $locationByStation)
    {
        $mergingRegionState = array_merge(
            json_decode(json_encode($locationByRegion), true),
            json_decode(json_encode($locationByState), true)
        );
        $merginonRegionStateStation = array_merge(
            $mergingRegionState,
            json_decode(json_encode($locationByStation), true)
        );
        return json_encode($merginonRegionStateStation);
    }
}
