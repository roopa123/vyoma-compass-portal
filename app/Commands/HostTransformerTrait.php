<?php

namespace App\Commands;
 
use App\Services\Constants\ApiConstants;
use App\Models\Host;
use App\Models\Code;
use DB;

trait HostTransformerTrait
{
    /* Result for data = ALL in the query String*/
    public function respondWithAllParams($host, $configResponse)
    {
        $response = [];
        $response[ApiConstants::PARAM_HOST_NAME] = $host->HostName;
        $response[ApiConstants::PARAM_APP_VERSION] = $host->CurrentAppVersion;
        $response[ApiConstants::PARAM_CONF_VERSION] = $host->CurrentConfigVersion;
        $response[ApiConstants::PARAM_IP_ADDRESS] = $host->LocalIPAddress;
        $response[ApiConstants::PARAM_MAC_ADDRESS] = $host->MacAddress;
        list($isRegister, $isOnline) = $this->setHostStatus($host);
        $response[ApiConstants::PARAM_REGISTERED] = $isRegister;
        $response[ApiConstants::PARAM_ONLINE] = $isOnline;
        $hostRole = $this->setHostRole($host);
        $response[ApiConstants::PARAM_ROLE] = $hostRole;
        $response[ApiConstants::PARAM_CONFIG] = $configResponse;
        return $response;
    }

    /* Result for requested params in the query String*/
    public function respondWithRequestedParams($host)
    {
        $response = [];
        $response[ApiConstants::PARAM_HOST_NAME] = $host->HostName;
        if (in_array(ApiConstants::PARAM_APP_VERSION, $this->paramArray)) {
            $response[ApiConstants::PARAM_APP_VERSION] = $host->CurrentAppVersion;
        }
        if (in_array(ApiConstants::PARAM_CONF_VERSION, $this->paramArray)) {
            $response[ApiConstants::PARAM_CONF_VERSION] = $host->CurrentConfigVersion;
        }
        if (in_array(ApiConstants::PARAM_IP_ADDRESS, $this->paramArray)) {
            $response[ApiConstants::PARAM_IP_ADDRESS] = $host->LocalIPAddress;
        }
        if (in_array(ApiConstants::PARAM_MAC_ADDRESS, $this->paramArray)) {
            $response[ApiConstants::PARAM_MAC_ADDRESS] = $host->MacAddress;
        }
        if (in_array(ApiConstants::PARAM_STATUS, $this->paramArray)) {
            list($isRegister, $isOnline) = $this->setHostStatus($host);
            $response[ApiConstants::PARAM_REGISTERED] = $isRegister;
            $response[ApiConstants::PARAM_ONLINE] = $isOnline;
        }
        if (in_array(ApiConstants::PARAM_ROLE, $this->paramArray)) {
            $hostRole = $this->setHostRole($host);
            $response[ApiConstants::PARAM_ROLE] = $hostRole;
        }
        return $response;
    }

    /* Return the Status of the Host */
    public function setHostStatus($host)
    {
        $isRegister = ApiConstants::NO_STRING;
        $isOnline = ApiConstants::NO_STRING;
        if ($host->IsRegistered == ApiConstants::YES_CHAR) {
            $isRegister = ApiConstants::YES_STRING;
        }
        if ($host->IsOnline == ApiConstants::YES_CHAR) {
            $isOnline = ApiConstants::YES_STRING;
        }
        return array($isRegister, $isOnline);
    }

    /* Return the Role of the Host */
    public function setHostRole($host)
    {
        if ($host->MasterOrPlayer == ApiConstants::HOST_MASTER) {
            return ApiConstants::ROLE_MASTER;
        }
        if ($host->MasterOrPlayer == ApiConstants::HOST_PLAYER && $host->ShowTicketData === ApiConstants::YES_CHAR) {
            return ApiConstants::ROLE_PLAYER;
        }
        if ($host->MasterOrPlayer == ApiConstants::HOST_BOTH && $host->ShowTicketData === ApiConstants::YES_CHAR) {
            return ApiConstants::ROLE_BOTH;
        }
        if (($host->MasterOrPlayer == ApiConstants::HOST_BOTH || $host->MasterOrPlayer == ApiConstants::HOST_PLAYER) && $host->ShowTicketData === ApiConstants::NO_CHAR) {
            return ApiConstants::ROLE_PLASMA;
        }
    }

    /* return State Code */
    public function getStateCodeforHost($host)
    {
        $stateCode = $this->getStatusCode($host['DeployStateId']);
        if (!is_null($stateCode)) {
            return "'".$stateCode."'";
        }
        return $stateCode;
    }

    /* return Reboot Timings */
    public function getRebootTimings($host)
    {
        if (is_null($host->ScreenStartTime) ? $rbtHour = $host->ScreenStartTime : $rbtHour = date('H', strtotime($host->ScreenStartTime)));

        if (is_null($host->ScreenStartTime) ? $rbtMin = $host->ScreenStartTime : $rbtMin = date('i', strtotime($host->ScreenStartTime)));

        if (is_null($host->ScreenEndTIme) ? $screenOffHour = $host->ScreenEndTIme : $screenOffHour = date('H', strtotime($host->ScreenEndTIme)));

        if (is_null($host->ScreenEndTIme) ? $screenOffMin = $host->ScreenEndTIme : $screenOffMin = date('i', strtotime($host->ScreenEndTIme)));
        
        return array($rbtHour, $rbtMin, $screenOffHour, $screenOffMin);
    }

    /* Get the MasterIpAddress of the Host players */
    public function setMasterIpAddress($host)
    {
        $masterIp = "";
        if ($host->MasterOrPlayer == ApiConstants::HOST_MASTER) {
            $masterIp = $host->LocalIPAddress;
        } else if ($host->MasterOrPlayer == ApiConstants::HOST_PLAYER) {
            $masterIp = $this->getLocalIp($host);
        } else if ($host->MasterOrPlayer == ApiConstants::HOST_BOTH) {
            $masterIp = $host->LocalIPAddress;
        }
        return $masterIp;
    }

    /* return the LocalIpAddress of Host players */
    public function getLocalIp($host)
    {
        $hostMasterDetails = Host::where('HostId', $host->HostMasterId)->first();

        return $hostMasterDetails->LocalIPAddress;
    }

    /* return the CounterType of Host  */
    public function setCounterType($host)
    {
        if ($host->HostCounterType == ApiConstants::HOST_COUNTER_PRS) {
            return  ApiConstants::COUNTER_PRS;
        }
        if ($host->HostCounterType == ApiConstants::HOST_COUNTER_UTS && $host->IsUTSDumb === ApiConstants::NO_CHAR) {
            return  ApiConstants::COUNTER_UTS;
        }
        if ($host->HostCounterType == ApiConstants::HOST_COUNTER_UTS && $host->IsUTSDumb === ApiConstants::YES_CHAR) {
            return  ApiConstants::COUNTER_UTS_DB;
        }
    }

    /* return the PlayerWinSize of Host  */
    public function getPlayerWinSize($host)
    {
        $playerWinSize = null;
        $codeDetails = Code::where('CodeType', 'PlayerWinSize')->where('Code', $host->PlayerWinSize)->first();
        if ($codeDetails) {
            return $codeDetails['Code'];
        }
        return $playerWinSize;
    }

    /* return RouterRebootSchedule cron entries for config Tokens*/
    public function getRouterRebootSchedule($hostObject)
    {
        list($rbtMin, $rbtHour) = $this->getHourMinuteFromCron($hostObject->RouterRebootSchedule);
        return array($rbtMin, $rbtHour);
    }


    /* return Playlogschedule cron entries for config Tokens*/
    public function getPlayLogSchedule($hostObject)
    {
        $logPushMin = "";
        $logPushHour = "";
        if (!is_null($hostObject->PlayLogUpdateSchedule) && trim($hostObject->PlayLogUpdateSchedule!="")) {
            list($logPushMin, $logPushHour) = $this->getHourMinuteFromCron($hostObject->PlayLogUpdateSchedule);
        }
        return array($logPushMin, $logPushHour);
    }
    
    /* return Contentschedule cron entries for config Tokens*/
    public function getContentSchedule($hostObject)
    {
        $updateSchedule = "";

        if (!is_null($hostObject->ContentUpdateSchedule) && trim($hostObject->ContentUpdateSchedule!="")) {
            $updateScheduleCronArray = explode(" ", $hostObject->ContentUpdateSchedule);
            if (!empty($updateScheduleCronArray)) {
                $updateSchedule = $updateScheduleCronArray[0];
            }
        }
        return $updateSchedule;
    }

    /* return Ticketschedule cron entries for config Tokens*/
    public function getTicketSchedule($hostObject)
    {
        $uploadCSVSchedule = "";
        if (!is_null($hostObject->TicketLogUpdateSchedule) && trim($hostObject->TicketLogUpdateSchedule!="")) {
            $codeDetails = Code::where('CodeType', 'TicketLogUpdateSchedule')->where('Code', $hostObject->TicketLogUpdateSchedule)->first();
            if ($codeDetails) {
                $csvScheduleCronArray = explode(" ", $codeDetails->Code);
                $uploadCSVSchedule = $csvScheduleCronArray[0];
            }
        }
        return $uploadCSVSchedule;
    }

    /* return Screenshotschedule for config Tokens*/
    public function getScreenShotSchedule($hostObject)
    {
        $screenShotMin = "";
        $screenShotHour = "";
        if (!is_null($hostObject->ScreenShotSchedule) && trim($hostObject->ScreenShotSchedule!="")) {
            // screenShot = 1 Enable, 0 Disable
            list($screenShotMin, $screenShotHour) = $this->getHourMinuteFromCron($hostObject->ScreenShotSchedule);
        }
        return array($screenShotMin, $screenShotHour);
    }

    /* return Hour, Minute from Cron Data*/
    public function getHourMinuteFromCron($cronData)
    {
        $cronMin = "";
        $cronHour = "";
        $transformedCronData = explode(" ", $cronData);
        if (!empty($transformedCronData)) {
            $cronMin = $transformedCronData[0];
            $cronHour = $transformedCronData[1];
        }
        return array($cronMin, $cronHour);
    }

    /* Get State Code */
    public function getStatusCode($deployId)
    {
        $stateCode = null;
        if (!empty($deployId)) {
            $stateCode = DB::table('Host')
                ->select('State.StateCode')
                ->join('State', 'Host.DeployStateId', '=', 'State.StateId')
                ->where('Host.DeployStateId', $deployId)->first();
                
            return $stateCode->StateCode;
        }
        return $stateCode;
    }

    /* Get Code Values from the Code Table for the specific CodeType*/
    public function getCodeValue($CodeType)
    {
        $codeObj = Code::where('CodeType', $CodeType)->first();
        return $codeObj->Code;
    }
}
