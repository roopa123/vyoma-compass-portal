<?php

namespace App\validator;

use Illuminate\Validation\Validator as IlluminateValidator;
use App\Repositories\HostRepository;


class HostVmcValidator extends IlluminateValidator
{ 
	private $_custom_messages = array(
	'check_counter_start_time' => 'Basic The Counter :attribute should < than Endtime',
    'check_counter_end_time' => 'Basic The Counter :attribute should > than StartTime',
    'bay_start_time' => "Basic The Counter :attribute should be > than Bay's StartTime",
    'bay_end_time' => "Basic The Counter :attribute should be < than Bay's Endtime",
    'master_exists' => "Basic Master already Exists!",
    'check_player' => "Cannot create Players until create Master/Both",
    'master_both_validation' => 'validation for master player'
    );

    private $hostRepo;

    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array(), HostRepository $hostRepo) {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes,  $hostRepo);

        $this->_set_custom_stuff();
        $this->hostRepo = $hostRepo;
    }

    /**
     * Setup any customizations etc
     *
     * @return void
     */
    protected function _set_custom_stuff() {
        //setup our custom error messages
        $this->setCustomMessages( $this->_custom_messages );
    }

    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateCheckCounterEndTime($attribute, $value, $parameters) {
        $end_time = $this->getValue($parameters[0]); // get the value of the parameter (start_time)    
        return (strtotime($value) >= strtotime($end_time));
    }

    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateCheckCounterStartTime($attribute, $value, $parameters) {
        $start_time = $this->getValue($parameters[0]); 
        return (strtotime($value) <= strtotime($start_time));
    }

    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateBayStartTime($attribute, $value, $parameters) {
        $baystart_time = $this->getValue($parameters[0]); 
        return (strtotime($value) >= strtotime($baystart_time));
    }

    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateBayEndTime($attribute, $value, $parameters) {
        $bayEnd_time = $this->getValue($parameters[0]); 
        return (strtotime($value) <= strtotime($bayEnd_time));
    }

    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateMasterExists($attribute, $value, $parameters) {
        $bayId = $this->getValue($parameters[0]); 
        if($value == "M"){
	        $master = $this->hostRepo->hostMasterExists($bayId);
	        if ($master) {        	
	        	return false;
	        }
	    }
        return true;        
    }

    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateCheckPlayer($attribute, $value, $parameters) {
        $bayId = $this->getValue($parameters[0]); 
        if($value == "P"){
        	$master = $this->hostRepo->hostMasterExists($bayId);
	        if ($master) {        	
	        	return true;
	        }
        }        
        return false;        
    }

    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateMasterBothValidation($attribute, $value, $parameters)
    {
                
    }
}