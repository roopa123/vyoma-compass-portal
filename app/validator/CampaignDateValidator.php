<?php

namespace App\validator;

use Illuminate\Validation\Validator as IlluminateValidator;
use App\Models\Campaign;
use App\Models\PurchaseOrderCampaign;
use App\Services\Constants\AppConstants;
use DB;

class CampaignDateValidator extends IlluminateValidator
 {
    private $_custom_messages = array(
        'start_date_lesser_equal' => 'Campaign The :attribute should accept greater or equals of Po Start Date',
        'end_date_lesser_equal' => 'Campaign The :attribute should accept lesser or equals of Po End Date',
        'end_date' => 'Campaign The :attribute should not be less than of Campaign Start Date',
        'po_date' => 'PO The :attribute should not be less than PO Date',
        'po_start_date' => 'PO The :attribute should not be less than of PO Start Date',
        'video_size' => 'The :attribute size limit is exceeded',
        'mime_type' => 'Supports only Video .mp4 format',
        'edit_campaign_name' => 'Campaign Name should be unique',
        'add_campaign_name' => 'Campaign Name should be unique',
        );

    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes );

        $this->_set_custom_stuff();
    }

    /**
     * Setup any customizations etc
     *
     * @return void
     */
    protected function _set_custom_stuff() {
        //setup our custom error messages
        $this->setCustomMessages( $this->_custom_messages );
    }
    /**
     * Validate that a date is before another one
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  array   $parameters
     * @return bool
     */
    public function validateStartDateLesserEqual($attribute, $value, $parameters) {
        $start_date = $this->getValue($parameters[0]); // get the value of the parameter (start_date)        
        return (strtotime($value) >= strtotime($start_date));
    }//validate_before_date
 

    public function validateEndDateLesserEqual($attribute, $value, $parameters) {
        $end_date = $this->getValue($parameters[0]); // get the value of the parameter (start_date)
        //dd($start_date);
        return (strtotime($value) <= strtotime($end_date));
    }//validate_before_date

    public function validateEndDate($attribute, $value, $parameters) {
        $start_date = $this->getValue($parameters[0]); // get the value of the parameter (start_date)        
        return (strtotime($value) >= strtotime($start_date));
    }//validate_before_date

    public function validatePoDate($attribute, $value, $parameters) {
        $po_date = $this->getValue($parameters[0]); // get the value of the parameter (start_date)        
        return (strtotime($value) >= strtotime($po_date));
    }//validate_before_date

     public function validatePoStartDate($attribute, $value, $parameters) {
        $po_start_date = $this->getValue($parameters[0]); // get the value of the parameter (start_date)        
        return (strtotime($value) >= strtotime($po_start_date));
    }//validate_before_date

    protected function validateVideoSize() {
        //converting mb to bytes
        //dd($this->files['file']);
        return $this->files['file']->getClientSize() <= '200000000';
    }

    protected function validateMimeType( ) {
        $mimeArray = array("video/mp4");
        /*$mimeArray = array("video/x-flv", "video/mp4", "video/mpeg", "video/3gpp", "video/x-msvideo", "video/quicktime", "video/x-ms-wmv","application/octet-stream", "video/avi");*/
        return  (in_array($this->files['file']->getClientMimeType(), $mimeArray));
    }

     public function validateEditCampaignName($attribute, $value, $parameters) 
    {
         $campaignName = $value;
         $originalPOId = $parameters[1];
         $campaignId = $parameters[2];
         return $this->editCampaignNameValidation($campaignName, $originalPOId, $campaignId);
    }

    public function validateAddCampaignName($attribute, $value, $parameters) 
    {
         $campaignName = $value;
         $originalPOId = $parameters[1];
         return $this->addCampaignNameValidation($campaignName, $originalPOId);
    }

    public function editCampaignNameValidation($campaignName, $originalPOId, $campaignId)
    {
        $campaignArray = DB::select(DB::raw("Select GROUP_CONCAT(CampaignId SEPARATOR 
                                            ',') as campaignId from POCampaign where OriginalPOId = 
                                            $originalPOId and CampaignId != $campaignId and Status in ('".AppConstants::PURCHASE_ORDER_ACTIVE."','".AppConstants::PURCHASE_ORDER_PARTIAL_STATUS."')"));
        if (!is_null($campaignArray[0]->campaignId))
        {
            return $this->checkCampaignNameExists($campaignName, $campaignArray);
        }
        return true;
    }

    public function addCampaignNameValidation($campaignName, $originalPOId)
    {
        $campaignArray = DB::select(DB::raw("Select GROUP_CONCAT(CampaignId SEPARATOR 
                                            ',') as campaignId from POCampaign where OriginalPOId = 
                                            $originalPOId and Status in ('".AppConstants::PURCHASE_ORDER_ACTIVE."','".AppConstants::PURCHASE_ORDER_PARTIAL_STATUS."')"));
        if (!is_null($campaignArray[0]->campaignId))
        {
            return $this->checkCampaignNameExists($campaignName, $campaignArray);
        }
       
        return true;
         
    }

    public function checkCampaignNameExists($campaignName, $campaignArray)
    {
       
        $campaignId = $campaignArray[0]->campaignId;
        $campaignNamesObj = Campaign::whereIn('CampaignId', explode(',',$campaignId))
                            ->select('CampaignName')->get();
        $campaignNamesArray = $this->convertIntoArray($campaignNamesObj);
        if (in_array($campaignName, $campaignNamesArray)) 
        {
            return false;
        }
        return true;
    }

    private function convertIntoArray($campaignNamesObj)
    {   
        $campaignNames = array();    
        if(is_object($campaignNamesObj)) {
            $i=0;
            foreach($campaignNamesObj as $campaign) {
                $campaignNames[$i] = $campaign->CampaignName;$i++;
            }
        }
        return $campaignNames;
    }
   
    
 
}//end class