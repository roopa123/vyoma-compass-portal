<?php

namespace App\validator;

use Illuminate\Validation\Validator as IlluminateValidator;

class creativeVideoValidator extends IlluminateValidator
{
    private $_custom_messages = array(
        'video_size' => 'The :attribute size limit is exceeded',
        'mime_type' => 'Supports only Video .mp4 format'
        );

    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes );

        $this->_set_custom_stuff();
    }

    /**
     * Setup any customizations etc
     *
     * @return void
     */
    protected function _set_custom_stuff() {
        //setup our custom error messages
        $this->setCustomMessages( $this->_custom_messages );
    }

    protected function validateVideoSize() {
        //converting mb to bytes
        dd($this->files['file']);
        return $this->files['file']->getClientSize() <= '200000000';
    }

    protected function validateMimeType( ) {
        $mimeArray = array("video/mp4");
        /*$mimeArray = array("video/x-flv", "video/mp4", "video/mpeg", "video/3gpp", "video/x-msvideo", "video/quicktime", "video/x-ms-wmv","application/octet-stream", "video/avi");*/
        return  (in_array($this->files['file']->getClientMimeType(), $mimeArray));
    }
}
