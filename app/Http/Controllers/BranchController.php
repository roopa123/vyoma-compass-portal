<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\BranchRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use DB;
use Gate;

class BranchController extends Controller
{
    /**
    * Instances of Admin Repository
    */
    protected $branchRepo;

    /**
    * Access all methods and objects in Repository
    */

    public function __construct(BranchRepositories $branchRepo)
    {

        $this->branchRepo = $branchRepo;
    }

    /*
     Returning view
    */
    public function getIndex()
    {
        //$listOfBranches = $this->branchRepo->branchlist();
        $noOfBranches = $this->branchRepo->branchCount();
        $noOfRegions = $this->branchRepo->regionCount();
        $noOfStations = $this->branchRepo->stationCount();
        $noOfBays = $this->branchRepo->bayCount();
        $noOfHosts = $this->branchRepo->hostCount();

        return view('branch.branch', compact('noOfBranches', 'noOfRegions',
            'noOfStations', 'noOfBays', 'noOfHosts'));
    }

    /*
     Save the Branch details
    */
    public function storeBranch()
    {
        if(Gate::denies('Create Branch')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $validator = \Validator::make(request()->all(), $this->branchRepo->validationRules, ['BranchName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422),422);
        }
        try {
            $result = $this->branchRepo->insertData(\Input::all());
            $getBranch = $this->branchRepo->getBranchDetails($result);

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.branch')." ".$getBranch[0]['BranchName']." ".\Lang::get('vyoma-messages.createSuccess')));
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        
    }

    /*
     Update Branch details based on id
    */
    public function updateBranch($branchId)
    {
        if(Gate::denies('Update Branch')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        
        $validator = \Validator::make(request()->all(), $this->branchRepo->editValidationRules(\Input::all()), ['BranchName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422),422);
        }
        try {
            $this->branchRepo->updateBranchDetails($branchId, \Input::all());
            $getBranch = $this->branchRepo->getBranchDetails($branchId);

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.branch')." ".$getBranch[0]['BranchName']." ".\Lang::get('vyoma-messages.updateSuccess'), 'redirect' => "false"));
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'redirect' => "false"));
        }        
    }

    /*
     Delete Branch details based on id
    */
    public function deleteBranch($branchId)
    {
        try {
            $this->branchRepo->deleteBranch($branchId);
            $getBranch = $this->branchRepo->getBranchDetails($branchId);

            return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.branch')." ".$getBranch[0]['BranchName']." ".\Lang::get('vyoma-messages.deleteSuccess')));
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }        
    }

    public function getCount()
    {
        $noOfBranches = $this->branchRepo->branchCount();

        return $noOfBranches;
    }

    public function autocomplete(){

        $searchValues = $this->branchRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }
}
