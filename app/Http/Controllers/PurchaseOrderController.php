<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PurchaseOrderRepository;
use App\Repositories\PoCampaignRepository;
use App\Repositories\UserRepository;
use App\Repositories\BranchRepositories;
use App\Repositories\AdvertiserRepository;
use App\Services\Constants\HttpStatusCodes;
use App\Services\Constants\AppConstants;
use App\Models\PurchaseOrder;

class PurchaseOrderController extends Controller
{

    protected $purchaseOrderRepository;

    protected $poCampaignRepository;

    protected $branchRepository;

    protected $advertiserRepository;

    protected $userRepository;

    /**
     * Create a PO controller instance.
     * @param  PurchaseOrderRepository  $purchaseOrderRepository
     * @param  PoCampaignRepository  $poCampaignRepository
     * @param  BranchRepositories  $branchRepository
     * @param  AdvertiserRepository  $advertiserRepository
     * @param  UserRepository  $userRepository
     * @return void
     */

    public function __construct(PurchaseOrderRepository $purchaseOrderRepository,
        PoCampaignRepository $poCampaignRepository,
        BranchRepositories $branchRepository,
        AdvertiserRepository $advertiserRepository,
        UserRepository $userRepository)
    {
        $this->purchaseOrderRepository = $purchaseOrderRepository;
        $this->poCampaignRepository = $poCampaignRepository;
        $this->branchRepository = $branchRepository;
        $this->advertiserRepository = $advertiserRepository;
        $this->userRepository = $userRepository;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('purchaseOrder.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     **/
    public function show($id)
    {
        $this->checkUserAuth();
        try {
            $poCampaign = $this->poCampaignRepository->getPOCampaignByPO($id);
            // Purchase Order History
            $poHistory = $this->purchaseOrderRepository->getPOHistory($poCampaign->OriginalPOId);
            return view('purchaseOrder.po-preview', compact('poHistory'));
        } catch (\Exception $e) {
            return view('purchaseOrder.error', compact('e'));
        }
    }


    /**
    * Permissions for PO based on Users
    */
    public function checkUserAuth()
    {
       if (Gate::denies('Show PO')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/";
            return view('403', compact('message', 'route'));
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        list($activeAdvertiser, $salesRep, $branchList) = $this->dropDownFormElements();
        return view('purchaseOrder.po-addnew', compact('activeAdvertiser', 'salesRep', 'branchList'));
    }


    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
                $formData = $this->convertJSONToArray($request->get('sendData'));
                $validator = $this->purchaseOrderRepository->validateForms($formData);
                if ($validator->fails())
                {
                    \DB::rollback();
                    return $this->validationFail($validator);
                }
                $poCampaignObj = $this->purchaseOrderRepository->createPurchaseOrder($formData);
                \DB::commit();
                $this->flashMessage('Purchase Order Added Successfully !');
                return $this->POSuccessResponse($poCampaignObj);
            } catch (\Exception $e) {
            \DB::rollback();
            return $this->validationException($e->getMessage());
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     * @param int $POCampaignId
     * @param  int  $campaignId
     * @return \Illuminate\Http\Response
     */
    public function poEdit($POCampaignId, $campaignId)
    {
        /* DropDown Form Elements*/
        list($activeAdvertiser, $salesRep, $branchList) = $this->dropDownFormElements();
        $POCampaignObj = $this->poCampaignRepository->getPOCampaignByCampaign($campaignId);
        list($POObj, $campaignObj, $creativeDetails, $locationDetailsJson) = 
                $this->purchaseOrderRepository->getPOElements($POCampaignObj,$campaignId);
        return view('purchaseOrder.po-edit', compact('activeAdvertiser', 'salesRep', 'branchList', 'POCampaignObj', 'POObj', 'campaignObj', 'creativeDetails', 'locationDetailsJson',
                'poCampaignId'));
   }


    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $formData = $this->convertJSONToArray($request->get('sendData'));
            $validator = $this->purchaseOrderRepository->validateForms($formData);
            if ($validator->fails()) {
                \DB::rollback();
                 return $this->validationFail($validator);
            }
            $poCampaignObj = $this->purchaseOrderRepository->updatePurchaseOrder($formData);
            \DB::commit();
            return $this->POSuccessResponse($poCampaignObj);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->validationException($e->getMessage());
        }
    }

    /*PO-Form Validation*/ 
    public function validatePO(Request $request)
    {
        $formData = $this->convertJSONToArray($request->get('sendData'));
        $validator = $this->purchaseOrderRepository->validatePoForm($formData);
        return $this->validationResponse($validator);
    }

    /*Campaign Form Validation*/ 
    public function validateCampaign(Request $request)
    {
        $formData = $this->convertJSONToArray($request->get('sendData'));
        $validator = $this->purchaseOrderRepository->validateCampaignForm($formData);
        return $this->validationResponse($validator);
    }

    /*Creative Form Validation*/
    public function validateCreative(Request $request)
    {
        $formData = $this->convertJSONToArray($request->get('sendData'));
        $validator = $this->purchaseOrderRepository->validateCreativeForm($formData);
        return $this->validationResponse($validator);
    }

    /* Returning Validation Response */
    public function validationResponse($validator)
    {
        if ($validator->fails()) {
        return $this->validationFail($validator);
        }
        return $this->validationSuccess();
    }

    /**
     * Show the form for Adding New Campaign.
     * @param int $POCampaignId
     * @return \Illuminate\Http\Response
     */
    public function addNewCampaignForPO($POCampaignId)
    {
        list($activeAdvertiser, $salesRep, $branchList) = $this->dropDownFormElements();
        $poDetaills = $this->purchaseOrderRepository->getPOAndPOCampaign($POCampaignId);
        return view('purchaseOrder.po-addcampaign', compact('poDetaills', 'activeAdvertiser', 'salesRep', 'branchList'));
    }

    /**
    * Store the specified resource in storage.
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function addCampaign(Request $request)
    {
        \DB::beginTransaction();
        try {
                $formData = $this->convertJSONToArray($request->get('sendData'));
                $validator = $this->purchaseOrderRepository->validateForms($formData);
                if ($validator->fails()) 
                {
                    \DB::rollback();
                    return $this->validationFail($validator);
                }
                $purchaseOrderObj = $this->purchaseOrderRepository->addCampaign($formData);
                \DB::commit();
                $this->flashMessage('New Campaign Added for Purchase Order Successfully !');
                return $this->POSuccessResponse($purchaseOrderObj);
        } catch (\Exception $e) {
            \DB::rollback();
             return $this->validationException($e->getMessage());
        }
    }

    /**
     * Show the form for Cloning the specified resource.
     * @param int $POCampaignId
     * @param  int  $campaignId
     * @return \Illuminate\Http\Response
     */
    public function clonePO($poCampaignId, $campaignId)
    {
        list($activeAdvertiser, $salesRep, $branchList) = $this->dropDownFormElements();
        $POCampaignObj = $this->poCampaignRepository->getPOCampaignByCampaign($campaignId);
        list($POObj, $campaignObj, $creativeDetails, $locationDetailsJson) = 
                $this->purchaseOrderRepository->getPOElements($POCampaignObj,$campaignId);
        return view('purchaseOrder.po-clonecampaign', compact('activeAdvertiser', 'salesRep', 'branchList', 'POCampaignObj', 'POObj', 'campaignObj', 'creativeDetails',
            'locationDetailsJson'));
    }


    /**
    * Delete PurchaseOrder
    * @param INT $POCampaignId
    * @param INT $campaignId
    */
    public function deletePO($POCampaignId, $campaignId)
    {
        $POCampaign = $this->poCampaignRepository->getPOCampaignByCampaign($campaignId);
        $originalPOId = $POCampaign[0]->OriginalPOId;
        $POId = $POCampaign[0]->POId;
        $campaignCount = $this->poCampaignRepository->getCampaignCount($originalPOId);
       \DB::beginTransaction();
        try {
            /* PO has only one Campaign */
            if ($campaignCount == 1)
            {
              $this->purchaseOrderRepository->deletePO($POId);
            }
            $this->purchaseOrderRepository->deletePOElements($POCampaign);
            \DB::commit();
            $this->flashMessage('Campaign Deleted Successfully !');
        } catch (\Exception $e){
            \DB::rollback();
            $this->flashMessage($e->getMessage());
        }
        return \Redirect('purchaseorder');
    }

    /*Drop Down Elements For PO */
    public function dropDownFormElements()
    {
        /*Drop Down Elements in PO Form*/
        $activeAdvertiser = $this->advertiserRepository->getActiveAdvertisers(true);
        $salesRep = $this->userRepository->fetchUserNameandUserIDByRole(
                           \Config::get('vyoma-constants.sales_rep'), true);
        $branchList = $this->branchRepository->getActiveBranches(true);
        return array($activeAdvertiser, $salesRep, $branchList);
    }

    public function getPOGridData()
    {
        $poList = $this->purchaseOrderRepository->getGridData();
        $PO = array(
                'records' => count($poList),
                'rows' => $poList
                
            );
        return $PO;       
    }
}

