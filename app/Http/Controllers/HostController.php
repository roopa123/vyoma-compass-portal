<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\BayRepository;
use App\Repositories\HostRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\StationRepository;
use App\Repositories\CodeRepository;
use App\Http\Requests\StoreHostNameRequest;
use App\Services\Constants\HostConfigConstants;
use DB;
use Gate;

class HostController extends Controller
{
    /**
    * Instances of Admin Repository
    */
    protected $hostRepo;
    protected $bayRepo;
    protected $stationRepo;
    private $codeRepo; 
    /**
    * Access all methods and objects in Repository
    */
    public function __construct(
        BayRepository $bayRepo,
        HostRepository $hostRepo,
        StationRepository $stationRepo,
        CodeRepository $codeRepo
    ) {
        $this->bayRepo = $bayRepo;
        $this->hostRepo = $hostRepo;
        $this->stationRepo = $stationRepo;
        $this->codeRepo = $codeRepo;
    }

    /*
     Fetch info of station and bay, returning it to view
    */
    public function getIndex()
    {
        $status = '';
        $searchOption = array('' => 'All', 'A' => 'Active', 'I' => 'Inactive');
        $search = '';
        if (\Request::segment(2) == 'region') {
            $id['region_id'] = \Request::segment(3);
            $branch = \Request::segment(7);
            $buildUrl = \URL::to('/host-grid'. '/region/'.$id['region_id'].'/branch/'.$branch);
            $listOfBays = $this->hostRepo->baylist();
            $bayID = "";
            $DetailsOfStation = $this->stationRepo->stationlist();
            $editTypeBayLocation = "select";
            $editTypeStation = "select";
            $editable = false;
            $stationName = "";
            $apps = "";
            $startTime = "";
            $endTime = "";
            $status = \Request::segment(5);
            $search = 'Active';
        }
        else if (\Request::segment(2) == 'station') {
            $id['station_id'] = \Request::segment(3);
            $buildUrl = \URL::to('/host-grid'. '?station_id='.$id['station_id']);
            $listOfBays = $this->hostRepo->baylist();
            $bayID = "";
            $DetailsOfStation = $this->stationRepo->stationlist();
            $editTypeBayLocation = "select";
            $editTypeStation = "select";
            $editable = false;
            $apps = "";
            $stationName = "";
            $startTime = "";
            $endTime = "";

        }
        else if (\Request::segment(2) == 'baylocation') {
            $id['bay_id'] = \Request::segment(3);
            $getBayLocation = $this->hostRepo->getBayLocationInfo($id['bay_id']);
            $baycode = $getBayLocation['BayLocationCode'];
            $buildUrl = \URL::to('/host-grid'. '?bay_location='.$baycode);
            $listOfBays = $this->hostRepo->baylist();
            $bayID = "";
            $DetailsOfStation = $this->stationRepo->stationlist();
            $editTypeBayLocation = "select";
            $editTypeStation = "select";
            $editable = false;
            $apps = "";
            $stationName = "";
            $startTime = "";
            $endTime = "";
        } 
        else if (\Request::segment(2) == 'bay') {
            $idOfBay = \Request::segment(3);
            $id['bay_id'] = $idOfBay;
            $bayApp = $this->hostRepo->getBayApp($id['bay_id']);
            $buildUrl = \URL::to('/host-grid'. '?bay_id='.$id['bay_id']. '&app='.$bayApp['App']);
            $bays = $this->hostRepo->bayDetailTest($id['bay_id']);
            $listOfBays = $bays[0]['BayLocationCode'];
            $apps = (string)$bays[0]['App'];
            $bayID =$bays[0]['BayId'];
            $startTime = $bays[0]['StartTime'];
            $endTime = $bays[0]['EndTIme'];
            $station = $this->hostRepo->stationDetail($id['bay_id']);
            $DetailsOfStation = $station['StationName'];
            $stationName = "For ".$DetailsOfStation;
            $editTypeBayLocation = "text";
            $editTypeStation = "text";
            $editable = true;
        } else {
            $id['id'] = "";
            $bayID = "";
            $apps = "";
            $buildUrl = \URL::to('/host-grid');
            $listOfBays = $this->hostRepo->baylist();
            $DetailsOfStation = $this->stationRepo->stationlist();
            $editTypeBayLocation = "select";
            $editTypeStation = "select";
            $editable = false;
            $stationName = "";
            $startTime = "";
            $endTime = "";
        }
        $redirect = '';
        if(\Request::segment(4) == 'redirect'){
            $redirect = '/redirect/true';
        }
        return view('host.host', compact('listOfBays', 'buildUrl','DetailsOfStation','redirect',
         'editTypeBayLocation', 'editTypeStation', 'bayID', 'editable', 'stationName', 'status', 'searchOption', 'search', 'apps', 'startTime', 'endTime'));
    }

    /*
     Save Host Information
    */
    public function saveHostInfo()
    {
        if(Gate::denies('Create Host')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $bayId = \Input::get('BayLocationBayId');
        $validator = \Validator::make(request()->all(), $this->hostRepo->validationRules);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => 'Follow the rules for Host Name','errors'=>$validator->errors(), 'status' => 422), 422);
        }
        try {
            $result = $this->hostRepo->insertData(\Input::all());
            $getHostName = $this->hostRepo->getHostName($result);
        } catch (\Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['invalid'=>[$e->getMessage()]] , 'status' => 422), 422);
        }

        if (\Request::segment(3) == 'redirect') {
            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.host')." ".$getHostName['HostName']." ".\Lang::get('vyoma-messages.createSuccess'), 'redirect' => true, 'bayId' => $bayId));
        }
        else {

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.host')." ".$getHostName['HostName']." ".\Lang::get('vyoma-messages.createSuccess'), 'redirect' => false, 'bayId' => $bayId));
        }
    }

    /*
    * Get player or master info based on bay id
    */
    public function getPlayerInfo($bayid)
    {
        $masterName = "";
        $checkMaster = $this->hostRepo->getPlayerOrMasterInfo($bayid);
        foreach ($checkMaster as $value) {
            if($value['MasterOrPlayer'] == \Config::get('vyoma-constants.masterType')) {
                $masterName = $value['HostMasterName'];
            }
            else if($value['MasterOrPlayer'] == \Config::get('vyoma-constants.bothType')){
                $masterName = $value['HostMasterName'];
            }
        }
        return $masterName;
    }

    public function getBayInfo($station_id){
        if($details = $this->hostRepo->getBayinfoWithStations($station_id)){
             return $details;
        }
    }

    public function getAppInfo($bayId)
    {
        $details = $this->hostRepo->getAppInfoWithBay($bayId);

        return $details['App'];
    }

    public function getHostname($host) {
        $info = $this->hostRepo->getHostId($host);
        //dd($info['BayId']);
        return $info;
    }

    public function getHostId($host) {
        $info = $this->hostRepo->getHostIdwithName($host);
        //dd($info['BayId']);
        return $info;
    }

    public function getBayLocationInfo($bay, $host) {
        $info = $this->hostRepo->getLocationInfo($bay, $host);
        //dd(response()->json($info));
        return response()->json($info);
    }

    public function updateHostInfo($hostid)
    {
        if(Gate::denies('Update Host')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $validator = \Validator::make(request()->all(), $this->hostRepo->editValidationRules(\Input::all()));

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => 'Follow the rules for Host Name','errors'=>$validator->errors(), 'status' => 422), 422);
        }
      try {
            $this->hostRepo->updateHostDetails($hostid, \Input::all());
            $getHostName = $this->hostRepo->getHostName($hostid); 

            return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.host')." ".$getHostName['HostName']." ".\Lang::get('vyoma-messages.updateSuccess')));
        } catch (\Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['invalid'=>[$e->getMessage()]] , 'status' => 422), 422);
        }
        
    }

    public function deleteHostInfo($hostid)
    {
        try {
            $this->hostRepo->deleteHost($hostid);
            $getHostName = $this->hostRepo->getHostName($hostid);
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }

        return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.host')." ".$getHostName['HostName']." ".\Lang::get('vyoma-messages.deleteSuccess')));
    }

    /*
    * AutoComplete for Station search
    */
    public function autocomplete(){
        $searchValues = $this->hostRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }    
}
