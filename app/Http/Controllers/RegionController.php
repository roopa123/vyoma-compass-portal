<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\RegionRepository;
use Illuminate\Http\Request;
use DB;
use Gate;

class RegionController extends Controller
{
    /**
    * Instances of Admin Repository
    */
    protected $regionRepo;

    /**
    * Access all methods and objects in Repository
    */
    public function __construct(RegionRepository $regionRepo) {
        $this->regionRepo = $regionRepo;
    }

    /*
     Retruning view
    */
    public function getIndex()
    {
        /*if (\Request::segment(2) == 'region') {
            $id['region_id'] = \Request::segment(3);
            $buildUrl = \URL::to('/region-grid'. '?region_id='.$id['station_id']);
        }
        else {

        }*/
        return view('region.region');
    }

    /*
     Save Region information
    */
    public function saveRegion()
    {
        if(Gate::denies('Create Region')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $validator = \Validator::make(request()->all(), $this->regionRepo->validationRules,  ['RegionName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $result = $this->regionRepo->insertData(\Input::all());
            $getRegion = $this->regionRepo->getRegionDetails($result);

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.region')." ".$getRegion[0]['RegionName']." ".\Lang::get('vyoma-messages.createSuccess')));
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }        
    }

    /*
     Update Region information with Region id
    */
    public function updateRegion($regid)
    {
        if(Gate::denies('Update Region')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $validator = \Validator::make(request()->all(), $this->regionRepo->editValidationRules(\Input::all()), ['RegionName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $this->regionRepo->updateRegionDetails($regid, \Input::all());
            $getRegion = $this->regionRepo->getRegionDetails($regid);

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.region')." ".$getRegion[0]['RegionName']." ".\Lang::get('vyoma-messages.updateSuccess')));
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }        
    }

    /*
     Delete Region information with Region id
    */
    public function deleteRegion($regid)
    {
        try {
            $this->regionRepo->deleteRegionrow($regid);
            $getRegion = $this->regionRepo->getRegionDetails($regid);

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.region')." ".$getRegion[0]['RegionName']." ".\Lang::get('vyoma-messages.deleteSuccess')));
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }        
    }

    /*
    * AutoComplete for Region
    */
    public function autocomplete(){
        $searchValues = $this->regionRepo->autocompleteSearch(\Input::get('term'));
        return $searchValues;
    }
}
