<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use App\Models\Creative;
use App\Models\Advertiser;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CreativeRepository;
use App\Http\Requests\creativeVideoValidation;
use App\Repositories\AdvertiserRepository;
use App\Repositories\CodeRepository;
use App\Repositories\FfmpegRepository;
use App\Repositories\GoogleDriveUpload;

class CreativeController extends Controller
{
    /**
     * Instances of CreativeRepository
     */
    protected $creativeRepo;

    /**
     * Instances of AdvertiserRepository
     */
    protected $advertiserRepo;

    /**
     * Instances of CodeRepository
     */
    protected $codeRepo;

    /**
     * Instances of googleUploadDrive
     */
    protected $googleUpload;

    /**
     * Access all methods and objects in Repository
     */
    public function __construct( CreativeRepository $creativeRepo, AdvertiserRepository $advertiserRepo,
            CodeRepository $codeRepo, FfmpegRepository $ffmpegRepo, GoogleDriveUpload $googleUpload)
    {
        $this->creativeRepo = $creativeRepo;
        $this->advertiserRepo = $advertiserRepo;
        $this->codeRepo = $codeRepo;
        $this->ffmpegRepo = $ffmpegRepo;
        $this->googleUpload = $googleUpload;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::denies('Show Creative')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/";
            return view('403', compact('message', 'route'));
        }
        $creatives = $this->creativeRepo->all(true);

        return view('Creative.index', compact('creatives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::denies('Create Creative')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/creative";
            return view('403', compact('message', 'route'));
        }
        $advertiserArray = $this->advertiserRepo->findAllAdvertisersByActiveStatus(true)->toArray();
        $advertiserArray[''] = 'Select';
        $categoryOneArray = $this->codeRepo->getCategoryByCodeType('One');
        $categoryOneArray[''] = 'Select';
        $categoryTwoArray = $this->codeRepo->getCategoryByCodeType('Two');
        $categoryTwoArray[''] = 'Select';

        return view('Creative.create', compact( 'advertiserArray',
            'categoryOneArray', 'categoryTwoArray'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        // try {
            $validator = \Validator::make(request()->all(), $this->creativeRepo->validationRules);
            if ($validator->fails()) {
                \DB::rollback();
                return \Redirect::to('creative/create')
                ->withInput()
                ->withErrors($validator);;
            }
            else {
                $creative = $this->creativeRepo->storeCreative(request()->all(),
                        $request->file('file'));
                \DB::commit();
                \Session::flash('success', 'Creative  '.$creative->CreativeName.' added successfully !');
                // Submit action redirect to index page
                if(\Input::get('formRequestType') === 'Submit') {
                    return redirect()->route('creative.index');
                }
                return \Redirect::to('creative/create')
                        ->withInput(\Request::except("creativeName"));
            }
        // } catch(\Exception $e) {
        //     \DB::rollback();
        //     return \Redirect::to('creative/create')
        //             ->withErrors(array('error' => $e->getMessage()))
        //             ->withInput();
        // }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::denies('Update Creative')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/creative";
            return view('403', compact('message', 'route'));
        }
        $creative = $this->creativeRepo->find($id);
        $advertiserArray = $this->advertiserRepo->findAllAdvertisersByActiveStatus(true);
        if($creative->Status == "U"){
            $status = array('U' => 'Uploaded');
        }else {
            $status = $this->advertiserRepo->Creativestatus;
        }


        if(auth()->user()->can('Delete Creative')) {
            $status = array_merge($status, array('D' => 'Delete'));
        }
        $categoryOneArray = $this->codeRepo->getCategoryByCodeType('One');
        $categoryTwoArray = $this->codeRepo->getCategoryByCodeType('Two');
        $fileMetadata = array();

        $fileMetadata = json_decode($creative->FileMetaData);
        $inputFileName = $creative->FileName;
        return view('Creative.edit', compact('advertiserArray', 'creative', 'status',
            'categoryOneArray', 'categoryTwoArray', 'fileMetadata', 'inputFileName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $fileData = "";
            $creativefile = $this->creativeRepo->find($id);

            if($creativefile['FileName'] != null){
                $checkPoCreatives = $this->creativeRepo->checkCampaignCreative($id);
                if(!is_null($checkPoCreatives)) {
                    $error = "Creative is associated with PO, You can't upload a file";
                    throw new \Exception($error);
                }
                else {
                    $fileData = $creativefile['FileName'];
                }                
            }
            $validator = \Validator::make(request()->all(),
                    $this->creativeRepo->editValidationRules(request()->all(), $fileData));

            if ($validator->fails()) {
                \DB::rollback();
                return \Redirect::to('creative/'.$request->input('creativeId').'/edit')
                ->withErrors($validator)
                ->withInput();
            }
            $creative = $this->creativeRepo->updateCreative(request()->all(), $id);
            \DB::commit();
            \Session::flash('success', 'Creative - '.$creative->CreativeName.' updated successfully !');
            return redirect('creative');
        } catch(\Exception $e) {
            \DB::rollback();
            return \Redirect::to('creative/'.$request->input('creativeId').'/edit')
                    ->withErrors(array('error' => $e->getMessage()))
                    ->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            if(Gate::denies('Delete Creative')) {
                $message = \Lang::get('vyoma-messages.AccessDenied');
                $route = "/creative";
                return view('403', compact('message', 'route'));
            }
            $res = $this->creativeRepo->statusDeleteforCreative($id);
            \DB::commit();            
            return ($res == true) ? 'success' : 'error';
        } catch(\Exception $e) {
            \DB::rollback();
            return \Redirect::to('creative/')
                ->withErrors(array('error' => $e->getMessage()));
        }
    }

    /**
    * Make undo status as review status
    * @param Int $creativeId
    * @return \Illuminate\Http\Response
    */
    public function updateStatusReview($id)
    {
        $updateStatus = $this->creativeRepo->updateStatus($id);

        return ($updateStatus == true) ? 'success':'error';
    }

    /**
    * Make  status as review status if it is Active and vice-versa
    * @param Int $CreativeId
    * @param Char $status
    * @return \Illuminate\Http\Response
    */
    public function updateStatusFields($id, $status)
    {
        $updateStatus = $this->creativeRepo->updateStatusValues($id, $status);
        if($updateStatus === 'associatedWithPo'){
            return 'associatedWithPo';
        }
        else if($updateStatus === true) {
            return 'success';
        }
        else  if($updateStatus === false){
            return 'error';
        }
        //return ($updateStatus == true) ? 'success':'error';
    }

    /**
    * Download video file
    * @param Int $fileId
    * @return string Url
    */
    public function googleFileDownload($fileId)
    {
        $videoFile = $this->googleUpload->viewWeblink($fileId);
    }

    /**
     * Remove  Video from storage.
     *
     * @param  string  $filename
     * @param  integer  $creativeId
     * @return \Illuminate\Http\Response
     */
    public function deleteVideoFromSource($filename, $creativeId)
    {
        $res = $this->creativeRepo->deleteInputFile($filename, $creativeId);

        return ($res == true) ? 'sucess':'error';
    }

    public function getCreativesByAdvertiser(Request $request)
    {
        $advertiserId = $request->get('advertiserId');
        $creativesByAdvertiser = $this->creativeRepo->getCreativesByAdvertiser($advertiserId);
        return $creativesByAdvertiser->toJson();
    }

    public function getPOCreatives($id)
    {
        return $this->creativeRepo->checkCampaignCreative($id);
    }
}
