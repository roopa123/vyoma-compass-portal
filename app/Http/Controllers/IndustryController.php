<?php

namespace App\Http\Controllers;

use Gate;
use App\Models\Permission;
use Request;
use App\Models\Role;
use App\Models\User;
use App\Models\Industry;
use App\Repositories\IndustryRepository;
use App\Models\RolesPermission;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndustryController extends Controller
{
    /**
     * Instances of Admin Repository
     */
    protected $industryRepo;

    /**
     * Access all methods and objects in Repository
     */
    public function __construct(
        IndustryRepository $industryRepo
    ) {
        $this->industryRepo = $industryRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::denies('Show Industry')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/";
            return view('403', compact('message', 'route'));
        }
        $industryList = $this->industryRepo->getIndustryValues();

        return view('Industry.index', compact('industryList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::denies('Create Industry')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/industry";
            return view('403', compact('message', 'route'));
        }
        $statuss = $this->industryRepo->status;
        $delStatus = array('D' => 'Delete');

        if(auth()->user()->can('Delete Industry')) {
            $statuss = array_merge($statuss, $delStatus);
        }
        return view('Industry.create', compact('statuss'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = \Validator::make(request()->all(), $this->industryRepo->validationRules);
            if ($validator->fails()) {
                return \Redirect::to('industry/create')
                ->withErrors($validator)->withInput();
            }
            $res = $this->industryRepo->saveIndustry(request()->all());
            \Session::flash('success', 'Industry '.$res->IndustryName.' created successfully !');
            return redirect()->route('industry.index');
        }catch(\Exception $e) {
            return \Redirect::to('industry/create')
            ->withErrors(array('error' => $e->getMessage()))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::denies('Update Industry')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/industry";
            return view('403', compact('message', 'route'));
        }
        $industry = $this->industryRepo->find($id);
        $statuss = $this->industryRepo->status;
        $delStatus = array('D' => 'Delete');

        if(auth()->user()->can('Delete Industry')) {
            $statuss = array_merge($statuss, $delStatus);
        }
        return view('Industry.edit', compact('industry', 'statuss'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = \Validator::make(request()->all(), $this->industryRepo->editValidationRules(request()->all()));

            if ($validator->fails()) {
                return \Redirect::to('industry/'.Request::get('industryId').'/edit')
                ->withErrors($validator)->withInput();
            }

             // Check for DELETE STATUS
            if(\Request::get('status') == "D") {
                // true on status update
                if( 'success' == $this->destroy($id)) {
                    \Session::flash('success', 'Industry deleted successfully !');
                    $this->industryRepo->updateIndustry($id, request()->all());
                } else {
                    \Session::flash('myerrors',  'Cannot delete this Industry, its associated with Advertiser !');
                }
                return redirect('industry');

            }
            $res = $this->industryRepo->updateIndustry($id, request()->all());
            \Session::flash('success', 'Industry '.$res->AdvertiserName.' updated successfully !');
            return redirect('industry');

        } catch(\Exception $e) {
            return \Redirect::to('industry/'.Request::get('industryId').'/edit')
                ->withErrors(array('error' => $e->getMessage()))->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(Gate::denies('Delete Industry')) {
                $message = \Lang::get('vyoma-messages.AccessDenied');
                $route = "/industry";
                return view('403', compact('message', 'route'));
            }
            $res = $this->industryRepo->industryDelete($id);
            return ($res == true)?'success':'error';

        }catch(\Exception $e) {
            return \Redirect::to('industry/')
            ->withErrors(array('error' => $e->getMessage()));
        }
    }
}
