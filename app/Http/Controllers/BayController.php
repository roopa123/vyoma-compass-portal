<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\BayRepository;
use App\Repositories\StationRepository;
use App\Repositories\BaylocationRepository;
use Illuminate\Http\Request;
use DB;
use Gate;

class BayController extends Controller
{
    /**
     * Instances of Admin Repository
    */
    protected $stationRepo;
    protected $bayRepo;
    protected $baylocationRepo;
    /**
    * Access all methods and objects in Repository
    */
    public function __construct(
        StationRepository $stationRepo,
        BayRepository $bayRepo,
        BaylocationRepository $baylocationRepo
    ) {
           $this->stationRepo = $stationRepo;
           $this->bayRepo = $bayRepo;
           $this->baylocationRepo = $baylocationRepo;
    }

    /*
     Fetching details of BayLocation List and return it to view
    */
    public function getIndex()
    {
        //$buildUrl = \URL::to('/bay-grid');
        $status = '';
        $stationName = '';
        $searchStation = '';
        $searchBay = '';
        $searchApp = '';
        $searchStatus = '';
        $searchJsonStr = '';
        $searchOption = array('' => 'All', 'A' => 'Active', 'I' => 'Inactive');
        if (\Request::segment(2) == 'region') {
            $status = \Request::segment(5);
            $branch = \Request::segment(7);
            $id['region_id'] = \Request::segment(3);
            $searchStatus = 'Active';
            $buildUrl = \URL::to('/bay-grid'. '/region/'.$id['region_id'].'/branch/'.$branch);

        }
        else if(\Request::segment(2) == 'bayId' ){
            $bayId = \Request::segment(3);
            $station = $this->stationRepo->stationNameWithBay($bayId);
            $stationId = $station['StationId'];
            $stationName = $station['StationName'];
            $buildUrl = \URL::to('/bay-grid'. '?station_id='.$stationId);

        }
        else {
            $buildUrl = \URL::to('/bay-grid');
            $searchDataStation = NULL;
            //dd(\Request::segment(2));
            if(!is_null(\Request::segment(2))) {
                $app = "";
                $bay = "";
                $bayStatus = "";
                $station = "";
                $combineArray = array();
                
                if(strpos(\Request::segment(2), '_')) {
                    $searchStationVal = explode('_', \Request::segment(2));
                    $searchStation = urldecode($searchStationVal[1]);
                    $searchStation = str_replace('.', ' ', $searchStation);
                    $searchDataStation = "'defaultValue' => ".$searchStation;
                    
                    //$searchDataStation = str_replace('"', "'", $modifyStatonData);
                    $station = array('field'=>'StationName','op'=>'eq','data'=> $searchStation 
                    );
                }
                if(strpos(\Request::segment(3), '_')) {
                    $searchBayVal = explode('_', \Request::segment(3));
                    $searchBay = urldecode($searchBayVal[1]);
                    $searchBay = str_replace('.', ' ', $searchBay);
                    //$searchBayrr = array('defaultValue' => $searchBay);
                    $bay = array('field'=>'BayLocationDesc','op'=>'eq','data'=> $searchBay 
                    );
                }
                if(strpos(\Request::segment(4), '_')) {
                    $searchAppVal = explode('_', \Request::segment(4));
                    $searchApp = urldecode($searchAppVal[1]);
                    $searchApp = str_replace('.', ' ', $searchApp);
                    //$searchAppArr = array('defaultValue' => $searchApp);
                    $app = array('field'=>'App','op'=>'eq','data'=> $searchApp 
                    );
                }
                if(strpos(\Request::segment(5), '_')) {
                    $searchStatusVal = explode('_', \Request::segment(5));
                    $searchStatus = $searchStatusVal[1];
                    $searchStatus = str_replace('.', ' ', $searchStatus);
                    //$searchStatusArr = array('defaultValue' => $searchStatus);
                    $bayStatus = array('field'=>'Bay.Status','op'=>'eq','data'=> $searchStatus 
                    );
                }         
                if(!empty($station)) {
                    array_push($combineArray, $station);
                }
                if(!empty($bay)) {
                    array_push($combineArray, $bay);
                }
                if(!empty($app)) {
                    array_push($combineArray, $app);
                }
                if(!empty($bayStatus)) {
                    array_push($combineArray, $bayStatus);
                }
                //dd($searchBay);
                $searchArrayvalue = $combineArray;
                $searchJsonStr1 = trim(json_encode($searchArrayvalue),'"');            
                $searchJsonStr = str_replace('"', "'", $searchJsonStr1);
            }
            
            /*$id['bay_id'] = \Request::segment(2);
            $buildUrl = \URL::to('/bay-grid'. '?bay_id='.$id['bay_id']);*/
        }
        $listOfStations = $this->stationRepo->stationlist();
        $listOfBayLocations = $this->baylocationRepo->baylocationlist();
        return view('bay.bay', compact('listOfStations', 'listOfBayLocations', 'buildUrl', 'status', 'searchOption', 'stationName', 'searchStation', 'searchBay', 'searchApp', 'searchStatus', 'searchJsonStr', 'searchStationArr', 'searchDataStation', 'search'));
    }

    /*
     Saving details of BayLocation
    */
    public function saveBayInfo()
    {
        if(Gate::denies('Create Bay')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $validator = \Validator::make(request()->all(), $this->bayRepo->validationRules, ['BayLocationDesc.required' => \Lang::get('vyoma-messages.BayLocationDesc')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $result = $this->bayRepo->insertData(\Input::all());
            $getBay = $this->bayRepo->getStationDetail($result);

            return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bay'). " ".$getBay['StationName']." ".\Lang::get('vyoma-messages.createSuccess')));
        } catch (\Exception $e)
         {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['duplicate'=>[$e->getMessage()]] , 'status' => 422), 422);
        }

        
    }

    /*
     Update the Bay Information
    */
    public function updateBayInfo($bayid)
    {
        try {

            if(Gate::denies('Update Bay')) {
                return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
            }            
            $this->bayRepo->updateBayData($bayid, \Input::all());
            $getBay = $this->bayRepo->getStationDetail($bayid);

            return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bay')." ".$getBay['StationName']." ".\Lang::get('vyoma-messages.updateSuccess')));
        } catch (\Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning'), 'errors'=>['duplicate'=>[$e->getMessage()]] , 'status' => 422), 422);
        }        
    }

    /*
     Delete the Bay Information
    */
    public function deleteBayInfo($bayid)
    {
        try {
            $this->bayRepo->deleteBay($bayid);
            $getBay = $this->bayRepo->getStationDetail($bayid);

            return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.bay')." ".$getBay['StationName']." ".\Lang::get('vyoma-messages.deleteSuccess')));
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => 'Something went wrong, please try again later.'));
        }        
    }

    /*
     Get station name based on bay location code
    */
    public function getStationInfo($code)
    {
       return $info = $this->bayRepo->getStationDetail($code);

    }

    /*
    * AutoComplete for bay search
    */
    public function autocomplete(){
        $searchValues = $this->bayRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }

    /**
    * Redirect host to bay
    */
    public function redirectHostStation()
    {
    }

}
