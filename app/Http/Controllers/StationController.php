<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\RegionRepository;
use App\Repositories\StationRepository;
use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\Region;
use DB;
use Gate;

class StationController extends Controller
{
/**
 * Instances of Admin Repository
 */
    protected $stationRepo;
    protected $regionRepo;
/**
 * Access all methods and objects in Repository
 */

    public function __construct(
        StationRepository $stationRepo,
        RegionRepository $regionRepo

    ) {
        $this->stationRepo = $stationRepo;
        $this->regionRepo = $regionRepo;

    }

    /*
     Returning with information about Region, Branch and State
    */
    public function getIndex($type='All', $Id=NULL)
    {
        $listOfStates = $this->stationRepo->stateList();
        $branch = array();
        $region = array();
        $search = '';
        $requestType = $this->stationRepo->findRequestType($type, $Id);
        $stype = "select";
        $searchOption = array('' => 'All', 'A' => 'Active', 'I' => 'Inactive');
        if($type == "branch") {
            $id['branch_id'] = \Request::segment(3);
            $buildUrl = \URL::to('/station-grid'. '?branch_id='.$id['branch_id']);
            $stype = "select";
            $search = 'Active';
        }
        else if($type == "region") {
            $id['region_id'] = \Request::segment(3);
            $buildUrl = \URL::to('/station-grid'. '?region_id='.$id['region_id']);
            $stype = "select";
            $search = 'Active';
        }
        else if($type == "regionHome") {
            $id['region_id'] = \Request::segment(3);
            $id['branch_id'] = \Request::segment(5);
            $buildUrl = \URL::to('/station-grid'. '/home_region_id/'.$id['region_id'].'/branch_id/'.$id['branch_id']);
            $stype = "select";
            $search = 'Active';
        }
        else {
            $buildUrl = \URL::to('/station-grid');
        }
            $listOfBranches = $this->regionRepo->branchlists();
            $listOfRegions = $this->regionRepo->regionlist();

        return view('station.station', compact('requestType', 'listOfStates', 'listOfBranches',
                'listOfRegions', 'buildUrl', 'stype', 'searchOption', 'search'));
    }

    /*
     Save Station Information
    */
    public function saveStation()
    {
        if(Gate::denies('Create Station')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $validator = \Validator::make(request()->all(), $this->stationRepo->validationRules, ['StationName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $result = $this->stationRepo->insertData(\Input::all());
            $getStationName = $this->stationRepo->getStationDetails($result);

            return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.station')." ".$getStationName[0]['StationName']." ".\Lang::get('vyoma-messages.createSuccess')));
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }        
    }

    /*
     Update Station details with id
    */
    public function updateStationDetails($stationid)
    {
        if(Gate::denies('Update Station')) {
            return response()->json(array('success' => false, 'message' =>  \Lang::get('vyoma-messages.AccessDenied'), 'errors'=> ['0' => 'Access Denied'], 'status' => 422),422);
        }
        $validator = \Validator::make(request()->all(), $this->stationRepo->editValidationRules(\Input::all()), ['StationName.regex' => \Lang::get('validation.hyphenRegex')]);

        if ($validator->fails()) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.alreadyExists'), 'errors'=> $validator->errors(), 'status' => 422), 422);
        }
        try {
            $this->stationRepo->updateStationData($stationid, \Input::all());
            $getStationName = $this->stationRepo->getStationDetails($stationid);

            return response()->json(array('success' => true, 'message' => \Lang::get('vyoma-messages.station')." ".$getStationName[0]['StationName']." ".\Lang::get('vyoma-messages.updateSuccess')));
        } catch (Exception $e) {
            return response()->json(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }        
    }

    /*
     Delete Station details with id
    */
    public function deleteStation($stationid)
    {
        try {
            $this->stationRepo->deleteStationrow($stationid);
            $getStationName = $this->stationRepo->getStationDetails($stationid);

            return json_encode(array('success' => true, 'message' => \Lang::get('vyoma-messages.station')." ".$getStationName[0]['StationName']." ".\Lang::get('vyoma-messages.deleteSuccess')));
        } catch (Exception $e) {
            return json_encode(array('success' => false, 'message' => \Lang::get('vyoma-messages.warning')));
        }        
    }

    /*
    * AutoComplete for Station search
    */

    public function autocomplete(){
        $searchValues = $this->stationRepo->autocompleteSearch(\Input::get('term'));

        return $searchValues;
    }

}
