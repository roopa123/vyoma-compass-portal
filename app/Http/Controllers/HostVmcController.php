<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\CodeRepository;
use App\Repositories\HostVmcRepository;
use App\Repositories\HostRepository;
use App\Repositories\BayRepository;
use App\Services\Constants\HostConfigConstants;

class HostVmcController extends Controller
{
    /**
    * Instances of Code Repository
    */
    
    private $codeRepo;
    private $hostVmcRepo;
    private $hostRepo;
    private $bayRepo;
    /**
    * Access all methods and objects in Repository
    */
    public function __construct(
        CodeRepository $codeRepo,
        HostVmcRepository $hostVmcRepo,
        HostRepository $hostRepo,
        BayRepository $bayRepo
    ) {
        $this->codeRepo = $codeRepo;
        $this->hostVmcRepo = $hostVmcRepo;
        $this->hostRepo = $hostRepo;
        $this->bayRepo = $bayRepo;
    }

    /**
    *Return view with details
    *@return view $details
    */
    public function getindex()
    {
        $bayId= (int)\Request::segment(3);
        $arrToBind   = $this->bayRepo->getBayObject($bayId);
        $mode = HostConfigConstants::VIEW_MODE_ADD;

        return view('vmcConfig.vmc-config-new', compact('arrToBind', 'mode'));
    }

    /**
    *Get Code Default values
    *@return view
    */
    public function create()
    {
        $codeDesc = $this->getValuesOfCode();
        $codeDefaults = $this->getDefaultFromCode();

        return view('host.create', compact('codeDefaults', 'codeDesc'));
    }

    public function storeData()
    {
        $data = array('StartTime' => '08:00:00', 'EndTime' => '21:00:00', 'BayStart' => '08:00:00', 'BayEnd' => '22:00:00', 'HostType' => 'P', 'BayId' => '257');
        $validator = \Validator::make($data, $this->hostVmcRepo->validationRules());
        if ($validator->fails()) {
           print_r($validator->errors()->getMessages()); exit;
        }
        else {
            echo "true"; exit;
        }
    }

    /**
    *Get Host values based on Host id
    *@return view
    */
    public function editHost($hostId)
    {
        $arrToBind = $this->hostRepo->getHostDetailsWithBay($hostId); 
        $mode = HostConfigConstants::VIEW_MODE_EDIT;

        return view('vmcConfig.vmc-config-edit', compact('arrToBind', 'mode'));
    }

    /**
    *Update Host values based on Host id
    *@return view
    */
    public function updateHost($hostId)
    {
        $data = array('StartTime' => '08:00:00', 'EndTime' => '21:00:00', 'BayStart' => '08:00:00', 'BayEnd' => '22:00:00', 'HostType' => 'M', 'BayId' => '257', 'StationId' => '828', 'HostId' => $hostId);
        dd($data);
        $validator = \Validator::make($data, $this->hostRepo->editValidationRules($data));

        if ($validator->fails()) {
            print_r($validator->errors()->getMessages()); exit;
        }
        else {
            echo "true"; exit;
        }
    }

    /**
    *Get  defaults from code table
    *@return Array $codeDefaults
    */
    public function getDefaultFromCode()
    {
        $codeObj = $this->codeRepo->getValuesOfCode(HostConfigConstants::CODE_DEFAULT);
        $codeDefaults = $this->codeRepo->getPreparedDefaultArr($codeObj);
        
        return $codeDefaults;
    }

    /**
    *Get Dropdown values from code table
    *@return Array $codeDesc
    */
    public function getValuesOfCode()
    {
        $codeTypes = HostConfigConstants::DROPDOWN_LISTS;
        $codeDesc = $this->codeRepo->getPreparedCodeValues($codeTypes);
        return $codeDesc;
    }

    /**
    * Get Master Host Of the Bay
    * @param int $bayId
    * @return JSON
    */
    public function getMasterHostOfBay($bayId)
    {
        $emptyJSON = [];
        $masterObj = $this->hostVmcRepo->getMasterObjOfBay($bayId);
        if (!is_null($masterObj)) {
            return response()->json($masterObj);
        }
        return $emptyJSON;
    }
}