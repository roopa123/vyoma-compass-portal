<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Dingo\Api\Routing\Helpers;
use App\Services\Constants\HttpStatusCodes;
use App\Services\Constants\AppConstants;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use Helpers;
    public function createUnique($arrayValue)
    {
        return array_unique($arrayValue);
    }

    public function implodeArray($arrayValue)
    {
        return implode(",", $arrayValue);
    }

    //Returning Response with Success Message
    public function validationSuccess()
    {
        return response()->json(array('result' => 'success',
                'code' => HttpStatusCodes::HTTP_SUCCESS_CODE));
    }

    //Returning Response with Error Message
    public function validationFail($validator)
    {
        return response()->json(array('result' => 'fail',
        'errors'=>$validator->errors()->getMessages(),
        'code' => HttpStatusCodes::HTTP_VALIDATION_ERROR_CODE));
    }

    public function validationException($message)
    {
      return response()->json(array('result' => 'fail','errors' => $message,
                'code' => HttpStatusCodes::HTTP_EXCEPTION_ERROR_CODE));
    }

    public function POSuccessResponse($POCampaignObj)
    {
        return response()->json(array('result' => 'success',
                'code' => HttpStatusCodes::HTTP_SUCCESS_CODE, 'obj' => $POCampaignObj));
    }

    //Converting to json form data to form array
    public function convertJSONToArray($data)
    {
        return json_decode($data, true);
    }

    public function flashMessage($message)
    {
        \Session::flash('success', $message);
    }

}
