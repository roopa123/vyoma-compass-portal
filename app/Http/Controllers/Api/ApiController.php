<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\RegionRepository;
use App\Repositories\StationRepository;
use App\Repositories\CreativeRepository;

class ApiController extends Controller
{
    private $regionRepository;

    private $stationRepository;

    private $creativeRepository;



    public function __construct(RegionRepository $regionRepository, StationRepository $stationRepository, CreativeRepository $creativeRepository)
    {
        $this->regionRepository = $regionRepository;
        $this->stationRepository = $stationRepository;
        $this->creativeRepository = $creativeRepository;

    }

    public function getRegionDetails()
    {
        $regionDetails = $this->regionRepository->getActiveRegionDetails();
        return $this->convertJsonResponse($regionDetails);
    }

    public function getStateDetails()
    {
        $states = $this->stationRepository->getStateDetails();
        return $this->convertJsonResponse($states);
    }

    public function getStationByRegionDetails($regionId)
    {
        $regionId = $this->returnSubstring($regionId);
        $stationsByRegion = $this->stationRepository->getStationByRegionDetails($regionId);
        return $this->convertJsonResponse($stationsByRegion);
    }

    public function getStationByStateDetails($stateId)
    {
        $stateId = $this->returnSubstring($stateId);
        $stationsBystate = $this->stationRepository->getStationByStateDetails($stateId);
        return $this->convertJsonResponse($stationsBystate);
    }

    public function getStationDetails()
    { 
        $stationDetails = $this->stationRepository->getStationsDetails();
        return $this->convertJsonResponse($stationDetails);
    }

    public function getCreativeDetailsById($creativeId)
    { 
        $creativeId = $this->returnSubstring($creativeId);
        $creativeById = $this->creativeRepository->getCreativeDetailsById($creativeId);
        return $this->convertJsonResponse($creativeById);
    }

    private function returnSubstring($str)
    {
        return substr($str, 1, -1);
    }

    public function checkCreativeToAddForCampaign($creativeId)
    {
        $creativeId = substr($creativeId, 1, -1);
        $creativeCountArr = $this->creativeRepository->checkCreativeToAddForCampaign($creativeId);
        if($creativeCountArr[0]->CreativeCount > 0)
        {
          return response()->json(array('result' => 'fail'));  
        }
        return response()->json(array('result' => 'success'));
    }

    public function convertJsonResponse($result)
    {
        return response()->json($result);
    }
}