<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use App\Models\Advertiser;
use App\Models\Industry;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\AdvertiserRepository;
use App\Repositories\CreativeRepository;
use App\Repositories\IndustryRepository;

class AdvertiserController extends Controller
{
    /**
     * Instances of Advertiser Repository
     */
    protected $advertiserRepo;

    /**
     * Instances of Industry Repository
     */
    protected $industryRepo;


    /**
     * Access all methods and objects in Repository
     */
    public function __construct(AdvertiserRepository $advertiserRepo,
        IndustryRepository $industryRepo,
        CreativeRepository $creativeRepo)
    {
        $this->advertiserRepo = $advertiserRepo;

        $this->creativeRepo = $creativeRepo;

        $this->industryRepo = $industryRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::denies('Show Advertiser')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/";
            return view('403', compact('message', 'route'));
        }
        $advertisers = $this->advertiserRepo->all();

        return view('Advertiser.index', compact('advertisers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($redirect = NULL, $redirectId=NULL, $redirectPoCamId=NULL)
    {
        if(Gate::denies('Create Advertiser')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/advertiser";
            return view('403', compact('message', 'route'));
        }
        $industryObj = $this->industryRepo->findActiveIndustry();
        //$industrys = array(''=>'Select');
        $sectors =  $this->advertiserRepo->sectors;
        $isPayings = $this->advertiserRepo->isPaying;
        $statuss = $this->advertiserRepo->status;
        $delStatus = array('D' => 'Delete');

        if(auth()->user()->can('Delete Advertiser')) {
            $statuss = array_merge($statuss, $delStatus);
        }
        foreach($industryObj as $industry) {
            $industrys[$industry->IndustryId] = $industry->IndustryName;
        }
        return view('Advertiser.create', compact('industrys', 'sectors',
            'redirect','redirectId', 'redirectPoCamId', 'isPayings', 'statuss'));
    }

    public function getIndustryList()
    {
        $industryObj = $this->industryRepo->activeIndustryList();

        return $industryObj;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {            
            if($request->input('sector') == 'Private')
                $validator = \Validator::make(request()->all(), $this->advertiserRepo->validationRulesIndustry);
            else
                $validator = \Validator::make(request()->all(), $this->advertiserRepo->validationRules);

            if ($validator->fails()) {

                if(!is_null(\Input::get('redirect'))) {
                    if(\Input::get('redirect') === 'creative'){
                    return \Redirect::to('advertiser/create/redirect/'.\Input::get('redirect'))
                    ->withErrors($validator)
                    ->withInput();
                }
                else if(\Input::get('redirect') === 'creativeEdit' && !is_null(\Input::get('redirectCampaign'))) {
                    return \Redirect::to('advertiser/create/redirect/creativeEdit/'.\Input::get('redirectCampaign'))
                    ->withErrors($validator)
                    ->withInput();
                }
                else if(\Input::get('redirect') === 'purchaseorder'){
                    return \Redirect::to('advertiser/create/redirect/'.\Input::get('redirect'))
                    ->withErrors($validator)
                    ->withInput();
                }
                else if(\Input::get('redirect') === 'purchaseorderAdd' && !is_null(\Input::get('redirectCampaign'))) {
                    return \Redirect::to('advertiser/create/redirect/purchaseorderAdd/'.\Input::get('redirectCampaign'))
                    ->withErrors($validator)
                    ->withInput();
                    }
                else if(\Input::get('redirect') === 'purchaseorderEdit' && !is_null(\Input::get('redirectCampaign')) && !is_null(\Input::get('redirectPOCampaign'))) {
                    return \Redirect::to('advertiser/create/redirect/purchaseorderEdit/'.\Input::get('redirectCampaign').'/edit/campaign/'.\Input::get('redirectPOCampaign'))
                    ->withErrors($validator)
                    ->withInput();
                    }
                else if(\Input::get('redirect') === 'pocampaignClone' && !is_null(\Input::get('redirectCampaign')) && !is_null(\Input::get('redirectPOCampaign'))) {
                    return \Redirect::to('advertiser/create/redirect/purchaseorderClone/'.\Input::get('redirectCampaign').'/clone/campaign/'.\Input::get('redirectPOCampaign'))
                    ->withErrors($validator)
                    ->withInput();
                    }
                }              
                return \Redirect::to('advertiser/create')
                ->withErrors($validator)
                ->withInput();              
                
            }
            $advertiser = $this->advertiserRepo->saveAdvertiser(request()->all());
            \Session::flash('success', 'Advertiser '.$advertiser->AdvertiserName.' created successfully !');
            // request has redirect then route the new one
            if(\Input::get('redirect') === 'creative') {
                return redirect()->route('creative.create');
            }
            else if(\Input::get('redirect') === 'creativeEdit' && !is_null(\Input::get('redirectCampaign'))) {
                return \Redirect::to('creative/'.\Input::get('redirectCampaign').'/edit');                  
            }
            else if(\Input::get('redirect') === 'purchaseorder') {
                return redirect()->route('purchaseorder.create');
            }
            else if(\Input::get('redirect') === 'purchaseorderEdit' && !is_null(\Input::get('redirectPOCampaign')) && !is_null(\Input::get('redirectCampaign'))) {
                return \Redirect::to('purchaseorder/'.\Input::get('redirectCampaign').'/edit/campaign/'.\Input::get('redirectPOCampaign'));         
            }
            else if(\Input::get('redirect') === 'purchaseorderAdd' && !is_null(\Input::get('redirectCampaign'))) {
                return \Redirect::to('purchaseorder/'.\Input::get('redirectCampaign').'/add/campaign');            
            }
            else if(\Input::get('redirect') === 'pocampaignClone' && !is_null(\Input::get('redirectPOCampaign')) && !is_null(\Input::get('redirectCampaign'))) {
                return \Redirect::to('purchaseorder/pocampaign/'.\Input::get('redirectCampaign').'/clone/campaign/'.\Input::get('redirectPOCampaign'));         
            }
            else {
                // default to index route
                return redirect()->route('advertiser.index'); 
            }
           
        } catch(\Exception $e) {
            return \Redirect::to('advertiser/create')
                ->withErrors(array('error' => $e->getMessage()))
                ->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::denies('Update Advertiser')) {
            $message = \Lang::get('vyoma-messages.AccessDenied');
            $route = "/advertiser";
            return view('403', compact('message', 'route'));
        }

        $Advertiser = $this->advertiserRepo->find($id);
        $sectors =  $this->advertiserRepo->sectors;
        $isPayings = $this->advertiserRepo->isPaying;
        $status = $this->advertiserRepo->status;
        $delStatus = array('D' => 'Delete');

        if(auth()->user()->can('Delete Advertiser')) {
            $status = array_merge($status, $delStatus);
        }
        $industryObj = $this->industryRepo->findActiveIndustry();
        $industrys = array(''=>'Select');

        foreach($industryObj as $industry) {
            $industrys[$industry->IndustryId] = $industry->IndustryName;
        }

        return view('Advertiser.edit', compact('industrys', 'sectors',
            'Advertiser', 'isPayings', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = \Validator::make(request()->all(),
                $this->advertiserRepo->editValidationRules($request->all()));
            if ($validator->fails()) {
                return \Redirect::to('advertiser/'.$request->input('advertiserId').'/edit')
                    ->withErrors($validator);
            }
            // Check for DELETE STATUS
            if(\Request::get('status') == "D") {
                if( 'success' == $this->destroy($id)) {
                    \Session::flash('success', 'Advertiser deleted successfully !');
                    $this->advertiserRepo->updateAdvertiser($id, request()->all());
                } else {
                    \Session::flash('myerrors',  'Cannot delete this Advertiser, its associated with Creative !');
                }
                return redirect('advertiser');

            }
            $res = $this->advertiserRepo->updateAdvertiser($id, request()->all());
            \Session::flash('success', 'Advertiser '.$res->AdvertiserName.' updated successfully !');
            return redirect('advertiser');

        } catch(\Exception $e) {
            return \Redirect::to('advertiser/')
            ->withErrors(array('error' => $e->getMessage()));
        }

    }

    public function getCreatives($id)
    {
        $creatives = $this->creativeRepo->getCreativesByAdvertiser($id);
        return response()->json($creatives);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(Gate::denies('Delete Advertiser')) {
                $message = \Lang::get('vyoma-messages.AccessDenied');
                $route = "/advertiser";
                return view('403', compact('message', 'route'));
            }
            $res = $this->advertiserRepo->deleteAdvertiser($id);
            return ($res == true) ? 'success' : 'error';
        } catch(\Exception $e) {
            return \Redirect::to('advertiser/')
            ->withErrors(array('error' => $e->getMessage()));
        }
    }
    

    //To Check Advertiser Has Creatives
    public function checkAdvertiser($advertiserId)
    {
        $creativeCount = $this->creativeRepo->checkAdvertiserHasCreatives($advertiserId);
        if($creativeCount > 0)
        {
            return response()->json(array('result' => 'success','creativeCount' => $creativeCount));
        }
        return response()->json(array('result' => 'fail','creativeCount' => 0));
        
    }
}
