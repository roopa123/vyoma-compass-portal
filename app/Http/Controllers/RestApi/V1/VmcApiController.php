<?php
namespace App\Http\Controllers\RestApi\V1;

use App\Http\Controllers\RestApi\ApiBaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response ;
use App\Models\Host as Host;
use App\Models\AppRelease;
use App\Repositories\HostRepository;
use App\Repositories\AppReleaseRepository;
use App\Services\Constants\ApiConstants;
use App\Services\Constants\AppErrorCodes;
use App\Services\Constants\HttpStatusCodes;
use App\Services\Transformers\DataTransformer;
use App\Transformers\HostTransformer;
use App\Transformers\PlayerTransformer;
use App\Services\ApiResponse\ApiResponse;
use App\Services\Validations\RequestValidatorService;
use App\Services\Validations\HostVersionService;
use App\Services\Validations\HostConfigValidatorService;
use App\Services\ConfigTokenCreation\ConfigTokenCreateService;
use DB;
use App\Services\Validations\BaseValidationService;

class VmcApiController extends ApiBaseController
{
    /**
    * @var App\Services\Transformers\DataTransformer
    */
    private $dataTransformer;

    /**
    * @var ApiResponse
    */
    private $apiResponse;

    /**
    * @var HostRepository
    */
    protected $hostRepository;

    /**
    * @var appReleaseRepo
    */
    protected $appReleaseRepo;

    protected $requestValidatorService;

    protected $hostVersionService;

    protected $hostConfigValidatorService;

    protected $baseValidation;

    protected $configTokenCreateService;

    public function __construct(
        HostRepository $hostRepository,
        DataTransformer $dataTransformer,
        ApiResponse $apiResponse,
        RequestValidatorService $requestValidatorService,
        HostVersionService $hostVersionService,
        HostConfigValidatorService $hostConfigValidatorService,
        AppReleaseRepository $appReleaseRepo,
        BaseValidationService $baseValidation,
        ConfigTokenCreateService $configTokenCreateService
    ) {
        $this->hostRepository = $hostRepository;
        $this->dataTransformer = $dataTransformer;
        $this->apiResponse = $apiResponse;
        $this->requestValidatorService = $requestValidatorService;
        $this->appReleaseRepo = $appReleaseRepo;
        $this->hostVersionService = $hostVersionService;
        $this->hostConfigValidatorService = $hostConfigValidatorService;
        $this->baseValidation = $baseValidation;
        $this->configTokenCreateService = $configTokenCreateService;
    }

    /**
    * Returns Json Respose for requested Parameter Details
    * @param STRING $hostName
    * @param OBJECT $request
    * @return JSON
    */
    public function getParametersInfo($hostName, Request $request)
    {
        /* To check Given Host name is Valid or Invalid */
        if ($this->hostRepository->validateHostname($hostName)) {
            /* Requested querystring into Array */
            $paramsArray = $this->requestValidatorService->getParamsArray($request);
            if ($this->requestValidatorService->validQueryString($paramsArray)) {
                /* To check for  querystring present in the requested URL */
                if (empty($paramsArray)) {
                    /* only check give hostname is valid or invalid */
                    return $this->reponseWithHeader(HttpStatusCodes::HTTP_SUCCESS_CODE);
                }
                $hostObject = $this->hostRepository->getHostObjectByName($hostName);
                if ($this->checkElementsInArray(ApiConstants::PARAM_PLAYERLIST, $paramsArray) || $this->checkElementsInArray(ApiConstants::PARAM_INLCUDE_PLAYERS, $paramsArray)) {
                    return $this->responseWithPlayerInfo($hostObject, $paramsArray);
                }
                if ($this->checkElementsInArray(ApiConstants::PARAM_CONFIG, $paramsArray)) {
                    //Response has data, config Object
                    return $this->responseWithRequestedData($hostObject, $paramsArray, ['config']);
                }
                /* Response has only data Object*/
                return $this->responseWithRequestedData($hostObject, $paramsArray);
            }
            /* Given Query string is invalid*/
            return $this->apiResponse->createErrorResponse(AppErrorCodes::QUERY_STRING_INVALID);
        }
        /* Given Host name is Invalid so the requested URL not found */
        return $this->reponseWithHeader(HttpStatusCodes::HTPP_NOT_FOUND);
    }

    /**
    * To return json response for requested Parameters
    * @param OBJECT $hostObject
    * @param ARRAY $paramsArray
    * @param ARRAY $includes
    * @return JSON Response
    */
    public function responseWithRequestedData($hostObject, $paramsArray, $includes = [])
    {
        $transformedData = $this->dataHostTransform($paramsArray, $hostObject, 'data', 'item', $includes);
        return $this->apiResponse->response($transformedData);
    }

    /**
    * To return json response for playerslist of requested Master
    * @param OBJECT $hostObject
    * @return JSON Response
    */
    public function responseWithPlayerInfo($hostObject, $paramsArray)
    {
        // To check Requested host is Master
        if ($hostObject->MasterOrPlayer == \Config::get('vyoma-constants.masterType') || $hostObject->MasterOrPlayer == \Config::get('vyoma-constants.bothType')) {
            if ($this->requestValidatorService->checkToAddAddtionalInfoWithPlayers($paramsArray)) {
                return $this->responseMasterWithPlayerIncludes($hostObject, $paramsArray);
            }
            return $this->responseWithRequestedData($hostObject, $paramsArray, ['players_list']);
        }
        //If the host name is not master,return Error Response
        return $this->apiResponse->createErrorResponse(AppErrorCodes::HOST_NOT_MASTER);
    }
    
    /**
    * Return list of playerslist along with master information
    * @param OBJECT $hostObject
    * @param ARRAY $paramsArray
    * @return JSON
    */
    public function responseMasterWithPlayerIncludes($hostObject, $paramsArray)
    {
        $includes = array('players_list');
        // To check whether the request has "config"
        if ($this->checkElementsInArray(ApiConstants::PARAM_CONFIG, $paramsArray)) {
            array_push($includes, 'config');
        }
        return $this->responseWithRequestedData($hostObject, $paramsArray, $includes);
    }

    /**
    * Return Array with Transformed Data
    * @param ARRAY $paramsArray
    * @param OBJECT $hostObject
    * @param ARRAY $includes
    * @return ARRAY
    */
    public function dataHostTransform(
        $paramsArray,
        $hostObject,
        $key,
        $type,
        $includes = []
    ) {
        return $this->dataTransformer->addData(
            $hostObject,
            new HostTransformer($paramsArray),
            $type,
            $key,
            $includes
        );
    }
    
    /* Download app version*/
    public function download($app)
    {
        if ($this->appReleaseRepo->validVersion($app)) {
            $appReleaseDetails = $this->appReleaseRepo->getAppReleaseDetails($app);
            if (!empty($appReleaseDetails['TarballURL'])) {
                 $file_path = $appReleaseDetails['TarballURL'];
                $status = HttpStatusCodes::HTTP_SEE_OTHER;

                header('X-MD5: '.md5($appReleaseDetails['Checksum']));
                header('Location: '.$file_path, true, $status);
                exit;
            }
            return $this->apiResponse->createErrorResponse(AppErrorCodes::NO_TARBALL_URL);
        }
        return $this->apiResponse->createErrorResponse(AppErrorCodes::INVALID_APP);
    }

    /**
    * Notify server update
    * @param $hostname, $request
    * @return Success response
    */
    public function notifyServerUpdate($hostName, Request $request)
    {
        $postData = $request->all();
        if (is_array($postData) && !empty($postData)) {
            /* To check Given Host name is Valid or Invalid */
            if ($this->hostRepository->validateHostname($hostName)) {
                $postJSONKey = array_keys($postData);
                if ($this->requestValidatorService->validPostKey($postJSONKey, ApiConstants::NOTIFY_SERVER_API_POST_JSON_KEYS)) {
                    $hostObject = $this->hostRepository->getHostObjectByName($hostName);
                    try {
                        list($validator, $errorArray) = $this->hostVersionService->validateServerParameters($hostObject, $postData);
                        $updateDataArray = $this->hostVersionService->getValidVersionArray($validator, $postData);
                        if (!empty($updateDataArray)) {
                            $hostUpdate = $this->hostRepository->updateTokens($hostObject, $updateDataArray);
                        }
                        return $this->returnPostUpdateResponse($errorArray, $validator, $this->hostVersionService, $postData);
                    } catch (\Exception $e) {
                        return $this->apiResponse->createErrorResponse(AppErrorCodes::APP_EXCEPTION, $e->getMessage());
                    }
                }
                /* Invalid POST keys in JSON*/
                return $this->apiResponse->returnErrorResponseForPostAPI('notify_server', AppErrorCodes::INVALID_POST_KEYS_IN_JSON);
            }
            /* Given Host name is Invalid so the requested URL not found */
            return $this->reponseWithHeader(HttpStatusCodes::HTPP_NOT_FOUND);
        } else {
            /* Invalid JSON String in POST*/
            return $this->apiResponse->returnErrorResponseForPostAPI('notify_server', AppErrorCodes::INVALID_POST_JSON_STRING);
        }
    }

    /* return response with Success and Details on Update */
    public function returnPostUpdateResponse($errorArray, $validator, $serviceObject, $postData)
    {
        $statusCode = HttpStatusCodes::HTTP_SUCCESS_CODE;
        $errorObject = array();
        if (!empty($errorArray)) {
            $errorObject[] = $serviceObject->convertErrorResponse($errorArray);
            if (isset($errorArray['registered'][ApiConstants::DUPLICATE_REGISTER])) {
                $statusCode = HttpStatusCodes::DUPLICATE_REGISTER;
            }
        }
        $successObject[] = $serviceObject->convertSuccessResponse($validator, $postData);
        return $this->apiResponse->postUpdateResponse($errorObject, $successObject, $statusCode);
    }

    public function updateConfigs($host, Request $request)
    {
        $params = $request->all();
        // To check for Valid POST JSON
        if (is_array($params) && !empty($params)) {
            //$params = $this->getContent($request);
            $hostObject = $this->hostRepository->getHostObjectByName($host);
            try {
                list($validator, $errorArray) = $this->hostConfigValidatorService->validateHostConfigTokens($hostObject, $params);
                if (empty($errorArray)) {
                    $updateDataArray = $this->configTokenCreateService->getConfigPostData($params);
                    $hostUpdate = $this->hostRepository->updateTokens($hostObject, $updateDataArray);
                    $status['config_update'] = array('success' => true);
                    $errorObject[] = [];
                    $successObject[] = $status;
                    return $this->apiResponse->postUpdateResponse($errorObject, $successObject);
                } else {
                    $errorObject[] = $this->hostConfigValidatorService->convertErrorResponse($errorArray);
                    $status['config_update'] = array('success' => false);
                    $successObject[] = $status;
                    return $this->apiResponse->postUpdateResponse($errorObject, $successObject);
                }
            } catch (\Exception $e) {
                return $this->apiResponse->createErrorResponse(AppErrorCodes::APP_EXCEPTION, $e->getMessage());
            }
        } else {
            /* Invalid JSON String in POST*/
            return $this->apiResponse->returnErrorResponseForPostAPI('config_update', AppErrorCodes::INVALID_POST_JSON_STRING);
        }
    }
}
