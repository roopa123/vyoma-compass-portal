<?php
namespace App\Http\Controllers\RestApi;

use App\Http\Controllers\Controller;
 
class ApiBaseController extends Controller
{
    /**
     * Parses json request and converts to array.
     *
     * @param \Illuminate\Http\Request $requestObject
     *
     * @return array \Illuminate\Http\Request
     */
    public function getContent($requestObject = null)
    {
        $request = $requestObject;
        if (!is_object($requestObject)) {
            $request = request();
        }
        return (array) json_decode($request->getContent());
    }

    public function reponseWithHeader($httpStatusCode)
    {
        return $this->response->noContent()->setStatusCode($httpStatusCode);
    }

    public function checkElementsInArray($value, $paramsArray)
    {
        return in_array($value, $paramsArray);
    }

    public function isJSON($string)
    {
        //return is_string($string) && is_array(json_decode($string, true)) ? true : false;
        $json = json_decode($string);
        return (is_object($json) && json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}
