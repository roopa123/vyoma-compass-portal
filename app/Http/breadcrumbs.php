<?php

/**
 * Custom functionality for breadcrumb
 *
 * @author  Subramania Bharathy <subramania.bharathy@compassitesinc.com>
 */

# Branch Home
Breadcrumbs::register('Branch Home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('Branch Home'));
});

# Region Home
Breadcrumbs::register('Region Home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('Branch Home'));
    $breadcrumbs->push('Region', route('Region Home'));
});

# Region Search

# Station Home
Breadcrumbs::register('Station Home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('Branch Home'));
    $breadcrumbs->push('Station', route('Station Home'));
});

# Station Search
Breadcrumbs::register('Region Search', function($breadcrumbs, $id)
{
    $breadcrumbs->push('Home', route('Branch Home'));
    $breadcrumbs->push('Station', route('Region Search'));
    $breadcrumbs->push($id, route('Region Search', $id));
});

# BayLocationHome
Breadcrumbs::register('BayLocationHome', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('Branch Home'));
    $breadcrumbs->push('Bay Location', route('BayLocationHome'));
});

# Bay
Breadcrumbs::register('Bay Home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('Branch Home'));
    $breadcrumbs->push('Bay', route('Bay Home'));
});

# Host Home
Breadcrumbs::register('Host Home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('Branch Home'));
    $breadcrumbs->push('Host', route('Host Home'));
});