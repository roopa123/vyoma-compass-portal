<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Operations
Route::group(['middleware' => 'auth'], function () {
/**
 *-------------------------------
 * Branch Crud
 *-------------------------------
 */
    // Default Landing Page
    Route::get('/', ['as' => 'Branch Home', 'uses' => 'HomeController@getIndex']);
    Route::get('/branch', ['as' => 'Branch Home', 'uses' => 'BranchController@getIndex']);
    Route::get('/home', ['as' => 'Home', 'uses' => 'HomeController@getIndex']);
    Route::post('/dashboard', ['as' => 'Branch Home', 'uses' => 'HomeController@getStatisticData']);

    // Get Data based on the Search cretiera
    Route::post('/branch-grid', ['as' => 'Branch Data', 'uses' =>  function () {
        GridEncoder::encodeRequestedData(new \App\Models\BranchRepository(new \App\Models\Branch()), Input::all());
    }]);
    // branch Add and Edit routes
    Route::post('/branch/crud', ['as' => 'Branch CRUD', 'uses' => function () {
        $branchController = new \App\Http\Controllers\BranchController(new \App\Repositories\BranchRepositories);

        switch (Input::get('oper'))
        {
            case 'add':
                return $branchController->storeBranch(Input::except('id', 'oper'));
                break;
            case 'edit':
                return $branchController->updateBranch(Input::get('BranchId'), Input::except('id', 'oper'));
                break;
            case 'del':
                return  $branchController->deleteBranch(Input::get('id'));
                break;
        }
    }]);

  /**
   *-------------------------------
   * Region Crud
   *-------------------------------
  */
        Route::get('region', ['as' => 'Region Home', 'uses' => 'RegionController@getIndex']);
        Route::post('/region-grid', ['as' => 'Region Data', 'uses' => function () {
            GridEncoder::encodeRequestedData(new \App\Models\RegionRepository(new \App\Models\Region()), Input::all());
        }]);
  Route::post('/region/crud', ['as' => 'Region CRUD', 'uses' => function() {
    $regionController = new \App\Http\Controllers\RegionController(new \App\Repositories\RegionRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $regionController->saveRegion(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $regionController->updateRegion(Input::get('RegionId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $regionController->deleteRegion(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Station Crud
   *-------------------------------
  */
  Route::get('station', ['as' => 'Station Home', 'uses' => 'StationController@getIndex']);
  Route::get('station/{type}/{Id}', ['as' => 'Station Search', 'uses' => 'StationController@getIndex']);
  Route::get('station/{type}/{Id}/{branch}/{id}', ['as' => 'Station Search', 'uses' => 'StationController@getIndex']);
  Route::post('/station-grid/home_region_id/{Id}/branch_id/{id}', ['as' => 'Station Data', 'uses' => function()
  {
      GridEncoder::encodeRequestedData(
        new \App\Models\StationRepository(new \App\Models\Station()), Input::all()
      );
  }]);
  Route::post('/station-grid', ['as' => 'Station Data', 'uses' => function()
  {
      GridEncoder::encodeRequestedData(
        new \App\Models\StationRepository(new \App\Models\Station()), Input::all()
      );
  }]);
  Route::post('/station/crud', ['as' => 'Station CRUD', 'uses' => function()  {
    $stationController = new \App\Http\Controllers\StationController(new \App\Repositories\StationRepository, new \App\Repositories\RegionRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $stationController->saveStation(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $stationController->updateStationDetails(Input::get('StationId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $stationController->deleteStation(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Bay Location Crud
   *-------------------------------
  */
  Route::get('baylocation', ['as' => 'BayLocationHome', 'uses' => 'BaylocationController@getIndex']);
  Route::post('/baylocation-grid', ['as' => 'BayLocationData', 'uses' => function()   {
      GridEncoder::encodeRequestedData(new \App\Models\BaylocationRepository(new \App\Models\Baylocation()), Input::all());
  }]);
  Route::post('/baylocation/crud', ['as' => 'BayLocationCRUD', 'uses' => function() {
    $stationController = new \App\Http\Controllers\BaylocationController(new \App\Repositories\BaylocationRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $stationController->saveBayLocation(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $stationController->updateBayLocation(Input::get('BayLocationCode'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $stationController->deleteBayLocation(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Bay  Crud
   *-------------------------------
  */
  Route::get('bay', ['as' => 'Bay Home', 'uses' => 'BayController@getIndex']);
  Route::get('bay/{station}/{bay}/{App}/{Status}', ['as' => 'Bay Home', 'uses' => 'BayController@getIndex']);
  Route::get('bay/bayId/{bayId}', ['as' => 'Bay Home', 'uses' => 'BayController@getIndex']);
  Route::get('bay/{type}/{Id}/status/{statusVal}', ['as' => 'Bay Search', 'uses' => 'BayController@getIndex']);
  Route::get('bay/{type}/{Id}/status/{statusVal}/branch/{id}', ['as' => 'Bay Search', 'uses' => 'BayController@getIndex']);

  Route::post('/bay-grid/region/{Id}/branch/{id}', ['as' => 'Bay Data', 'uses' => function() {
      GridEncoder::encodeRequestedData(new \App\Models\BayRepository(new \App\Models\Bay()), Input::all());
  }]);
  Route::post('/bay-grid', ['as' => 'Bay Data', 'uses' => function() {
      GridEncoder::encodeRequestedData(new \App\Models\BayRepository(new \App\Models\Bay()), Input::all());
  }]);
  Route::post('/bay/crud', ['as' => 'Bay CRUD', 'uses' => function() {
    $bayController = new \App\Http\Controllers\BayController(new \App\Repositories\StationRepository, new \App\Repositories\BayRepository, new \App\Repositories\BaylocationRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $bayController->saveBayInfo(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $bayController->updateBayInfo(Input::get('BayId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $bayController->deleteBayInfo(Input::get('id'));
        break;
    }
  }]);

  /**
   *-------------------------------
   * Host Crud
   *-------------------------------
  */
  Route::get('host', ['as' => 'Host Home', 'uses' => 'HostController@getIndex']);
  Route::get('host/{type}/{Id}/{redirect}', ['as' => 'Host Search', 'uses' => 'HostController@getIndex']);

  Route::get('host/{type}/{Id}/{redirect}/{station}/{bay}/{App}/{Status}', ['as' => 'Host Search', 'uses' => 'HostController@getIndex']);

  Route::get('host/{type}/{Id}/status/{statusVal}/branch/{id}', ['as' => 'Host Search', 'uses' => 'HostController@getIndex']);

  Route::get('host/{type}/{Id}', ['as' => 'Host Search', 'uses' => 'HostController@getIndex']);

  Route::post('/host-grid/region/{Id}/branch/{id}', ['as' => 'Host Data', 'uses' => function() {
      GridEncoder::encodeRequestedData(new \App\Models\HostRepository(new \App\Models\Host()), Input::all());
  }]);
  Route::post('/host-grid', ['as' => 'Host Data', 'uses' => function() {
      GridEncoder::encodeRequestedData(new \App\Models\HostRepository(new \App\Models\Host()), Input::all());
  }]);
  Route::post('/host/crud/redirect/true', ['as' => 'Host CRUD', 'uses' => function() {
    $hostController = new \App\Http\Controllers\HostController(new \App\Repositories\BayRepository, new \App\Repositories\HostRepository, new \App\Repositories\StationRepository, new \App\Repositories\CodeRepository);

    switch (Input::get('oper'))
    {
      case 'add':
        return $hostController->saveHostInfo(Input::except('id', 'oper'));
        break;
    }
  }]);
   Route::post('/host/crud', ['as' => 'Host CRUD', 'uses' => function() {
    $hostController = new \App\Http\Controllers\HostController(new \App\Repositories\BayRepository, new \App\Repositories\HostRepository, new \App\Repositories\StationRepository, new \App\Repositories\CodeRepository);
    switch (Input::get('oper'))
    {
      case 'add':
        return $hostController->saveHostInfo(Input::except('id', 'oper'));
        break;
      case 'edit':
        return $hostController->updateHostInfo(Input::get('HostId'), Input::except('id', 'oper'));
        break;
      case 'del':
        return  $hostController->deleteHostInfo(Input::get('id'));
        break;
    }
  }]);

  Route::get('stationinfo/{bay_id}', ['as' => 'Station Info',
        'uses' => 'BayController@getStationInfo']);
  Route::get('playerinfo/{bay_id}', 'HostController@getPlayerInfo');
  Route::get('appinfo/{bay_id}', 'HostController@getAppInfo');
  Route::get('bayInfo/{station_id}', 'HostController@getBayInfo');
  Route::get('getCount', 'BranchController@getCount');
  Route::get('getbayId/{host}', 'HostController@getHostname');
  Route::get('getHostId/{host}', 'HostController@getHostId');
  Route::get('getbayLocation/{bayId}/{hostid}', 'HostController@getBayLocationInfo');
  Route::post('redirectHostWithStation', 'BayController@redirectHostStation');

  Route::get('search/branchAutocomplete/{column}', 'BranchController@autocomplete');
  Route::get('search/regionAutocomplete/{column}', 'RegionController@autocomplete');
  Route::get('search/stationAutocomplete/{column}', 'StationController@autocomplete');
  Route::get('search/bayLocationAutocomplete/{column}', 'BaylocationController@autocomplete');
  Route::get('search/bayAutocomplete/{column}', 'BayController@autocomplete');
  Route::get('search/hostAutocomplete/{column}', 'HostController@autocomplete');
  

  /** New set of routes for HostVMC*/
  //Route::resource('host','HostVmcController');
  Route::get('host/create', 'HostVmcController@create');
 
  Route::get('bayDetails/{station_id}', 'HostVmcController@getBayInfoFromStation');
  //Route::get('host/{type}/{Id}/{redirect}/{station}/{bay}/{App}/{Status}', ['as' => 'Host Search', 'uses' => 'HostVmcController@getIndex']);
  Route::get('masterName/{bay_id}', 'HostVmcController@getPlayerInfo');
  Route::get('host/vmc', 'HostVmcController@storeData');







/** End Of Operations **/

//Admin Activity
  Route::group(
      ['prefix' => 'admin'],
      function () {
        Route::resource('roles', 'RoleController');

        Route::resource('users', 'AdminUsersController');

        Route::resource('permissions', 'PermissionController');
      }
  );

  /**
 * CCC
 * Customer Commitment Routes
 * Resource Controller for Create ,Update ,show and Delete
 */


  /**
  * Industry Grid
  */
  Route::resource('industry', 'IndustryController');
  Route::post('/industry-grid', ['as' => 'Industry Data', 'uses' => function() {
        GridEncoder::encodeRequestedData(
              new \App\Repositories\IndustryRepository(
                  new \App\Models\Industry())
              , Input::all());
  }]);
  Route::get('/industryList', 'AdvertiserController@getIndustryList');

  /**
  * Advertiser Grid
  */
  Route::resource('advertiser', 'AdvertiserController');
  
  Route::post('/advertiser-grid', ['as' => 'Advertiser Data', 'uses' => function() {
        GridEncoder::encodeRequestedData(
              new \App\Repositories\AdvertiserRepository(
                  new \App\Models\Advertiser())
              , Input::all());
  }]);

  Route::get('advertiser/create/redirect/{redirect}', 'AdvertiserController@create');
  Route::get('advertiser/create/redirect/{redirect}/{campaignId}', 'AdvertiserController@create');
  Route::get('advertiser/create/redirect/{redirect}/{pocampaignId}/edit/campaign/{campaignId}', 'AdvertiserController@create');
  Route::get('advertiser/create/redirect/{redirect}/{pocampaignId}/clone/campaign/{campaignId}', 'AdvertiserController@create');
  Route::get('advertiser/{id}/creatives', 'AdvertiserController@getCreatives');
  Route::get('checkadvertiser/{id}', 'AdvertiserController@checkAdvertiser');

  /**
  * Creative Grid
  */
  Route::resource('creative', 'CreativeController');
  Route::post('/creative-grid', ['as' => 'Creative Data', 'uses' => function() {
        GridEncoder::encodeRequestedData(
              new \App\Repositories\CreativeRepository(
                  new \App\Models\Creative(), new \App\Repositories\FfmpegRepository(),
                  new \App\Repositories\CodeRepository())
              , Input::all());
  }]);
  Route::get('deleteVideo/{filename}/{creativeId}', 'CreativeController@deleteVideoFromSource');
  Route::get('add/creative', 'CreativeController@addAnotherCreative');
  Route::get('updateUndoStatus/{creativeId}', 'CreativeController@updateStatusReview');
  Route::get('creativesFile/{creativeId}/{format}', 'CreativeController@fileDownload');
  Route::get('updateStatuses/{creative}/{status}', 'CreativeController@updateStatusFields');
  Route::get('checkPoAdv/{creativeId}', 'CreativeController@getPOCreatives');
  Route::get('googleDownload/{fileId}', 'CreativeController@googleFileDownload');


  /**
   * Purchase order grid
   */
  Route::resource('purchaseorder','PurchaseOrderController');
  /*Route::post('/po-grid', ['as' => 'purchase.order.data', 'uses' => function() {
        GridEncoder::encodeRequestedData(
              new App\Repositories\PurchaseOrderRepository(
                  new App\Models\PurchaseOrder())
              , Input::all());
  }]);*/
  Route::post('/po-grid', ['as' => 'PO', 'uses' => 'PurchaseOrderController@getPOGridData']);

  Route::get('purchaseorder/{poCampaignId}/edit/campaign/{cid}', 'PurchaseOrderController@poEdit');
  Route::get('purchaseorder/{poCampaignId}/add/campaign/', 'PurchaseOrderController@addNewCampaignForPO');
  Route::get('purchaseorder/pocampaign/{poCampaignId}/clone/campaign/{campaignId}', 
        'PurchaseOrderController@clonePO');
  Route::get('purchaseorder/pocampaign/{poCampaignId}/delete/campaign/{campaignId}', 
        'PurchaseOrderController@deletePO');
  Route::post('purchaseorderstore', 'PurchaseOrderController@store');
  Route::post('addcampaign', 'PurchaseOrderController@addCampaign');
  Route::get('purchaseOrder/{id}/{status}', 'PurchaseOrderController@getPoWithStatus');
  Route::get('purchaseOrder/{poid}/{campaignid}/{status}', 'PurchaseOrderController@getpoCampaignDetailsWithStatus');

  Route::post('purchaseOrder/validate/po', 'PurchaseOrderController@validatePO');
  Route::post('purchaseOrder/validate/campaign', 'PurchaseOrderController@validateCampaign');
  Route::post('purchaseOrder/validate/creative', 'PurchaseOrderController@validateCreative');

  /**
   * API ROUTES
  */

  Route::get('api/regions', 'Api\ApiController@getRegionDetails');
  Route::get('api/states', 'Api\ApiController@getStateDetails');
  Route::get('api/stations', 'Api\ApiController@getStationDetails');
  Route::get('api/region/{regionId}', 'Api\ApiController@getStationByRegionDetails');
  Route::get('api/state/{stateId}', 'Api\ApiController@getStationByStateDetails');
  Route::get('api/creative/{creativeId}', 'Api\ApiController@getCreativeDetailsById');
  Route::get('api/checkcreative/{creativeId}', 'Api\ApiController@checkCreativeToAddForCampaign');


/**
 * VMC config routes
 */
    Route::get('vmc-config', function(){
        return view('vmcConfig.vmc-config-new');
    });
    Route::get('vmc-config/edit/{id}', 'HostVmcController@editHost');
    Route::get('master/{bay_id}', 'HostVmcController@getMasterHostOfBay');
});






