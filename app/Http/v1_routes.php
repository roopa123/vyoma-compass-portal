<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('V1', function ($api) {
    $api->group(['namespace' => 'App\Http\Controllers\RestApi\V1'], function ($api) {
        $api->get('host/{host}', 'VmcApiController@getParametersInfo');
        $api->get('app/{version}', 'VmcApiController@download');
        $api->post('config/{hostname}', 'VmcApiController@updateConfigs');
        $api->post('host/{hostname}', 'VmcApiController@notifyServerUpdate');
    });
});