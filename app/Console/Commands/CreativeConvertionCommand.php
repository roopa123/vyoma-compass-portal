<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Repositories\CreativeRepository;
use App\Jobs\CreativeConveter;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CreativeConvertionCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'creative:convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Converts user videos into MP4 and MP2 format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CreativeRepository $creativeRepo)
    {

        // Move upload file into user directory;
        $ffmpegJob = New CreativeConveter($creativeRepo);

        $this->dispatch($ffmpegJob);
    }
}
