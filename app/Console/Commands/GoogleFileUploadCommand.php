<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Repositories\CreativeRepository;
use App\Jobs\GoogleDriveApi;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GoogleFileUploadCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gDrive:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload File to Google Drive using API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CreativeRepository $creativeRepo)
    {

        // Move upload file into user directory;
        $ffmpegJob = New GoogleDriveApi($creativeRepo);

        $this->dispatch($ffmpegJob);
    }
}
