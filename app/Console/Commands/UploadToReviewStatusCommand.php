<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Repositories\CreativeRepository;
use App\Jobs\UploadToReview;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UploadToReviewStatusCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'creative:updateStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change uploaded file status to review';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CreativeRepository $creativeRepo)
    {
        // Store converted video information
        $statusJob = New UploadToReview($creativeRepo);
        $this->dispatch($statusJob);
    }
}
