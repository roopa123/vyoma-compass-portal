<?php

return [

    'upload_path' => env('APP_UPLOAD_PATH').'videos/',

    'upload_user_path' => env('APP_UPLOAD_PATH').'users/',    
    
    'max_file_size' => '15 MB',

    'extension' => array('mov', 'mp4', 'mpg', 'avi', 'flv'),

    'convert_into' => array('mp4', 'mpg')
];