<?php

return [

    'client_account' => env('Google_account',false),

    'private_key' => env('Google_private_key', false),


    'upload_path' => env('APP_UPLOAD_PATH').'videos/',

    'upload_user_path' => env('APP_UPLOAD_PATH').'users/',

    'google_drive_webView_Path' => 'https://drive.google.com/file/d/',

    'max_file_size' => '15 MB',

    'codec' => 'video/mp4',

    'chunk_size' => 1024,

    'drive_url' => 'https://www.googleapis.com/auth/drive',

    'extension' => array('mov', 'mp4', 'mpg', 'avi', 'flv'),

    'convert_into' => array('mp4', 'mpg'),

    'required_time_for_update_creativeStatus' => 3
];