<?php
return [

    /*
    * Constant value for Master, Player type in Host Crud
    */

    'masterType' => 'M',

    'playerType' => 'P',

    'bothType' => 'B',

    'noOfsuggestions' => 5,

    'super_admin_role_name' => 'Super Admin',

    'super_admin_role_id' => env('SUPER_ADMIN_ROLE', false),

    'super_admin_id' => env('SUPER_ADMIN', false),

    'sales_rep' => env('SALES_REP', false),

    'status' => array('Active'=> 'A', 'Inactive'=>'I', 'Delete' => 'D')
];
