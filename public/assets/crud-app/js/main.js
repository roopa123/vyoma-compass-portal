/**
 * Created by Bharathy  on 04-01-2016.
 */

$(document).ready(function() {
    $("#jqGridBranches, #jqGridHost, #jqGridBay, #jqGridBayLocation, #jqGridStation, #jqGridRegion").bind("jqGridAddEditErrorTextFormat",
        function (data, response) {
            $errors = response.responseJSON.errors;
            $( '#form-success' ).css('display', 'none');
            $.each( $errors, function( key, value ) {
                errorTextFormat(value); //showing only the first error.
                return false;
            });

            // Flash Messages
            $("#success-alert").fadeTo(1000, 500).slideUp(500, function(){
                $("#success-alert").alert('close');
            });
        });

    /****** branch grid ******/
    var branchTopPager = document.getElementById( "jqGridBranchesPager" );
    $('#gview_jqGridBranches .ui-jqgrid-titlebar').next().prepend(branchTopPager);

    /****** region grid ******/
    var regionTopPager = document.getElementById( "jqGridRegionPager" );
    $('#gview_jqGridRegion .ui-jqgrid-titlebar').next().prepend(regionTopPager);

    /****** station grid ******/
    var stationTopPager = document.getElementById( "jqGridStationPager" );
    $('#gview_jqGridStation .ui-jqgrid-titlebar').next().prepend(stationTopPager);

    /****** bay-location grid ******/
    var bayLocTopPager = document.getElementById( "jqGridBayLocationPager" );
    $('#gview_jqGridBayLocation .ui-jqgrid-titlebar').next().prepend(bayLocTopPager);

    /****** branch grid ******/
    var bayTopPager = document.getElementById( "jqGridBayPager" );
    $('#gview_jqGridBay .ui-jqgrid-titlebar').next().prepend(bayTopPager);

    /****** hosts grid ******/
    var hostsTopPager = document.getElementById( "jqGridHostPager" );
    $('#gview_jqGridHost .ui-jqgrid-titlebar').next().prepend(hostsTopPager);

    // MAKE POPUP AS STATIC DURING ADD-HOST EVENT
    $.jqm.params.closeoverlay = false;


    /**** moving the download icons right side ****/
    /* bay grid */
    var bayXls = document.getElementById( "jqGridBayXlsButton" );
    var bayCsv = document.getElementById( "jqGridBayCsvButton" );
    var bayPageInfo= $('td#jqGridBayPager_right .ui-paging-info');
    $('td#jqGridBayPager_right').append('<div id="rightDownloadBtn"></div>');
    $('td#jqGridBayPager_right #rightDownloadBtn').append(bayXls).append(bayCsv).append(bayPageInfo);;

    /* bayLocation grid */
    var bayLocXls = document.getElementById( "jqGridBayLocationXlsButton" );
    var bayLocCsv = document.getElementById( "jqGridBayLocationCsvButton" );
    var bayLocPageInfo= $('td#jqGridBayLocationPager_right .ui-paging-info');
    $('td#jqGridBayLocationPager_right').append('<div id="rightDownloadBtn"></div>');
    $('td#jqGridBayLocationPager_right #rightDownloadBtn').append(bayLocXls).append(bayLocCsv).append(bayLocPageInfo);

    /* branch grid */
    var BranchesXls = document.getElementById( "jqGridBranchesXlsButton" );
    var BranchesCsv = document.getElementById( "jqGridBranchesCsvButton" );
    var BranchesInfo= $('td#jqGridBranchesPager_right .ui-paging-info');
    $('td#jqGridBranchesPager_right').append('<div id="rightDownloadBtn"></div>');
    $('td#jqGridBranchesPager_right #rightDownloadBtn').append(BranchesXls).append(BranchesCsv).append(BranchesInfo);


    /* region grid */
    var regionXls = document.getElementById( "jqGridRegionXlsButton" );
    var regionCsv = document.getElementById( "jqGridRegionCsvButton" );
    var regionInfo= $('td#jqGridRegionPager_right .ui-paging-info');
    $('td#jqGridRegionPager_right').append('<div id="rightDownloadBtn"></div>');
    $('td#jqGridRegionPager_right #rightDownloadBtn').append(regionXls).append(regionCsv).append(regionInfo);

    /* Station grid */
    var stationXls = document.getElementById( "jqGridStationXlsButton" );
    var stationCsv = document.getElementById( "jqGridStationCsvButton" );
    var stationInfo= $('td#jqGridStationPager_right .ui-paging-info');
    $('td#jqGridStationPager_right').append('<div id="rightDownloadBtn"></div>');
    $('td#jqGridStationPager_right #rightDownloadBtn').append(stationXls).append(stationCsv).append(stationInfo);

    /* host grid */
    var hostXls = document.getElementById( "jqGridHostXlsButton" );
    var hostCsv = document.getElementById( "jqGridHostCsvButton" );
    var hostInfo= $('td#jqGridHostPager_right .ui-paging-info');
    $('td#jqGridHostPager_right').append('<div id="rightDownloadBtn"></div>');
    $('td#jqGridHostPager_right #rightDownloadBtn').append(hostXls).append(hostCsv).append(hostInfo);

    $("div.ui-jqgrid-view")
        .children("div.ui-jqgrid-titlebar")
        .css("text-align", "center")
        .children("span.ui-jqgrid-title")
        .css("float", "none");
});

$(document).on("change","#MasterOrPlayer", function (e) {
    var urlParameter = window.location.pathname;
    var vars = urlParameter.split("/");
    var Id;
    id = '#BayId';
    if (vars[2] == 'bay'){
        id = '#BayLocationBayId';
    }
    if($("#MasterOrPlayer").val() == 'P')
    {
        updateMasterName($(id).val(), false);
    } else {
        $('#HostMasterName').prop('readonly', false);
        $('#HostMasterName').val('');
    }
});

function afterSubmitEvent(response, postdata)
{

    /*** show loader ***/
    $('#load_jqGridBay').hide();

    //Parses the JSON response that comes from the server.
    result = JSON.parse(response.responseText);
    //result.success is a boolean value, if true the process continues,
    //if false an error message appear and all other processing is stopped,
    //result.message is ignored if result.success is true.

    if(result.status === 422) {
        $( '#form-success' ).css('display', 'none');
        $errors = result.errors;
        $.each( $errors, function( key, value ) {
            //errorTextFormat(value);
            return value;
        });

    } else {
        $( '#form-success' ).css('display', 'block');
        errorsHtml = '<div class="alert alert-success" id="success-alert" style="display: block">';
        errorsHtml += result.message; //showing only the first error.
        errorsHtml += '</div>';

        $( '#form-success' ).html( errorsHtml );

        console.log(result.redirect);
        if(result.redirect == true)
        {
            urlParameter = window.location.pathname;
            console.log(urlParameter);
            var vars = urlParameter.split("/");
            console.log("URLS "+vars);
            var baycode = vars[6], stationCode= vars[5], appCode = vars[7], statusCode = vars[8];
            window.location="/bay/"+stationCode+'/'+baycode+'/'+appCode+'/'+statusCode;
            //window.location="/bay/bayId/"+result.bayId;

            return [result.success, result.message];
        }
        setTimeout(function(){ $('.alert-success').fadeOut(2000) }, 1000);

        return [result.success, result.message];
    }

}

function updateMasterName(bayId, setselected) {
    console.log(bayId)
    $.ajax({

        url: '/playerinfo/'+bayId,
        type: "GET",
        success: function (data) {
            var sn = data;
            console.log(data)
            $("#HostMasterName").val(sn);
            $('#HostMasterName').prop('readonly', true);

        }
    });
}


function createLink(cellvalue, options, rowObject) {
    console.log("create link"+rowObject.BayId);
}


function findAllStationByBranch(cellvalue, options, rowObject) {
    return "<a href='station/branch/"+rowObject.BranchId+"'>"+cellvalue+"</a>";
}

function linksToRegion(cellvalue, options, rowObject) {
    return "<a href='station/region/"+rowObject.regId_sum+"'>"+cellvalue+"</a>";
}

function homeGridComplete() {
    var x = $('.jqGridHomeghead_0');
    var branchName, branchValue, domMani, spanEle;
    x.each(function(index,value) {
        domMani = $(value).find('td:first');
        spanEle = $(domMani).html();
        branchName = domMani.text();
        spanEle = spanEle.replace(branchName, '');
        branchValue = $(value).next().find('td[aria-describedby="jqGridHome_branchId_sum"]').text();
        domMani.html(spanEle+' <a href="station/branch/'+branchValue+'">'+branchName+'</a>');
    });

    pivotHomeTableCalculate();
    pivotHomeTableRowSpan();
    dynamicHeight(true);
}

function linksToBranch(cellvalue, options, rowObject) {
    return cellvalue;
}

function noOfStations(cellvalue, options, rowObject){
    console.log(rowObject);
    return '<a href="/station/regionHome/'+rowObject.regId_sum+'/branch/'+rowObject.branchId_sum+'">'+rowObject.stations_sum+'</a>';

}

function noOfBays(cellvalue, options, rowObject){
    return '<a href="/bay/region/'+rowObject.regId_sum+'/status/A/branch/'+rowObject.branchId_sum+'">'+rowObject.bay_sum+'</a>';

}

function noOfHosts(cellvalue, options, rowObject){
    return '<a href="/host/region/'+rowObject.regId_sum+'/status/A/branch/'+rowObject.branchId_sum+'">'+rowObject.host_sum+'</a>';

}


function statusDisplay(cellvalue, options, rowObject){
    console.log(cellvalue);
    if(cellvalue == 'A') {
        cellvalue = 'Active';
    }
    else if(cellvalue == 'I'){
        cellvalue = 'Inactive';
    }
    else if(cellvalue == 'D'){
        cellvalue = 'Delete';
    }
    return cellvalue;
}

var errorTextFormat = function (response) {
    $('.FormError.ui-state-error').text(response).show();

    $(".FormError.ui-state-error").fadeTo(2000, 1000).slideUp(1000, function(){
    });
}

var gridCompleteEvent = function () {

    $('.ui-search-input input').attr("placeholder", "Search");
    $('grid_selector').trigger("reloadGrid");
    $("#jqGridBranchesXlsButton").attr("title", "Download Xls");
    $("#jqGridBranchesCsvButton").attr("title", "Download Csv");
    $("#jqGridRegionXlsButton").attr("title", "Download Xls");
    $("#jqGridRegionCsvButton").attr("title", "Download Csv");
    $("#jqGridStationXlsButton").attr("title", "Download Xls");
    $("#jqGridStationCsvButton").attr("title", "Download Csv");
    $("#jqGridBayLocationXlsButton").attr("title", "Download Xls");
    $("#jqGridBayLocationCsvButton").attr("title", "Download Csv");
    $("#jqGridBayXlsButton").attr("title", "Download Xls");
    $("#jqGridBayCsvButton").attr("title", "Download Csv");
    $("#jqGridHostXlsButton").attr("title", "Download Xls");
    $("#jqGridHostCsvButton").attr("title", "Download Csv");

    $('.ui-icon.ui-icon-pin-s').attr("title", "Pin Grid");
    $(".ui-icon.ui-icon-refresh").attr("title", "Refresh Grid");
}

var refreshPagewithcount = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_BranchName', 'jqGridBranches');
}


var gridWithAutoSearch = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_RegionName', 'jqGridRegion');
}

var stationAutoComplete = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_StationName', 'jqGridStation');
    searchAutoComplete('#gs_BranchName', 'jqGridStation');
    searchAutoComplete('#gs_RegionName', 'jqGridStation');
    searchAutoComplete('#gs_StateName', 'jqGridStation');

    $("#jqGridStation tbody tr").each(function(){
        var branch = $(this).find('td[aria-describedby*="jqGridStation_BranchName"]');
        //lert(branch.text().formatToUpperCase());
    });
}


var readOnlyColumn = function() {
    gridCompleteEvent();
    searchAutoComplete('#gs_BayLocationCode', 'jqGridBayLocation');
    searchAutoComplete('#gs_BayLocationDesc', 'jqGridBayLocation');
}


var addButtonHostEvent = function()  {
    accessPermissionHost();

    var urlParameter = window.location.pathname;
    console.log(urlParameter);
    var vars = urlParameter.split("/");
    console.log(vars[2]);

    if (vars[1]== 'bay' && vars[2] == 'bayId') {
        $('#gsh_jqGridBay_StationName .clearsearchclass').click(
            function() {
                window.location ='/bay'
            });


        $('#gsh_jqGridBay_StationName #gs_StationName').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                $(this).val('');
                window.location ='/bay'
            }
        });
    }


    gridCompleteEvent();
    var gridObj, ids;
    gridObj = jQuery('#jqGridBay');
    ids = jQuery('#jqGridBay').jqGrid('getDataIDs');
    console.log(ids);
    for(var i=0;i < ids.length;i++){
        var cl = ids[i];
        be = "<input class='add_cell_btn' style='height:30px;width:100px;' type='button' value='Add Host' onclick=\"jQuery('#add_jqGridHost').saveRow('"+cl+"');\"  />";
        gridObj.jqGrid('setRowData',ids[i],{addHost:be});
    }

    $('.ui-search-input #gs_actions').hide();
    $('.ui-search-clear .clearsearchclass').hide();

    searchAutoComplete('#gs_StationName', 'jqGridBay');
    searchAutoComplete('#gs_BayLocationDesc', 'jqGridBay');
    searchAutoComplete('#gs_App', 'jqGridBay');


    $("#jqGridBay tbody tr").each(function(){
        var startTime = $(this).find('td[aria-describedby*="jqGridBay_StartTime"]');
        var endTime = $(this).find('td[aria-describedby*="jqGridBay_EndTIme"]');
        var srartvalue= startTime.text();
        var endTimeValue = endTime.text();
        if(srartvalue.length == 8 && endTimeValue.length == 8) {
            srartvalue = srartvalue.slice(0, -3);
            endTimeValue = endTimeValue.slice(0, -3);
            startTime.text(srartvalue);
            endTime.text(endTimeValue);
        }
        else {
            console.log('m in else if');
            srartvalue = "00:00";
            endTimeValue = "00:00";
            startTime.text(srartvalue);
            endTime.text(endTimeValue);
        }

    });


    console.log("URLS "+window.location.href);
    var urlMatch = window.location.href;
//if(urlMatch.match('/Station_|Station/gi')) {

    getReturnURL = function (id, value){

        var url = window.location.pathname;
        var vars = url.split("/");
        var returl = "/bay";

        if (vars.length == 6)
        {
            $('#'+id).val('');
            var  stationCode= vars[2], baycode = vars[3], appCode = vars[4], statusCode = vars[5];
            if (value == 'Station') {
                stationCode = value;
            } else if (value == 'BayLoc') {
                baycode = value;
            } else if (value == 'App') {
                appCode = value;
            } else if (value == 'Status') {
                statusCode = value;
            }
            var returl ="/bay/"+stationCode+"/"+baycode+"/"+appCode+"/"+statusCode;
        }
        return returl;
    }

    $('#gsh_jqGridBay_StationName .ui-search-clear').click(
        function() {

            var returl = getReturnURL('gs_StationName', 'Station');
            window.location = returl;
        });

    $('#gsh_jqGridBay_BayLocationDesc .ui-search-clear').click(
        function() {
            //alert('1234');
            var returl = getReturnURL('gs_BayLocationDesc', 'BayLoc');

            window.location = returl;
        });

    $('#gsh_jqGridBay_App .ui-search-clear').click(
        function() {
            var returl = getReturnURL('gs_App', 'App');
            window.location = returl;
        });

    $('#gsh_jqGridBay_Bay\\.Status .ui-search-clear').click(
        function() {

            var returl = getReturnURL('Bay.Status', 'Status');
            window.location = returl;
        });
//}
}


function addHomeRegionLink (cellvalue, options, rowObject) {
    return '<a href="/station/region/'+rowObject.regId+'">'+rowObject.regName+'</a>';
}

var hostFunctionAfterGridComplete = function() {
    gridCompleteEvent();
    $('.ui-icon-plus').hide();
    $('.ui-icon-trash').hide();
    $('#tr_BayId').hide();
    console.log($('#StationName').val());


    var stationNameObj, bayLocationObj,locationid, stationNameVal, bayLocationVal, locationval, urlParameter = window.location.pathname;
    var vars = urlParameter.split("/");

    if (vars[2] == 'bay') {
        $('.ui-icon-plus').click();
        $('#tr_BayId').hide();
        stationNameObj = $('#StationName');
        stationNameVal = stationNameObj.attr('value');
        stationNameObj.val(stationNameVal);

        appNameObj = $('#App');
        appNameVal = appNameObj.attr('value');
        appNameObj.val(appNameVal);

        bayLocationObj = $('#BayLocation');
        bayLocationVal = bayLocationObj.attr('value');
        bayLocationObj.val(bayLocationVal);

        locationid = $('#BayLocationBayId');
        locationval = locationid.attr('value');
        locationid.val(locationval);

        dropdownForVMC();
    }



    if (vars[1]== 'host' && vars[2] == 'bay') {
        var baycode = vars[6], stationCode= vars[5], appCode = vars[7], statusCode = vars[8];
        $('#cData').click(function(){

            window.location='/bay/'+stationCode+'/'+baycode+'/'+appCode+'/'+statusCode;
            //window.location='/bay/bayId/'+locationval;

        });

        $('.ui-jqdialog-titlebar-close').click(function(){
            window.location='/bay/'+stationCode+'/'+baycode+'/'+appCode+'/'+statusCode;
            //window.location='/bay/bayId/'+locationval;
        });
    }

    searchAutoComplete('#gs_StationName', 'jqGridHost');
    searchAutoComplete('#gs_BayLocationCode', 'jqGridHost');
    searchAutoComplete('#gs_HostName', 'jqGridHost');
    searchAutoComplete('#gs_HostMasterName', 'jqGridHost');
    searchAutoComplete('#gs_HostCounterType', 'jqGridHost');
    searchAutoComplete('#gs_CounterNumber', 'jqGridHost');
    searchAutoComplete('#gs_BranchName', 'jqGridHost');
    searchAutoComplete('#gs_RegionName', 'jqGridHost');

    searchAutoComplete('#gs_Host.StartTime', 'jqGridHost');
    searchAutoComplete('#gs_App', 'jqGridHost');
    searchAutoComplete('#gs_Host.EndTIme', 'jqGridHost');

    $("#jqGridHost tbody tr").each(function(){
        var startTime = $(this).find('td[aria-describedby*="jqGridHost_Host.StartTime"]');
        var endTime = $(this).find('td[aria-describedby*="jqGridHost_Host.EndTIme"]');
        var srartvalue= startTime.text();
        var endTimeValue = endTime.text();
        if(srartvalue.length == 8 && endTimeValue.length == 8) {
            srartvalue = srartvalue.slice(0, -3);
            endTimeValue = endTimeValue.slice(0, -3)
            startTime.text(srartvalue);
            endTime.text(endTimeValue);
        }

    });


}


function dropdownForVMC() {
    var app = $('#App').val().trim();
    if(app === 'VMC') {
        console.log($('#App').val());
        var myOptions = {
            ''  : 'Select',
            'M' : 'Master',
            'P' : 'Player'
        };
        $('#MasterOrPlayer').empty();
        var mySelect = $('#MasterOrPlayer');
        $.each(myOptions, function(val, text) {
            mySelect.append(
                $('<option></option>').val(val).html(text)
            );
        });
    }
}

function editDropdownFOrVMC(selected) {
    var app = $('#App').val().trim();
    if(app === 'VMC') {
        $("option[value='B']").remove();
    }
}



var afterShowFormFunction = function() {
    console.log('form after show');
}

/* Make the Actions column as
 * hidden in jQgrid colmodel object
 * and prevent from export
 **/
function bayAddHostExportFix() {

    $("#jqGridBayXlsButton, #jqGridBayCsvButton").hover(
        function() {
            colmodelData = jQuery("#jqGridBay").getGridParam("colModel");
            colmodelData[8].hidden=true;
        }, function() {
            colmodelData = jQuery("#jqGridBay").getGridParam("colModel");
            colmodelData[8].hidden=false;
        }
    );
}


//autocomplete for search
function searchAutoComplete(colName, gridName)
{

    var sourceUrl, col;
    if(gridName === "jqGridBranches") {
        col = colName.substring(4);
        sourceUrl = "/search/branchAutocomplete/"+col;
    }
    else if(gridName === 'jqGridRegion'){
        col = colName.substring(4);
        sourceUrl = "/search/regionAutocomplete/"+col;
    }
    else if(gridName === 'jqGridStation'){
        col = colName.substring(4);
        sourceUrl = "/search/stationAutocomplete/"+col;
    }
    else if(gridName === 'jqGridBayLocation'){
        col = colName.substring(4);
        sourceUrl = "/search/bayLocationAutocomplete/"+col;
    }
    else if(gridName === 'jqGridBay'){
        col = colName.substring(4);
        sourceUrl = "/search/bayAutocomplete/"+col;
    }
    else if(gridName === 'jqGridHost'){
        col = colName.substring(4);
        sourceUrl = "/search/hostAutocomplete/"+col;
        if (col == 'Host.StartTime')
        {
            col = 'StartTime';
            sourceUrl = "/search/hostAutocomplete/"+col;
            colName = '#gs_Host\\.StartTime';
        }
        if (col == 'Host.EndTIme')
        {
            col = 'EndTIme';
            sourceUrl = "/search/hostAutocomplete/"+col;
            colName = '#gs_Host\\.EndTIme';
        }

    }

    $(colName).autocomplete({
        source: sourceUrl,
        minLength: 2,
        select: function(event, ui) {
            $(colName).val(ui.item.value);
            var e = jQuery.Event("keypress");
            e.which = 13; // # Some key code value
            e.keyCode = 13;
            $(colName).trigger(e);
        }
    });
}

var disableInactiveAddHosts = function () {
    var row = $('.jqgrow');
    $.each(row, function(index,value){
        txt = $('> td:eq(7)', this).text();
        if(txt == 'Inactive') {
            $(this).parent();
            $('> td:eq(8) > a > input', this).attr('disabled', true).css('color', 'red');
        }
        //Search field values
        var searchStationkey = 'Station';
        var searchBaykey = 'BayLoc';
        var searchAppkey = 'App';
        var searchStatuskey = 'Status';

        var searchStationVal= $('#gs_StationName').val();
        if(searchStationVal != "") {
            searchStationVal = "_"+searchStationVal;
            searchStationkey = searchStationkey.concat(searchStationVal);
        }

        var searchBayVal= $('#gs_BayLocationDesc').val();
        if(searchBayVal != "") {
            searchBayVal = "_"+searchBayVal;
            searchBaykey = searchBaykey.concat(searchBayVal);
        }

        var searchAppVal= $('#gs_App').val();
        if(searchAppVal != "") {
            searchAppVal = "_"+searchAppVal;
            searchAppkey = searchAppkey.concat(searchAppVal);
        }

        var searchStatusOption = document.getElementById('gs_Bay.Status');
        var searchStatusVal= searchStatusOption.options[searchStatusOption.selectedIndex].value;
        if(searchStatusVal != "") {
            searchStatusVal = "_"+searchStatusVal;
            searchStatuskey = searchStatuskey.concat(searchStatusVal);
        }

        /*console.log(searchStationkey);
         searchStationVal = $('#gs_StationName').val();
         searchBayCode = $('#gs_BayLocationDesc').val();
         searchApp = $('#gs_App').val();
         var searchStatusOption = document.getElementById('gs_Bay.Status');
         searchStatus= searchStatusOption.options[searchStatusOption.selectedIndex].value;*/


        var href=$('> td:eq(8) > a', this).attr('href')+'/'+searchStationkey+'/'+searchBaykey+'/'+searchAppkey+'/'+searchStatuskey;
        $('> td:eq(8) > a', this).attr('href',href);
    });
    dynamicHeight();

}

// HOME DASHBOARD CALCULATE TOTAL COUNTS
function pivotHomeTableCalculate() {
    // List of columns for count operation
    var columns = [4, 5, 6];

    // Iterate through the columns list
    $.each(columns, function(index, value) {
        var countArr = [], count = 0; loopCount = 0;
        // Iterate through the grid table rows
        $("#jqGridHome tr").each(function() {
            loopCount++;
            // Skip the first two empty rows
            if(loopCount < 3) return true;
            content = $(this).find('td:eq('+value+')').text();
            // If digits
            if(content.match(/^\d+$/) != null) {
                count = count + parseInt(content);
            }
            // If non-digits
            if (content.match(/^\d+$/) == null) {
                // Push to array
                countArr.push(count);
                // clear count
                count = 0;
            }
        });

        // Push the last count to array
        countArr.push(count);
        // Clear loopCount
        loopCount = 0;

        // Put the collected(array) data to the grid
        $("#jqGridHome tr.jqgroup").each(function () {
            $(this).find('td:eq('+value+')').text('Total Count : ' + countArr[loopCount]).css({'text-align':'center','font-weight':'normal','color':'#156F86'});
            loopCount++;
        });
    });
// Regions count
    homeGridRegionCount();
}

function homeGridRegionCount() {
    var countArr = [], count = 0; loopCount = 0;
    // Iterate through the grid table rows
    $("#jqGridHome tr").each(function() {
        loopCount++;
        // Skip the first two empty rows
        if(loopCount < 3) return true;
        content = $(this).find('td:eq(1)').text();
        // If content of td is not null
        if(content.length > 2) {
            count++
        }
        if (content.length < 2) {
            // Push to array
            countArr.push(count);
            // clear count
            count = 0;
        }
    });

    // Push the last count to array
    countArr.push(count);
    // Clear loopCount
    loopCount = 0;

    // Put the collected(array) data to the grid
    $("#jqGridHome tr.jqgroup").each(function () {
        $(this).find('td:eq(1)').text('Total Count : ' + countArr[loopCount]).css({'text-align':'center','font-weight':'normal','color':'#156F86'});
        loopCount++;
    });
}

// HOME DASHBOARD PIVOT TABLE ROWSPAN
function pivotHomeTableRowSpan() {
    $("#jqGridHome tr").each(function(){
        cell = $(this).find("td:first");
        if(cell.text().length == 0)
            cell.css('border-bottom', 'none');
        x = cell.text();
        if(x.length > 0) {
            $(this).prev().find("td:first").css('border-bottom', '1px solid #449FB7');
        }
    });
    $('#jqGridHome tr:last').find("td:first").css('border-bottom', '1px solid #449FB7');
}

// SET JQGRID HEIGHT DYNAMICALLY
function dynamicHeight(home) {

    /* routing to vmc edit page on each row */
    $('.ui-icon-pencil').bind('click',function(){
        var rowId = $('.ui-state-highlight').attr('id');
        if(rowId){
            //window.location = '/vmc-config/edit/'+rowId;
        }
    });


    if (home == true) {
        bDivHeight = 127;
    } else {
        bDivHeight = 177;
    }
    var bdiv_height = $(window).height()- bDivHeight;
    var view_height = $(window).height() - 50;
    $('.ui-jqgrid-bdiv').css('height', bdiv_height+'px');
    $('.ui-jqgrid-view').css('height', view_height+'px');
}

// SET JQGRID HEIGHT DYNAMICALLY(Branch, Home and Region dashboards)
function dynamicHeightBR() {
    var bdiv_height = $(window).height()- 220;
    var view_height = $(window).height() - 90;
    $('.ui-jqgrid-bdiv').css('height', bdiv_height+'px');
    $('.ui-jqgrid-view').css('height', view_height+'px');
}

// Select Dropdown Option formatter
$.fn.formatToUpperCase = function() {
    $(this).each(function() {
        string = $(this).text();
        formatString = string.charAt(0).toUpperCase() + string.slice(1);
        // alert(formatString);
        $(this).html(formatString);
    });
}

// Select Dropdown Option Sorter
$.fn.sortOptions = function(defaultVal){
    $(this).each(function(){
        var op = $(this).children("option");
        op.sort(function(a, b) {
            return a.text.toUpperCase() > b.text.toUpperCase() ? 1 : -1;
        });
        return $(this).empty().append(op);
    });
    $(this).val(defaultVal);
}

function getHostTime(host) {
    console.log(host);
    $.ajax({
        url: '/getHostId/'+host,
        type: "GET",
        success: function (data) {
            var start = data.StartTime.slice(0, -3);
            var end =data.EndTIme.slice(0, -3);
            $('#v_StartTime').text(start);
            $('#v_EndTIme').text(end);
        }
    });
}

function getHostInfo(host) {
    console.log(host);
    $.ajax({
        url: '/getHostId/'+host,
        type: "GET",
        success: function (data) {
            getBayLocation(data.BayId, data.HostId);
        }
    });
}

//Customer-Commitment CRUD
function updateDeleteValue(dataVariable, url) {
    var id, redirectUrl = url, message;
    $('.del-btn').click(function() {
        if(dataVariable == 'industryid') {
            message = "Are you sure want to delete all the Industry info and associated Advertisers ?";
        }
        else {
            message = "Are you sure want to delete?";
        }
        id = $(this).data(dataVariable);
        bootbox.confirm(message, function(result) {
            if(result == true) {
                $.ajax({type: "DELETE", url: redirectUrl+id,
                    success: function(result){
                        location.reload();
                    }});
            }
        });
    });
}

//Status active inactive display
function changeStatusValue(tableid) {
    $("table#"+tableid+" tr td#status").each(function(index,value){
        if($(value).text() == 'I') {
            $(value).text('Inactive');
        } else {
            $(value).text('Active');
        }
    });
}

function changeIspayingValue (tableid) {
    $("table#"+tableid+" tr td#isPaying").each(function(index,value){
        if($(value).text() == 'Y') {
            $(value).text('Yes');
        } else {
            $(value).text('No');
        }
    });
}

/*Yes or no toggle btn*/
function enableYesOption(){
    $('#ispaying-no').hide();
    // $('#payingWrapper .payingNoBtn').removeClass('payingActive').addClass('color-gray');
    // $('#payingWrapper .payingYesBtn').addClass('payingActive');
}
function enableNoOption(){
    $('#ispaying-yes').hide();
    // $('#payingWrapper .payingYesBtn').removeClass('payingActive').addClass('color-gray');
    // $('#payingWrapper .payingNoBtn').addClass('payingActive');
}

/*checkBox Enable in roles grid*/
function selectAllCheckbox(name,status){
    if(status.checked==true){
        $('.'+name+' '+'.panel-body ul li.checkbox input').prop('checked', true);
    }else{
        $('.'+name+' '+'.panel-body ul li.checkbox input').prop('checked', false);
    }
}

function plusIconHide(wrapper){
    $('.'+wrapper+' '+'.plus-icon').hide();
    $('.'+wrapper+' '+'.minus-icon').show();
}

function minusIconHide(wrapper){
    $('.'+wrapper+' '+'.plus-icon').show();
    $('.'+wrapper+' '+'.minus-icon').hide();
}


