/*** Created by Chethan JN on 16-06-2016. ***/
jQuery(function () {
        $('#po-step,#campaign-step').addClass('rightmark');/* by default add right mark for po and campaign form */
    formBinding();
    $('#campaignbtn,#creativebtn, #locationbtn, #previewsavebtn, #addcampaignbtn')
    .click(function(event) {   /* Updation of Purchaseorder Flow */
          var buttonClicked = $(this).attr("id");
          if (buttonClicked != 'addcampaignbtn')
          {   
                if($('#formurl').val() !='/addcampaign' && $('#stepsComplete').val() == 1 )
                {
                $('#stepsComplete').val(0);  
                var message = "Click on Save&Exit makes Campaign as Partial";
                bootbox.confirm(message, function(result) 
                {
                    if(result == true) 
                    {
                    formSubmit(buttonClicked);
                    }
                }); 
                }
                else
                {
                    formSubmit(buttonClicked);
                }
         }
        else 
        {
            formSubmit(buttonClicked);
        }
        event.preventDefault();
        
    });//Click Function End

    $('#posubmitbtn').click(function(event) {
        var buttonClicked = $(this).attr("id");
        emptyAllErros();  /* empty all errors  */
        $('#stepsComplete').val(1);
        goToPreviewWithLocationData(); /* binding location data to preview */
        var sendData = setpostdata();
        var povalue = $('#purchaseorder_id').val();
        var locationVal = $.trim($('#po-location').val());
        var locationValue = $.trim(locationVal.slice(1, -1));
        $("#purchaseorderform").valid(); /* client side validations */
        if($("#purchaseorderform").valid()){
            checkAdvertiser($('#advertiser').val()).done(function (data) {
                if (data.result == 'fail'){
                    displayPOFormValaidationMessage('The Selected Advertiser has No Creatives !');
                    emptyAllErros(); /* close alert dilogs */
                }
                if (data.result == 'success'){
                    if(locationValue==""){
                        displayLocationFormValaidationMessage('Please Select Location');
                        emptyAllErros(); /* close alert dilogs */
                    }
                    else if(!$('#creativeValues').val().length > 0){
                        displayCreativeFormValaidationMessage('Please Select Creative');
                        emptyAllErros(); /* close alert dilogs */
                    }else{
                        if($('#creativeValues').val().length > 0){
                            checkCreative(creativeIds).done(function(data){
                                if (data.result == 'fail'){
                                    displayCreativeFormValaidationMessage('Please Add only Active Creatives to the Campaign');
                                    emptyAllErros(); /* close alert dilogs */
                                }
                                if (data.result == 'success'){
                                    ajaxcall(sendData, povalue, buttonClicked);
                                }
                            });
                        }
                    }
                }
            });//Advertiser Check Ajax End
        }
        event.preventDefault();
    });
    /*$('#addcampaignbtn').click(function(){
        var returnURL = '/purchaseorder/'+$('#pocampaignid').val()+'/add/campaign';
        redirectFn(returnURL);
    });*/
    //po-form navigation in edit
    showCampaignForm();
});

function formSubmit(buttonClicked)
{ 
        emptyAllErros(); /* empty all errors  */
        goToPreviewWithLocationData(); /* binding location data to preview */
        var sendData = setpostdata(),povalue = $('#purchaseorder_id').val(),locationVal = $.trim($('#po-location').val()),
        locationValue = $.trim(locationVal.slice(1, -1));
        $("#purchaseorderform").valid(); /* client side validation */
        if($("#purchaseorderform").valid()){
        checkAdvertiser($('#advertiser').val()).done(function (data) {
        if (data.result == 'fail'){
        activeCampaign = false;
        validation = false;
        displayPOFormValaidationMessage('The Selected Advertiser has No Creatives !');
        emptyAllErros(); /* close alert dilogs */
        }
        if (data.result == 'success'){
            emptyAllErros(); /* empty all errors */
            if($('#creativeValues').val().length > 0){
                checkCreative(creativeIds).done(function (data){
                    if (data.result == 'fail'){
                    displayCreativeFormValaidationMessage('Please Add only Active Creatives to the Campaign');
                    emptyAllErros(); /* close alert dilogs */
                    }
                    if (data.result == 'success'){
                        emptyAllErros(); /* empty all errors */
                        submitData(locationValue, buttonClicked, povalue);
                    }
                });
            } else {
                emptyAllErros();
                submitData(locationValue, buttonClicked, povalue);
            }
        }
        });
        }
}

function ajaxcall(sendData,povalue,buttonclicked){
    emptyAllErros(); /* empty all errors */
    var returnURL = '/purchaseorder';
    $.ajax({
        type        : 'PUT',
        url         : '/purchaseorder/'+povalue,
        data        :  sendData,
        dataType    : 'json'
    }).done(function(data){
        if (data.result == 'fail'){
            $.each( data.errors, function( key, value){displayValidationMessageInParticularPage(value);});
        }
        if (data.result == 'success'){
             if(buttonclicked == 'addcampaignbtn'){
                returnURL = '/purchaseorder/'+data.obj.POCampaignId+'/add/campaign';
             }
            redirectFn(returnURL);
        }
    });
}
function setpostdata(){
    var formData = {
        'stepsComplete'         : $('#stepsComplete').val(),
        'POId'                  : $('#purchaseorder_id').val(),
        'POCampaignId'          : $('#pocampaignid').val(),
        'creativeCampaignId'    : $('#creativecampaignid').val(),
        'locationCampaignId'    : $('#locationcampaignid').val(),
        'originalCampaignId'    : $('#originalcampaignid').val(),
        'originalPOId'          : $('#originalpoid').val(),
        'poStatus'              : $('#postatus').val(),
        'salesrep'              : $('#salesrep').val(),
        'poDate': moment($('input[name=purchase_order_date]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'startDate': moment($('input[name=startDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'endDate': moment($('input[name=endDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'advertiser'            : $('#advertiser').val(),
        'branch'                : $('#branch').val(),
        'roReference'           : $('#roreference').val(),
        'campaignId'            : $('#campaignid').val(),
        'campaignName'          : $('#CompaignName').val(),
        'campaignStartDate':  moment($('input[name=start_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'campaignEndDate':  moment($('input[name=end_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'spotsPerDay'           : $('#SpotsPerDay').val(),
        'oldCreatives'          : $('#oldcreativeValues').val(),
        'creativeValues'        : $('#creativeValues').val(),
        'oldLocation'           : $('#oldlocation').val(),
        'location'              : $('#po-location').val(),
        'editPOCampaignStatus'  : $('#editpocampaignstatus').val(),
        'locationDifference'    : $('#locationDifference').val(),
        'mode'                  : $('#mode').val()
    };
    $('#postjsondata').val(JSON.stringify(formData));
    var sendData = {'sendData':$('#postjsondata').val()};
    return sendData;
}

function addAnotherCampaign(locationValue, buttonClicked, povalue)
{
    if(locationValue=="") {
    bootbox.confirm(global_CampaignAlertMessage, function(result) {
    if(result == true) {
        var sendData = setpostdata();
        ajaxcall(sendData, povalue, buttonClicked); 
    }
    });
    } else {
        $('#stepsComplete').val(1);
        var sendData = setpostdata();
        ajaxcall(sendData, povalue, buttonClicked);
    } 
}

function submitData(locationValue, buttonClicked, povalue)
{
    if (buttonClicked == "addcampaignbtn") {
        addAnotherCampaign(locationValue, buttonClicked, povalue);
    } else {
        var sendData = setpostdata();
        ajaxcall(sendData, povalue, buttonClicked);
    }
}

