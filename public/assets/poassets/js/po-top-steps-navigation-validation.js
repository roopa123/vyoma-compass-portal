/**
 * Created by Chethan JN on 08-07-2016.
 */


$('#po-step').click(function(){
    $('#location-step').removeClass('rightmark');
    //$("#purchaseorderform").valid(); /* client side validation */
    //if($("#purchaseorderform").valid()){
        goToPreviewWithLocationData();
        if(creativeIds.length > 0){
            checkCreative(creativeIds).done(function (data){
                if (data.result == 'fail')
                {
                    $('#creative-step').removeClass('rightmark');
                }
            });
        }
        if(finalArrayForSubmit.length > 0)
        {
            $('#location-step').addClass('rightmark');
        }
        $('.setup-content').not('#step-1').hide();
        $('#step-1').show();
        $(this).addClass('rightmark');
    //}
});
$('#campaign-step').click(function(){
    $('#location-step').removeClass('rightmark');
    $("#purchaseorderform").valid(); /* client side validation */
    if($("#purchaseorderform").valid()){
        goToPreviewWithLocationData();
        if(creativeIds.length > 0){
            checkCreative(creativeIds).done(function (data){
                if (data.result == 'fail')
                {
                    $('#creative-step').removeClass('rightmark');
                }
            });
        }
        if(finalArrayForSubmit.length > 0)
        {
            $('#location-step').addClass('rightmark');
        }
        $('.setup-content').not('#step-2').hide();
        $('#step-2').show();
       /* $(this).addClass('btn-default rightmark');
        $('#preview-step').removeAttr('disabled').addClass('btn-primary');*/
    }
});
$('#creative-step').click(function(){
    $("#purchaseorderform").valid(); /* client side validation */
    if($("#purchaseorderform").valid()){
        goToPreviewWithLocationData();
        $('#location-step').removeClass('rightmark');
        getCreative($("#advertiser>option:selected").val()); /* load creatives based on advertiser */
        if(creativeIds.length > 0) {
            submitAllSeletedCreativeSelection();
        } /* LOAD creative grid based on creatives */
        if(finalArrayForSubmit.length > 0)
        {
            $('#location-step').addClass('rightmark');
        }
        $('.setup-content').not('#step-3').hide();
        $('#step-3').show();
    }
});
$('#location-step').click(function(){
    goToPreviewWithLocationData();
    $("#purchaseorderform").valid(); /* client side validation */
    if($("#purchaseorderform").valid()){
        if(creativeIds.length > 0){
            checkCreative(creativeIds).done(function (data){
                if (data.result == 'fail'){
                    $('#creative-step').removeClass('rightmark');
                }
            });
            $('#creative-step').addClass('rightmark');
        }else{
            $('#creative-step').removeClass('rightmark');
        }
        $('.setup-content').not('#step-4').hide();
        $('#step-4').show();
    }
});
$('#preview-step').click(function(){
    $("#purchaseorderform").valid(); /* client side validation */
    if($("#purchaseorderform").valid()){
        $('#location-step').removeClass('rightmark');
        if(creativeIds.length > 0){
            checkCreative(creativeIds).done(function (data){
                if (data.result == 'fail')                {
                    $('#creative-step').removeClass('rightmark');
                }else{
                    $('#creative-step').addClass('rightmark');
                }
            });
        }
        if(finalArrayForSubmit.length > 0)        {
            $('#location-step').addClass('rightmark');
        }
        $('.setup-content').not('#step-5').hide();
        $('#step-5').show();
    }
});