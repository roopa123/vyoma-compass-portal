/**
 * Created by Chethan JN on 10-06-2016.
 */
var creativeIds=[];
var creativeIdAndName=[];

columnIndexForGridBasedOnTableIdAndColumnName = function (grid, columnName) {
    var cm = jQuery(grid).jqGrid('getGridParam', 'colModel'), i, l = cm.length;
    for (i = 0; i < l; i++) {
        if (cm[i].name === columnName) {
            return i; // return the index
        }
    }
    return -1;
};
getCreativeDetailsByID = function(id,name,element) {
    if($(element).prop("checked")==true){
        creativeIds.push(id);
        creativeIdAndName.push({"CreativeId":id, "CreativeName":name});
      /*  $('#gbox_POCreativeGrid').remove();
        $('#CreativeGridPager').remove();
        $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
            'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');*/
    }else{
        $.each(creativeIds,function(index,jsonObject){
            if(jsonObject == id){
                creativeIds.splice(index,1);
                creativeIdAndName.splice(index,1);
            }
        });
        if(creativeIds.length==0){
            $('#creative-step').removeClass('rightmark');
        }
       /* $('#gbox_POCreativeGrid').remove();
        $('#CreativeGridPager').remove();
        $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
            'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');*/
    }
};
chooseAll = function(){
    creativeIds.length=0;
    creativeIdAndName.length=0;
    $('.ui-multiselect-checkboxes input:checkbox').prop('checked', 'checked');
    $('.ui-multiselect-checkboxes input:checkbox').each(function(){
        creativeIds.push($(this).val());
        creativeIdAndName.push({"CreativeId":$(this).val(), "CreativeName":$(this).attr('title')});
       /* $('#gbox_POCreativeGrid').remove();
        $('#CreativeGridPager').remove();
        $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
            'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');*/
    });
};
clearAll = function(){
    $('.ui-multiselect-checkboxes input:checkbox').prop('checked', '');
    creativeIds.length=0;
    creativeIdAndName.length=0;
    $('#gbox_POCreativeGrid').remove();
    $('#CreativeGridPager').remove();
    $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
        'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');
    $('#creativeValues').val('');
    $('.creativeNamesInPreview').empty();
    $('#creativeValuesJson').val('');
    $('.ui-multiselect-menu.ui-widget.ui-widget-content.ui-corner-all').hide(); /* hiding multiselect dropdown */
    $('#creative-step').removeClass('rightmark');
};
resetCreativesForm = function(){
    var message = "Are you sure, you want to reset creative form ? all updated data will lost !";
    bootbox.confirm(message, function(result) {
        if(result == true) {
            $('.ui-multiselect-checkboxes input:checkbox').prop('checked', '');
            creativeIds.length=0;
            creativeIdAndName.length=0;
            $('#gbox_POCreativeGrid').remove();
            $('#CreativeGridPager').remove();
            $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
                'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');
            $('#creativeValues').val('');
            $('.creativeNamesInPreview').empty();
            $('#creativeValuesJson').val('');
            $('#creative-step').removeClass('rightmark');
            $('.ui-multiselect-menu.ui-widget.ui-widget-content.ui-corner-all').hide(); /* hiding multiselect dropdown */
        }
    });
    return false;
};
submitAllSeletedCreativeSelection = function(){
    $('#creativeValues').val(creativeIds);
    $('#creativeValuesJson').val(JSON.stringify(creativeIdAndName));
    if(creativeIds.length > 0){
        $('#creativeerrors').empty();
        getCreativesByIds(creativeIds).done(function (dataJson) {
            /* clearing of creative grid*/
            $('#gbox_POCreativeGrid').remove();
            $('#CreativeGridPager').remove();
            $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
                'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');
                $('.ui-multiselect-menu.ui-widget.ui-widget-content.ui-corner-all').hide(); /* hiding multiselect dropdown */
                $('#creativeerrors').html('');
                setTimeout(function(){
                    var   $grid_creative = $("#POCreativeGrid");
                    $grid_creative.jqGrid({
                            datatype: 'local',
                            mtype: "POST",
                            treeGridModel: "adjacency",
                            data: dataJson,
                            colNames: ['CreativeId', 'CreativeName', 'FileMetaData','File', 'FileDuration', 'Status'],
                            colModel: [
                                {name: 'CreativeId',key: true,'hidden': true,resizable:false },
                                {name: 'CreativeName',search: false,resizable:false},
                                {name: 'FileMetaData',search: false, 'hidden':true,resizable:false},
                                {name: 'File',search: false,  edittype: 'textarea',resizable:false,width:133},
                                {name: 'FileDuration', search: false, 
                                    resizable:false,
                                    sortable: true,
                                    sorttype: function (cellValue,rowObject) 
                                    {  
                                        var jsonResul = JSON.parse(rowObject.FileMetaData);  
                                        return parseInt(jsonResul.duration);
                                    }, 
                                    index:'FileMetaData',  
                                    formatter: function(cellvalue, options, rowobject){
                                        var jsonval = JSON.parse(cellvalue);
                                        var resultmin = Math.floor(jsonval.duration / 60);
                                        var resultsec = jsonval.duration - resultmin * 60;
                                        if(resultmin.toString().length == 1)
                                            var resultmin = "0"+resultmin;
                                        if(resultsec.toString().length == 1)
                                            var resultsec = "0"+resultsec;
                                        var timeVal = resultmin+":"+resultsec;
                                        return timeVal;
                                 }},
                                {name: 'Status',search: false,edittype: 'textarea',resizable:false,
                                    formatter: function (cellvalue, options, rowobject) {
                                        if(cellvalue=='A'){
                                            cellvalue='Active';
                                        }
                                        if(cellvalue=='D'){
                                            cellvalue='Deleted';
                                        }
                                        if(cellvalue=='R'){
                                            cellvalue='Review';
                                        }
                                        if(cellvalue=='U'){
                                            cellvalue='Uploaded';
                                        }
                                        return '<div class="cellValue" style="visibility:hidden">' + cellvalue + '</div>';
                                    }
                                }
                                //{name: 'Actions', width: 180, sortable: false, search: false, formatter: 'actions',resizable:false}
                            ],
                            rowNum: 10,pager: '#CreativeGridPager',rownumbers: true,viewrecords: true,
                            autoWidth: true,height: 'auto',
                            loadComplete: function () {
                                /* replacing industry column null values with 'Blank' */
                                $.each($("#POCreativeGrid").jqGrid('getGridParam','data'),function(index,value){
                                    if(value.Status=='A'){
                                        value.Status='Active';
                                    }
                                    if(value.Status=='D'){
                                        value.Status='Deleted';
                                    }
                                    if(value.Status=='R'){
                                        value.Status='Review';
                                    }
                                    if(value.Status=='U'){
                                        value.Status='Uploaded';
                                    }
                                });
                                $('.toggle-blob').click(function(){return false;});
                                var iColStatus = columnIndexForGridBasedOnTableIdAndColumnName('#POCreativeGrid', 'Status');
                                var iColFile = columnIndexForGridBasedOnTableIdAndColumnName('#POCreativeGrid', 'File');
                                var iColAction = columnIndexForGridBasedOnTableIdAndColumnName('#POCreativeGrid', 'Actions');
                                var statusValue=[];
                                /*** status col***/
                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColStatus + 1) + ")")
                                    .each(function(value,key) {
                                        statusValue.push($(this).children('.cellValue').text());
                                        var className = "toggle"+$(this).children('.cellValue').text();
                                        $(this).append('<div class="toggle-light"><div class="'+className+'"></div></div>');

                                        if($(this).children('.cellValue').text()=='Active'){
                                            $('.'+className).toggles({drag:false,text:{on:'Active',off:'Review'},on:true}).addClass('disabled');

                                        }else if($(this).children('.cellValue').text()=='Review'){
                                            $('.'+className).toggles({drag:false,text:{on:'Active',off:'Review'},off:true});
                                             
                                        }
                                    });
                                /*** file col***/
                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColFile + 1) + ")")
                                    .each(function(value,key) {
                                        if(statusValue[value] == 'Active'){
                                            $(this).append('<img class="fileIconStatus" data-toggle="modal" data-target="#myModal" src="/assets/ccc-app/img/activeIcon.png"/>');
                                        }else if(statusValue[value] == 'Review'){
                                            $(this).append('<img class="fileIconStatus" data-toggle="modal" data-target="#myModal" src="/assets/ccc-app/img/ReviewVidIcon.png"/>');
                                        }else if(statusValue[value] == 'Deleted'){
                                            $(this).append('<img class="fileIconStatus" src="/assets/ccc-app/img/deltedIcon.png"/>');
                                        }
                                        else if(statusValue[value] == 'Uploaded'){
                                            $(this).append('<img class="fileIconStatus" src="/assets/ccc-app/img/deltedIcon.png"/>');
                                        }
                                    });
                                /*** Action col***/
                                $(this).find(">tbody>tr.jqgrow>td:nth-child(" + (iColAction + 1) + ")")
                                    .each(function(value,key) {
                                        $("<div>", {
                                            mouseover: function (e) {
                                                id = $(e.target).closest("tr.jqgrow").attr("id");
                                                $(this).children('.update-link').attr("href", "/creative/" + id + "/edit");
                                                $(this).children('.delete-link').attr("onclick", "deleteRow('" + id + "', '#POCreativeGrid')");
                                            }
                                        }).css({ cursor: "pointer"})
                                            .addClass("col-md-12 col-xs-12 wrapperForButtons")
                                            .append('<a href="#" class="col-md-5 col-xs-12 delete-link">Delete</a>')
                                            .prependTo($(this).children("div"));
                                    });
                            }
                        });

                    /* jq grid drag and drop rows */
                    $("#POCreativeGrid tbody").sortable();
                   /* $("#POCreativeGrid").jqGrid("sortableRows");
                    $("#POCreativeGrid").jqGrid('gridDnD');
*/
                    var cm = $grid_creative.jqGrid('getGridParam', 'colModel');
                    $grid_creative.click(function (e) {
                        var $td = $(e.target).closest('td'), $tr = $td.closest('tr.jqgrow'), rowId = $tr.attr('id'), ci;
                        if (rowId) {
                            ci = $.jgrid.getCellIndex($td[0]); // works mostly as $td[0].cellIndex
                            var targeTHtml = $(e.target).html();
                            /*** status col click ***/
                            if(cm[ci].name == 'Status') {
                                /*if(targeTHtml=='Active'){
                                    targeTHtml = 'R';
                                    updateStatuses(rowId, targeTHtml, '#POCreativeGrid');
                                }else */
                                if(targeTHtml=='Review'){
                                    targeTHtml = 'A';
                                    updateStatuses(rowId, targeTHtml, '#POCreativeGrid');
                                   $(e.target).parent().parent().parent().removeClass('toggleReview').addClass('toggleActive disabled');
                                   //$(this).append('<img class="fileIconStatus" data-toggle="modal" data-target="#myModal" src="/assets/ccc-app/img/activeIcon.png"/>');
                                }
                            }
                            /*** file col click ***/
                            if (cm[ci].name == 'File') {
                                var w = 830, h = 640; // default sizes
                                var t = h/2;var l = w/2;
                                    w = window.innerWidth - 200;
                                    h = window.innerHeight - 200;
                                    t =   h/2;l=w/2;
                                var getVideoMetaValue = JSON.parse($(e.target).parent().parent()
                                    .find('td').filter("[aria-describedby='POCreativeGrid_FileMetaData']")[0].innerText);
                                var redirectGoogleDriveUrl=google_drive_webView_Path+getVideoMetaValue.url;

                                /* window to be opened in the center of the screen */
                                var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                                var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                                var top = ((height / 2) - (h / 2)) + dualScreenTop;

                                var newWindow = window.open(redirectGoogleDriveUrl,'Google Drive Videos','width='+w+',height='+h+',top='+100+',' +
                                    'left='+100+',right='+100,'titlebar=yes');

                                // Puts focus on the newWindow
                                if (window.focus) {
                                    newWindow.focus();
                                }
                            }
                        }
                    });
                    $grid_creative.jqGrid('navGrid', '#pager', {edit: false,add: false,del: false,search: false}, {}, {}, {}, {multipleSearch: true,multipleGroup: true,recreateFilter: true,overlay: 0});
                    $('.ui-search-toolbar th').css("visibility", "hidden");
                },1000);
            });
    }else{
        creativeIds.length=0;
        creativeIdAndName.length=0;
        $('#gbox_POCreativeGrid').remove();
        $('#CreativeGridPager').remove();
        $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
            'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');
        $('#creativeValues').val('');
        $('.creativeNamesInPreview').empty();
        $('#creativeValuesJson').val('');
        $('#creative-step').removeClass('rightmark');
    }
};
getCreative = function (advertiserId)   {
    $('.creativeOkBtn').remove(); //clear the multiselect checkboxes
    $('.ui-multiselect-checkboxes').empty(); //clear the multiselect checkboxes
    getCreativesByAdvertiserId(advertiserId).done(function(data) {
        var select = $("#Creative"), 
        optionsadded = '';
        select.empty();
        for(var i=0;i<data.length; i++){
            optionsadded += '<li class=" "><label for="ui-multiselect-Creative-option-'+i+'" title="" class="ui-corner-all ui-state-hover">' +
                '<input id="ui-multiselect-Creative-option-'+i+'" name="multiselect_Creative" ' +
                'type="checkbox" onchange="getCreativeDetailsByID('+ data[i].CreativeId +',\''+data[i].CreativeName+'\',this)"' +
                'title="'+ data[i].CreativeName +'" value="'+ data[i].CreativeId +'" title="Select" ' +
                'aria-selected="true"><span>'+ data[i].CreativeName +'</span></label></li>';
        }
        $('.ui-multiselect-checkboxes').html(optionsadded);
        $('.ui-multiselect-menu .creativeOkBtn').remove(); //clear the multiselect checkboxes
        $('.ui-multiselect-menu').append('<ul class="creativeOkBtn padding-0 ok-button list-unstyled"><li class="padding-0">' +
            '<button class="btn btn-default selectALLSubmitBtn" onclick="submitAllSeletedCreativeSelection()">ok</button></li></ul>');

        /* this is for edit creatives to check the checkbox in multiSelect search box*/
        if(creativeIds.length > 0) {
            $('.ui-multiselect-checkboxes li label input').each(function (i, input) {
                $.each(creativeIds, function(index,value){
                    if($(input).val()==value){
                        $(input).prop('checked', true);
                    }
                })
            });
        }
        //multiSelect dropdown box in po-creative block
        creativeMultiSelectDropDown();
    });
};

