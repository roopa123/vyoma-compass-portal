/**
 * Created by Chethan JN on 08-07-2016.
 */

checkAdvertiser = function(advertiserId) {
    return $.ajax({
        type: 'get',
        url: '/checkadvertiser/'+advertiserId,
        dataType: 'json'
    });
};
validatePo = function(sendData){
   return $.ajax({
        type        : 'post',
        url         : '/purchaseOrder/validate/po',
        data        :  sendData,
        dataType    : 'json'
    });
};
validateCampaign = function(sendData){
    return $.ajax({
        type        : 'post',
        url         : '/purchaseOrder/validate/campaign',
        data        :  sendData,
        dataType    : 'json'
    });
};
checkCreative = function(creativeIds){
    return $.ajax({
        type        : 'get',
        url         : '/api/checkcreative/'+'['+creativeIds+']',
        dataType    : 'json'
    });
};
loadRegions = function(){
    return $.ajax({
        type: "get",
        url: "/api/regions",
        dataType: "json"
    });
};
loadState = function(){
    return $.ajax({
        type: "get",
        url: "/api/states",
        dataType: "json"
    });
};
loadStations = function(){
    return $.ajax({
        type: "get",
        url: "/api/stations",
        dataType: "json"
    });
};
getCreativesByIds = function(creativeIds){
    return  $.ajax({
        type: "GET",
        url: "/api/creative/" + '[' + creativeIds + ']',
        dataType: "json"
    });
};
getCreativesByAdvertiserId = function(advertiserId){
    return  $.ajax({
        type        : 'GET',
        url         : '/advertiser/'+advertiserId+'/creatives',
        dataType    : 'JSON'
    });
};
purchaseOrderStore = function(sendData){
    return $.ajax({
        type: 'post',
        url: '/purchaseorderstore',
        data: sendData,
        dataType: 'json'
    });
};
purchaseOrder = function(sendData,povalue){
    $.ajax({
        type        : 'PUT',
        url         : '/purchaseorder/'+povalue,
        data        :  sendData,
        dataType    : 'json'
    })
};
addCampaign = function(sendData){
    return $.ajax({
        type        : 'POST',
        url         : '/addcampaign',
        data        :  sendData,
        dataType    : 'json'
    })
};
getStationsByArrayOfRegionIds = function(regionIdsArray){
   return $.ajax({
        type: "get",
        url: "/api/region/["+regionIdsArray+"]",
        dataType: "json"
    })
};
getStationsByArrayOfStateIds = function(stateIdsArray){
   return $.ajax({
        type: "get",
        url: "/api/state/["+stateIdsArray+"]",
        dataType: "json"
    })
};