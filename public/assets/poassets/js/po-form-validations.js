/**
 * Created by Chethan JN on 16-06-2016.
 */

jQuery(function () {
    $('#purchaseorderform').validate({ // initialize the plugin
        rules: {
            purchase_order_date: {required: true},
            startDate: {required: true},
            endDate: {required: true},
            branch: {required: true},
            salesrep: {required: true},
            advertiser: {required: true},
            CompaignName: {required: true},
            start_date_campaign: {required: true},
            end_date_campaign: {required: true},
            SpotsPerDay: {required: true,number:true},
            Creative: {required: true}
        },
        messages: {
            purchase_order_date: "Please Enter Purchase Order Date !",
            startDate: "Please Enter Start Date !",
            endDate: "Please Enter End Date !",
            branch: "Please Select Branch !",            
            salesrep: "Please Select Sales Representative !",
            advertiser:"Please Select Advertiser !",
            CompaignName:"Please Enter Campaign Name !",
            start_date_campaign:"Please Enter Campaign Start Date !",
            end_date_campaign:"Please Enter Campaign End Date !",
            SpotsPerDay:"Please Enter Numeric Value !",
            Creative: "Please Select Creative !"
        }
    });
});