/*** Created by Chethan JN on 26-05-2016. ***/
checkAdvertiserHasCreative = function(advertiserId, askConfirmation){

    var oldAdvertiserId = $('#oldAdvertiserId').val();
    if (askConfirmation == 1)
    {
        var message = "If you change the Advertiser, all the Creatives associated with current PO will be lost";
        bootbox.confirm(message, function(result) 
        {
            if(result == true)
            {
                creativeIds.length=0;
                $('#gbox_POCreativeGrid').remove();
                $('#CreativeGridPager').remove();
                $('#creativeValues').val('');
                $('#creative-step').removeClass('rightmark');
                $('.creativeNamesInPreview').empty();
                $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
                'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');
                checkAdvertiser(advertiserId).done(function (data) {
                if (data.result == 'fail'){
                displayPOFormValaidationMessage('The Selected Advertiser has No Creatives');
                emptyAllErros(); /* close alert dilogs */
                return false;
                }
                if (data.result == 'success'){
                return true;
                }
                });
            }
            else 
            {
              $('#advertiser').val(oldAdvertiserId); 
            }
        });
    }
    else
    {
        creativeIds.length=0;
        $('#gbox_POCreativeGrid').remove();
        $('#CreativeGridPager').remove();
        $('#creativeValues').val('');
         $('.creativeNamesInPreview').empty();
        $('#creative-step').removeClass('rightmark');
        $('.POCreativeGridBlock').html('<table id="POCreativeGrid" class="col-md-12 col-xs-12 table-hover table-striped ' +
        'table-condensed table-responsive table"></table><div id="CreativeGridPager"></div>');
        checkAdvertiser(advertiserId).done(function (data) {
        if (data.result == 'fail'){
        displayPOFormValaidationMessage('The Selected Advertiser has No Creatives');
        emptyAllErros(); /* close alert dilogs */
        return false;
        }
        if (data.result == 'success'){
        return true;
        }
        });
    }
     
        
    
};

checkActiveCreativesInGrid = function (){
    checkCreative(creativeIds).done(function (data){
        if (data.result == 'fail'){
            displayCreativeFormValaidationMessage('Please Add only Active Creatives to the Campaign');
            emptyAllErros(); /* close alert dilogs */
            return false;
        }
        if (data.result == 'success'){return true;}
    });
};
 

displayPOFormValaidationMessage = function(msg){
    $('.step-1').removeAttr('disabled').addClass('btn-primary');
    $('#step-1').show();
    $('.setup-content').not('#step-1').hide();
    $('#poerrors ul').html('<li class="alert alert-danger">'+msg+'</li>');
};

displayCampaignFormValaidationMessage = function(msg){
    $('.step-2').removeAttr('disabled').addClass('btn-primary');
    $('#step-2').show();
    $('.setup-content').not('#step-2').hide();
    $('#campaignerrors ul').html('<li class="alert alert-danger">'+msg+'</li>');
};

displayCreativeFormValaidationMessage = function(msg){
    console.log("dsf");
    $('.step-3').removeAttr('disabled').addClass('btn-primary');
    $('#step-3').show();
    $('.setup-content').not('#step-3').hide();
    $('#creativegriderrors ul').html('<li class="alert alert-danger">'+msg+'</li>');
};

displayLocationFormValaidationMessage = function(msg){
    $('.step-4').removeAttr('disabled').addClass('btn-primary');
    $('#step-4').show();
    $('.setup-content').not('#step-4').hide();
    $('#locationerrors ul').html('<li class="alert alert-danger">'+msg+'</li>');
};

displayValidationMessageInParticularPage = function (value){
    var errorString = value.toString();
    var text = errorString.split(" ");
    var first = text.shift();
    var trimmedstr = $.trim(errorString);
    var msg = trimmedstr.substr(trimmedstr.indexOf(" ") + 1);
    if(first == 'PO')
    {
        displayPOFormValaidationMessage(msg);
    }
    else if(first == 'Campaign')
    {
        displayCampaignFormValaidationMessage(msg);
    }
    else if(first == 'Creative')
    {
        displayCreativeFormValaidationMessage(msg);
    }
    else if(first == 'Location')
    {
        displayLocationFormValaidationMessage(msg);
    }
};