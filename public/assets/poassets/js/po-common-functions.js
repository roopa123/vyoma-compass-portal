/********  PO Flow *******/
//multiSelect dropdown box in po-creative block
creativeMultiSelectDropDown = function () {
    var $elem = $('#Creative');
    var inToolbar = typeof id === "string";

    var options = {
            selectedList: 2,
            height: "auto",
            checkAllText: "Select All",
            uncheckAllText: "Clear",
            noneSelectedText: "Add Creative",
            open: function () {
                var $menu = $(".ui-multiselect-menu:visible");
                $menu.width("auto");
                $menu.find(">ul").css("maxHeight", "150px"); // some max-height value
                return;
            }
        },
        $options = $elem.find("option");
    if ($options.length > 0 && $options[0].selected) {
        $options[0].selected = false; // unselect the first selected option
    }

    if (inToolbar) {
        options.minWidth = 'auto';
    }

    //$elem.multiselect(options);
    $elem.multiselect(options).multiselectfilter({placeholder: 'Enter Keyword'});
    $elem.siblings('button.ui-multiselect').css({
        width: inToolbar ? "98%" : "100%",
        marginTop: "1px",
        marginBottom: "1px",
        paddingTop: "3px"
    });

    $('.ui-multiselect-header a.ui-multiselect-all').children('span').next('span').attr('onclick', 'chooseAll()');
    $('.ui-multiselect-header a.ui-multiselect-none').children('span').next('span').attr('onclick', 'clearAll()');
};
/* google drive web view path */
var google_drive_webView_Path = 'https://drive.google.com/file/d/';
/* Add Another Campaign Validation Alert Message*/
var global_CampaignAlertMessage = "The current campaign will be saved as partial, do you want to proceed ?";
/**** start Date and End Date selection ****/
$(function(){
    //po Dates
    $('#purchase_order_date').datetimepicker({
        dayViewHeaderFormat:true,
        format: 'DD-MM-YYYY',
        defaultDate:new Date(),
        pickTime: false,
        autoclose: true
    });
    $('#startDate').datetimepicker({
        dayViewHeaderFormat:true,
        format: 'DD-MM-YYYY',
        defaultDate:new Date(),
        pickTime: false,
        autoclose: true
    });
    $('#endDate').datetimepicker({
        dayViewHeaderFormat:true,
        format: 'DD-MM-YYYY',
        defaultDate:new Date(),
        pickTime: false,
        autoclose: true
    });
    $('#purchase_order_date').addClass("whiteColor");$('#startDate').addClass("whiteColor");$('#endDate').addClass("whiteColor");
    $("#purchase_order_date").on("dp.change",function (e) {
        $('#purchase_order_date').removeClass("whiteColor");
        $('#startDate').data("DateTimePicker").setMinDate(e.date);
        $('#endDate').data("DateTimePicker").setMinDate(e.date);
        $('input').blur();
    });
    $("#startDate").on("dp.change",function (e) {
        $('#startDate').removeClass("whiteColor");
        $('#start-date-campaign').removeClass("whiteColor");
        $('#endDate').data("DateTimePicker").setMinDate(e.date);
        $('#start-date-campaign').val(jQuery('#startDate').val());
        $('#start-date-campaign').data("DateTimePicker").setMinDate(e.date);
        $('#end-date-campaign').data("DateTimePicker").setMinDate(e.date);
        $('input').blur();
    });
    $("#endDate").on("dp.change",function (e) {
        $('#endDate').removeClass("whiteColor");
        $('#end-date-campaign').removeClass("whiteColor");
        $('#startDate').data("DateTimePicker").setMaxDate(e.date);
        $('#end-date-campaign').val(jQuery("#endDate").val());
        $('#end-date-campaign').data("DateTimePicker").setMaxDate(e.date);
        $('#start-date-campaign').data("DateTimePicker").setMaxDate(e.date);
        $('input').blur();
    });

//campaignDates();
    $('#start-date-campaign').datetimepicker({
        dayViewHeaderFormat: true,
        format: 'DD-MM-YYYY',
        defaultDate:new Date(),
        pickTime: false,
        autoclose: true
    });
    $('#end-date-campaign').datetimepicker({
        dayViewHeaderFormat: true,
        format: 'DD-MM-YYYY',
        defaultDate:new Date(),
        pickTime: false,
        autoclose: true
    });
    $('#start-date-campaign').addClass("whiteColor");$('#end-date-campaign').addClass("whiteColor");
//campaign dates
    $("#start-date-campaign").on("dp.change",function (e) {
        $('#start-date-campaign').removeClass("whiteColor");
        jQuery('#end-date-campaign').data("DateTimePicker").setMinDate(e.date);
        $('input').blur();
    });
    $("#end-date-campaign").on("dp.change",function (e) {
        $('#end-date-campaign').removeClass("whiteColor");
        jQuery('#start-date-campaign').data("DateTimePicker").setMaxDate(e.date);
        $('input').blur();
    });
    creativeMultiSelectDropDown(); /* multiSelect dropdown box in po-creative block */
    $('#step-3 .ui-multiselect.ui-widget').addClass('form-control'); /* add form-control class to multiselect dropdown in creative */

    //ajaxStart and ajax Stop preloader
    $(document).ajaxStart(function(){ $('#preLoaderBlock').show(); /* show pre loader */ });
    $(document).ajaxComplete(function(){ $('#preLoaderBlock').hide(); /* hide pre loader */ });

    /* collapse and expand for regions, state, stations blocks */
    hideAndShowLongText('.long-text');

    $(".creativeNamesInPreview span").not(":last-child").append(","); /* only adding comma to the last element*/

    /* load regions,States,Stations ajax */
    loadRegions().done(function(dataJson) {
        $('.region-devisions-ul-list').empty();
        $('#locationerrors ul').empty();
        $.each(dataJson, function(index,jsonObject){
            $('.region-devisions-ul-list').append('<li class="color-gray" title="'+jsonObject.RegionId+'">' +
                ''+jsonObject.RegionName+'' +
                '<div id="regionCheckBtn">' +
                '<label>' +
                '<input type="checkbox" ' +
                'onchange="toggleClassAddBtn(this,'+jsonObject.RegionId+',\''+jsonObject.RegionName+'\')" value="'+jsonObject.RegionId+'"><span>Add</span>' +
                '</label>' +
                '</div>' +
                '</li>');
        });
    });
    loadState().done(function(dataJson) {
        $('.state-devisions-ul-list').empty();
        $('#locationerrors ul').empty();
        $.each(dataJson, function(index,jsonObject){
            $('.state-devisions-ul-list').append('<li class="color-gray" title="'+jsonObject.StateId+'">' +
                ''+jsonObject.StateName+'' +
                '<div id="regionCheckBtn"><label><input type="checkbox" ' +
                'onchange="toggleClassStateAddBtn(this,'+jsonObject.StateId+',\''+jsonObject.StateName+'\')" value="'+jsonObject.StateId+'"><span>Add</span></label></div>' +
                '</li>');
        });
    });
    $('.listNav').remove();//re intilize the list nav in station list
    loadStations().done(function(dataJson) {
        $('#locationerrors ul').empty();
        $.each(dataJson, function(index,jsonObject){
            $('.station-sortings-list').append('<li>' +
                '<div class="checkbox"><label><input type="checkbox" onchange="' +
                'stationToSelectedListFn(this,\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
                ''+jsonObject.RegionId+','+jsonObject.StateId+','+jsonObject.BranchId+')">' +
                ''+jsonObject.StationName+'</label></div>' +
                '</li>');
        });
        // location block station alphabetical sortings
        setTimeout(function(){
            $('.station-sortings-list').listnav({
                initLetter: 'a',        // filter the list to a specific letter on init ('a'-'z', '-' [numbers 0-9], '_' [other])
                includeAll: true,      // Include the ALL button
                includeOther: false,    // Include a '...' option to filter non-english characters by
                includeNums: false,
                onClick:  function(letter) { chooseAllStationStations();/* this is for onclick or each change of leters choose all stations */ }
            });
            chooseAllStationStations();//this is for by default choose all stations
        },500);
    });
});
/* collapse and expand for regions, state, stations blocks */
hideAndShowLongText = function(className){
    var minimized_elements = $(className);
    minimized_elements.each(function(){
        var t = $(this).text();
        if(t.length < 60)
            return;

        $(this).empty().html(t.slice(0,60)+'<span>... </span><a href="#" class="more">More</a>'+
            '<span style="display:none;">'+ t.slice(60,t.length)+' <a href="#" class="less">Less</a></span>'
        );
    });
    $('a.more', minimized_elements).click(function(event){
        event.preventDefault();
        $(this).hide().prev().hide();
        $(this).next().show();
    });
    $('a.less', minimized_elements).click(function(event){
        event.preventDefault();
        $(this).parent().hide().prev().show().prev().show();
    });
};

// Add Advertsiser Redirection confirm
function redirectAdvertiser(redirectURL) {
    console.log('I AM Ready');
    var message = "Are you sure you want to move away?Data you entered will not be saved";
    bootbox.confirm(message, function(result) {
        if(result == true) {
            document.location = redirectURL;
        }
    });
    return false;
}
function toggleCollapseClass(element) {
    var classNameOfElement=$(element).attr('class');
    console.log(classNameOfElement);
    if(classNameOfElement=='collapse-mode'){
        $(element).removeClass('collapse-mode').addClass('expand-mode');
        $(element).parent().parent().next().hide(800);
    }else{
        $(element).removeClass('expand-mode').addClass('collapse-mode');
        $(element).parent().parent().next().show(800);
    }
}
/* reset po form */
function resetPOForm(){
    var message = "Are you sure, you want to reset PO form ? all the entered data will be lost !";
    bootbox.confirm(message, function(result) {
        if(result == true) {
            $('#po-step').removeClass('rightmark');
            $('#salesrep').val("");
            $('#purchase_order_date').val("");
            $('#startDate').val(""); $('#endDate').val("");
            $('#advertiser').val(""); $('#branch').val(""); $('#roreference').val("");
            jQuery('#startDate').data("DateTimePicker").setMinDate('');
            jQuery('#endDate').data("DateTimePicker").setMinDate('');
        }
    });
    return false;
}
/* reset Campaign form */
function resetCampaignForm(){
    var message = "Are you sure, you want to reset Campaign form ? all the entered data will be lost !";
    bootbox.confirm(message, function(result) {
        if(result == true) {
            $('#CompaignName').val("");
         /*   $('#start-date-campaign').val("");
            $('#end-date-campaign').val("");*/
            $('#SpotsPerDay').val("");
            $('#campaign-step').removeClass('rightmark');
        }
    });
    return false;
}
/* reset Location form */
function resetLocForm(){
    var message = "Are you sure, you want to reset Location form ? all updated data will lost !";
    bootbox.confirm(message, function(result) {
        if(result == true) {
            $('ul.selected-lists li').remove();
            $('ul.selected-state-station-lists li').remove();
            finalRegionStationItems.length=0; finalStateStationItems.length=0;
            clrFinalStationStationItems();
            $('#location-step').removeClass('rightmark');
        }
    });
    return false;
}
bindingpreview = function (){
    var formData = {
        'salesrep'              : $('#salesrep').val(),
        'poDate': moment($('input[name=purchase_order_date]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'startDate': moment($('input[name=startDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'endDate': moment($('input[name=endDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'advertiser'            : $('#advertiser').val(),
        'branch'                : $('#branch').val(),
        'roReference'           : $('#roreference').val(),
        'advertiserName'        : $("#advertiser option:selected").text(),
        'branchName'            : $("#branch option:selected").text(),
        'salesRepName'          : $("#salesrep option:selected").text(),
        'campaignName'          : $('#CompaignName').val(),
        'campaignStartDate':  moment($('input[name=start_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'campaignEndDate':  moment($('input[name=end_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'spotsPerDay'           : $('#SpotsPerDay').val(),
        'creativeValues'        : $('#creativeValues').val(),
        'location'              : $('#location').val(),
        'mode'                  : $('#mode').val()
    };
    $('#postjsondata').val(JSON.stringify(formData));
    var previewData = JSON.parse($('#postjsondata').val());
    if(previewData.salesRepName == 'Please Select Sales Representative'){ $('.salesrep').html(''); }else{
        $('.salesrep').html(previewData.salesRepName);
    }
    if(previewData.branchName == 'Please Select Branch'){ $('.branchname').html(''); }else{
        $('.branchname').html(previewData.branchName);
    }
    if(previewData.advertiserName == 'Please Select Advertiser'){ $('.advertisername').html(''); }else{
        $('.advertisername').html(previewData.advertiserName);
    }
    if(previewData.poDate == 'Invalid date'){ $('.podate').html(''); }else{
        $('.podate').html(moment(previewData.poDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    }
    if(previewData.startDate == 'Invalid date'){ $('.postart').html(''); }else{
        $('.postart').html(moment(previewData.startDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    }
    if(previewData.endDate == 'Invalid date'){ $('.poend').html(''); }else{
        $('.poend').html(moment(previewData.endDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    }
    if(previewData.campaignStartDate == 'Invalid date'){ $('.campaignstart').html(''); }else{
        $('.campaignstart').html(moment(previewData.campaignStartDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    }
    if(previewData.campaignEndDate == 'Invalid date'){ $('.campaignend').html(''); }else{
        $('.campaignend').html(moment(previewData.campaignEndDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
        $('.creativeEndDate').html(moment(previewData.campaignEndDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    }
    if(previewData.roReference == '' || previewData.roReference == null){  $('.roreference').parent('li').hide(); }else{
        $('.roreference').parent('li').show();
        $('.roreference').html(previewData.roReference);
    }
    $('.campaignname').html(previewData.campaignName);
    $('.slots').html(previewData.spotsPerDay);
    $('#creativePreview').css('display', 'none');
    goToPreviewWithLocationData(); /* binding locations to preview */
    if($('#creativeValues').val().length > 0) {
        setCreativeNameToPreview();
        $('#creativePreview').css('display', 'block');
        removeDuplicateTextInPreviewLocationBlock('.creativeNamesInPreview span');
    }
    /* removing class for date inputs */
    $('input').removeClass('whiteColor');
};



//Binding of Creative To Preview Page.
setCreativeNameToPreview = function(){
    $('.creativeNamesInPreview').empty();
    if($('#creativeValues').val().length > 0){
        $('.creativeNamesInPreview').empty();
        creativeIds = $('#creativeValues').val().split(',');
        getCreativesByIds(creativeIds).done(function (dataJson) {
            $.each(dataJson,function(index,value){
                var creativePreviewUrl = JSON.parse(value.FileMetaData);
                $('.creativeNamesInPreview').append('<span>'+value.CreativeName+'' +
                    '<a onclick="getPreviewCreativeUrl(\''+creativePreviewUrl.url+'\')"><img class="creativeVidPlayIcon" src="/assets/poassets/images/locationCreativePreview.png"></a></span>');
            });
            $(".creativeNamesInPreview span").not(":last-child").append(","); /* only adding comma to the last element*/
        });
    }
};
getPreviewCreativeUrl = function(url){
    var redirectGoogleDriveUrl=google_drive_webView_Path+url;
    var w = 830, h = 640; // default sizes
    var t = h/2;var l = w/2;
    w = window.innerWidth - 200;
    h = window.innerHeight - 200;
    t =   h/2;l=w/2;

    /* window to be opened in the center of the screen */
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;

    var newWindow = window.open(redirectGoogleDriveUrl,'Google Drive Videos','width='+w+',height='+h+',top='+100+',' +
        'left='+100+',right='+100,'titlebar=yes');

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
};
getCreativeUrl = function(filemetadata) {
    var redirectGoogleDriveUrl=google_drive_webView_Path+filemetadata.url;
    var w = 830, h = 640; // default sizes
    var t = h/2;var l = w/2;
    w = window.innerWidth - 200;
    h = window.innerHeight - 200;
    t =   h/2;l=w/2;

    /* window to be opened in the center of the screen */
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;

    var newWindow = window.open(redirectGoogleDriveUrl,'Google Drive Videos','width='+w+',height='+h+',top='+100+',' +
        'left='+100+',right='+100,'titlebar=yes');

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
};
setAddPOFormPostData = function(){
    var formData =
    {
        'stepsComplete' : $('#stepsComplete').val(),
        'POId'      : $('#purchaseorder_id').val(),
        'originalPOId' : $('#originalpoid').val(),
        'salesrep': $('#salesrep').val(),
        'poDate': moment($('input[name=purchase_order_date]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'startDate': moment($('input[name=startDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'endDate': moment($('input[name=endDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'advertiser': $('#advertiser').val(),
        'branch': $('#branch').val(),
        'roReference': $('#roreference').val(),
        'campaignName': $('#CompaignName').val(),
        'campaignStartDate':  moment($('input[name=start_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'campaignEndDate':  moment($('input[name=end_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'spotsPerDay': $('#SpotsPerDay').val(),
        'creativeValues': $('#creativeValues').val(),
        'location': $('#po-location').val(),
        'mode': $('#mode').val()
    };
    $('#postjsondata').val(JSON.stringify(formData));
    var sendData =
    {
        'sendData': $('#postjsondata').val(),
    };
    return sendData;
};
showCampaignForm = function(){
    $('.setup-content').not('#step-2').hide();
    $('#step-2').show();
};
redirectFn = function(url){window.location= url;};
emptyAllErros = function (){
    window.setTimeout(function() {
        $("li.alert.alert-danger,li.alert.alert-success").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 2500);
};
doEnableDisableNavigationButtons = function(hideBtn, showBtn){
    $('.'+hideBtn).addClass('rightmark');
    $('.'+showBtn).removeAttr('disabled').addClass('btn-primary');
    $('#'+hideBtn).hide();
    $('#'+showBtn).show();
};
formBinding = function(){
    $('#creativePreview').css('display', 'none');
    var po_location_arry=[], regionToStationItems_edit=[], stateToStationItems_edit=[]; /* Displaying Campaign Details Step on Edit */
    bindingpreview(); /*Binding PO and Campaign Data */
    getCreative($("#advertiser>option:selected").val());/*Binding Creatives to Grid in creative Page */ //load creatives based on advertiser
    if(creativeIds.length > 0){ /* LOAD creative grid based on creatives */
        $('#creative-step').addClass('rightmark'); /* add right mark for cretive form */
        $('#creativeerrors').empty();
        submitAllSeletedCreativeSelection(); /* binding creatives to multiselect dropdown */
    }else{
        $('#creative-step').removeClass('rightmark'); /* add right mark for cretive form */
        $('#creativeerrors').html('<label class="error" ' +
            'id="salesrep-error">Please Select Creatives ! </label>');
    }
    if($('#po-location').val().length > 0){  // Binding Of Location Details to the Location Page
        po_location_arry = JSON.parse($('#oldlocation').val());
        $.each(po_location_arry,function(index,jsonObject){
            if(jsonObject.locationType=='R' ){
                regionToStationItems_edit.push({RegionName:jsonObject.groupName,StationId:jsonObject.stationId,StationName:jsonObject.stationName,
                    RegionId:jsonObject.groupId});
            }else if(jsonObject.locationType=='E'){
                stateToStationItems_edit.push({StateName:jsonObject.groupName,StationId:jsonObject.stationId,StationName:jsonObject.stationName,
                    StateId:jsonObject.groupId});
            }else if(jsonObject.locationType=='S'){
                stationToSelectedItems.push({StationId:jsonObject.stationId,StationName:jsonObject.stationName});
            }
        });
        $.each(regionToStationItems_edit,function(index,jsonObject){
            regionToStationItems = $.grep(regionToStationItems_edit, function(e){ return e.RegionId == jsonObject.RegionId; });
            moveAllRegionStationsToSelectedList();
        });
        $.each(stateToStationItems_edit,function(index,jsonObject){
            stateToStationItems = $.grep(stateToStationItems_edit, function(e){ return e.StateId == jsonObject.StateId; });
            moveAllStateStationsToSelectedList();
        });
        moveAllStationStationsToSelectedList();
    }
    goToPreviewWithLocationData(); //Binding Location To Preview Page
};
