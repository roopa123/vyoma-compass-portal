/**
 * Created by Chethan JN on 03-06-2016.
 */
var regionToStationItems=[];
var finalRegionStationItems=[];

regionToStationListFn = function(element, regionName, StationName, StationId, RegionId){
    if($(element).is(':checked')){
        regionToStationItems.push({RegionName:regionName,StationId:StationId,StationName:StationName,RegionId:RegionId});
    }else{
        $.each(regionToStationItems,function(index,jsonObject){
            if(jsonObject.StationName==StationName){
                regionToStationItems.splice(index,1);
            }
        });
    }
};
chooseAllRegionStations = function(){
    regionToStationItems.length=0;
    $('.region-station-list input:checkbox').prop('checked', 'checked');
    $('.region-station-list input:checkbox').each(function(){
        $(this).trigger("change");
    });
};
clearAllRegionStations = function(){
    $('.region-station-list input:checkbox').prop('checked', '');
    regionToStationItems.length=0;
};

moveAllRegionStationsToSelectedList = function(){
    console.log("regionToStationItems",regionToStationItems);
    var noDuplicateRegions,noDuplicateRegionStations;
    $.each(regionToStationItems, function(index, jsonObject){
        noDuplicateRegions = $.grep(finalRegionStationItems, function(e){ return e.RegionId == jsonObject.RegionId; });
        if(noDuplicateRegions.length <= 0) {
            emptyAllErros(); /* close alert dilogs */
            $('.selected-lists').append('<li class="selected-stations'+jsonObject.RegionId+'"><span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.RegionName + '">' + jsonObject.RegionName + ' </span><span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="pull-right">' +
                '<span class="totCountOfSelectedItems">'+0+'</span>' +
                '<span class="selectAll-clearBtns">' +
                '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrFinalRegionStationItems('+ jsonObject.RegionId +')"></i></span>' +
                '</span></span><ul class="selected-list-items' + jsonObject.RegionId + ' list-unstyled" style="padding: 18px;">' +
                '</ul></li>');
        }

        noDuplicateRegionStations = $.grep(finalRegionStationItems, function(e){ return e.StationId == jsonObject.StationId; });
        if(noDuplicateRegionStations.length <= 0){
            $('.selected-list-items' + jsonObject.RegionId + '').append('<li class="station-' + jsonObject.StationId + '">'
                + jsonObject.StationName + '  <span class="closeIcon" onclick="clrFinalRegionStationItem(' + jsonObject.StationId + ',' + jsonObject.RegionId + ')"' +
                '></span></li>');
            finalRegionStationItems.push(jsonObject);
            $('#locationerrors ul').html('<li class="alert alert-success">Stations added successfully !</li>');
            emptyAllErros();
        }else{
            $('#locationerrors ul').html('<li class="alert alert-danger">Stations added already !</li>');
            emptyAllErros();
        }
        $('.selected-stations'+jsonObject.RegionId+'  span.totCountOfSelectedItems').text($('#Region  .selected-list-items'+jsonObject.RegionId+' li').length);
    });
};
clrFinalRegionStationItems = function(regionId){
    var regionFinalArr=[];
    $('.selected-stations'+regionId).empty();
    $('.selected-stations'+regionId+'  span.totCountOfSelectedItems').html(0);
    $.each(finalRegionStationItems,function(index,jsonObject){
        if(jsonObject.RegionId!=regionId){
            regionFinalArr.push(jsonObject);
        }
    });
    finalRegionStationItems.length=0;
    finalRegionStationItems=regionFinalArr;
};
clrFinalRegionStationItem = function(stationId,regionId){
    $('.station-'+stationId).remove();
    $('.selected-stations'+regionId+'  span.totCountOfSelectedItems').text($('#Region  .selected-list-items'+regionId+' li').length);
    if($('.selected-list-items'+regionId+' li').length==0){
        $('.selected-stations'+regionId).empty();
    }
    $.each(finalRegionStationItems,function(index,jsonObject){
        if(jsonObject.StationId==stationId){
            finalRegionStationItems.splice(index,1);
        }
    });
};
/*toggleClassAddBtn = function(element,regionID,regionName){
 $('#regionCheckBtn label input').prop('checked', '');// reset all active buttons except current one
 $(element).prop('checked', 'checked');// set active for current one
 regionToStationItems.length=0;//update new stations selected from the region
 if($(element).is(':checked')){
 $.ajax({
 type: "get",
 url: "/api/region/"+regionID,
 dataType: "json"
 }).done(function(dataJson) {
 $('#Region .proccedSelectionBtn').show();//show button after json response
 $('.regionNameHdr').html(regionName);
 $('.regionNameHdr').attr('title',regionName);
 $('.region-station-list').empty();
 $.each(dataJson, function(index,jsonObject){
 $('.region-station-list').append('<li>' +
 '<div class="checkbox"><label><input type="checkbox" onchange="' +
 'regionToStationListFn(this,\''+regionName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
 ''+jsonObject.RegionId+')">' +
 ''+jsonObject.StationName+'</label></div>' +
 '</li>');
 });
 chooseAllRegionStations();//select all regions
 });
 }
 $('.region-devisions-ul-list li').not(element).removeClass("color-black").addClass("color-gray");
 $(element).removeClass("addBtn").toggleClass("addBtnActive");
 $(element).parent().parent().parent().removeClass("color-gray").addClass("color-black");
 //$('.region-devisions-ul-list').not(element).removeClass("color-black");
 };*/
/*chooseAllRegions = function(){
 $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').prop('checked', 'checked');
 $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
 $(this).trigger("change");
 });
 };*/
/*toggleClassAddBtn = function(element,regionID,regionName,allRegions){
 regionToStationItems.length=0;//update new stations selected from the region
 var allRegionIds=[]; // array to hold region ids
 if(allRegions == 'true'){
 $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
 $(this).prop('checked', 'checked');
 allRegionIds.push(parseInt($(this).val()));
 $('.region-devisions-ul-list li').not(this).addClass("color-black").removeClass("color-gray");
 $(this).removeClass("addBtn").toggleClass("addBtnActive");
 $(this).closest('li').removeClass("color-gray").addClass("color-black");
 });
 getStationsByArrayOfRegionIds(allRegionIds).done(function(dataJson) {
 $('#Region .proccedSelectionBtn').show();//show button after json response
 $('.regionNameHdr').html('Stations for Region').attr('title','Stations for Region');
 $('.region-station-list').empty();
 $.each(dataJson, function(index,jsonObject){
 $('.region-station-list').append('<li>' +
 '<div class="checkbox"><label><input type="checkbox" onchange="' +
 'regionToStationListFn(this,\''+jsonObject.RegionName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
 ''+jsonObject.RegionId+')">' +
 ''+jsonObject.StationName+'</label></div>' +
 '</li>');
 });
 chooseAllRegionStations();//select all stations for region
 });
 }else{
 if($(element).is(':checked')){
 $(element).closest('li').removeClass("color-gray").addClass("color-black");
 allRegionIds.length=0;
 $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
 if($(this).prop("checked") == true){
 allRegionIds.push(parseInt($(this).val()));
 $(this).removeClass("addBtn").toggleClass("addBtnActive");
 }else{
 $(this).closest('li').removeClass("color-black").addClass("color-gray");
 }
 });
 console.log("singleList",allRegionIds);
 getStationsByArrayOfRegionIds(allRegionIds).done(function(dataJson) {
 $('#Region .proccedSelectionBtn').show();//show button after json response
 $('.regionNameHdr').html('Stations for Region').attr('title','Stations for Region');
 $('.region-station-list').empty();
 $.each(dataJson, function(index,jsonObject){
 $('.region-station-list').append('<li>' +
 '<div class="checkbox"><label><input type="checkbox" onchange="' +
 'regionToStationListFn(this,\''+jsonObject.RegionName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
 ''+jsonObject.RegionId+')">' +
 ''+jsonObject.StationName+'</label></div>' +
 '</li>');
 });
 chooseAllRegionStations();//select all regions
 });
 }else{
 allRegionIds.length=0;
 $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
 if($(this).prop("checked") == true){
 allRegionIds.push(parseInt($(this).val()));
 }else{
 $(this).closest('li').removeClass("color-black").addClass("color-gray");
 }
 });
 console.log("cleared Arrays",allRegionIds);
 getStationsByArrayOfRegionIds(allRegionIds).done(function(dataJson) {
 $('#Region .proccedSelectionBtn').show();//show button after json response
 $('.regionNameHdr').html('Stations for Region').attr('title','Stations for Region');
 $('.region-station-list').empty();
 $.each(dataJson, function(index,jsonObject){
 $('.region-station-list').append('<li>' +
 '<div class="checkbox"><label><input type="checkbox" onchange="' +
 'regionToStationListFn(this,\''+jsonObject.RegionName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
 ''+jsonObject.RegionId+')">' +
 ''+jsonObject.StationName+'</label></div>' +
 '</li>');
 });
 chooseAllRegionStations();//select all stations for region
 });
 }
 }
 };*/
toggleClassAddBtn = function(element,regionID,regionName,allRegions){
    regionToStationItems.length=0;//update new stations selected from the region
    var allRegionIds=[],noDuplicateRegionsINRegions,noDuplicateStationsINRegions,tempRegionArr=[]; // array to hold region ids
    if(allRegions == 'true'){
        $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
            $(this).prop('checked', 'checked');
            allRegionIds.push(parseInt($(this).val()));
            $('.region-devisions-ul-list li').not(this).addClass("color-black").removeClass("color-gray");
            $(this).removeClass("addBtn").toggleClass("addBtnActive");
            $(this).closest('li').removeClass("color-gray").addClass("color-black");
        });
        tempRegionArr.length=0;
        getStationsByArrayOfRegionIds(allRegionIds).done(function(dataJson) {
            $('#Region .proccedSelectionBtn').show();//show button after json response
            $('.regionNameHdr').html('Stations for Region').attr('title','Stations for Region');
            $('.region-station-list').empty();
            $.each(dataJson, function(index, jsonObject){
                noDuplicateRegionsINRegions = $.grep(tempRegionArr, function(e){ return e.RegionId == jsonObject.RegionId; });
                if(noDuplicateRegionsINRegions.length <= 0) {
                    emptyAllErros();
                    /* close alert dilogs */
                    $('.region-station-list').append('<li class="selected-region-stations' + jsonObject.RegionId + '">' +
                        '<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                        '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.RegionName + '">' + jsonObject.RegionName + ' </span>' +
                        '<span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="clearAllBtnInRegion pull-right">' +
                        '<span class="totCountOfSelectedItems">' + 0 + '</span>' +
                        '<span class="selectAll-clearBtns">' +
                        '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrRegionStationItems(' + jsonObject.RegionId + ',' + jsonObject.StationId + ')"></i></span>' +
                        '</span></span><ul class="selected-region-list-items' + jsonObject.RegionId + ' list-unstyled" style="padding: 18px;">' +
                        '</ul></li>');
                }

                noDuplicateStationsINRegions = $.grep(tempRegionArr, function(e){ return e.StationId == jsonObject.StationId; });
                if(noDuplicateStationsINRegions.length <= 0) {
                    $('.selected-region-list-items' + jsonObject.RegionId + '').append('<li>' +
                        '<div class="checkbox"><label><input type="checkbox" onchange="' +
                        'regionToStationListFn(this,\'' + jsonObject.RegionName + '\',\'' + jsonObject.StationName + '\',' + jsonObject.StationId + ',' +
                        '' + jsonObject.RegionId + ')">' +
                        '' + jsonObject.StationName + '</label></div>' +
                        '</li>');
                    tempRegionArr.push(jsonObject);
                    $('.selected-region-stations' + jsonObject.RegionId + '  span.totCountOfSelectedItems').text($('.selected-region-list-items' + jsonObject.RegionId + ' li').length);
                }
            });
            chooseAllRegionStations();//select all stations for region
        });
    }else{
        if($(element).is(':checked')){
            $(element).closest('li').removeClass("color-gray").addClass("color-black");
            allRegionIds.length=0;
            $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
                if($(this).prop("checked") == true){
                    allRegionIds.push(parseInt($(this).val()));
                    $(this).removeClass("addBtn").toggleClass("addBtnActive");
                }else{
                    $(this).closest('li').removeClass("color-black").addClass("color-gray");
                }
            });
            tempRegionArr.length=0;
            getStationsByArrayOfRegionIds(allRegionIds).done(function(dataJson) {
                $('#Region .proccedSelectionBtn').show();//show button after json response
                $('.regionNameHdr').html('Stations for Region').attr('title','Stations for Region');
                $('.region-station-list').empty();
                $.each(dataJson, function(index, jsonObject){
                    noDuplicateRegionsINRegions = $.grep(tempRegionArr, function(e){ return e.RegionId == jsonObject.RegionId; });
                    if(noDuplicateRegionsINRegions.length <= 0) {
                        emptyAllErros();
                        /* close alert dilogs */
                        $('.region-station-list').append('<li class="selected-region-stations' + jsonObject.RegionId + '">' +
                            '<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                            '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.RegionName + '">' + jsonObject.RegionName + ' </span>' +
                            '<span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="clearAllBtnInRegion pull-right">' +
                            '<span class="totCountOfSelectedItems">' + 0 + '</span>' +
                            '<span class="selectAll-clearBtns">' +
                            '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrRegionStationItems(' + jsonObject.RegionId + ',' + jsonObject.StationId + ')"></i></span>' +
                            '</span></span><ul class="selected-region-list-items' + jsonObject.RegionId + ' list-unstyled" style="padding: 18px;">' +
                            '</ul></li>');
                    }

                    noDuplicateStationsINRegions = $.grep(tempRegionArr, function(e){ return e.StationId == jsonObject.StationId; });
                    if(noDuplicateStationsINRegions.length <= 0) {
                        $('.selected-region-list-items' + jsonObject.RegionId + '').append('<li>' +
                            '<div class="checkbox"><label><input type="checkbox" onchange="' +
                            'regionToStationListFn(this,\'' + jsonObject.RegionName + '\',\'' + jsonObject.StationName + '\',' + jsonObject.StationId + ',' +
                            '' + jsonObject.RegionId + ')">' +
                            '' + jsonObject.StationName + '</label></div>' +
                            '</li>');
                        tempRegionArr.push(jsonObject);
                        $('.selected-region-stations' + jsonObject.RegionId + '  span.totCountOfSelectedItems').text($('.selected-region-list-items' + jsonObject.RegionId + ' li').length);
                    }
                });
                chooseAllRegionStations();//select all stations for region
            });
        }else{
            allRegionIds.length=0;
            $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
                if($(this).prop("checked") == true){
                    allRegionIds.push(parseInt($(this).val()));
                }else{
                    $(this).closest('li').removeClass("color-black").addClass("color-gray");
                }
            });
            tempRegionArr.length=0;
            getStationsByArrayOfRegionIds(allRegionIds).done(function(dataJson) {
                $('#Region .proccedSelectionBtn').show();//show button after json response
                $('.regionNameHdr').html('Stations for Region').attr('title','Stations for Region');
                $('.region-station-list').empty();
                $.each(dataJson, function(index, jsonObject){
                    noDuplicateRegionsINRegions = $.grep(tempRegionArr, function(e){ return e.RegionId == jsonObject.RegionId; });
                    if(noDuplicateRegionsINRegions.length <= 0) {
                        emptyAllErros();
                        /* close alert dilogs */
                        $('.region-station-list').append('<li class="selected-region-stations' + jsonObject.RegionId + '">' +
                            '<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                            '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.RegionName + '">' + jsonObject.RegionName + ' </span>' +
                            '<span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="clearAllBtnInRegion pull-right">' +
                            '<span class="totCountOfSelectedItems">' + 0 + '</span>' +
                            '<span class="selectAll-clearBtns">' +
                            '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrRegionStationItems(' + jsonObject.RegionId + ',' + jsonObject.StationId + ')"></i></span>' +
                            '</span></span><ul class="selected-region-list-items' + jsonObject.RegionId + ' list-unstyled" style="padding: 18px;">' +
                            '</ul></li>');
                    }

                    noDuplicateStationsINRegions = $.grep(tempRegionArr, function(e){ return e.StationId == jsonObject.StationId; });
                    if(noDuplicateStationsINRegions.length <= 0) {
                        $('.selected-region-list-items' + jsonObject.RegionId + '').append('<li>' +
                            '<div class="checkbox"><label><input type="checkbox" onchange="' +
                            'regionToStationListFn(this,\'' + jsonObject.RegionName + '\',\'' + jsonObject.StationName + '\',' + jsonObject.StationId + ',' +
                            '' + jsonObject.RegionId + ')">' +
                            '' + jsonObject.StationName + '</label></div>' +
                            '</li>');
                        tempRegionArr.push(jsonObject);
                        $('.selected-region-stations' + jsonObject.RegionId + '  span.totCountOfSelectedItems').text($('.selected-region-list-items' + jsonObject.RegionId + ' li').length);
                    }
                });
                chooseAllRegionStations();//select all stations for region
            });
        }
    }
};

clearAllRegions = function(){
    regionToStationItems.length=0;
    $('.region-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
        $(this).prop('checked', '');
        $(this).removeClass("addBtnActive").toggleClass("addBtn");
        $(this).closest('li').addClass("color-gray").removeClass("color-black");
    });
};
clrRegionStationItems = function(regionId,stationId){
    var regionToStationItemTemp=[];
    $(".selected-region-stations"+regionId).remove();
    $.each(regionToStationItems,function(index,jsonObject){
        if(jsonObject.RegionId!=regionId){
            regionToStationItemTemp.push(jsonObject);
        }
    });
    regionToStationItems.length=0;
    regionToStationItems=regionToStationItemTemp;
};



