/**
 * Created by Chethan JN on 13-06-2016.
 */
var stationToSelectedItems=[];
var finalStationStationItems=[];
var finalArrayForSubmit=[];

stationToSelectedListFn = function(element, StationName, StationId){
    if($(element).is(':checked')){
        stationToSelectedItems.push({StationId:StationId,StationName:StationName});
    }else{
        $.each(stationToSelectedItems,function(index,jsonObject){
            if(jsonObject.StationName==StationName){
                stationToSelectedItems.splice(index,1);
            }
        });
    }
};
chooseAllStationStations = function(){
    stationToSelectedItems.length=0;
    $('.station-sortings-list .listNavShow input:checkbox').prop('checked', 'checked');
    $('.station-sortings-list .listNavShow input:checkbox').each(function(){
        $(this).trigger("change");
    });
};
unCheckAllStationStationsCheckBoxs = function(){
    $('.station-sortings-list .listNavShow input:checkbox').prop('checked', '');
    stationToSelectedItems.length=0;
};
moveAllStationStationsToSelectedList = function(){
    $('.stationClearBtn.fa-times-circle').show();
    $.each(stationToSelectedItems, function(index, jsonObject){
        var noDuplicateStationStations = $.grep(finalStationStationItems, function(e){ return e.StationId == jsonObject.StationId; });
        if(noDuplicateStationStations.length <= 0){
            emptyAllErros(); /* close alert dilogs */
            $('.selected-station-station-list-items').append('<li class="station-station-'+jsonObject.StationId+'" ' +
                '>'
                +jsonObject.StationName+'  <span class="closeIcon" onclick="clrFinalStationStationItem('+jsonObject.StationId+')" ' +
                '></span></li>');
            finalStationStationItems.push(jsonObject);
            $('#locationerrors ul').html('<li class="alert alert-success">Stations added successfully !</li>');
            emptyAllErros();
        }else{
            $('#locationerrors ul').html('<li class="alert alert-danger">These Stations added already !</li>');
            emptyAllErros(); /* close alert dilogs */
        }
    });
};
clrFinalStationStationItems = function(){
    $('.selected-station-station-list-items').empty();
    finalStationStationItems.length=0;//clear all selected stations
    $('.stationClearBtn.fa-times-circle').hide();
};
clrFinalStationStationItem = function(stationId){
    $('.station-station-'+stationId).remove();
    $.each(finalStationStationItems,function(index,value){
        if(value.StationId==stationId){
            finalStationStationItems.splice(index,1);
        }
    });
};


/*** FINAL PO locations for submit and preview***/
goToPreviewWithLocationData = function (){
    finalArrayForSubmit.length=0;
    $('.preview-selected-regions').empty();
    $('.preview-selected-stations').empty();
    $('.preview-selected-states').empty();
    emptyAllErros(); /* close alert dilogs */
    /* region selected items */
    if(finalRegionStationItems.length > 0){
        $('.regionSection').show();
        $.each(finalRegionStationItems, function(index, jsonObject){
            var noDuplicateStationsForSubmit;
            noDuplicateStationsForSubmit = $.grep(finalArrayForSubmit, function(e){ return e.StationId == jsonObject.StationId && e.type == 'R'; });
            if(noDuplicateStationsForSubmit.length == 0){
                finalArrayForSubmit.push({"groupId":jsonObject.RegionId,"groupName":jsonObject.RegionName,
                    "locationType":"R","stationId":jsonObject.StationId,"stationName":jsonObject.StationName});


                $('.preview-selected-regions').append('<span>'+jsonObject.RegionName+', '+'</span>');
                $('.preview-selected-stations').append('<span>'+jsonObject.StationName+', '+'</span>');
            }
        });
        $('#location-step').addClass('rightmark'); /* add right mark for cretive form */
    }else{
        $('.regionSection').hide();
    }


    /* state selected Items */
    if(finalStateStationItems.length > 0) {
        $('.stateSection').show();
        $.each(finalStateStationItems, function (index, jsonObject) {
            var noDuplicateStationsForSubmit;
            noDuplicateStationsForSubmit = $.grep(finalArrayForSubmit, function (e) {
                return e.StationId == jsonObject.StationId && e.type == 'E';
            });
            if (noDuplicateStationsForSubmit.length == 0) {
                finalArrayForSubmit.push({
                    "groupId": jsonObject.StateId, "groupName": jsonObject.StateName,
                    "locationType": "E", stationId: jsonObject.StationId, "stationName": jsonObject.StationName
                });
                $('.preview-selected-states').append('<span>' + jsonObject.StateName + ', ' + '</span>');
                $('.preview-selected-stations').append('<span>' + jsonObject.StationName + ', ' + '</span>');
            }
        });
        $('#location-step').addClass('rightmark'); /* add right mark for cretive form */
    }else{
        $('.stateSection').hide();
    }

    /* station selected Items */
    if(finalStationStationItems.length > 0) {
        $('.stationSection').show();
        $.each(finalStationStationItems, function (index, jsonObject) {
            var noDuplicateStationsForSubmit;
            noDuplicateStationsForSubmit = $.grep(finalArrayForSubmit, function (e) {
                return e.StationId == jsonObject.StationId && e.type == 'S';
            });
            if (noDuplicateStationsForSubmit.length == 0) {
                finalArrayForSubmit.push({
                    "groupId": jsonObject.StationId, "groupName": jsonObject.StationName,
                    "locationType": "S", stationId: jsonObject.StationId, "stationName": jsonObject.StationName
                });
                $('.preview-selected-stations').append('<span>' + jsonObject.StationName + ', ' + '</span>');
            }
        });
        $('#location-step').addClass('rightmark'); /* add right mark for cretive form */
    }
    else{
        $('.stationSection').hide();
    }
    $('#po-location').val(JSON.stringify(finalArrayForSubmit));
    removeDuplicateTextInPreviewLocationBlock('.preview-selected-regions span');
    removeDuplicateTextInPreviewLocationBlock('.preview-selected-stations span');
    removeDuplicateTextInPreviewLocationBlock('.preview-selected-states span');
    //collapse and expand for regions, state, stations blocks for dynamic content
    setTimeout(function(){
        hideAndShowLongText('.long-text');
    },200);
};

/**** function to remove duplicate text values in preview location block ****/
removeDuplicateTextInPreviewLocationBlock = function(element){
    var obj = {};
    $(element).each(function(){
        var text = $.trim($(this).text());
        if(obj[text]){
            $(this).remove();
        } else {
            obj[text] = true;
        }
    });
};