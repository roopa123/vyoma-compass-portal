/**
 * Created by Chethan JN on 16-06-2016.
 */
jQuery(function ()
{
    $('input').removeClass('whiteColor');
    // This Trigger When We Clone the Campaign All details binded in Form
    if($('#binding').val() == 1){
        formBinding();
    }
    //PO FLOW PARTIAL COMPLETION - SAVE AND EXIT BUTTON CLICK
    $('#campaignbtn, #creativebtn, #locationbtn, #previewsavebtn, #addcampaignbtn')
        .click(function (event)
        {
            var buttonClicked = $(this).attr("id");
            emptyAllErros(); /* empty all errors */
            goToPreviewWithLocationData(); /* binding locations data in the preview */
            var locationVal = $.trim($('#po-location').val());
            var locationValue = $.trim(locationVal.slice(1, -1));
            $('#stepsComplete').val(0);
            $("#purchaseorderform").valid(); /* client side validations */
            if($("#purchaseorderform").valid()){ /* To Check Advertiser Has Creatives */
            checkAdvertiser($('#advertiser').val()).done(function (data) {
                if (data.result == 'fail') {
                    displayPOFormValaidationMessage('The Selected Advertiser has No Creatives !');
                    emptyAllErros(); /* close alert dilogs */
                }
                if (data.result == 'success'){
                    if($('#creativeValues').val().length > 0) {
                        checkCreative(creativeIds).done(function (data) {
                            /* To Check Only Active Creatives in Grid */
                            if (data.result == 'fail') {
                                displayCreativeFormValaidationMessage('Please Add only Active Creatives to the Campaign');                                emptyAllErros(); /* close alert dilogs */
                            }
                            else
                            {
                               submitData(locationValue, buttonClicked); 
                            }
                        });
                    }
                    else
                    {
                       submitData(locationValue, buttonClicked);  
                    }
                }
            });
            }
            event.preventDefault();
            
        });//End of Insertion


    $('#posubmitbtn').click(function (event)
    {
        var buttonClicked = $(this).attr("id");
        emptyAllErros(); /* empty all errors */
        $('#stepsComplete').val(1);
        goToPreviewWithLocationData(); /* binding location data at preview */
        var sendData = setAddPOFormPostData();
        var locationVal = $.trim($('#po-location').val());
        var locationValue = $.trim(locationVal.slice(1, -1));
        $("#purchaseorderform").valid(); /* client side validation */
        if($("#purchaseorderform").valid()){
            if(locationValue==""){
                displayLocationFormValaidationMessage('Please Select Location');
                emptyAllErros(); /* close alert dilogs */
            }else{
                checkAdvertiser($('#advertiser').val()).done(function (data) {
                    if (data.result == 'fail'){
                        displayPOFormValaidationMessage('The Selected Advertiser has No Creatives !');
                        emptyAllErros(); /* close alert dilogs */
                    }
                    if (data.result == 'success'){
                        if($('#creativeValues').val().length > 0) {
                            checkCreative(creativeIds).done(function (data) {
                                if (data.result == 'fail') {
                                    displayPOFormValaidationMessage('The Selected Advertiser has No Creatives !');
                                    emptyAllErros(); /* close alert dilogs */   
                                }
                                if (data.result == 'success') {
                                    ajaxcall(sendData, buttonClicked);
                                }
                            });
                        }else{
                            displayCreativeFormValaidationMessage('Creatives Required');  
                        }
                    }
                });
            }
        }
        event.preventDefault();
    });//End of Insertion

    if($('#formurl').val() == '/addcampaign'){
        showCampaignForm();
        if($('#binding').val() == 0) {
            $('#po-step').addClass('rightmark');
            $('#campaign-step').removeAttr('disabled').addClass('btn-primary');
            $('#preview-step').removeAttr('disabled').addClass('btn-primary'); /* by default add right mark for po and campaign form */
            jQuery('#start-date-campaign').val(jQuery('#startDate').val());
            jQuery('#end-date-campaign').val(jQuery("#endDate").val());
        }
        else if($('#binding').val() == 1){
            $('#po-step,#campaign-step').addClass('rightmark');
        }
    }
    if($('#formurl').val()=='/purchaseorderstore') {
        $('.setup-content').not('#step-1').hide();
        $('#step-1').show();
    }
});

function ajaxcall(sendData,buttonclicked){
    emptyAllErros(); /* empty all errors */
    var returnURL = '/purchaseorder';
    var formURL = $('#formurl').val();
    $.ajax({
        type: 'post',
        url:  formURL,
        data: sendData,
        dataType: 'json'

    }).done(function (data) {
        if (data.result == 'fail')
        {
            $.each(data.errors, function (key, value)
            {
                displayValidationMessageInParticularPage(value);
            });
        }
        if (data.result == 'success')
        {
            if(buttonclicked == 'addcampaignbtn')
            {
                returnURL = '/purchaseorder/'+data.obj.POCampaignId+'/add/campaign';
            }
            redirectFn(returnURL);
        }
    });
}

function addAnotherCampaign(locationValue, buttonClicked)
{
    if(locationValue=="") {
        bootbox.confirm(global_CampaignAlertMessage, function(result) {
        if(result == true) {
        var sendData = setAddPOFormPostData();
        ajaxcall(sendData,buttonClicked);      
        }
        });
    }
    else 
    {
    $('#stepsComplete').val(1);
    var sendData = setAddPOFormPostData();
    ajaxcall(sendData,buttonClicked);
    }  
}

function submitData(locationValue, buttonClicked)
{
    if (buttonClicked == "addcampaignbtn") {
        addAnotherCampaign(locationValue, buttonClicked);
    } else {
        var sendData = setAddPOFormPostData();
        ajaxcall(sendData,buttonClicked);
    }
}
 
 
