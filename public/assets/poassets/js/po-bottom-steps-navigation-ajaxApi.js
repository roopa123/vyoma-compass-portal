jQuery(function () {
    /* Navigation Button */
    $('#goToCampign,#goToCreative,#goToLocation,#goToPreview').click(function(event){
        var buttonclicked=$(this).attr("id");
        if (buttonclicked == 'goToCampign'){
            var sendData = setFormDataForPostToCheckValidation(); /* data object for ajax */
            $("#purchaseorderform").valid(); /* client side validation */
            if($("#purchaseorderform").valid()){
                checkAdvertiser($('#advertiser').val()).done(function(data){
                    if (data.result == 'fail'){
                        $('#poerrors ul').html('<li class="alert alert-danger">The Selected Advertiser Has No Creatives</li>');
                        emptyAllErros(); /* empty errors */
                    }
                    if (data.result == 'success'){
                        ajaxCall(sendData, buttonclicked);
                    }
                });
            }
        }
        else if (buttonclicked == 'goToCreative'){
            var sendData = setFormDataForPostToCheckValidation();
            $("#purchaseorderform").valid();
            if($("#purchaseorderform").valid()){                
                ajaxCall(sendData, buttonclicked);
            }
        }
        else if (buttonclicked == 'goToLocation'){
            var sendData = setFormDataForPostToCheckValidation();
            if($('#creativeValues').val().length > 0){
                ajaxCall(sendData, buttonclicked);
            }
            else{
                $('#creativeerrors').html('<label class="error" ' +
                    'id="salesrep-error">Please Select Creatives ! </label>');
            }
        }else if (buttonclicked == 'goToPreview'){
            var sendData = setFormDataForPostToCheckValidation();
            var previewData = JSON.parse($('#postjsondata').val());
            bindFormDataToPreviewPage(previewData);
            doEnableDisableNavigationButtons('step-4', 'step-5');
            goToPreviewWithLocationData(); /*Binding locations in preview*/
            setCreativeNameToPreview(); /*Binding craetives in preview*/
        }
        event.preventDefault(); // stop the form from submitting the normal way and refreshing the page
    });
});

function ajaxCall(sendData, buttonClicked){
    emptyAllErros(); /* clearing all issues */
    if(buttonClicked == 'goToCampign') {
      validatePo(sendData).done(function(data){
            if (data.result == 'fail'){
                $.each( data.errors, function( key, value){
                    $('#poerrors ul').html('<li class="alert alert-danger">'+value+'</li>');
                });
            }
            if (data.result == 'success'){
                doEnableDisableNavigationButtons('step-1', 'step-2');
            }
        });
    }
    if (buttonClicked == 'goToCreative'){
        validateCampaign(sendData).done(function(data) {
            if (data.result == 'fail'){                
                $.each( data.errors, function( key, value){
                    $('#campaignerrors ul').html('<li class="alert alert-danger">'+value+'</li>');
                    emptyAllErros(); /* empty errors */
                });
            }
         if (data.result == 'success'){
            $('#location-step,#preview-step').removeAttr('disabled').addClass('btn-primary');
                getCreative($("#advertiser>option:selected").val()); /* load creatives based on advertiser */
                if(creativeIds.length > 0) { submitAllSeletedCreativeSelection(); } /* LOAD creative grid based on creatives */
                doEnableDisableNavigationButtons('step-2', 'step-3');
            }
        });
    }
    if (buttonClicked == 'goToLocation') {
        if(creativeIds.length > 0){
            checkCreative(creativeIds).done(function (data){
                    if (data.result == 'fail'){
                        $('#creative-step').removeClass('rightmark');
                        $('#creativegriderrors ul').html('<li class="alert alert-danger" ' +
                            '>Please Add only Active Creatives to the Campaign ! </li>');
                    }
                    if (data.result == 'success'){
                        doEnableDisableNavigationButtons('step-3', 'step-4');
                    }
                });
        }else{
            $('#creativeerrors ul').html('<label class="error" ' +
                'id="salesrep-error">Please Select Creative ! </label>');
        }
    }
}

function setFormDataForPostToCheckValidation(){
    var formData =  {
        'salesrep'              : $('#salesrep').val(),
        'poDate': moment($('input[name=purchase_order_date]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'startDate': moment($('input[name=startDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'endDate': moment($('input[name=endDate]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'advertiser'            : $('#advertiser').val(),
        'branch'                : $('#branch').val(),
        'roReference'           : $('#roreference').val(),
        'advertiserName'        : $("#advertiser option:selected").text(),
        'branchName'            : $("#branch option:selected").text(),
        'salesRepName'          : $("#salesrep option:selected").text(),
        'campaignName'          : $('#CompaignName').val(),
        'campaignStartDate':  moment($('input[name=start_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'campaignEndDate':  moment($('input[name=end_date_campaign]').val(),"DD-MM-YYYY").format("YYYY-MM-DD"),
        'spotsPerDay'           : $('#SpotsPerDay').val(),
        'creativeValues'        : $('#creativeValues').val(),
        'location'              : $('#location').val(),
        'originalPOId'          : $('#originalpoid').val(),
        'campaignId'            : $('#campaignid').val(),
        'mode'                  : $('#mode').val()
    };
    $('#postjsondata').val(JSON.stringify(formData));
    var sendData = { 'sendData': $('#postjsondata').val()};
    return sendData;
}

function bindFormDataToPreviewPage(previewData){
    if(previewData.roReference == '' || previewData.roReference == null){  $('.roreference').parent('li').hide(); }else{
        $('.roreference').parent('li').show();
        $('.roreference').html(previewData.roReference);
    }
    $('.salesrep').html(previewData.salesRepName);
    $('.branchname').html(previewData.branchName);
    $('.advertisername').html(previewData.advertiserName);
    $('.podate').html(moment(previewData.poDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    $('.postart').html(moment(previewData.startDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    $('.poend').html(moment(previewData.endDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    $('.campaignname').html(previewData.campaignName);
    $('.campaignstart').html(moment(previewData.campaignStartDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    $('.campaignend').html(moment(previewData.campaignEndDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    $('.creativeEndDate').html(moment(previewData.campaignEndDate,"YYYY-MM-DD").format("DD-MM-YYYY"));
    $('.slots').html(previewData.spotsPerDay);
}

