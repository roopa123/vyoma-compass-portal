/**
 * Created by Chethan JN on 09-06-2016.
 */
var stateToStationItems=[];
var finalStateStationItems=[];

stateToStationListFn = function(element, StateName, StationName, StationId, StateId){
    if($(element).is(':checked')){
        stateToStationItems.push({StateName:StateName,StationId:StationId,StationName:StationName,StateId:StateId});
    }else{
        $.each(stateToStationItems,function(index,jsonObject){
            if(jsonObject.StationName==StationName){
                stateToStationItems.splice(index,1);
            }
        });
    }
};
chooseAllStateStations = function(){
    stateToStationItems.length=0;
    $('.state-station-list input:checkbox').prop('checked', 'checked');
    $('.state-station-list input:checkbox').each(function(){
        $(this).trigger("change");
    });
};
clearAllStateStations = function(){
    $('.state-station-list input:checkbox').prop('checked', '');
    stateToStationItems.length=0;
};
moveAllStateStationsToSelectedList = function(){
    $.each(stateToStationItems, function(index, jsonObject){
        var noDuplicateState,noDuplicateStateStations;
        noDuplicateStateStations = $.grep(finalStateStationItems, function(e){ return e.StateId == jsonObject.StateId; });
        if(noDuplicateStateStations.length <= 0) {
            $('.selected-state-station-lists').append('<li class="state-selected-stations'+jsonObject.StateId+'">' +
                '<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.StateName + '">' + jsonObject.StateName + ' </span><span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="pull-right">' +
                '<span class="totCountOfSelectedItems">'+0+'</span>' +
                '<span class="selectAll-clearBtns">' +
                '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrFinalStateStationItems('+ jsonObject.StateId +')"></i>' +
                '</span>' +
                '</span></span><ul class="state-selected-list-items' + jsonObject.StateId + ' list-unstyled" style="padding: 18px;">' +
                '</ul></li>');
        }

        noDuplicateStateStations = $.grep(finalStateStationItems, function(e){ return e.StationId == jsonObject.StationId; });
        if(noDuplicateStateStations.length == 0){
            $('.state-selected-list-items'+jsonObject.StateId+'').append('<li class="state-station-'+jsonObject.StationId+'">'
                +jsonObject.StationName+'  <span class="closeIcon" onclick="clrFinalStateStationItem('+jsonObject.StationId+','+jsonObject.StateId+')"></span></li>');
            finalStateStationItems.push(jsonObject);
            $('#locationerrors ul').html('<li class="alert alert-success">Stations added successfully !</li>');
            emptyAllErros();
        }else{
            $('#locationerrors ul').html('<li class="alert alert-danger">These Stations added already !</li>');
            emptyAllErros(); /* close alert dilogs */
        }
        $('.state-selected-stations'+jsonObject.StateId+'  span.totCountOfSelectedItems').html($('#State .state-selected-list-items'+jsonObject.StateId+' li').length);
    });
};
clrFinalStateStationItems = function(stateId){
    var stateFinalArr=[];
    $('.state-selected-stations'+stateId).empty();
    $('.state-selected-stations'+stateId+'  span.totCountOfSelectedItems').html(0);
    $.each(finalStateStationItems,function(index,value){
        if(value.StateId!=stateId){
            stateFinalArr.push(value);
        }
    });
    finalStateStationItems.length=0;
    finalStateStationItems=stateFinalArr;
};
clrFinalStateStationItem = function(stationId,stateId){
    $('.state-station-'+stationId).remove();
    $('.state-selected-stations'+stateId+'  span.totCountOfSelectedItems').html($('#State .state-selected-list-items'+stateId+' li').length);
    if($('.state-selected-list-items'+stateId+' li').length==0){
        $('.state-selected-stations'+stateId).empty();
    }
    $.each(finalStateStationItems,function(index,jsonObject){
        if(jsonObject.StationId==stationId){
            finalStateStationItems.splice(index,1);
        }
    });
};
/*toggleClassStateAddBtn = function(element,stateId,stateName){
 $('#regionCheckBtn label input').prop('checked', '');// reset all active buttons except current one
 $(element).prop('checked', 'checked');// set active for current one
 stateToStationItems.length=0;//update new stations selected from the region
 if($(element).is(':checked')){
 $.ajax({
 type: "get",
 url: "/api/state/"+stateId,
 dataType: "json"
 }).done(function(dataJson) {
 $('#State .proccedSelectionBtn').show();//show button after json response
 $('.stateNameHdr').html(stateName);
 $('.stateNameHdr').attr('title',stateName);
 $('.state-station-list').empty();
 $.each(dataJson, function(index,jsonObject){
 $('.state-station-list').append('<li>' +
 '<div class="checkbox"><label><input type="checkbox" onchange="' +
 'stateToStationListFn(this,\''+stateName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
 ''+jsonObject.StateId+')">' +
 ''+jsonObject.StationName+'</label></div>' +
 '</li>');
 });
 chooseAllStateStations();//choose all regions
 });
 }
 $('.state-devisions-ul-list li').not(element).removeClass("color-black").addClass("color-gray");
 $(element).removeClass("addBtn").toggleClass("addBtnActive");
 $(element).parent().parent().parent().removeClass("color-gray").addClass("color-black");
 };*/

toggleClassStateAddBtn = function(element,stateId,stateName,allStates){
    stateToStationItems.length=0; //update new stations selected from the state
    var allStateIds=[],noDuplicateStateINStates,noDuplicateStationsINStates,tempStateArr=[]; //array to hold state ids
    if(allStates == 'true'){
        $('.state-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
            $(this).prop('checked', 'checked');
            allStateIds.push(parseInt($(this).val()));
            $(this).removeClass("addBtn").toggleClass("addBtnActive");
            $(this).closest('li').removeClass("color-gray").addClass("color-black");
        });
        tempStateArr.length=0;
        getStationsByArrayOfStateIds(allStateIds).done(function(dataJson) {
            $('#State .proccedSelectionBtn').show();//show button after json response
            $('.stateNameHdr').html('Stations for State').attr('title','Stations for State');
            $('.state-station-list').empty();
            $.each(dataJson, function(index,jsonObject){
                noDuplicateStateINStates = $.grep(tempStateArr, function(e){ return e.StateId == jsonObject.StateId; });
                if(noDuplicateStateINStates.length <= 0) {
                    emptyAllErros();
                    /* close alert dilogs */
                    $('.state-station-list').append('<li class="selected-state-stations' + jsonObject.StateId + '">' +
                        '<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                        '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.StateName + '">' + jsonObject.StateName + ' </span>' +
                        '<span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="clearAllBtnInRegion pull-right">' +
                        '<span class="totCountOfSelectedItems">' + 0 + '</span>' +
                        '<span class="selectAll-clearBtns">' +
                        '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrStateStationItems(' + jsonObject.StateId + ',' + jsonObject.StationId + ')"></i></span>' +
                        '</span></span><ul class="selected-state-list-items' + jsonObject.StateId + ' list-unstyled" style="padding: 18px;">' +
                        '</ul></li>');
                }

                noDuplicateStationsINStates = $.grep(tempStateArr, function(e){ return e.StationId == jsonObject.StationId; });
                if(noDuplicateStationsINStates.length <= 0) {
                    $('.selected-state-list-items' + jsonObject.StateId + '').append('<li>' +
                        '<div class="checkbox"><label><input type="checkbox" onchange="' +
                        'stateToStationListFn(this,\''+jsonObject.StateName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
                        ''+jsonObject.StateId+')" value="'+jsonObject.StateId+'">' +
                        ''+jsonObject.StationName+'</label></div>' +
                        '</li>');
                    tempStateArr.push(jsonObject);
                    $('.selected-state-stations' + jsonObject.StateId + '  span.totCountOfSelectedItems').text($('.selected-state-list-items' + jsonObject.StateId + ' li').length);
                }
            });
            chooseAllStateStations();//choose all regions
        });
    }else{
        if($(element).is(':checked')){
            $(element).closest('li').removeClass("color-gray").addClass("color-black");
            allStateIds.length=0;
            $('.state-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
                if($(this).prop("checked") == true){
                    allStateIds.push(parseInt($(this).val()));
                    $(this).removeClass("addBtn").toggleClass("addBtnActive");
                }else{
                    $(this).closest('li').removeClass("color-black").addClass("color-gray");
                }
            });
            tempStateArr.length=0;
            getStationsByArrayOfStateIds(allStateIds).done(function(dataJson) {
                $('#State .proccedSelectionBtn').show();//show button after json response
                $('.stateNameHdr').html('Stations for state').attr('title','Stations for State');
                $('.state-station-list').empty();
                $.each(dataJson, function(index,jsonObject){
                    /*  $('.state-station-list').append('<li>' +
                     '<div class="checkbox"><label><input type="checkbox" onchange="' +
                     'stateToStationListFn(this,\''+jsonObject.StateName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
                     ''+jsonObject.StateId+')" value="'+jsonObject.StateId+'">' +
                     ''+jsonObject.StationName+'</label></div>' +
                     '</li>');*/
                    noDuplicateStateINStates = $.grep(tempStateArr, function(e){ return e.StateId == jsonObject.StateId; });
                    if(noDuplicateStateINStates.length <= 0) {
                        emptyAllErros();
                        /* close alert dilogs */
                        $('.state-station-list').append('<li class="selected-state-stations' + jsonObject.StateId + '">' +
                            '<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                            '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.StateName + '">' + jsonObject.StateName + ' </span>' +
                            '<span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="clearAllBtnInRegion pull-right">' +
                            '<span class="totCountOfSelectedItems">' + 0 + '</span>' +
                            '<span class="selectAll-clearBtns">' +
                            '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrStateStationItems(' + jsonObject.StateId + ',' + jsonObject.StationId + ')"></i></span>' +
                            '</span></span><ul class="selected-state-list-items' + jsonObject.StateId + ' list-unstyled" style="padding: 18px;">' +
                            '</ul></li>');
                    }

                    noDuplicateStationsINStates = $.grep(tempStateArr, function(e){ return e.StationId == jsonObject.StationId; });
                    if(noDuplicateStationsINStates.length <= 0) {
                        $('.selected-state-list-items' + jsonObject.StateId + '').append('<li>' +
                            '<div class="checkbox"><label><input type="checkbox" onchange="' +
                            'stateToStationListFn(this,\''+jsonObject.StateName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
                            ''+jsonObject.StateId+')" value="'+jsonObject.StateId+'">' +
                            ''+jsonObject.StationName+'</label></div>' +
                            '</li>');
                        tempStateArr.push(jsonObject);
                        $('.selected-state-stations' + jsonObject.StateId + '  span.totCountOfSelectedItems').text($('.selected-state-list-items' + jsonObject.StateId + ' li').length);
                    }
                });
                chooseAllStateStations();//choose all regions
            });
        }else{
            allStateIds.length=0;
            $('.state-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
                if($(this).prop("checked") == true){
                    allStateIds.push(parseInt($(this).val()));
                }else{
                    $(this).closest('li').removeClass("color-black").addClass("color-gray");
                }
            });
            tempStateArr.length=0;
            getStationsByArrayOfStateIds(allStateIds).done(function(dataJson) {
                $('#State .proccedSelectionBtn').show();//show button after json response
                $('.stateNameHdr').html('Stations for State').attr('title','Stations for State');
                $('.state-station-list').empty();
                $.each(dataJson, function(index,jsonObject){
                    noDuplicateStateINStates = $.grep(tempStateArr, function(e){ return e.StateId == jsonObject.StateId; });
                    if(noDuplicateStateINStates.length <= 0) {
                        emptyAllErros();
                        /* close alert dilogs */
                        $('.state-station-list').append('<li class="selected-state-stations' + jsonObject.StateId + '">' +
                            '<span class="col-md-12 col-xs-12 display-inline-block selected-list-in-collapse">' +
                            '<span class="margin-left-10"><span class="stationNameHdr" title="' + jsonObject.StateName + '">' + jsonObject.StateName + ' </span>' +
                            '<span class="collapse-mode" onclick="toggleCollapseClass(this)"></span></span><span class="clearAllBtnInRegion pull-right">' +
                            '<span class="totCountOfSelectedItems">' + 0 + '</span>' +
                            '<span class="selectAll-clearBtns">' +
                            '<i class="fa fa-times-circle" title="Clear All" aria-hidden="true" onclick="clrStateStationItems(' + jsonObject.StateId + ',' + jsonObject.StationId + ')"></i></span>' +
                            '</span></span><ul class="selected-state-list-items' + jsonObject.StateId + ' list-unstyled" style="padding: 18px;">' +
                            '</ul></li>');
                    }

                    noDuplicateStationsINStates = $.grep(tempStateArr, function(e){ return e.StationId == jsonObject.StationId; });
                    if(noDuplicateStationsINStates.length <= 0) {
                        $('.selected-state-list-items' + jsonObject.StateId + '').append('<li>' +
                            '<div class="checkbox"><label><input type="checkbox" onchange="' +
                            'stateToStationListFn(this,\''+jsonObject.StateName+'\',\''+jsonObject.StationName+'\','+jsonObject.StationId+',' +
                            ''+jsonObject.StateId+')" value="'+jsonObject.StateId+'">' +
                            ''+jsonObject.StationName+'</label></div>' +
                            '</li>');
                        tempStateArr.push(jsonObject);
                        $('.selected-state-stations' + jsonObject.StateId + '  span.totCountOfSelectedItems').text($('.selected-state-list-items' + jsonObject.StateId + ' li').length);
                    }
                });
                chooseAllStateStations();//choose all regions
            });
        }
    }
};

clearAllStates = function(){
    stateToStationItems.length=0;
    $('.state-devisions-ul-list li #regionCheckBtn input:checkbox').each(function() {
        $(this).prop('checked', '');
        $(this).removeClass("addBtnActive").toggleClass("addBtn");
        $(this).closest('li').addClass("color-gray").removeClass("color-black");
    });
};

clrStateStationItems = function(stateID,stationId){
    var stateToStationItemTemp=[];
    $(".selected-state-stations"+stateID).remove();
    $.each(stateToStationItems,function(index,jsonObject){
        if(jsonObject.StateId!=stateID){
            stateToStationItemTemp.push(jsonObject);
        }
    });
    stateToStationItems.length=0;
    stateToStationItems=stateToStationItemTemp;
};