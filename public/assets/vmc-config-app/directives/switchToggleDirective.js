/**
 * Created by Chethan JN on 25-08-2016.
 */
angular.module('mesmerizeNG.combotree', [])
    .directive('combotree', function() {
        return {
            restrict: 'EA',
            // responsible for registering DOM listeners as well as updating the DOM
            link: function(scope, element, attrs) {
                $(element).toggles({drag:false,text:{on:'Enable',off:'Disable'},on:true});
            }
        };
    });
