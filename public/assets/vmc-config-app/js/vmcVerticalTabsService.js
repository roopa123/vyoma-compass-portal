/**
 * Created by Chethan JN on 07-09-2016.
 */
app.factory("vmcVerticalTabsService", function(){
    /* vertical tabs code starting*/
    var tabClasses;
    return{
        initTabs : function () {tabClasses = ["","","","","",""];},
        getTabClass : function (tabNum) {return tabClasses[tabNum];},
        getTabPaneClass : function (tabNum) {return "tab-pane " + tabClasses[tabNum];},
        setActiveTab : function (tabNum) {this.initTabs();tabClasses[tabNum] = "active";},
        goToNextAndPrevTab : function(tabNum){ //go to nextTab and prevTab
            this.initTabs();
            this.setActiveTab(tabNum);
            this.getTabClass(tabNum);
            this.getTabPaneClass(tabNum);
        }
    };
    /* vertical tabs code ends here */
});