app.factory('vmcConfigService',
    function ($http, $q) {
        return {
            getMasterNameByBayID: function (bayId) {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: '/masterName/'+bayId,
                    headers:{'Content-Type':'charset=UTF-8'}
                })
                    .success(function (data, status, headers, config) {
                        deferred.resolve(data);
                    })
                    .error(function (data, status, headers, config) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            }
        }
    });
