/**
 * Created by Chethan JN on 07-09-2016.
 */
app.controller('vmcConfigEdit',['$scope','$log','vmcVerticalTabsService', function($scope,$log,vmcVerticalTabsService) {
    $scope.vmcConfig={};
    $scope.vmcConfig.counterStartTime=new Date();
    $scope.vmcConfig.counterEndTime=new Date();
    $scope.vmcConfig.startTime=new Date();
    $scope.vmcConfig.endTime=new Date();
    $scope.vmcConfig.endTime_sun=new Date();

    $scope.counterStartTimeActive=false;
    $scope.counterEndTimeActive=false;
    $scope.StartTimeActive=false;
    $scope.endTimeActive=false;
    $scope.endTimeActive_sun=false;
    //$log.log(' $scope.name: ' + $scope.name);
    $scope.ismeridian = false;


    $scope.vmcTabService=vmcVerticalTabsService;
    $scope.vmcTabService.initTabs();
    $scope.vmcTabService.setActiveTab(1);//Initialize

    $scope.$watch('vmcConfig.counterNumber',function (){
        $log.log($scope.vmcConfig.counterNumber);
        if($scope.vmcConfig.counterNumber!=undefined){
            $scope.vmcConfig.localIPAddress="192.168.1.1"+$scope.vmcConfig.counterNumber;
        }
    });


    $scope.hostTypeArr=[{type:"Player",id:1},{type:"Master",id:2},{type:"Both",id:3},{type:"ber",id:7}];
    $scope.appArr=[{name:"DDiS",id:1},{name:"VMC",id:2},{name:"Both",id:3}];
    $scope.stationArr=[{name:"Bangalore",id:1},{name:"Delhi",id:2},{name:"Hyderbad",id:3}];
    $scope.bayCodeArr=[{name:"BU",id:1},{name:"DL",id:2},{name:"HD",id:3}];
    $scope.counterTypeArr=[{name:"AA",id:1},{name:"BB",id:2},{name:"CC",id:3}];
    $scope.routerRebootScheduleArr=[{name:"Default",id:1},{name:"Other",id:2}];
    $scope.playerWindowSizeArr=[{name:"0 0 1080 1440 (1080p)"},{name:"20 15 1260 695 (Plasma)"},{name:"15 30 1065 1440 (Crown)"}];
    $scope.hdmiModeArr=[5,16,18];
    $scope.displayTypeArr=["LCD","LED","PLASMA"];
    $scope.displaySizeArr=[32,22,19];
    $scope.cableTypeArr=["HDMI","DVI","Default"];
    $scope.piVersionArr=["Pi B","Pi B+","Pi 2","Pi 3"];
    $scope.playLogUpdateScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];
    $scope.ticketLogUpdateScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];
    $scope.contentUpdateScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];
    $scope.screenShotScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];
}]);