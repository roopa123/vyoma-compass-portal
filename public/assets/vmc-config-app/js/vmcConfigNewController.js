/**
 * Created by Chethan JN on 07-09-2016.
 */
app.controller('vmcConfigNew',['$scope','$log','vmcVerticalTabsService','vmcConfigService','$http',
    function($scope,$log,vmcVerticalTabsService,vmcConfigService,$http) {
        $scope.vmcConfig={};
        $scope.vmcConfig.counterStartTime=new Date();
        $scope.vmcConfig.counterEndTime=new Date();
        /*   $scope.vmcConfig.startTime=new Date();
         $scope.vmcConfig.endTime=new Date();
         $scope.vmcConfig.endTime_sun=new Date();*/

        $scope.counterStartTimeActive=false;
        $scope.counterEndTimeActive=false;
        $scope.StartTimeActive=false;
        $scope.endTimeActive=false;
        $scope.endTimeActive_sun=false;
        $scope.ismeridian = false;

        /** vertical tabs system **/
        $scope.vmcTabService=vmcVerticalTabsService;
        $scope.vmcTabService.initTabs();
        $scope.vmcTabService.setActiveTab(1);//Initialize

        /** watch the counterNumber change and update to localIp Address **/
        $scope.$watch('vmcConfig.counterNumber',function (current){
            if(current!=undefined && current.toString().length < 3){
                $scope.vmcConfig.localIPAddress="192.168.1.1"+current;
            }else if(current.toString().length >= 3){
                $scope.vmcConfig.localIPAddress="192.168.1."+current;
            }
        });

        /** get masterName By BayId **/
        $scope.getMasterName = function(bayId){
            if($scope.vmcConfig.hostType == 'P'){
                vmcConfigService.getMasterNameByBayID(bayId).then(function(response){
                    $scope.vmcConfig.master = response;
                    $log.log($scope.vmcConfig.master);
                });
            }
        };

        /** this fn takes hrs and mins and formats into date object for binding into timePickers **/
        $scope.formatTimeToBindInTimePickers = function(hrs,mins){
            var formattedCounterTimeForUpdate = new Date();
            formattedCounterTimeForUpdate.setHours(hrs);
            formattedCounterTimeForUpdate.setMinutes(mins);
            return formattedCounterTimeForUpdate;
        };

        /** update screen startTime, entime to 15 mins **/
        $scope.counterStartTimeChanged = function(){
            var counterStartTime = new Date($scope.vmcConfig.counterStartTime);
            counterStartTime.setMinutes(-15);
            $scope.vmcConfig.startTime = counterStartTime;

        };
        $scope.counterEndTimeChanged = function(){
            var counterEndTime = new Date($scope.vmcConfig.counterEndTime);
            counterEndTime.setMinutes(-15);
            $scope.vmcConfig.endTime = counterEndTime;
            $scope.vmcConfig.endTime_sun = counterEndTime;
        };



        $scope.hostTypeArr=[{type:"Player",id:"P"},{type:"Master",id:"M"},{type:"Both",id:"B"}];
        $scope.appArr=[{name:"DDiS",id:1},{name:"VMC",id:2}];
        $scope.stationArr=[{name:"Bangalore",id:1},{name:"Delhi",id:2},{name:"Hyderbad",id:3}];
        $scope.bayCodeArr=[{name:"BU",id:1},{name:"DL",id:2},{name:"HD",id:3}];
        $scope.counterTypeArr=[{name:"PRS",id:'PRS'},{name:"UTS",id:'UTS'}];
        $scope.routerRebootScheduleArr=[{name:"Default",id:1},{name:"Other",id:2}];
        $scope.playerWindowSizeArr=[{name:"0 0 1080 1440 (1080p)"},{name:"20 15 1260 695 (Plasma)"},{name:"15 30 1065 1440 (Crown)"}];
        $scope.hdmiModeArr=[5,16,18];
        $scope.displayTypeArr=["LCD","LED","PLASMA"];
        $scope.displaySizeArr=[32,22,19];
        $scope.cableTypeArr=["HDMI","DVI","Default"];
        $scope.piVersionArr=["Pi B","Pi B+","Pi 2","Pi 3"];
        $scope.playLogUpdateScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];
        $scope.ticketLogUpdateScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];
        $scope.contentUpdateScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];
        $scope.screenShotScheduleArr=["0 2 3 *","1 11 1 *","1 1 1 * 2"];


    }]);