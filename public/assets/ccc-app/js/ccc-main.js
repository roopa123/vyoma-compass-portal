$(document).ready(function() {
    /****** region grid ******/
    var regionTopPager = document.getElementById("IndustryGridPager");
    $('.ui-jqgrid-titlebar').append(regionTopPager);

    //ajaxStart and ajax Stop preloader
    $('.formSubmitUpdatePreloader').bind("click",function() {
        $('#preLoaderBlock').show(); /* show pre loader */
        return true;
    });
});


//Status active inactive display
function changeStatusValue(tableid, columnid) {
    $("table#"+tableid+" tr td[aria-describedby="+columnid+"]").each(function(index,value){
        if($(value).text() == 'I') {
            $(value).text('Inactive');
        } else if($(value).text() == 'D') {
            $(value).text('Delete');
        } else {
            $(value).text('Active');
        }
    });
}

function changeIspayingValue (tableid, columnid) {
    $("table#"+tableid+" tr td[aria-describedby="+columnid+"]").each(function(index,value){
        if($(value).text() == 'Y') {
            $(value).text('Yes');
        } else {
            $(value).text('No');
        }
    });
}

function industryDropdown (id, selected) {
    $.ajax({
        url: '/industryList',
        type: "GET",
        success: function (data) {
            var sn = data;
            console.log(data);
            $('#industry').empty();
            $.each(data, function(i, obj){
                console.log(i);
                if(selected == obj) {
                    $('#industry').append($('<option>').text(obj).attr('value', $("#industry").val()));
                } else {
                    $('#industry').append($('<option>').text(obj).attr('value', i));
                }
            });

            $('#industry').append($("#industry option").remove().sort(function(a, b) {
                var getFirstValueText = $(a).text(), getSecondValueText = $(b).text();
                return (getFirstValueText.toUpperCase() > getSecondValueText.toUpperCase())?1:((getFirstValueText.toUpperCase() < getSecondValueText.toUpperCase())?-1:0);
            })).prepend("<option value='' selected>Select</option>");
        }
    });
}

function dropDownSorting() {
    $('#advertiser').append($("#advertiser option").remove().sort(function(a, b) {
        var getFirstValueText = $(a).text(), getSecondValueText = $(b).text();
        return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
    }));

    $('#Category1').append($("#Category1 option").remove().sort(function(a, b) {
        var getFirstValueText = $(a).text(), getSecondValueText = $(b).text();
        return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
    }));

    $('#Category2').append($("#Category2 option").remove().sort(function(a, b) {
        var getFirstValueText = $(a).text(), getSecondValueText = $(b).text();
        return (getFirstValueText > getSecondValueText)?1:((getFirstValueText < getSecondValueText)?-1:0);
    }));
}

var displayMessage = function(message, condition, gridName) {
    console.log(condition)
    if (condition) {
        $('#form-success').addClass('alert alert-success').html(message).show();
    } else {
        $('#form-success').addClass('alert alert-danger').html(message).show();
    }
    $("#form-success.alert.alert-success").fadeTo(2000, 500).slideUp(500, function(){
        $(this).removeClass();
        $(this).hide();
        location.reload();
        /*if(gridName != 'CreativeGrid') {
         location.reload();
         }*/
    });
    $("#form-success.alert.alert-danger").fadeTo(2000, 1000).slideUp(1000, function(){
        $(this).removeClass();
        $(this).hide();
        location.reload();
        /*if(gridName != 'CreativeGrid'){
         location.reload();
         }*/
    });
};

var alertForStatusUpdateCreative = function(message, condition) {
    if (condition) {
        $('#form-success').addClass('alert alert-success').html(message).show();
    } else {
        $('#form-success').addClass('alert alert-danger').html(message).show();
    }
    $("#form-success.alert.alert-success").fadeTo(2000, 500).slideUp(500, function(){
        $(this).removeClass();
        $(this).hide();
    });
    $("#form-success.alert.alert-danger").fadeTo(2000, 1000).slideUp(1000, function(){
        $(this).removeClass();
        $(this).hide();
    });
};

updateStatuses = function (id, status, gridName) {
    $.ajax({type: "GET", url: "/updateStatuses/"+id+"/"+status,
        success: function(results){
            if(results == 'success'){
                if(gridName == '#POCreativeGrid') {
                    alertForStatusUpdateCreative("Creative Status Updated successfully", true);
                    //$('#POCreativeGrid').trigger( 'reloadGrid' );
                    //submitAllSeletedCreativeSelection();
                }
                else {
                    displayMessage("Creative Status Updated successfully", true, 'CreativeGrid');
                }

            }
            else if(results == 'associatedWithPo') {
                displayMessage("Can't Update to Review Status, It is Associated with PO", false, 'CreativeGrid');
                $('#CreativeGrid').trigger( 'reloadGrid' );
            }
        }});
}


/* Delete Creative through ajax call*/
deleteRow = function (id, gridName){
    message = "Are you sure, you want to delete the Creative?";
    bootbox.confirm(message, function(result) {
        if(result == true) {
            $.ajax({type: "DELETE", url: "/creative/"+id,
                success: function(results){
                    if(results == 'success'){
                        if(gridName == '#POCreativeGrid') {
                            alertForStatusUpdateCreative("Creative deleted successfully", true);
                            $('.ui-multiselect-checkboxes li label input').each(function (i, input) {
                                $.each(creativeIds, function(index,value){
                                    if($(input).val()==value){
                                        $(input).prop('checked', '');
                                        creativeIds.splice(index,1);
                                    }
                                })
                            });
                            $('#POCreativeGrid').jqGrid('delRowData',id);
                        }
                        else {
                            displayMessage("Creative deleted successfully", true);
                        }
                    }
                    else if(results == 'error'){
                        if(gridName == '#POCreativeGrid') {
                            alertForStatusUpdateCreative("Cannot delete Creatives, its associated with Purchase Order", false);
                            $('#POCreativeGrid').trigger( 'reloadGrid' );
                        }
                        else {
                            displayMessage("Cannot delete Creatives, its associated with Purchase Order", false, 'CreativeGrid');

                        }
                    }
                }});
        }
    });
}


