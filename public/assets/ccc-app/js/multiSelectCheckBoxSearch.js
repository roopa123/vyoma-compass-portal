/**
 * Created by Chethan JN on 26-04-2016.
 */
var myDefaultSearch = "cn";
var columnNameForCheck;
var columnNameForWithAppliedFilterArr=[];
var clearExtraOptionsArray = [];
getColumnIndexByName = function (columnName) {
    if(columnNameForWithAppliedFilterArr.indexOf(columnName)==-1){
        columnNameForWithAppliedFilterArr.push(columnName);
    }
    var cm = $(this).jqGrid('getGridParam', 'colModel'), i, l = cm.length;
    for (i = 0; i < l; i += 1) {
        if (cm[i].name === columnName) {
            return i; // return the index
        }
    }
    return -1;
};
columnIndexForGridBasedOnTableIdAndColumnName = function (grid, columnName) {
    var cm = jQuery(grid).jqGrid('getGridParam', 'colModel'), i, l = cm.length;
    for (i = 0; i < l; i++) {
        if (cm[i].name === columnName) {
            return i; // return the index
        }
    }
    return -1;
};
modifySearchingFilter = function (separator) {
    var i, l, rules, rule, parts, j, group, str, iCol, cmi, cm = this.p.colModel,
        filters = $.parseJSON(this.p.postData.filters);
    if (filters && filters.rules !== undefined && filters.rules.length > 0) {
        rules = filters.rules;
        for (i = 0; i < rules.length; i++) {
            rule = rules[i];
            iCol = getColumnIndexByName.call(this, rule.field);
            cmi = cm[iCol];
            if (iCol >= 0 &&
                ((cmi.searchoptions === undefined || cmi.searchoptions.sopt === undefined)
                && (rule.op === myDefaultSearch)) ||
                (typeof (cmi.searchoptions) === "object" &&
                $.isArray(cmi.searchoptions.sopt) &&
                cmi.searchoptions.sopt[0] === rule.op)) {
                // make modifications only for the 'contains' operation
                parts = rule.data.split(separator);
                if (parts.length > 1) {
                    if (filters.groups === undefined) {
                        filters.groups = [];
                    }
                    group = {
                        groupOp: 'OR',
                        groups: [],
                        rules: []
                    };
                    filters.groups.push(group);
                    for (j = 0, l = parts.length; j < l; j++) {
                        str = parts[j];
                        if (str) {
                            // skip empty '', which exist in case of two separaters of once
                            group.rules.push({
                                data: parts[j],
                                op: rule.op,
                                field: rule.field
                            });
                        }
                    }
                    rules.splice(i, 1);
                    i--; // to skip i++
                }
            }
        }
        this.p.postData.filters = JSON.stringify(filters);
        getFilterData(filters);
    }else{
        getFilterData(filters);
        $('.ui-multiselect-menu .ui-multiselect-checkboxes li').show().css("visibility","visible").css("height","auto");
        $('.ui-multiselect-filter input').focus();
    }
};
dataInitMultiSelect = function (elem) {
    setTimeout(function () {
        var $elem = $(elem), id = elem.id,
            inToolbar = typeof id === "string" && id.substr(0, 3) === "gs_",
            options = {
                selectedList: 2,
                height: "auto",
                checkAllText: "All",
                uncheckAllText: "Clear",
                noneSelectedText: "Any",
                open: function () {
                    var $menu = $(".ui-multiselect-menu:visible");
                    $menu.width("auto");
                    $menu.find(">ul").css("maxHeight", "150px"); // some max-height value
                    return;
                }
            },
            $options = $elem.find("option");
        if ($options.length > 0 && $options[0].selected) {
            $options[0].selected = false; // unselect the first selected option
        }
        if (inToolbar) {
            options.minWidth = 'auto';
        }
        //$elem.multiselect(options);
        $elem.multiselect(options).multiselectfilter({ placeholder: 'Enter Keyword' });
        $elem.siblings('button.ui-multiselect').css({
            width: inToolbar ? "98%" : "100%",
            marginTop: "1px",
            marginBottom: "1px",
            paddingTop: "3px"
        });
    }, 50);
};
getUniqueNames = function(columnName, $grid, data) {
    var columnTextVal1=[],columnTextVal=[];
    $.each(data, function(key, value) {
        $.each(value, function(key, value) {
            //if(key==columnName && JSON.stringify(value)!='null'){
            if(key==columnName){
                columnTextVal1.push(value);
            }
        });
    });
    $.each(columnTextVal1,function(index,value){
        if(value==null){
            columnTextVal.push("Blank");
        }else{
            columnTextVal.push(value);
        }
    });
    var texts = columnTextVal, uniqueTexts = [],
        textsLength = texts.length, text, textsMap = {}, i;
    for (i=0;i<textsLength;i++) {
        text = texts[i];
        if (text !== undefined && textsMap[text] === undefined) {
            // to test whether the texts is unique we place it in the map.
            textsMap[text] = true;

            if(text!=""){
                uniqueTexts.push(text);
            }
        }
    }
    return uniqueTexts;
};
buildSearchSelect = function(uniqueNames) {
    var values=":All";
    $.each (uniqueNames, function() {
        values += ";" + this + ":" + this;
    });
    return values;
};
setMultiSearchSelect = function(columnName, $grid, data) {
    $grid.jqGrid('setColProp', columnName,
        {
            stype: 'select',
            searchoptions: {
                sopt: ['eq', 'ne'],
                value: buildSearchSelect(getUniqueNames(columnName, $grid, data)),
                multiple: true,
                dataInit: function(elem){
                    dataInitMultiSelect(elem)
                }
            }
        }
    );
};
setMultiSearchSelectForStatus = function(columnName, $grid, data, value) {
    $grid.jqGrid('setColProp', columnName,
        {
            stype: 'select',
            searchoptions: {
                sopt: ['eq', 'ne'],
                //value: ":All;A:Active;D:Delete",
                value: value,
                multiple: true,
                dataInit: function(elem){
                    dataInitMultiSelect(elem)
                }
            }
        }
    );
};
nullFormatter = function(cellvalue, options, rowObject) {
    if(cellvalue == undefined || cellvalue == null) {
        cellvalue = '(Blank)';
    }
    return cellvalue;
};

/* removing or hiding extra search options functions*/
getFilterData = function(filters){
    var columnName,filterKeyArray=[];
    if(filters!=null){
        if (JSON.stringify(filters.rules.length) > 0 && filters.rules!=undefined) {
            filterKeyArray = [];
            filterKeyArray.push(filters.rules[0].data);
            columnName = filters.rules[0].field;
            //columnNameForCheck = filters.rules[0].field;
            $.each(filters.rules,function(index,value){
                columnName = filters.rules[0].field;
                //columnNameForCheck = value.field;
            });
        }else{
            filterKeyArray = [];
            if(filters.groups!=undefined || filters.groups!=null){
                $.each(filters.groups, function (index, value) {
                    filterKeyArray = [];
                    $.each(value.rules,function(index, value){
                        filterKeyArray.push(value.data);
                        columnName = value.field;
                    });
                });
            }
        }
    }
};
removeUnWantedSearchOptions = function(rowNo,cellName,searchResult,gridName){
    if(columnNameForWithAppliedFilterArr.length > 0){
        $.each(columnNameForWithAppliedFilterArr,function(index,value){
            columnNameForCheck=value;
        })
    }
    var columnNameWithInputname = "multiselect_gs_"+columnNameForCheck;
    $(".ui-multiselect-menu:eq("+rowNo+") ul.ui-multiselect-checkboxes li").hide().css("visibility","hidden").css("height","0px");
    $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input').each(function(i, input){
        if(i>0){
            takeEachInputFromEachLoop(searchResult,input,cellName,rowNo,gridName);
            if($(input).prop('checked')){
                /*  if(columnNameForCheck==cellName){
                 $(".ui-multiselect-menu:eq("+rowNo+") a.ui-multiselect-none").removeClass("disableAnchorTag");
                 }else{
                 fnToDisableInputCheckBoxIfOnlyOneExists(rowNo);
                 $(".ui-multiselect-menu:eq("+rowNo+") a.ui-multiselect-none").addClass("disableAnchorTag");
                 }*/
                fnToDisableInputCheckBoxIfOnlyOneExists(rowNo);
                if($(input).attr('name')==columnNameWithInputname){
                    if(columnNameForWithAppliedFilterArr.length > 1){
                        $.each(clearExtraOptionsArray,function(index,element){
                            $(element).parent('label').parent('li').show().css("visibility","visible").css("height","auto");
                        });
                    }else{
                        $(".ui-multiselect-menu:eq("+rowNo+") ul.ui-multiselect-checkboxes li").show().css("visibility","visible").css("height","auto");
                    }
                }
            }
        }
    });
    /* clear the search and options */
    $(".ui-multiselect-menu:eq("+rowNo+") a.ui-multiselect-none").bind('click',function(){
        clearAppliedFilters(gridName,cellName);
        clearExtraOptionsArray=[];
        var tempArryForAppliedFilterArr=[];
        $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input').removeAttr("disabled");
        $.each(columnNameForWithAppliedFilterArr,function(index,value){
            if(value!=cellName){
                tempArryForAppliedFilterArr.push(value);
            }
        });
        columnNameForWithAppliedFilterArr=tempArryForAppliedFilterArr;
        $('.ui-multiselect-filter input').focus();
        setTimeout(function(){
            $(".ui-multiselect-menu:eq("+rowNo+")").hide();
        },200);
    });
};
takeEachInputFromEachLoop = function(searchResult,input,cellName,rowNo,gridName){
    $.each(searchResult,function(index,jsonObj){
        if(jsonObj[cellName]==$(input).attr('title')){
            $(input).parent('label').parent('li').show().css("visibility","visible").css("height","auto");
        }else if(jsonObj[cellName]==$(input).val()){
            $(input).parent('label').parent('li').show().css("visibility","visible").css("height","auto");
        }
    });
    $(input).bind('click',function(){
        if($(input).prop('checked')){
            /* on check get all visible checkboxs */
            clearExtraOptionsArray=[];
            $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input:visible').each(function(index,val){
                if(clearExtraOptionsArray.indexOf(val==-1)){
                    clearExtraOptionsArray.push(val);
                }
            });
            $.each(columnNameForWithAppliedFilterArr,function(index,value){
                if(value==cellName){
                    columnNameForWithAppliedFilterArr.splice(index,1);
                }
            });
            columnNameForWithAppliedFilterArr.push(cellName);
            $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input').removeAttr("disabled");
        }else{
            /* on unCheck get all visible checkBox's */
            clearExtraOptionsArray=[];
            $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input:visible').each(function(index,val){
                if(clearExtraOptionsArray.indexOf(val==-1)){
                    clearExtraOptionsArray.push(val);
                }
            });
            $.each(columnNameForWithAppliedFilterArr,function(index,value){
                if(value==cellName){
                    columnNameForWithAppliedFilterArr.splice(index,1);
                }
            });
            columnNameForWithAppliedFilterArr.push(cellName);
            fnToDisableInputCheckBoxIfOnlyOneExists(rowNo);
            var atLeastOneIsChecked = $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input:checkbox').is(':checked');
            if(!atLeastOneIsChecked){
                clearAppliedFilters(gridName,cellName);
                clearExtraOptionsArray=[];
                $.each(columnNameForWithAppliedFilterArr,function(index,value){
                    if(value==cellName){
                        columnNameForWithAppliedFilterArr.splice(index,1);
                    }
                });
                $('.ui-multiselect-filter input').focus();
                setTimeout(function(){
                    $(".ui-multiselect-menu:eq("+rowNo+")").hide();
                },200);
            }
        }
    });
};
/* function to clear the filters for column */
clearAppliedFilters = function(gridName,cellName){
    var gridName=$('#'+gridName);
    var filters = $.parseJSON(gridName.jqGrid("getGridParam", "postData").filters);
    if ( filters.rules!=undefined && filters.groups != null && filters.rules.length > 0) {
        $.each(filters.rules,function(index,value){
            if(value!=undefined && value.field==cellName){
                //delete filters.rules[index];
                filters.rules.splice(index,1);
            }
        });
    }else{
        if(filters.groups != undefined && filters.groups != null && filters.groups.length > 0){
            $.each(filters.groups, function (index, value) {
                if(value!=undefined && value.rules[0].field==cellName){
                    //delete filters.groups[index];
                    filters.groups.splice(index,1);
                }
            });
        }
    }

    gridName.jqGrid('setGridParam',{postData: { filters : filters }, search:true }).trigger("reloadGrid");
};
/* function to disable input checkbox if no of input checkboxes equals to one */
fnToDisableInputCheckBoxIfOnlyOneExists=function(rowNo){
    var nofCheckBoxChecked = $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input:visible:checkbox:checked').length;
    if(nofCheckBoxChecked==1){
        $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input:visible:checkbox:checked').attr("disabled","disabled");
    }else{
        $('.ui-multiselect-menu:eq('+rowNo+') ul.ui-multiselect-checkboxes li label input').removeAttr("disabled");
    }
};
/* highlightFilteredColumns */
highlightFilteredColumns = function () {
    var $self = $(this), postData = $self.jqGrid("getGridParam", "postData"), i, l,
        cm = $self.jqGrid("getGridParam", "colModel"), control,
        filtersColumns = {},
        getAllFilteredColumns = function (f) {
            var j, n;
            if (f) {
                if (f.groups !== undefined && f.groups !== null) {
                    for (j = 0, n = f.groups.length; j < n; j++) {
                        getAllFilteredColumns(f.groups[j]);
                    }
                }
                if (f.rules !== undefined && f.rules !== null) {
                    for (j = 0, n = f.rules.length; j < n; j++) {
                        filtersColumns[f.rules[j].field] = true;
                    }
                }
            }
        };
    if (typeof postData.filters === "string") {
        getAllFilteredColumns($.parseJSON(postData.filters));
    } else if (postData.filters !== null && typeof postData.filters === "object") {
        getAllFilteredColumns(postData.filters);
    } else {
        return; // do nothing
    }

    for (i = 0, l = cm.length; i < l; i += 1) {
        control = $("#gs_" + $.jgrid.jqID(cm[i].name));
        if (control.length > 0) {
            if (filtersColumns[cm[i].index || cm[i].name] === true) {
                //control.parent().addClass("ui-state-error").css({ borderTop: "0", borderBottom: "0" });
                $("#" + $.jgrid.jqID($self[0].id) + "_" + $.jgrid.jqID(cm[i].name)).addClass("columnHeighlighter");
            } else {
                //control.parent().removeClass("ui-state-error").css({ borderTop: "0", borderBottom: "0" });
                $("#" + $.jgrid.jqID($self[0].id) + "_" + $.jgrid.jqID(cm[i].name)).removeClass("columnHeighlighter");
            }
        }
    }
};
